﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class FeePakagelist : AvaimaThirdpartyTool.AvaimaWebPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            Fillrptrfeepakages();
    }

    protected void rptrfeepakages_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }

    protected void rptrfeepakages_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.CommandName == "select")
            {
                this.Redirect("FeesPackage.aspx?id=" + e.CommandArgument + "&f=E");
                //string message = "Fees Package Updated on";
                //Packagehistory(Convert.ToInt32(hfpakageid.Value), message);
            }
            else if (e.CommandName == "delete")
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("DELETE FROM tbl_feesPakageMaster WHERE ID=@did  Delete from tbl_feesPakgaeDetail where DID=@did", con))
                    {
                        if (con.State.ToString() != "Open")
                            con.Open();
                        cmd.Parameters.AddWithValue("@did", e.CommandArgument);
                        cmd.ExecuteNonQuery();
                        //string message = "Fees Package Deleted on";
                        //Packagehistory(Convert.ToInt32(hfpakageid.Value), message);
                        Fillrptrfeepakages();
                    }
                }
            }
        }
    }
    public void Fillrptrfeepakages()
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT ID,Title,Amount,LateFeeCharges FROM tbl_feesPakageMaster WHERE InstanceId=@InstanceId and studentflag=0", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    
                    if (dt.Rows.Count > 0)
                    {
                       
                        lblgriderror.Visible = false;
                    }
                    else
                    {
                        lblgriderror.Visible = true;
                    }
                    rptrfeepakages.DataSource = dt;
                    rptrfeepakages.DataBind();
                }
                con.Close();
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT ID,Title,Amount,LateFeeCharges,LateFeeCharges FROM tbl_feesPakageMaster WHERE InstanceId=@InstanceId and  Title LIKE '%" + txtsearchtext.Text + "%'", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    //cmd.Parameters.AddWithValue("@title", "'%" + txtsearchtext.Text + "%'");
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    rptrfeepakages.DataSource = dt;
                    rptrfeepakages.DataBind();
                    if (dt.Rows.Count > 0)
                    {

                        lblgriderror.Visible = false;
                    }
                    else
                    {
                        lblgriderror.Visible = true;

                    }

                }
                con.Close();
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void Unnamed1_Click1(object sender, EventArgs e)
    {
        this.Redirect("FeesPackage.aspx?f=A");
    }
}