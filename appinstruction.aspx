﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="appinstruction.aspx.cs" Inherits="appinstruction" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            height: 7px;
        }

        #para p {
            padding: 10px 0 0 10px;
        }

        #para div {
            line-height: 22px;
        }

        #para ol {
            padding: 3px 30px;
        }

        #para ul {
            padding: 5px 30px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="Main_leftdiv">
        </div>
        <div class="clearboth"></div>
        <h4 style="margin-left:10px">
            For more information about Time & Attendance go to <a href="About.aspx">About</a>.
        </h4>
        <br />
        <table>
            <tr>
                <td>
                    <div style="display: inline-block; float: left;" id="para">
                        <div class="formTitleDiv">
                            How to start?
                        </div>
                        <div style="line-height: 20px">
                            <p>
                                If you've added this application to your dashboard, you will be presented with <a href="Default.aspx">Home</a> screen.
                            </p>
                            <p>
                                You can start by Adding Employee or Department, you can create as many employees and organize them in department/sub department as you want.
                            </p>
                        </div>
                        <br />
                        <div class="formTitleDiv">
                            How to add an employee?
                        </div>
                        <div>
                            <ul>
                                <li>Go to <a href="Default.aspx">Home</a> page, Click <b>Add Employee</b> link to open Add Employee popup.
                                </li>
                                <li>Enter First Name, Last Name and Email Address of the employee you want to add.
                                </li>
                                <li>Click <b>send email</b> which will send a an email containing instructions of how to use Time & Attendance Management.
                                </li>
                            </ul>
                            <br />
                            <div class="formTitleDiv">
                                What is a super admin
                            </div>
                            <p>
                                Super admin is a user which add Time & Attendance application from <a href="http://www.avaima.com">avaima</a> appstore
                                into his dashboard. 
                                A super admin can manage departments, administrators and employees. He can monitor all departments
                                and employees under them supervision.
                                He can perform all administrative action on all employees and administrators as well. 
                                He can assign administrators to departments and can delete his administrators as well.
                            </p>
                            <br />
                            <div class="formTitleDiv">
                                How to make a super admin
                            </div>
                            <p>
                                Super admin is a user which add Time & Attendance application from <a href="http://www.avaima.com">avaima</a> appstore
                                into his dashboard. 
                                So basically the user who add Time & Attendance Management into his dashboard first time is made super-admin.
                            </p>
                            <br />
                            <div class="formTitleDiv">
                                What is an admin
                            </div>
                            <p>
                                Admin is an employee who can manage and minitor employees under his assigned department.
                            </p>
                            <br />
                            <div class="formTitleDiv">
                                How to make admin
                            </div>
                            <p>
                                Super user can assign admin to a department only.
                            </p>
                            <ul>
                                <li>Go to <a href="Default.aspx">Home</a> page and click setting icon of a department.
                                </li>
                                <li>Select an employee
                                which you want to assign admin to that department and click Add.
                                </li>
                            </ul>
                            <p>
                                You can also promote an employee to admin from his dashboard.
                            </p>
                            <ul>
                                <li>Go to <a href="Default.aspx">Home</a> and select the employee you want to promote to admin.
                                </li>
                                <li>
                                    Navigate to the bottom of the page and click on pen icon along with <b>Role</b>.
                                </li>
                                <li>
                                    Select a role and click Update.
                                </li>
                            </ul>
                            <br />
                            <div class="formTitleDiv">
                                What is an employee
                            </div>
                            <p>
                                Employee is a user added by admin/super-admin which will work under their supervisions.
                                Employee is restricted to Clock-in, Clock-out, view his statistics, managing his breaks, 
                                deleting time, and viewing his reports.
                            </p>
                            <br />
                            <div class="formTitleDiv">
                                How can I verify the logins
                            </div>
                            <p>
                                Time & Attendance Management uses IP Address verification method. Which matches your
                                specified IP Address with the IP Address of the computer from which a user is signing in.
                            </p>
                            <p>
                                To provide IP Address verification, go to <a href="settings.aspx">Settings</a>.
                            </p>
                            <p>
                                From setting screen, you will be presented with <b>Verification through IP information </b>
                                section, provide title and IP address that you want to match on Clock-In verification.
                            </p>
                            <br />
                            <div class="formTitleDiv">
                                What is auto-present and how to use it
                            </div>
                            <p>
                                Auto present is a check which provides Clocking-In and Out automatically.                                
                            </p>
                            <ul>
                                <li>To use auto-present go to <a href="Add_worker.aspx">your</a> dashboard or Employee's dashboard
                                    by selecting Employee from <a href="Default.aspx">Home</a>.
                                </li>
                                <li>Click on Working Hours whichi wil open up a <b>Working Hours</b> dialog.
                                </li>
                                <li>Provide working hours and select auto-present check to on and click save.
                                </li>
                                <li>From then selected employee will be Clocked-in and out by on their specified time.
                                </li>
                            </ul>
                            <br />
                            <div class="formTitleDiv">
                                How can I improve report statistics
                            </div>
                            <p>
                                Provide <b>Working Hours</b> to improve reporting statistics which will help you finding
                                out worked hours with actual hours.
                            </p>
                            <ul>
                                <li>Go to <a href="settings.aspx">Settings</a>, navigate to <b>Working hours</b> section.
                                </li>
                                <li>Click <b>Define working hours</b>, provide working hours of your organization.
                                </li>
                                <li>You can also provide working hours for a specific employee separately by navigating to 
                                    employee's dashboard from <a href="Default.aspx">Home</a> page.
                                </li>
                            </ul>
                            <br />
                            <div class="formTitleDiv">
                                How to manage locations
                            </div>
                            <p>
                                You can manage locations from <b>Locations Information</b> section in <a href="settings.aspx">Settings</a>
                                page. 
                                Employee can then select the location while clocking-in.
                                <br />
                            </p>
                            <br />
                            <div class="formTitleDiv">
                                How can I add Holidays and Breaks
                            </div>
                            <ul>
                                <li>Go to <a href="settings.aspx">Settings</a>
                                    page, navigate to <b>Holidays Information</b> section to manage holidays.
                                </li>
                                <li>Navigate to <b>Breaks Information</b> to manage breaks.
                                </li>
                            </ul>
                            <br />
                            <div class="formTitleDiv">
                                How to edit clock-in / Clock-out time
                            </div>
                            <p>
                                Only admin and super-admin has ability to edit clock-in and clock-out time of an employee. If
                                you are a employee please contact to your administrator<br />
                                Admin can edit clock in/out hours from Employee dashboard and Montly report as well.
                            </p>
                            <ul>
                                <li>From history grid, click <b>Black Pen</b> icon of the day you want to edit time.
                                </li>
                                <li>Select clock-in and clock-out time, leave comments and click <b>Update Time</b>.
                                </li>
                            </ul>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="Main_rightdiv" style="display: inline-block; float: right; margin-right: 16px">
                        <div class="formTitleDiv">Contact Us</div>
                        <div class="Divimproveavaima" style="margin-top: 10px;">
                            <table id="tb_gen" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="formCaptionTd">Type:
                                    </td>
                                    <td class="formFieldTd">
                                        <asp:DropDownList ID="ddlreq" runat="server">
                                            <asp:ListItem Value="Request a Problem">Request a Problem</asp:ListItem>
                                            <asp:ListItem Value="Request a Feature">Request a Feature</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="formVerticalSpacer">
                                            &nbsp;
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="formCaptionTd">Description:
                                    </td>
                                    <td class="formFieldTd">
                                        <asp:TextBox ID="txtmsg" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="auto-style1">
                                        <div class="formVerticalSpacer">
                                            &nbsp;
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="formCaptionTd"></td>
                                    <td class="formFieldTd">
                                        <asp:Button ID="btnsend" Text=" Send " runat="server" ValidationGroup="mail"
                                            CssClass="standardFormButton" OnClick="btnsend_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="formCaptionTd"></td>
                                    <td class="formFieldTd">
                                        <asp:Label ID="lblmsg" ForeColor="Green" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>


        </table>
    </form>
</body>
</html>
