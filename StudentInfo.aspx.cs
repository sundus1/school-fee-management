﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class StudentInfo : AvaimaThirdpartyTool.AvaimaWebPage
{
    DataTable dtcharges = new DataTable();
    DataTable dtdiscount = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DateTime dtdatee = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            txtppstartdate.Text = dtdatee.ToString("dd-MMMM-yyyy");
            txtppstartdate.Enabled = false;
            var stid = Request["stid"];
            if (stid == null)
            {
                //lnkshowhistory.Visible = false;
                divmenu.Visible = false;
                hfparentid.Value = Request["pid"];
                Getpackagedetails(hfparentid.Value, "0");
                txtstartdate.Text = dtdatee.ToString("dd-MMMM-yyyy");
                txtstartdate.Enabled = false;
                Filldatesetting();
                Fillrptrfeepakages();
                Fillrptrdiscount();
                btnsave.Text = "Save";
                btnSaveClose.Text = "Save & Close";
                btnAddNew.Text = "Save & Add Another Student";

            }
            else
            {
                //lnkshowhistory.Visible = true;
                divmenu.Visible = true;
                btnsave.Text = "Update";
                btnSaveClose.Text = "Update & Close";
                btnAddNew.Text = "Update & Add Another Student";
                hfparentid.Value = Request["pstate"];
                hfstudentid.Value = Request["stid"];
                Filldatesetting();
                Showstudenthistory(hfstudentid.Value);
                Getpackagedetails(hfparentid.Value, hfstudentid.Value);
                getstudentpackage();
                Fillrptrfeepakages();
                Fillrptrdiscount();
               
            }
        }
    }
    public void getstudentpackage()
    {
        try
        {
            string template = @"<tr><td class=""formCaptionTd"">#type#:</td>
                            <td class=""formFieldTd"">                                          
                                <input type=""text"" id=""#type##count#"" name=""#type##count#"" value=""#value#"" class=""standardField"" MaxLength=""50"" /></td></tr>";

            string template2 = @"&nbsp;<a id=""a#type##count#"" href=""javascript:addrow('#type##count#','#count1#','#type#');"">Add Row</a>&nbsp;<a id=""d#type##count#"" href=""javascript:delrow('#type##count#','#type##count2#','#count2#','#type#');"">Delete Row</a>";

            string template3 = @"<tr><td class=""formCaptionTd"">#type#:</td>
                            <td class=""formFieldTd"">                                          
                                <input type=""text"" id=""#SSS##count#"" name=""#BBB##count#"" value=""#value#"" class=""standardField"" MaxLength=""50"" /></td></tr>";

            string template4 = @"&nbsp;<a id=""a#SSS##count#"" href=""javascript:addrow('#SSS##count#','#count1#','#type#');"">Add Row</a>&nbsp;<a id=""d#SSS##count#"" href=""javascript:delrow('#SSS##count#','#SSS##count2#','#count2#','#type#');"">Delete Row</a>";


            tr_charge.Attributes.Remove("style");
            tr_charge1.Attributes.Remove("style");
            tr_discount.Attributes.Remove("style");
            tr_discount1.Attributes.Remove("style");
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("getstudentbystudentid", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    if (!string.IsNullOrEmpty(Convert.ToString(Request["stid"])))
                        cmd.Parameters.AddWithValue("studentid", Request["stid"]);
                    else
                        cmd.Parameters.AddWithValue("studentid", hfstudentid.Value);

                    cmd.CommandType = CommandType.StoredProcedure;
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        //lnkshowhistory.Visible = true;
                        if (!string.IsNullOrEmpty(dt.Rows[0]["Assignedpackage"].ToString()))
                            hfstudentpackage.Value = dt.Rows[0]["Assignedpackage"].ToString();

                        if (!string.IsNullOrEmpty(dt.Rows[0]["Assigndiscount"].ToString()))
                            hfdiscount.Value = dt.Rows[0]["Assigndiscount"].ToString();

                        if (!string.IsNullOrEmpty(dt.Rows[0]["Title"].ToString()))
                            txtStudentname.Text = dt.Rows[0]["Title"].ToString();

                        if (!string.IsNullOrEmpty(dt.Rows[0]["ParentName"].ToString()))
                            txtParentname.Text = dt.Rows[0]["ParentName"].ToString();

                        if (!string.IsNullOrEmpty(dt.Rows[0]["packagechangedate"].ToString()))
                            txtstartdate.Text = Convert.ToDateTime(dt.Rows[0]["packagechangedate"].ToString()).ToString("dd-MMMM-yyyy");
                        

                        if (!string.IsNullOrEmpty(dt.Rows[0]["StudentEmail"].ToString()))
                        {
                            txtstudentemail.Text = dt.Rows[0]["StudentEmail"].ToString();
                            hfstudentemail.Value = dt.Rows[0]["StudentprimaryID"].ToString();
                            txtstudentemail.Enabled = false;
                            //viewstudent.HRef = "viewprofile.aspx?userid=" + dt.Rows[0]["StudentprimaryID"].ToString() + "&id=" + hfstudentid.Value;
                            //viewstudent.Visible = true;
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[0]["ParentEmail"].ToString()))
                        {
                            txtparentsemail.Text = dt.Rows[0]["ParentEmail"].ToString();
                            txtparentsemail.Enabled = false;
                            hfparentemail.Value = dt.Rows[0]["ParentId"].ToString();
                            //viewparent.HRef = "viewprofile.aspx?userid=" + dt.Rows[0]["ParentId"].ToString() + "&id=" + hfstudentid.Value;
                            //viewparent.Visible = true;

                        }
                        if (!string.IsNullOrEmpty(dt.Rows[0]["studentrollnumber"].ToString()))
                            txtrollnum.Text = dt.Rows[0]["studentrollnumber"].ToString();
                        //if (!string.IsNullOrEmpty(dt.Rows[0]["OthersEmail"].ToString()))
                        //{
                        //    txtothersemail.Text = dt.Rows[0]["OthersEmail"].ToString();
                        //    txtothersemail.Enabled = false;
                        //    hfotheremail.Value = dt.Rows[0]["OthersId"].ToString();
                        //    viewother.HRef = "viewprofile.aspx?userid=" + dt.Rows[0]["OthersId"].ToString() + "&id=" + hfstudentid.Value;
                        //    viewother.Visible = true;addmissionfeebit
                        //}
                        //////chknewuser.Checked = dt.Rows[0]["addmissionfeebit"].ToString() == "True" ? true : false;
                        //////hfnewuser.Value = dt.Rows[0]["addmissionfeebit"].ToString() == "True" ? "1" : "0";
                        //////if (chknewuser.Checked)
                        //////{
                        //////    ScriptManager.RegisterStartupScript(this, GetType(), "checknewuser", "checknewuser()", true);
                        //////    chknewuser.Enabled = false;
                        //////}

                        if (!dt.Rows[0]["studentphone"].ToString().Contains("|"))
                        {
                            Phone0.Text = dt.Rows[0]["studentphone"].ToString();
                            lt_SPhone.Text = "";
                            aPhone0.Visible = true;
                        }
                        else
                        {
                            string[] phones = dt.Rows[0]["studentphone"].ToString().Split('|');
                            StringBuilder sbphone = new StringBuilder();
                            Phone0.Text = phones[0];
                            for (int phone = 1; phone < phones.Length; phone++)
                            {
                                if (!String.IsNullOrEmpty(phones[phone]))
                                    sbphone.Append(template.Replace("#type#", "Phone").Replace("#count#", phone.ToString()).Replace("#value#", phones[phone]));
                            }
                            if (sbphone.Length > 0)
                                lt_SPhone.Text = sbphone.Replace("</td></tr>", "", sbphone.Length - 10, 10).Append(template2.Replace("#type#", "Phone").Replace("#count#", (Convert.ToInt32(phones.Length) - 1).ToString()).Replace("#count1#", (Convert.ToInt32(phones.Length)).ToString()).Replace("#count2#", (Convert.ToInt32(phones.Length) - 2).ToString())).ToString();
                            aPhone0.Visible = false;
                        }

                        if (!dt.Rows[0]["Parentphone"].ToString().Contains("|"))
                        {
                            arenthone0.Text = dt.Rows[0]["Parentphone"].ToString();
                            lt_PPhone.Text = "";
                            a1.Visible = true;
                        }
                        else
                        {
                            string[] pphones = dt.Rows[0]["Parentphone"].ToString().Split('|');
                            StringBuilder psbphone = new StringBuilder();
                            arenthone0.Text = pphones[0];
                            for (int phone = 1; phone < pphones.Length; phone++)
                            {
                                if (!String.IsNullOrEmpty(pphones[phone]))
                                    psbphone.Append(template3.Replace("#type#", "Parent/Guardian Phone").Replace("#count#", phone.ToString()).Replace("#value#", pphones[phone]).Replace("#SSS#", "ParentPhone").Replace("#BBB#", "arenthone"));
                            }
                            if (psbphone.Length > 0)
                                lt_PPhone.Text = psbphone.Replace("</td></tr>", "", psbphone.Length - 10, 10).Append(template4.Replace("#type#", "Parent/Guardian Phone").Replace("#count#", (Convert.ToInt32(pphones.Length) - 1).ToString()).Replace("#count1#", (Convert.ToInt32(pphones.Length)).ToString()).Replace("#count2#", (Convert.ToInt32(pphones.Length) - 2).ToString()).Replace("#SSS#", "ParentPhone").Replace("#BBB#", "Parent")).ToString();
                            a1.Visible = false;
                        }
                    }


                }
                con.Close();
            }
        }
        catch (Exception ex)
        {

        }
    }
    public void Filldatesetting()
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("select * from tbl_feesPakageMaster where InstanceId=@InstanceId", con))
            {
                cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                ddldateset.DataSource = dt;
                ddldateset.DataTextField = "Title";
                ddldateset.DataValueField = "ID";
                ddldateset.DataBind();
                ddldateset.Items.Insert(0, new ListItem("Select", "0"));
            }
            using (SqlCommand cmd = new SqlCommand("select foldersetting,packageid from tbl_feesManagement where studendId=@studendId", con))
            {
                cmd.Parameters.AddWithValue("@studendId", hfstudentid.Value);
                DataTable dt1 = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt1);
                if (dt1.Rows.Count > 0)
                {
                    ddldateset.SelectedValue = String.IsNullOrEmpty(dt1.Rows[0]["packageid"].ToString()) ? "0" : dt1.Rows[0]["packageid"].ToString();
                    hfpackageid.Value = String.IsNullOrEmpty(dt1.Rows[0]["packageid"].ToString()) ? "0" : dt1.Rows[0]["packageid"].ToString();

                    if (hfpackageid.Value != "0")
                        lnkadddepartment.Text = "Update Package";
                }
            }
            using (SqlCommand cmd = new SqlCommand("SELECT * FROM Addtionalfee WHERE InstanceId=@InstanceId union select * from Onetimefee WHERE InstanceId=@InstanceId", con))
            {
                cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                DataTable dtfee = new DataTable();
                SqlDataAdapter adpfee = new SqlDataAdapter(cmd);
                adpfee.Fill(dtfee);
                if (dtfee.Rows.Count > 0)
                {
                    //tr_new1.Attributes.Remove("style");
                    //tr_new2.Attributes.Remove("style");
                    //tr_new3.Style.Add("display", "none");
                    ddlfee.DataSource = dtfee;
                    ddlfee.DataTextField = "feetitle";
                    ddlfee.DataValueField = "Feeamount";
                    ddlfee.DataBind();
                    ddlfee.Items.Insert(0, new ListItem("Select Fee", "0"));
                    txtfeedate.Text = DateTime.Now.ToString("dd-MMMM-yyyy");
                    if (dtfee.Rows.Count == 1)
                    {
                        ddlfee.SelectedValue = dtfee.Rows[0]["Feeamount"].ToString();
                        ddlfee.Enabled = false;
                    }
                }
                
            }

        }
    }
    public void Fillrptrfeepakages()
    {
        try
        {
            dtcharges.Columns.Add("Title");
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT ID,Title,ChargeType,IsFixed,Amount FROM tbl_ChargeDiscount WHERE InstanceId=@InstanceId and ChargeType='Charge'", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    rptrfeepakages.DataSource = dt;
                    rptrfeepakages.DataBind();
                    if (dtcharges.Rows.Count > 0)
                    {
                        reptcharges.DataSource = dtcharges;
                        reptcharges.DataBind();
                    }
                    else
                    {
                        reptcharges.DataSource = null;
                        reptcharges.DataBind();
                    }
                }
                con.Close();
            }
        }
        catch (Exception ex)
        {

        }
    }
    public void Fillrptrdiscount()
    {
        try
        {
            dtdiscount.Columns.Add("Title");
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT ID,Title,ChargeType,IsFixed,Amount FROM tbl_ChargeDiscount WHERE InstanceId=@InstanceId and ChargeType='Discount'", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    reptdiscount.DataSource = dt;
                    reptdiscount.DataBind();
                    if (dtdiscount.Rows.Count > 0)
                    {
                        reptadiscount.DataSource = dtdiscount;
                        reptadiscount.DataBind();
                    }
                    else
                    {
                        reptadiscount.DataSource = null;
                        reptadiscount.DataBind();
                    }

                }
                con.Close();
            }
        }
        catch (Exception ex)
        {

        }
    }
    public string StudentPackagedetail()
    {
        var ids = string.Join(",", (from r in rptrfeepakages.Items.Cast<RepeaterItem>()
                                    let selected = (r.FindControl("chkselecteditem") as CheckBox).Checked
                                    let id = (r.FindControl("hf_ID") as HiddenField).Value
                                    where selected
                                    select id).ToList());
        return ids;
    }
    public void Getpackagedetails(string Pid, string studid)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("getpackagebystudid", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Pid", Pid);
                    cmd.Parameters.AddWithValue("@StudendId", studid);
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        StringBuilder html = new StringBuilder();
                        html.Append(dt.Rows[0]["Title"].ToString() + " (Total Amount: " + dt.Rows[0]["Totalamount"].ToString() + ")");
                        lblpackage.Text = html.ToString();
                        hfcurrency.Value = dt.Rows[0]["Currency"].ToString();
                    }
                }
                con.Close();
            }
        }
        catch (Exception ex)
        {

        }
    }
    public string Studentdiscount()
    {
        var ids = string.Join(",", (from r in reptdiscount.Items.Cast<RepeaterItem>()
                                    let selected = (r.FindControl("chkselecteditem") as CheckBox).Checked
                                    let id = (r.FindControl("hf_ID") as HiddenField).Value
                                    where selected
                                    select id).ToList());
        return ids;
    }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        SaveStudent();
    }

    public void SaveStudent()
    {
        try
        {
            string Pphone = "";
            string Sphone = "";
            IEnumerator en = Request.Form.GetEnumerator();
            while (en.MoveNext())
            {
                if (en.Current.ToString().Contains("Phone"))
                {
                    Sphone += Request.Form[en.Current.ToString()] + "|";
                }
                else if (en.Current.ToString().Contains("arenthone"))
                {
                    Pphone += Request.Form[en.Current.ToString()] + "|";
                }
            }

            bool ch = false;
            if (btnsave.Text == "Save")
            {

                if (txtstudentemail.Text != "" && txtstudentemail.Enabled)
                {
                    Sendemailtouser(txtstudentemail.Text, 1);
                }
                if (txtparentsemail.Text != "" && txtparentsemail.Enabled)
                {
                    Sendemailtouser(txtparentsemail.Text, 2);
                }
                //if (txtothersemail.Text != "" && txtothersemail.Enabled)
                //{
                //    Sendemailtouser(txtothersemail.Text,3);
                //}
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("sp_insertstudent", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@PID", Request["pid"]);
                        //txtStudentname
                        cmd.Parameters.AddWithValue("@title", txtStudentname.Text);
                        cmd.Parameters.AddWithValue("@StudentEmail", txtstudentemail.Text);
                        cmd.Parameters.AddWithValue("@StudentprimaryID", hfstudentemail.Value);
                        cmd.Parameters.AddWithValue("@studentrollnumber", txtrollnum.Text);
                        cmd.Parameters.AddWithValue("@packagechangedate", txtstartdate.Text);
                        cmd.Parameters.AddWithValue("@ParentEmail", txtparentsemail.Text);
                        cmd.Parameters.AddWithValue("@ParentId", hfparentemail.Value);
                        cmd.Parameters.AddWithValue("@parentname", txtParentname.Text);
                        cmd.Parameters.AddWithValue("@Parentphone", Pphone.Trim('|'));
                        cmd.Parameters.AddWithValue("@studentphone", Sphone.Trim('|'));
                        cmd.Parameters.AddWithValue("@feesbit", ch);
                        cmd.Parameters.AddWithValue("@InstanceID", this.InstanceID);
                        //hfstudentpackage.Value = StudentPackagedetail();
                        //cmd.Parameters.AddWithValue("@package", hfstudentpackage.Value);
                        cmd.CommandType = CommandType.StoredProcedure;
                        //getstudentpackage();
                        hfstudentid.Value = cmd.ExecuteScalar().ToString();
                        btnsave.Text = "Update";
                        btnSaveClose.Text = "Update & Close";
                        btnAddNew.Text = "Update & Add Another Student";
                        //lnkshowhistory.Visible = true;
                        //Insertstudenthistory(hfstudentid.Value, txtStudentname.Text+" record Inserted ");
                        ScriptManager.RegisterStartupScript(this, GetType(), "updatealert", "alert('student info inserted sucessfully !')", true);

                        //DataTable dt = new DataTable();
                        //SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        //adp.Fill(dt);
                        //rptrfeepakages.DataSource = dt;
                        //rptrfeepakages.DataBind();

                    }
                    using (SqlCommand cmd = new SqlCommand("incrementall", con))
                    {
                        cmd.Parameters.AddWithValue("@id", hfparentid.Value);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                    con.Close();
                }
                getstudentpackage();

            }
            else
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("updatestudentrecord", con))
                    {
                        con.Open();
                        //cmd.Parameters.AddWithValue("@PID", Request["pid"]);
                        //txtStudentname
                        cmd.Parameters.AddWithValue("@title", txtStudentname.Text);
                        cmd.Parameters.AddWithValue("@StudentEmail", txtstudentemail.Text);
                        cmd.Parameters.AddWithValue("@StudentprimaryID", hfstudentemail.Value);
                        cmd.Parameters.AddWithValue("@studentrollnumber", txtrollnum.Text);
                        cmd.Parameters.AddWithValue("@studentid", hfstudentid.Value);

                        cmd.Parameters.AddWithValue("@packagechangedate", txtstartdate.Text);
                        cmd.Parameters.AddWithValue("@ParentEmail", txtparentsemail.Text);
                        cmd.Parameters.AddWithValue("@ParentId", hfparentemail.Value);
                        cmd.Parameters.AddWithValue("@parentname", txtParentname.Text);
                        cmd.Parameters.AddWithValue("@Parentphone", Pphone.Trim('|'));
                        cmd.Parameters.AddWithValue("@studentphone", Sphone.Trim('|'));
                        cmd.Parameters.AddWithValue("@chfees", ch);
                        cmd.Parameters.AddWithValue("@InstanceID", this.InstanceID);
                        //hfstudentpackage.Value = StudentPackagedetail();
                        //cmd.Parameters.AddWithValue("@package", hfstudentpackage.Value);
                        cmd.CommandType = CommandType.StoredProcedure;
                        //getstudentpackage();
                        cmd.ExecuteNonQuery();
                        //Insertstudenthistory(hfstudentid.Value,txtStudentname.Text +" record Updated ");
                        ScriptManager.RegisterStartupScript(this, GetType(), "updatealert", "alert('student info updated sucessfully !')", true);
                        //DataTable dt = new DataTable();
                        //SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        //adp.Fill(dt);
                        //rptrfeepakages.DataSource = dt;
                        //rptrfeepakages.DataBind();

                    }
                    con.Close();
                }
                getstudentpackage();
            }
        }
        catch (Exception ex)
        {

        }

    }

    private void generateinvoice(string studentid)
    {
        studentinvoice objstudent = new studentinvoice();
        studentparameter objinvoice = new studentparameter();
        objinvoice.Monthrange = ddlfee.SelectedItem.Text + " Date:" + txtfeedate.Text;
        objinvoice.TotalPackagefees = Convert.ToDecimal(ddlfee.SelectedValue);
        objinvoice.Totalcharges = 0;
        objinvoice.Totaldiscount = 0;
        objinvoice.feespaid = 0;
        objinvoice.feesbalance = Convert.ToDecimal(ddlfee.SelectedValue);
        objinvoice.Totalfees = Convert.ToDecimal(ddlfee.SelectedValue);
        objinvoice.extracharges = 0;
        objinvoice.extradiscount = 0;
        objinvoice.adjustment = 0;
        objinvoice.studentid = studentid;
        objinvoice.paidcomplete = "0";
        objinvoice.fromdate = Convert.ToDateTime(txtfeedate.Text);
        objinvoice.todate = Convert.ToDateTime(txtfeedate.Text);
        int s = objstudent.insertinvoice(objinvoice);

    }
    protected void rptrfeepackage_itemdatabound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            List<string> packagelist = new List<string>();
            HiddenField hfpackgeid = e.Item.FindControl("hf_ID") as HiddenField;
            //hfpackgeid.Value;

            packagelist.AddRange(hfstudentpackage.Value.Split(','));
           
            if (packagelist.Contains(hfpackgeid.Value))
            {
                Label lblgridtitle = e.Item.FindControl("lblgridtitle") as Label;
                Label lblgridamount = e.Item.FindControl("lblgridamount") as Label;
                CheckBox chkselecteditem = e.Item.FindControl("chkselecteditem") as CheckBox;
                chkselecteditem.Checked = true;
                dtcharges.Rows.Add(new object[] { lblgridtitle.Text + " &ndash; " + hfcurrency.Value + " " + lblgridamount.Text });
            }
        }

    }
    protected void rptrdiscount_itemdatabound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            List<string> packagelist = new List<string>();
            HiddenField hfpackgeid = e.Item.FindControl("hf_ID") as HiddenField;
            //hfpackgeid.Value;

          
            packagelist.AddRange(hfdiscount.Value.Split(','));
            if (packagelist.Contains(hfpackgeid.Value))
            {
                Label lblgridtitle = e.Item.FindControl("lblgridtitle") as Label;
                Label lblgridamount = e.Item.FindControl("lblgridamount") as Label;
                CheckBox chkselecteditem = e.Item.FindControl("chkselecteditem") as CheckBox;
                chkselecteditem.Checked = true;
                dtdiscount.Rows.Add(new object[] { lblgridtitle.Text + " &ndash; " + hfcurrency.Value + " " + lblgridamount.Text });
            }
        }

    }
    public void Insertstudenthistory(string sid, string message)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("INSERT INTO tbl_studenthistory ( StudentId, Message )VALUES  ( @sid, @message)", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@sid", sid);
                    cmd.Parameters.AddWithValue("@message", message);
                    cmd.ExecuteNonQuery();
                }
            }
            Showstudenthistory(sid);
        }
        catch (Exception)
        {

            throw;
        }
    }

    public void Showstudenthistory(string sid)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT ID,Message,DateTime  FROM tbl_studenthistory WHERE StudentId=@id", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", sid);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    rptrhistory.DataSource = dt;
                    rptrhistory.DataBind();
                }
            }
        }
        catch (Exception)
        {

            throw;
        }


    }

    protected void rptrhistory_itemdatabound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
            object container = e.Item.DataItem;

            Label lblmodeficationdate = e.Item.FindControl("lblgridamount") as Label;
            lblmodeficationdate.Text = objATZ.GetDate(DataBinder.Eval(container, "DateTime").ToString(),
                                                      HttpContext.Current.User.Identity.Name);
            lblmodeficationdate.Text += " " + objATZ.GetTime(DataBinder.Eval(container, "DateTime").ToString(), HttpContext.Current.User.Identity.Name);


        }
    }

    public void Sendemailtouser(string useremail,int uid)
    {
        Hashtable objuser = new Hashtable();
        AvaimaUserProfile objwebser = new AvaimaUserProfile();
        objuser.Add("userid", Guid.NewGuid().ToString());
        objuser.Add("email", useremail == "" ? "" : useremail);
        //objuser.Add("Contractid", Request["id"]);
        
        // adding student info
        if (uid == 1)
            hfstudentemail.Value = objuser["userid"].ToString();

        //adding parentinfo
        if (uid == 2)
            hfparentemail.Value = objuser["userid"].ToString();

        //adding otherinfo
        //if (uid == 3)
        //    hfotheremail.Value = objuser["userid"].ToString();

        objwebser.insertUser(objuser["userid"].ToString(), "", objuser["email"].ToString(), "", "", "",
                             "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");   // objuser["Contractid"].ToString(), "");
        AvaimaEmailAPI email = new AvaimaEmailAPI();
        //string message = "<a href='http://www.avaima.com/a43d3b60-38ed-40fa-8abd-a2ca82bf7c45/viewprofile.aspx?userid=" + objuser["userid"].ToString() + "&id=" + Request["id"] + "&role=Contractor'>click here</a>";
        //email.send_email(txtcontactoremail.Text, "Contract Management", "", message, "Provide Information");
        //email.Adduseremail(HttpContext.Current.User.Identity.Name, objuser["userid"].ToString(), useremail, "Attendance Management");

        string message = "Hi,<br/>I am using School Fees Management application on Avaima.com <br/>";
        message += "<br/> Student Name: " + txtStudentname.Text;
        message += "<br/>Avaima.com will send you timely reminders before this contract expires.<br/> <br/>Thanks,<br/>";
 //       string[] user = objwebser.Getonlineuserinfo(this.Context.User.Identity.Name).Split('#');
   //     message += user[0] + " " + user[1];
   //     string name = user[0] + " " + user[1];
     //   email.send_email(useremail, name, user[2], message, "Attendace Management");
    }
    protected void btninsert_Click(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("update tbl_feesManagement set PackageId=@PackageId,packagechangedate=@packagechangedate where studendId=@studendId", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@PackageId", ddldateset.SelectedValue);
                cmd.Parameters.AddWithValue("@packagechangedate", txtppstartdate.Text);
                cmd.Parameters.AddWithValue("@studendId", hfstudentid.Value);
                cmd.CommandType = CommandType.Text;
                //getstudentpackage();
                cmd.ExecuteNonQuery();
                string ch = "Package change, Following are the changes:<br /> Previous package start date:" + txtstartdate.Text + "<br />";
                ch = ch + "New package start date:" + txtppstartdate.Text + "<br />";
                ch = ch + "Previous package name:" + lblpackage.Text.Split('(')[0] + "<br />";
                ch = ch + "New package name:" + ddldateset.SelectedItem.Text + "<br />";
                Insertstudenthistory(hfstudentid.Value,ch);
                Filldatesetting();
            }
        }
    }
    protected void lnkadddepartment_Click(object sender, EventArgs e)
    {
        //if (hfpackageid.Value == "0")
        //    this.Redirect("FeesPackage.aspx?studentid=" + hfstudentid.Value);
        //else
        //    this.Redirect("FeesPackage.aspx?id=" + hfpackageid.Value);
        string stid = "";
        if (!String.IsNullOrEmpty(Request.QueryString["stid"]))
        {
            stid = "&studentid=" + Request.QueryString["stid"];
            stid += "&pstate=" + Request.QueryString["pstate"];
        }
        else
        {
            stid = "&new=" + Request.QueryString["new"];
            stid += "&pid=" + Request.QueryString["pid"];
        }
        this.Redirect("FeesPackage.aspx?student=y" + stid);
    }
    protected void btnsavecharges_Click(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("update tbl_student  set Assignedpackage = @package where StudendId = @studentid ", con))
            {
                con.Open();
                hfstudentpackage.Value = StudentPackagedetail();
                cmd.Parameters.AddWithValue("@package", hfstudentpackage.Value);
                cmd.Parameters.AddWithValue("@studentid", hfstudentid.Value);
                cmd.ExecuteNonQuery();
            }
            con.Close();
        }
        getstudentpackage();
        Fillrptrfeepakages();
    }
    protected void btndiscount_Click(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("update tbl_student  set Assigndiscount = @package where StudendId = @studentid ", con))
            {
                con.Open();
                hfdiscount.Value = Studentdiscount();
                cmd.Parameters.AddWithValue("@package", hfdiscount.Value);
                cmd.Parameters.AddWithValue("@studentid", hfstudentid.Value);
                cmd.ExecuteNonQuery();
            }
            con.Close();
        }
        getstudentpackage();
        Fillrptrdiscount();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            int value = 0;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_insertchargediscount1", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@title", txtcharges.Text);
                    cmd.Parameters.AddWithValue("@chargetype", "Charge");
                    cmd.Parameters.AddWithValue("@isfixed", "Fixed");
                    cmd.Parameters.AddWithValue("@amount", txtChargesamount.Text);
                    cmd.Parameters.AddWithValue("@InstanceID", this.InstanceID);
                    cmd.CommandType = CommandType.StoredProcedure;

                    value = Convert.ToInt32(cmd.ExecuteScalar());
                }
                con.Close();
                txtcharges.Text = "";
                txtChargesamount.Text = "";
            }
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("update tbl_student  set Assignedpackage = @package where StudendId = @studentid ", con))
                {
                    con.Open();
                    hfstudentpackage.Value = StudentPackagedetail();
                    cmd.Parameters.AddWithValue("@package", hfstudentpackage.Value + "," + value);
                    cmd.Parameters.AddWithValue("@studentid", hfstudentid.Value);
                    cmd.ExecuteNonQuery();
                }
                con.Close();
            }
            getstudentpackage();
            Fillrptrfeepakages();
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        try
        {
            int value = 0;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_insertchargediscount1", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@title", txtchargetitle.Text);
                    cmd.Parameters.AddWithValue("@chargetype", "Discount");
                    cmd.Parameters.AddWithValue("@isfixed", "Fixed");
                    cmd.Parameters.AddWithValue("@amount", txtamount.Text);
                    cmd.Parameters.AddWithValue("@InstanceID", this.InstanceID);
                    cmd.CommandType = CommandType.StoredProcedure;
                    value = Convert.ToInt32(cmd.ExecuteScalar());
                }
                con.Close();
                txtchargetitle.Text = "";
                txtamount.Text = "";
            }
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("update tbl_student  set Assigndiscount = @package where StudendId = @studentid ", con))
                {
                    con.Open();
                    hfdiscount.Value = Studentdiscount();
                    cmd.Parameters.AddWithValue("@package", hfdiscount.Value + "," + value);
                    cmd.Parameters.AddWithValue("@studentid", hfstudentid.Value);
                    cmd.ExecuteNonQuery();
                }
                con.Close();
            }
            getstudentpackage();
            Fillrptrdiscount();
        }

        catch (Exception ex)
        {

        }
    }
    protected void btngeninvoice_Click(object sender, EventArgs e)
    {
        try
        {
            generateinvoice(hfstudentid.Value);
            ScriptManager.RegisterStartupScript(this, GetType(), "updatealert", "alert('student info updated sucessfully !')", true);
        }
        catch (Exception ex)
        {
            lblmsg.Text = ex.Message;
        }
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        SaveStudent();
        this.Redirect("StudentInfo.aspx?new=1&pid=" + hfparentid.Value);
    }

    protected void btnSaveClose_Click(object sender, EventArgs e)
    {
        SaveStudent();
        this.Redirect("Index.aspx");
    }
}