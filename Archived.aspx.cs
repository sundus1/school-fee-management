﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Collections;
using AvaimaWS;
using System.Text.RegularExpressions;
using System.Text;

public partial class Archived : AvaimaThirdpartyTool.AvaimaWebPage
{
    PagedDataSource pds = new PagedDataSource();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            int id = Convert.ToInt32(Request.QueryString["id"]);
            BindData(id);
        }
     
    }

    //Archive Students List
    public void BindData(int id)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("getArchiveStudents", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@PID", id);
                    cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);

                    pds.AllowPaging = true;
                    pds.DataSource = dt.DefaultView;
                    pds.CurrentPageIndex = PagerControl.CurrentPage;
                    pds.PageSize = PagerControl.PageSize;
                    PagerControl.TotalRecords = dt.Rows.Count;
                    RptrArchive.DataSource = pds;
                    RptrArchive.DataBind();
                    PagerControl.LoadStatus();
                    
                    if (dt.Rows.Count > 0)
                    {
                        lblgriderror.Text = "";
                    }
                    else
                    {
                        lblgriderror.Text = "No Record Found";
                        RptrArchive.DataSource = pds;
                        RptrArchive.DataBind();
                    }
                }
                con.Close();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void lnkback_Click(object sender, EventArgs e)
    {
        this.Redirect("Index.aspx");
    }

    protected void RptrArchive_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }

    protected void RptrArchive_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }

    protected void lnkRestore_Click(object sender, EventArgs e)
    {
        int PID = Convert.ToInt32(Request.QueryString["id"]);

        var ids = (from r in RptrArchive.Items.Cast<RepeaterItem>()
                   let selected = (r.FindControl("chkselecteditem") as CheckBox).Checked
                   let id = (r.FindControl("hf_ID") as HiddenField).Value
                   where selected
                   select id).ToList();            

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            con.Open();
            foreach (var id in ids)
            {
                using (SqlCommand cmd = new SqlCommand("restoreStudents", con))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                    cmd.Parameters.AddWithValue("@PID",PID);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
              
            }
        }

        BindData(PID);
    }
}