﻿function removeConfirmation(control_uniqueid, name_deleted) {
    if ($(".cb_select input:checked").size()) {
        $("#b_name_to_be_deleted").html(name_deleted);
        var dlg = $("#removemail").dialog({
            resizable: false,
            height: 150,
            width: 400,
            modal: true,
            autoOpen: true,
            buttons: {
                "Yes": function () {
                    $(this).dialog("close");
                    __doPostBack('lbtndel_Click', 'OnClick');
                    //__doPostBack(control_uniqueid, 'OnClick');
                    //lbtn_del
                },
                "No": function () {
                    $(this).dialog("close");
                    return false;
                }
            }
        });
        return false;
    } else {
        alert('Select record');
    }
    return false;
}



function setvalue(id) {
    var x = document.getElementById("hftreenodevalue").value = id;
    return false;
}



//for start date
$(function () {
    $('.disabletext').keypress(function (event) {
        event.preventDefault();
    });
    // $("#txtstartdate").datepicker({ dateFormat: 'yyyy-mm-dd' });
    $("#txtstartdate").datepick({ dateFormat: 'dd-MM-yyyy' });


    //for end date

    //$("#txtenddate").datepicker({ dateFormat: 'yyyy-mm-dd' });
    $("#txtenddate").datepick({ dateFormat: 'dd-MM-yyyy' });



    // for checking that value is not in root
    $("#lnkaddstudent").click(function () {
        if ($("#hfparentid").val() == "0") {
            alert("student could not be added at root");
            return false;
        }
    });


    

    //for toogle div search
    $('#lnksearch').click(function () {
        if (document.getElementById('hfdivstate').value == "0") {
            $('#searchdate').toggle();
            $('#divmenu').toggle();
            $('#divresult').toggle();
            $('#divheadtotalresult').toggle();

            document.getElementById('hfdivstate').value = 1;
        }
            resizeiframepage();
        return false;
    });

});


// for delete
//$(function () {
//    $('#lbtn_del').click(function () {
//        return confirm("are you sure you want to delete");
//    });
//});



//$(function () {
//    $('#btn_cancel').click(function () {
//        $('#txtstartdate').val("");
//        $('#txtenddate').val("");
//        $('#searchdate').toggle();
//        $('#divmenu').toggle();
//        document.getElementById('hfdivstate').value = 0;
//        return false;
//    });
//});


//for move popup
$(function () {
    $("#lnkmove").click(function () {
        if ($(".cb_select input:checked").size() > 0) {
            //if ($('.cb_select_all').is(':checked')) {
            $('#lblerror').text("");
            var dlg = $("#divtreeview").dialog({
                height: 400,
                width: 250,
                show: {
                    height: 100
                    //effect: "blind",
                    //duration: 1000
                },
                modal: true,
                hide: {
                    effect: "blind",
                    //duration: 1000
                }
            });
             resizeiframepage();

            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");

            return false;
        } else {
            alert('Select record');
        }
        return false;
        //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
    });
    $("#txtstartdate").datepicker({
        dateFormat: 'dd-MM-yy',
        changeMonth: true,
        changeYear: true
    });

    $('#lbtnupload').click(function () {

        var dlg = $("#div_upload").dialog({
            width: 530,
            show: {

            },
            modal: true,
            hide: {
                effect: "blind",
            }
        });
        dlg.parent().appendTo($("form:first"));
        dlg.parent().css("z-index", "1000");
        resizeiframepage();
        return false;


        //$('#div_upload').toggle('blind', 'slow', function () {
        //    resizeiframepage();
        //});
        //return false;

        
    });


    $('#lbtn_del').click(function () {
        var dlg = $("#div_delete").dialog({
            width: 530,
            show: {

            },
            modal: true,
            hide: {
                effect: "blind",
            }
        });
        dlg.parent().appendTo($("form:first"));
        dlg.parent().css("z-index", "1000");
        //resizeiframepage();
        return false;
    });

    

    $('#lnkCancel').click(function () {
        var dlg = $("#div_delete").dialog('close');
        return false;
    });


    //for add value popup
    $(function () {

        $("#lnkadddepartment").click(function () {
            $("#txttitle").val("");
            $("#ddlpackage").val("");
           // $("#hfeditid").val("");
            
            $("#lbltitlereq").hide();
            $("#lblddlerror").hide();
            $('#lblerror').text("");
            var dlg = $("#divdepart").dialog({
                width:400,
                show: {


                    //effect: "blind",
                    //duration: 1000
                },
                modal: true,
                hide: {


                    effect: "blind",
                    //duration: 1000
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            resizeiframepage();
            return false;
            //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
        });



        //-- for searchdate range 
        //$('#btnserachdate').click(function () {
        //    if ($('#txtstartdate').val() == "" || $('#txtenddate').val() == "") {
        //        if ($('#txtstartdate').val() == "")
        //            $('#txtstartdate').focus();
        //        if ($('#txtenddate').val() == "") {
        //            $('#txtenddate').focus();
        //        }
        //        return false;
        //    }

        //});

        //for toogle div search
        $('#lnksearch').click(function () {
            if (document.getElementById('hfdivstate').value == "0") {
                $('#searchdate').toggle();
                $('#divmenu').toggle();
                $('#divresult').toggle();
                $('#divheadtotalresult').toggle();

                document.getElementById('hfdivstate').value = 1;
            }
               resizeiframepage();
            return false;
        });

        // for insert click
        $("#btninsert").click(function () {
            $("#lbltitlereq").hide();
            $("#lblddlerror").hide();
            if ($('#txttitle').val() == "" || $('#ddlpackage').val() == "") {
                if ($('#txttitle').val() == "") {
                    $("#lbltitlereq").show();
                    $('#txttitle').focus();
                }
                if ($('#ddlpackage').val() == "") {
                    $("#lblddlerror").show();
                    $('#ddlpackage').focus();
                }
                // lbltitlereq
                // lblddlerror
                // $('#lblerror').text('required');
                return false;
            }
        });


        $('.cb_select_all input').change(function () {
            if (this.checked)
                $($(this).parents("table")[0]).find(".cb_select input").each(function () {
                    this.checked = true;
                });
            else {
                $($(this).parents("table")[0]).find(".cb_select input").each(function () {
                    this.checked = false;
                });
            }
        });
    });
});

function setvalue(id) {
    var x = document.getElementById("hftreenodevalue").value = id;
    return false;
}

function openeditdiv() {
    $("#lbltitlereq").hide();
    $("#lblddlerror").hide();
    //$('#lblerror').text("");
    var dlg = $("#divdepart").dialog({
        width: 400,
        show: {



            //effect: "blind",
            //duration: 1000
        },
        modal: true,
        hide: {
            effect: "blind",
            //duration: 1000
        }
    });
    dlg.parent().appendTo($("form:first"));
    dlg.parent().css("z-index", "1000");
    resizeiframepage();
    return false;
    //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
}

function resizeiframepage() {
    var iframe = window.parent.document.getElementById("MainContent_ifapp");
    var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
    if (innerDoc.body.offsetHeight) {
        iframe.height = innerDoc.body.offsetHeight + 400 + "px";
    }
    else if (iframe.Document && iframe.Document.body.scrollHeight) {
        iframe.style.height = iframe.Document.body.scrollHeight + "px";
    }
}