﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Archived.aspx.cs" Inherits="Archived" %>

<%@ Register Src="~/WebApplication1/PagerControl.ascx" TagPrefix="uc1" TagName="PagerControl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <link href="_assets/css/style.css" rel="stylesheet" />
    <link href="_assets/jquery-ui.css" rel="stylesheet" />
    <link href="_assets/jquery.datepick.css" rel="stylesheet" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <script src="_assets/jquery.datepick.js"></script>
    <script src="_assets/js/pages/indexscript.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="mainContentDiv">

            <div id="divmenu" runat="server">
                <div id="divddlhide" runat="server">
                    <asp:LinkButton ID="lnkback" Text="Back" OnClick="lnkback_Click" runat="server"></asp:LinkButton> 
                    |
                    <asp:LinkButton runat="server" ID="lnkRestore" Text="Restore" OnClick="lnkRestore_Click"></asp:LinkButton>                
                </div>
            </div>

            <div id="divresult" runat="server">
                <asp:Repeater ID="RptrArchive" runat="server" OnItemCommand="RptrArchive_ItemCommand" OnItemDataBound="RptrArchive_ItemDataBound">
                    <HeaderTemplate>
                        <table id="tblconctract" class="smallGrid">
                            <tr class="smallGridHead">
                                <td>
                                    <asp:CheckBox ID="chkselectall" runat="server" CssClass="cb_select_all" />
                                </td>                               
                                <td style="width: 350px">Title 
                                </td>
                                <td>Reason
                                </td>
                                <td>Modification Date
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="smallGrid">
                            <td>
                                <asp:CheckBox ID="chkselecteditem" runat="server" CssClass="cb_select" />
                                <asp:HiddenField runat="server" ID="hf_ID" Value='<%# Eval("SID") %>' />
                                <asp:HiddenField runat="server" ID="hf_PID" Value='<%# Eval("PID") %>' />
                            </td>
                            <td>
                                <asp:LinkButton ID="lnktitle" Text='<%# Eval("Title") %>' CommandArgument='<%# Eval("SID") %>' CommandName="search" runat="server"></asp:LinkButton>
                            </td>
                            <td>
                                 <asp:Label ID="lblDeactivateReason" Text='<%# Eval("DeactivateReason") %>' runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label runat="server" Text='<%# Eval("ModificationDate") %>' ID="lblmodeficationdate"></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>

                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <asp:HiddenField runat="server" ID="hfparentid" />

                <asp:Label ID="lblgriderror" runat="server" Text="" ForeColor="red"></asp:Label>
                <div style="width: 300px">
                    <uc1:PagerControl runat="server" ID="PagerControl" PageSize="20" />
                </div>

            </div>

        </div>
    </form>
</body>
</html>
