﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Invoice.aspx.cs" Inherits="Invoice" %>

<%@ Register Src="~/WebApplication1/PagerControl.ascx" TagPrefix="uc1" TagName="PagerControl" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body {
            font: 13px/16px arial, helvetica, sans-serif;
        }

        .InvoiceTable {
            border-collapse: collapse;
        }

            .InvoiceTable .Date {
                font-weight: bold;
            }

            .InvoiceTable .DateTop {
                background: #F3F3F3;
            }

            .InvoiceTable .TdDesc {
                padding: 20px 16px;
            }

            .InvoiceTable .TdTotal {
                background: none repeat scroll 0 0 #FBFBFB;
                border: 1px solid #CCCCCC;
                padding: 10px 16px;
            }

        .LableTd {
            width: 488px;
        }

        .TotalTop {
            border-top: 1px solid #000;
        }

        .FontWeight {
            font-weight: bold;
        }

        .tbsetting {
            float: right;
        }

        .errorMsg {
            color: red;
        }

        .lnkTxt {
            color: blue !important;
            cursor: pointer !important;
        }
    </style>
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <link href="_assets/jquery-ui.css" rel="stylesheet" />
    <link href="_assets/js/tooltipster.css" rel="stylesheet" />

    <link href="_assets/jquery.datepick.css" rel="stylesheet" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <script src="_assets/jquery.datepick.js"></script>
    <script src="_assets/js/jquery.tooltipster.min.js"></script>
    <script type="text/javascript">
        var mousePos;
        function handleMouseMove(event) {
            event = event || window.event; // IE-ism
            mousePos = {
                x: event.clientX,
                y: event.clientY
            };
        }
        $(document).ready(function () {
            $('.tbsetting').click(function () {
                try {
                    //showCursor($(this));
                    var dataabid = $(this).attr('data-id');
                    var totalamount = $(this).attr('total-amount');
                    var feespaid = $(this).attr('paid-amount');
                    var TotalPackagefees = $(this).attr('TotalPackagefees');
                    var extracharges = $(this).attr('extracharges');
                    var extradiscount = $(this).attr('extradiscount');
                    var Totalcharges = $(this).attr('Totalcharges');
                    var Totaldiscount = $(this).attr('Totaldiscount');
                    var adjustment = $(this).attr('adjustment');
                    var adjustmentflag = $(this).attr('adjustmentflag');

                    var pos = mousePos;
                    if (!pos) {
                        $('#menu').css('left', pos.x);
                        $('#menu').css('top', pos.y);
                        $('#menu').css('position', 'absolute');
                    }
                    else {
                        $('#menu').css('left', pos.x);
                        $('#menu').css('top', pos.y);
                        $('#menu').css('position', 'absolute');
                    }

                    $('#menu').show(200);
                    $('.ankdiscount').attr('data-id', dataabid);
                    $('.ankcharges').attr('data-id', dataabid);
                    $('.ankadjustment').attr('data-id', dataabid);

                    $('.ankdiscount').attr('total-amount', totalamount);
                    $('.ankcharges').attr('total-amount', totalamount);
                    $('.ankadjustment').attr('total-amount', totalamount);

                    $('.ankdiscount').attr('paid-amount', feespaid);
                    $('.ankcharges').attr('paid-amount', feespaid);
                    $('.ankadjustment').attr('paid-amount', feespaid);

                    $('.ankdiscount').attr('TotalPackagefees', TotalPackagefees);
                    $('.ankcharges').attr('TotalPackagefees', TotalPackagefees);
                    $('.ankadjustment').attr('TotalPackagefees', TotalPackagefees);

                    $('.ankdiscount').attr('Totaldiscount', Totaldiscount);
                    $('.ankcharges').attr('Totaldiscount', Totaldiscount);
                    $('.ankadjustment').attr('Totaldiscount', Totaldiscount);

                    $('.ankdiscount').attr('Totalcharges', Totalcharges);
                    $('.ankcharges').attr('Totalcharges', Totalcharges);
                    $('.ankadjustment').attr('Totalcharges', Totalcharges);

                    $('.ankdiscount').attr('extracharges', extracharges);
                    $('.ankcharges').attr('extracharges', extracharges);
                    $('.ankadjustment').attr('extracharges', extracharges);

                    $('.ankdiscount').attr('extradiscount', extradiscount);
                    $('.ankcharges').attr('extradiscount', extradiscount);
                    $('.ankadjustment').attr('extradiscount', extradiscount);

                    $('.ankdiscount').attr('adjustment', adjustment);
                    $('.ankcharges').attr('adjustment', adjustment);
                    $('.ankadjustment').attr('adjustment', adjustment);

                    $('.ankdiscount').attr('adjustmentflag', Totalcharges);
                    $('.ankcharges').attr('adjustmentflag', Totalcharges);
                    $('.ankadjustment').attr('adjustmentflag', Totalcharges);

                    if (parseFloat(extracharges) > 0) {
                        $('.ankcharges').text('Edit Charges');
                    }
                    else
                        $('.ankcharges').text('Add Charges');

                    if (parseFloat(extradiscount) > 0) {
                        $('.ankdiscount').text('Edit Discount');
                    }
                    else
                        $('.ankdiscount').text('Add Discount');

                    if (parseFloat(adjustment) > 0) {
                        $('.ankadjustment').text('Edit Adjustment');
                    }
                    else
                        $('.ankadjustment').text('Add Adjustment');

                    $('#menu').menu();
                } catch (e) {
                    alert(e.message);
                }
            });

            try {
                window.onmousemove = handleMouseMove;
            } catch (e) {
                alert(e.message);
            }

            $('body').click(function (event) {
                if (event.target.nodeName != 'IMG') {
                    $('#menu').hide(200);
                }
            })
            $(document).on('click', '#btnpayment', function () {
                var txtval = $('#txtpayment').val();
                if (txtval == "") {
                    $('#paymenterrmsg').text('Required Amount')
                    return false;
                }
                else {
                    var filter = /(?:^\d{1,3}(?:\.?\d{3})*(?:,\d{2})?$)|(?:^\d{1,3}(?:,?\d{3})*(?:\.\d{2})?$)/;
                    if (filter.test(txtval)) {
                        $('#paymenterrmsg').text('');
                        var dlg = $("#div1").dialog({
                            width: 416,
                            show: {

                            },
                            modal: true,
                            hide: {
                                effect: "blind",
                            }
                        });
                        dlg.parent().appendTo($("form:first"));
                        dlg.parent().css("z-index", "1000");
                        return false;
                    }
                    else {
                        $('#paymenterrmsg').text('Invalid Amount')
                        return false;
                    }
                }
            });

            $(document).on('click', '#addPaymentMethods', function () {

                var dlg = $("#additionalPaymentMethods").dialog({
                    width: 416,
                    show: {

                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;


            });

            $(document).on('click', '.ankdiscount', function () {
                var dataid = $(this).attr('data-id');
                var dataamount = $(this).attr('total-amount');
                var feespaid = $(this).attr('paid-amount');
                var TotalPackagefees = $(this).attr('TotalPackagefees');
                var Totalcharges = $(this).attr('Totalcharges');
                var Totaldiscount = $(this).attr('Totaldiscount');
                var extracharges = $(this).attr('extracharges');
                var extradiscount = $(this).attr('extradiscount');
                var adjustment = $(this).attr('adjustment');
                var adjustmentflag = $(this).attr('adjustmentflag');

                $('#hfEC').val(extracharges);
                $('#hfED').val(extradiscount);
                $('#hfEA').val(adjustment);
                $('#hfAT').val(adjustmentflag);
                $('#hfIDcharges').val(Totalcharges);
                $('#hfIDdiscount').val(Totaldiscount);
                $('#hftotalpackagefees').val(TotalPackagefees);
                $('#hffeespaid').val(feespaid);
                $('#hfid').val(dataid);
                $('#hftotalamount').val(dataamount);
                var dlg = $("#divDiscount").dialog({
                    width: 416,
                    show: {

                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });

            $(document).on('click', '.ankcharges', function () {
                var dataid = $(this).attr('data-id');
                var dataamount = $(this).attr('total-amount');
                var feespaid = $(this).attr('paid-amount');
                var TotalPackagefees = $(this).attr('TotalPackagefees');
                var Totalcharges = $(this).attr('Totalcharges');
                var Totaldiscount = $(this).attr('Totaldiscount');
                var extracharges = $(this).attr('extracharges');
                var extradiscount = $(this).attr('extradiscount');
                var adjustment = $(this).attr('adjustment');
                var adjustmentflag = $(this).attr('adjustmentflag');

                $('#hfEC').val(extracharges);
                $('#hfED').val(extradiscount);
                $('#hfEA').val(adjustment);
                $('#hfAT').val(adjustmentflag);
                $('#hfIDcharges').val(Totalcharges);
                $('#hfIDdiscount').val(Totaldiscount);
                $('#hftotalpackagefees').val(TotalPackagefees);
                $('#hffeespaid').val(feespaid);
                $('#hfid').val(dataid);
                $('#hftotalamount').val(dataamount);
                var dlg = $("#div2").dialog({
                    width: 416,
                    show: {

                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });
            $('.tooltip').tooltipster({
                interactive: true
            });
            //ajdustdel

            $(document).on('click', '.ankadjustment', function () {
                var dataid = $(this).attr('data-id');
                var dataamount = $(this).attr('total-amount');
                var feespaid = $(this).attr('paid-amount');
                var TotalPackagefees = $(this).attr('TotalPackagefees');
                var Totalcharges = $(this).attr('Totalcharges');
                var Totaldiscount = $(this).attr('Totaldiscount');
                var extracharges = $(this).attr('extracharges');
                var extradiscount = $(this).attr('extradiscount');
                var adjustment = $(this).attr('adjustment');
                var adjustmentflag = $(this).attr('adjustmentflag');

                $('#hfEC').val(extracharges);
                $('#hfED').val(extradiscount);
                $('#hfEA').val(adjustment);
                $('#hfAT').val(adjustmentflag);

                $('#hfIDcharges').val(Totalcharges);
                $('#hfIDdiscount').val(Totaldiscount);
                $('#hftotalpackagefees').val(TotalPackagefees);
                $('#hffeespaid').val(feespaid);
                $('#hfid').val(dataid);
                $('#hftotalamount').val(dataamount);
                var dlg = $("#div3").dialog({
                    width: 416,
                    show: {

                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });

            $("#txtpaiddate").datepicker({
                dateFormat: 'dd MM yy',
                changeMonth: true,
                changeYear: true,
            });

        });
        function resizeiframepage() {
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
            if (innerDoc.body.offsetHeight) {
                iframe.height = innerDoc.body.offsetHeight + 400 + "px";
            }
            else if (iframe.Document && iframe.Document.body.scrollHeight) {
                iframe.style.height = iframe.Document.body.scrollHeight + "px";
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="mainContentDiv">
            <%--<asp:LinkButton ID="lbtnvoucher" runat="server" Text="Generate Voucher" OnClick="lbtnvoucher_Click"></asp:LinkButton>--%>
            <a id="ankvoucher" runat="server" href="#" target="_blank">Generate Voucher</a>
            <asp:Panel ID="panel11" runat="server" DefaultButton="btnpayment">
                <h2>
                    <asp:Label ID="lblName" runat="server" Text="Muhammad Usman"></asp:Label></h2>
                <br />
                <br />
                <div class="Paymentdiv">
                    <p id="p_lastpayment" runat="server">
                        <b>Last Payment:</b>
                        <asp:Label ID="lblpaydate" runat="server"></asp:Label>, By
                        <asp:Label ID="lblpaymethod" runat="server"></asp:Label>, received by
                        <asp:Label ID="lblreceivedby" runat="server"></asp:Label>
                    </p>
                    <br />
                    <h2>Total Balance:
                    <asp:Label ID="lbltotladue" runat="server"></asp:Label></h2>
                    Receive Amount:
                <asp:TextBox ID="txtpayment" runat="server" ClientIDMode="Static" CssClass="standardField"></asp:TextBox><asp:Button ID="btnpayment" runat="server" Text="Collect Payment" CssClass="standardFormButton" /><asp:Label ID="paymenterrmsg" runat="server" ClientIDMode="Static" ForeColor="Red"></asp:Label>

                </div>
                <br />
                <asp:Repeater ID="reptinvoice" runat="server" OnItemDataBound="reptinvoice_ItemDataBound" OnItemCommand="reptinvoice_ItemCommand">
                    <ItemTemplate>
                        <table cellspacing="1" class="standardTable" style="float: left; margin-right: 20px;">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <asp:Label ID="lblmonth" runat="server" Text='<%# Eval("Monthrange") %>'></asp:Label>
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icon_info.png" CssClass="tooltip" />
                                        <asp:Image ImageUrl="~/images/gear.png" ID="tbimage" runat="server" data-id='<%# encodedvalue(Eval("recid")) %>' total-amount='<%# encodedvalue(Eval("Totalfees")) %>' paid-amount='<%# encodedvalue(Eval("feespaid")) %>' TotalPackagefees='<%# encodedvalue(Eval("TotalPackagefees")) %>'
                                            extracharges='<%# Eval("extracharges") %>' extradiscount='<%# Eval("extradiscount") %>' adjustment='<%# Eval("adjustment") %>' adjustmentflag='<%# Eval("adjustmentflag") %>'
                                            Totalcharges='<%# encodedvalue(Eval("Totalcharges")) %>' Totaldiscount='<%# encodedvalue(Eval("Totaldiscount")) %>' CssClass="tbsetting" />&nbsp;
                                        <asp:ImageButton ID="ImageButton1" runat="server" CommandName="invoicedel" ImageUrl="~/images/1394467620_cross-circle-frame.png" CssClass="tbsetting" OnClientClick="return confirm('Are You Sure You Want to Delete This Invoice. !')" Style="padding-right: 10px" />
                                        <asp:HiddenField ID="hfrecid" runat="server" Value='<%# Eval("recid") %>' />
                                    </th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Total Package Fees</td>
                                    <td class="pricingCol">
                                        <asp:Label ID="lblTPfess" runat="server" Text='<%# Eval("TotalPackagefees") %>'></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>Total Individual Charges</td>
                                    <td class="pricingCol">
                                        <asp:Label ID="lblcharges" runat="server" Text='<%# Eval("Totalcharges") %>'></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>Total Individual Discount</td>
                                    <td class="pricingCol">
                                        <asp:Label ID="lbldiscount" runat="server" Text='<%# Eval("Totaldiscount") %>'></asp:Label></td>
                                </tr>
                                <asp:Repeater ID="reptadjust" runat="server" OnItemCommand="reptadjust_ItemCommand">
                                    <ItemTemplate>
                                        <tr id="tr_charges" runat="server">
                                            <td>
                                                <asp:Label ID="lblcomment" runat="server" Text='<%# Eval("comments") %>'></asp:Label>&nbsp;<asp:ImageButton ID="delcharges" runat="server" CommandName="charges" ImageUrl="~/images/1394467620_cross-circle-frame.png" CssClass="ajdustdel" />
                                            </td>
                                            <td class="pricingCol">
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("amount") %>'></asp:Label>
                                                <asp:HiddenField ID="rowid" runat="server" Value='<%# Eval("recid") %>' />
                                                <asp:HiddenField ID="rowtype" runat="server" Value='<%# Eval("type") %>' />
                                                <asp:HiddenField ID="indd" runat="server" Value='<%# Eval("Invoiceid") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr style="display: none" id="tr_charges" runat="server">
                                    <td>Extra Charges&nbsp;<asp:Image ID="imgEC" runat="server" ImageUrl="~/images/icon_info.png" CssClass="tooltip" />&nbsp;<asp:ImageButton ID="delcharges" runat="server" CommandName="charges" ImageUrl="~/images/1394467620_cross-circle-frame.png" />
                                    </td>
                                    <td class="pricingCol">
                                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("extracharges") %>'></asp:Label></td>
                                </tr>
                                <tr style="display: none" id="tr_discount" runat="server">
                                    <td>Extra Discount &nbsp;<asp:Image ID="imgED" runat="server" ImageUrl="~/images/icon_info.png" CssClass="tooltip" />&nbsp;<asp:ImageButton ID="deldiscount" runat="server" CommandName="discount" ImageUrl="~/images/1394467620_cross-circle-frame.png" /></td>
                                    <td class="pricingCol">
                                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("extradiscount") %>'></asp:Label></td>
                                </tr>
                                <tr style="display: none" id="tr_adjustment" runat="server">
                                    <td>Adjustment&nbsp;<asp:Image ID="imgEA" runat="server" ImageUrl="~/images/icon_info.png" CssClass="tooltip" />&nbsp;<asp:ImageButton ID="deladjustment" runat="server" CommandName="adjustment" ImageUrl="~/images/1394467620_cross-circle-frame.png" /></td>
                                    <td class="pricingCol">
                                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("adjustment") %>'></asp:Label></td>
                                </tr>
                                <tr class="highlightedRow bg2 boldRow">
                                    <td>Total</td>
                                    <td class="pricingCol">
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("Totalfees") %>'></asp:Label></td>
                                </tr>
                                <tr class="highlightedRow bg2 boldRow">
                                    <td>Paid</td>
                                    <td class="pricingCol">
                                        <asp:Label ID="lblpaid" runat="server" Text='<%# Eval("feespaid") %>'></asp:Label></td>
                                </tr>
                                <tr class="highlightedRow boldRow">
                                    <td>Balance</td>
                                    <td class="pricingCol">
                                        <asp:Label ID="lblbalance" runat="server" Text='<%# Eval("feesbalance") %>'></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <a href="#" id="ankadjustment" class="menuitem ankadjustment" data-id='<%# encodedvalue(Eval("recid")) %>' total-amount='<%# encodedvalue(Eval("Totalfees")) %>' paid-amount='<%# encodedvalue(Eval("feespaid")) %>' totalpackagefees='<%# encodedvalue(Eval("TotalPackagefees")) %>'
                                            extracharges='<%# Eval("extracharges") %>' extradiscount='<%# Eval("extradiscount") %>' adjustment='<%# Eval("adjustment") %>' adjustmentflag='<%# Eval("adjustmentflag") %>'
                                            totalcharges='<%# encodedvalue(Eval("Totalcharges")) %>' totaldiscount='<%# encodedvalue(Eval("Totaldiscount")) %>'>Add Adjustment</a> | 
                                         <a href="#" id="ankcharges" class="menuitem ankcharges" data-id='<%# encodedvalue(Eval("recid")) %>' total-amount='<%# encodedvalue(Eval("Totalfees")) %>' paid-amount='<%# encodedvalue(Eval("feespaid")) %>' totalpackagefees='<%# encodedvalue(Eval("TotalPackagefees")) %>'
                                             extracharges='<%# Eval("extracharges") %>' extradiscount='<%# Eval("extradiscount") %>' adjustment='<%# Eval("adjustment") %>' adjustmentflag='<%# Eval("adjustmentflag") %>'
                                             totalcharges='<%# encodedvalue(Eval("Totalcharges")) %>' totaldiscount='<%# encodedvalue(Eval("Totaldiscount")) %>'>Add Charges</a> |
                                         <a href="#" id="ankdiscount" class="menuitem ankdiscount" data-id='<%# encodedvalue(Eval("recid")) %>' total-amount='<%# encodedvalue(Eval("Totalfees")) %>' paid-amount='<%# encodedvalue(Eval("feespaid")) %>' totalpackagefees='<%# encodedvalue(Eval("TotalPackagefees")) %>'
                                             extracharges='<%# Eval("extracharges") %>' extradiscount='<%# Eval("extradiscount") %>' adjustment='<%# Eval("adjustment") %>' adjustmentflag='<%# Eval("adjustmentflag") %>'
                                             totalcharges='<%# encodedvalue(Eval("Totalcharges")) %>' totaldiscount='<%# encodedvalue(Eval("Totaldiscount")) %>'>Add Discount</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>

                <ul id="menu" style="display: none">
                    <li>
                        <a href="#" id="ankdiscount" class="menuitem ankdiscount">Add Discount</a>
                    </li>
                    <li>
                        <a href="#" id="ankcharges" class="menuitem ankcharges">Add Charges</a>
                    </li>
                    <li>
                        <a href="#" id="ankadjustment" class="menuitem ankadjustment">Add Adjustment</a>
                    </li>
                </ul>
            </asp:Panel>

            <div style="clear: both; padding-bottom: 20px;"></div>
            <div>
                <h2>Payment History</h2>
                <asp:Repeater ID="repthistory" runat="server" OnItemDataBound="repthistory_ItemDataBound">
                    <HeaderTemplate>
                        <table id="tblconctract" class="smallGrid">
                            <tr class="smallGridHead">
                                <td>Date</td>
                                <td>Paid By</td>
                                <td>Recived By</td>
                                <td>Amount Paid</td>
                                <td></td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="smallGrid">
                            <td>
                                <asp:Label ID="lbldate" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblpaidby" runat="server" Text='<%# Eval("Paidby") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblrecivedby" runat="server"></asp:Label>
                                <asp:HiddenField ID="hfuserid" runat="server" Value='<%# Eval("userid") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblamount" runat="server" Text='<%# Eval("Feesubmit") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Image ID="imgEC" runat="server" ImageUrl="~/images/icon_info.png" CssClass="tooltip" />
                            </td>
                        </tr>
                    </ItemTemplate>

                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <div>
                    <uc1:PagerControl runat="server" ID="PagerControl" PageSize="10" />
                </div>
            </div>

        </div>
        <asp:HiddenField ID="hfid" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hftotalpackagefees" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hffeespaid" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hftotalamount" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hfIDcharges" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hfIDdiscount" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hfED" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hfEA" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hfEC" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hfAT" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hfbalanceamount" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hfrecid" runat="server" Value="0" />
        <div id="divDiscount" title="Discount" style="display: none">
            <asp:Panel ID="Panel2" DefaultButton="btndisamount" runat="server">
                <table>
                    <tr>
                        <td>Discount Amount:
                        </td>
                        <td>
                            <asp:TextBox ID="txtdisamount" runat="server" CssClass="standardField"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ValidationGroup="asd" ControlToValidate="txtdisamount" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid" ValidationGroup="asd" ValidationExpression="\d+(\.\d{1,2})?" ControlToValidate="txtdisamount" ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Comments:
                        </td>
                        <td>
                            <asp:TextBox ID="txtdiscountcomment" runat="server" TextMode="MultiLine" Rows="3" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btndisamount" runat="server" Text="Add Amount" ValidationGroup="asd" OnClick="btndisamount_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>

        <div id="div2" title="Charges" style="display: none">
            <asp:Panel ID="Panel3" DefaultButton="btnchargeamount" runat="server">
                <table>
                    <tr>
                        <td>Charges Amount:
                        </td>
                        <td>
                            <asp:TextBox ID="txtchargeamount" runat="server" CssClass="standardField"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ValidationGroup="asd1" ControlToValidate="txtchargeamount" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Invalid" ValidationGroup="asd1" ValidationExpression="\d+(\.\d{1,2})?" ControlToValidate="txtchargeamount" ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>

                    </tr>
                    <tr>
                        <td>Comments:
                        </td>
                        <td>
                            <asp:TextBox ID="txtchargescomment" runat="server" TextMode="MultiLine" Rows="3" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btnchargeamount" runat="server" Text="Add Amount" ValidationGroup="asd1" OnClick="btnchargeamount_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
        <div id="div3" title="Adjustment" style="display: none">
            <asp:Panel ID="Panel4" DefaultButton="btnadjustamount" runat="server">
                <table>
                    <tr>
                        <td>Adjustment Amount:
                        </td>
                        <td>
                            <asp:TextBox ID="txtadjustamount" runat="server" CssClass="standardField"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ValidationGroup="asd2" ControlToValidate="txtadjustamount" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Invalid" ValidationGroup="asd2" ValidationExpression="\d+(\.\d{1,2})?" ControlToValidate="txtadjustamount" ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Comments:
                        </td>
                        <td>
                            <asp:TextBox ID="txtajdustmentcomment" runat="server" TextMode="MultiLine" Rows="3" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Add/Subtract:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlAtype" runat="server">
                                <asp:ListItem Value="0" Selected>Add Amount</asp:ListItem>
                                <asp:ListItem Value="1">Subtract Amount</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btnadjustamount" runat="server" Text="Add Amount" ValidationGroup="asd2" OnClick="btnadjustamount_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>

        <div id="div1" title="Payment Method" style="display: none">
            <asp:Panel ID="Panel1" DefaultButton="btnprocessed" runat="server">
                <table>
                    <tr>
                        <td>Paid Date:
                        </td>
                        <td>
                            <asp:TextBox ID="txtpaiddate" runat="server" CssClass="standardField" ClientIDMode="Static"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Paid By:
                        </td>
                        <td>
                            <asp:TextBox ID="txtpaidby" runat="server" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Payment Method:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlpayment" runat="server" AppendDataBoundItems="True">
                            </asp:DropDownList><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="ddlpayment" ForeColor="Red" InitialValue="0" ValidationGroup="payment"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:HyperLink runat="server" ID="addPaymentMethods" Text="Add Payment Method" CssClass="lnkTxt"></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td>Comments:
                        </td>
                        <td>
                            <asp:TextBox ID="txtpaycomment" runat="server" TextMode="MultiLine" Rows="3" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btnprocessed" runat="server" Text="Pay" CssClass="standardFormButton" ValidationGroup="payment" OnClick="btnprocessed_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>


        <div id="additionalPaymentMethods" title="Payment Method" style="display: none">
            <asp:Panel ID="Panel5" DefaultButton="btnaddmethod" runat="server">
                <table>
                    <tr>
                        <td>Title:
                        </td>
                        <td>
                            <asp:TextBox ID="txtMethodName" runat="server" CssClass="standardField" ClientIDMode="Static"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtMethodName" ValidationGroup="paymentmethod" ErrorMessage="Required" InitialValue="" CssClass="errorMsg"></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btnaddmethod" runat="server" Text="Save Payment Method" CssClass="standardFormButton" ValidationGroup="paymentmethod" OnClick="btnaddmethod_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>

    </form>
</body>
</html>
