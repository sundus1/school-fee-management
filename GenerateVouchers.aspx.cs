﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GenerateVouchers : AvaimaThirdpartyTool.AvaimaWebPage
{
    string Totalcharges = "";
    string Totaldiscount = "";
    studentinvoice objstudent;
    studentparameter objinvoice;
    AvaimaStorageUploader objuploader;
    decimal totalfees = 0;
    decimal totaldue = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtinstanceid.Text = this.InstanceID;
            Totalstudent();
        }
    }

    public void Totalstudent()
    {
        DataTable dtdata = new DataTable();
        DataTable dtdata1 = new DataTable();
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            string query = "getstudents12";
            using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
            {
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@instanceid", txtinstanceid.Text);
                da.Fill(dtdata);
            }

            if (dtdata.Rows.Count > 0)
            {
                lbltotalstudent.Text = "Total Student : " + dtdata.Rows.Count.ToString();
            }

            string aq = @"SELECT * FROM schema_8ee08d08_68ab_4415_8b50_9fa722151856.Studentinvoice
WHERE studentid IN (
SELECT studendid FROM schema_8ee08d08_68ab_4415_8b50_9fa722151856.tbl_feesManagement
WHERE isstudent=1 AND instanceid='" + txtinstanceid.Text + "') AND fromdate='2017-04-01'";
            using (SqlDataAdapter da = new SqlDataAdapter(aq, conn))
            {
                da.SelectCommand.CommandType = CommandType.Text;
                da.Fill(dtdata1);
            }
            if (dtdata1.Rows.Count > 0)
            {
                lblMarchVouchers.Text = "April Voucher : " + dtdata1.Rows.Count.ToString();
            }

        }
       




    }
    protected void btnst_Click(object sender, EventArgs e)
    {
        Totalstudent();
    }
    protected void btngent_Click(object sender, EventArgs e)
    {
        AutoGenerateInvoice();
        Label1.Visible = true;
    }

    public void AutoGenerateInvoice()
    {
        //if (DateTime.UtcNow.AddHours(5).ToString("h tt") == "10 AM")
        //{
        DataTable dtdata = new DataTable();
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            string query = "getstudents12";
            using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
            {
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@instanceid", txtinstanceid.Text);
                da.Fill(dtdata);
            }
        }
        if (dtdata.Rows.Count > 0)
        {
            for (int a = 0; a < dtdata.Rows.Count; a++)
            {
                calculatefees(dtdata.Rows[a]["StudendId"].ToString(), dtdata.Rows[a]["PID"].ToString(), dtdata.Rows[a]["parentEmail"].ToString(), dtdata.Rows[a]["instanceid"].ToString(), dtdata.Rows[a]["schoolname"].ToString());
            }
        }
        //}
    }
    public void calculatefees(string studentid, string pid, string parentemail, string instanceid, string schoolname)
    {
        DataSet ds = new DataSet();

        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            string query = "getdatesbyids";
            using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
            {
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@StudentID", studentid);
                da.SelectCommand.Parameters.AddWithValue("@Pid", pid);
                da.Fill(ds);
            }
        }
        decimal totalactualfees = 0; // will hold data after calculation
        int totalmonth = 0; // will hold data after calculation
        int geninvoiceday = 0;
        if (ds.Tables[7].Rows.Count > 0)
        {
            geninvoiceday = Convert.ToInt32(ds.Tables[7].Rows[0]["generatedate"].ToString());
        }
        decimal netfee = Convert.ToDecimal(ds.Tables[2].Rows[0]["Totalamount"].ToString());
        decimal latefeecharge = Convert.ToDecimal(ds.Tables[2].Rows[0]["LateFeeCharges"].ToString()); // pick up from db
        decimal latefee_expirydate = 0; // pick up from db
        string[] feeduedatearray = ds.Tables[0].Rows[0]["Fee_DueDate"].ToString().Split('#');
        int feeduedate = Convert.ToInt32(feeduedatearray[0]);
        string cyclestartdate = ds.Tables[2].Rows[0]["cyclestartday"].ToString();
        DateTime todaydate = DateTime.Now;
        DateTime Packagestartdate = Convert.ToDateTime(ds.Tables[2].Rows[0]["startday"]);
        DateTime lastsubmitdate;
        if (ds.Tables[4].Rows.Count > 0)
        {
            lastsubmitdate = Convert.ToDateTime(ds.Tables[4].Rows[0]["todate"]);
            lastsubmitdate = lastsubmitdate.AddDays(1);
        }
        else
        {
            lastsubmitdate = Convert.ToDateTime(ds.Tables[1].Rows[0]["SubmitDate"]);
        }
        //DateTime packagedate = Convert.ToDateTime(ds.Tables[1].Rows[0]["packagedate"]);
        int month = Convert.ToInt32(todaydate.ToString("MM"));
        int year = Convert.ToInt32(todaydate.ToString("yyyy"));
        int date = Convert.ToInt32(todaydate.ToString("dd"));

        int w = 1;
        int days = 0;
        string[] cycle = ds.Tables[2].Rows[0]["paymentby"].ToString().Split('#');
        if (cycle[1] == "Months")
        {
            DateTime datemonth = lastsubmitdate.AddDays(-geninvoiceday);
            w = ((todaydate.Year - lastsubmitdate.Year) * 12) + (todaydate.Month) - datemonth.Month;
            days = 30;
            if (w == 0 && todaydate > datemonth)
            {
                w = w + 1;
            }
            w = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(w / Convert.ToInt32(cycle[0]))));
            w = 1;
        }
        else if (cycle[1] == "Weeks")
        {
            w = NumberOfWeeks(lastsubmitdate, todaydate);
            days = 7;
        }
        else if (cycle[1] == "Years")
        {
            TimeSpan span = todaydate.Subtract(lastsubmitdate);
            if (span.Days <= 365)
                w = 1;
            else
            {
                decimal dds = Math.Ceiling(Convert.ToDecimal(span.Days) / 365);
                w = Convert.ToInt32(dds);
            }
            days = 365;
        }
        else if (cycle[1] == "Day")
        {
            TimeSpan span = todaydate.Subtract(lastsubmitdate);
            w = span.Days;
            days = 1;
        }

        DataTable prepackage = new DataTable();

        DateTime dtfdate = lastsubmitdate;
        objinvoice = new studentparameter();
        objinvoice.studentid = studentid;
        bool bit = false;
        bool chw = true;
        StringBuilder msg = new StringBuilder();
        for (int invoice = 0; invoice < w; invoice++)
        {
            string mon = "";
            netfee = Convert.ToDecimal(ds.Tables[2].Rows[0]["Totalamount"].ToString());
            objinvoice.TotalPackagefees = netfee;
            objinvoice.Totalfees = netfee;
            if (cycle[1] == "Months")
            {
                if (Convert.ToInt32(cyclestartdate) == dtfdate.Day || bit)
                {
                    objinvoice.Monthrange = dtfdate.ToString("MMM dd, yyyy") + " to " + dtfdate.AddMonths(Convert.ToInt32(cycle[0])).AddDays(-1).ToString("MMM dd, yyyy");
                    objinvoice.fromdate = dtfdate;
                    objinvoice.todate = dtfdate.AddMonths(Convert.ToInt32(cycle[0])).AddDays(-1);
                    dtfdate = dtfdate.AddMonths(Convert.ToInt32(cycle[0]));
                    chw = true;
                }
                else if (!bit)
                {
                    while (dtfdate > Packagestartdate)
                    {
                        Packagestartdate = Packagestartdate.AddMonths(Convert.ToInt32(cycle[0]));
                    }
                    objinvoice.Monthrange = dtfdate.ToString("MMM dd, yyyy") + " to " + Packagestartdate.AddDays(-1).ToString("MMM dd, yyyy");
                    objinvoice.fromdate = dtfdate;
                    objinvoice.todate = Packagestartdate.AddDays(-1);
                    TimeSpan da = Packagestartdate.AddDays(-1) - dtfdate;
                    dtfdate = Packagestartdate;
                    bit = true;
                    chw = false;
                    objinvoice.Totalfees = Math.Ceiling((((Convert.ToDecimal(netfee.ToString()) / Convert.ToDecimal(cycle[0])) * 12) / 365) * da.Days);
                    objinvoice.TotalPackagefees = objinvoice.Totalfees;
                }
                //if (dtfdate > DateTime.Now)
                //{
                //    break;
                //}
                msg = new StringBuilder();
                decimal h = 0;
                decimal beforeamount = 0;
                decimal afteramount = 0;
                TimeSpan beforedays = new TimeSpan();
                TimeSpan afterdays = new TimeSpan();
                objstudent = new studentinvoice();
                decimal Finalamount = 0;
                prepackage = objstudent.Getpreviouspackages(objinvoice.fromdate.ToString(), objinvoice.todate.ToString(), ds.Tables[0].Rows[0]["ID"].ToString());
                if (prepackage.Rows.Count > 0)
                {
                    DateTime def = objinvoice.fromdate;
                    DateTime dee = objinvoice.todate;
                    for (int y1 = 0; y1 < prepackage.Rows.Count; y1++)
                    {
                        beforeamount = 0;
                        afteramount = 0;
                        DateTime SD = Convert.ToDateTime(prepackage.Rows[y1]["startday"].ToString());
                        DateTime ED = Convert.ToDateTime(prepackage.Rows[y1]["enddate"].ToString());

                        if (SD.Date < def.Date && ED.Date > dee.Date)
                        {
                            Finalamount += Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString());
                            msg.Append("Previous Package Fees<br />");
                            if (!chw)
                            {
                                TimeSpan da = dee - def;
                                Finalamount = Math.Ceiling((((Finalamount / Convert.ToDecimal(cycle[0])) * 12) / 365) * da.Days);
                                chw = true;
                            }
                            break;
                        }

                        if (SD.Date == def.Date && ED.Date == dee.Date)
                        {
                            Finalamount += Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString());
                            msg.Append("Previous Package Fees<br />");
                            if (!chw)
                            {
                                TimeSpan da = dee - def;
                                Finalamount = Math.Ceiling((((Finalamount / Convert.ToDecimal(cycle[0])) * 12) / 365) * da.Days);
                                chw = true;
                            }
                            break;
                        }
                        else if (def.Date <= SD.Date && dee.Date >= ED.Date)
                        {
                            /*time beach ma*/
                            if (Finalamount == 0)
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                                beforedays = SD.Subtract(def);
                                beforeamount = (h / new DateTime(def.Year, def.Month, 1).AddMonths(1).AddDays(-1).Day) * beforedays.Days;
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            }
                            if (prepackage.Rows.Count == (y1 + 1))
                            {
                                h = netfee / Convert.ToDecimal(cycle[0]);
                            }
                            else if (prepackage.Rows.Count > (y1 + 1))
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1 + 1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            }
                            afterdays = dee.Subtract(ED);
                            afteramount = (h / new DateTime(dee.Year, dee.Month, 1).AddMonths(1).AddDays(-1).Day) * afterdays.Days;
                            Finalamount += Math.Round(beforeamount) + Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) + Math.Round(afteramount);


                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + ED.ToString("MMM dd, yyyy") + ": " + prepackage.Rows[y1]["Totalamount"].ToString() + "<br />");
                            msg.Append("Fees from " + ED.AddDays(+1).ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (SD.Date > def.Date && SD.Date <= dee.Date && ED.Date >= dee.Date)
                        {
                            /*startday beach ma*/
                            if (prepackage.Rows.Count > 1 && y1 > 1)
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1 - 1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                                beforedays = SD.Subtract(def);
                                beforeamount = (h / new DateTime(def.Year, def.Month, 1).AddMonths(1).AddDays(-1).Day) * beforedays.Days;
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            }
                            h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            afterdays = dee.Subtract(SD);
                            afteramount = (h / new DateTime(dee.Year, dee.Month, 1).AddMonths(1).AddDays(-1).Day) * afterdays.Days;
                            Finalamount += Math.Round(beforeamount) + Math.Round(afteramount);


                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (ED.Date > def.Date && ED.Date <= dee.Date && SD.Date <= def.Date)
                        {
                            /*Enddate beach ma*/
                            h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            beforedays = ED.Subtract(def);
                            beforeamount = (h / new DateTime(def.Year, def.Month, 1).AddMonths(1).AddDays(-1).Day) * beforedays.Days;
                            if (prepackage.Rows.Count == 1 || prepackage.Rows.Count == (y1 + 1))
                            {
                                h = netfee / Convert.ToDecimal(cycle[0]);
                                afterdays = dee.Subtract(ED);
                                afteramount = (h / new DateTime(dee.Year, dee.Month, 1).AddMonths(1).AddDays(-1).Day) * afterdays.Days;
                                Finalamount += Math.Round(beforeamount) + Math.Round(afteramount);

                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                                msg.Append("Fees from " + ED.ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                            }
                            else
                            {
                                Finalamount += Math.Round(beforeamount);
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                                def = ED;
                                continue;
                            }

                        }
                    }
                    netfee = Finalamount;
                    objinvoice.Totalfees = Finalamount;
                    objinvoice.TotalPackagefees = objinvoice.Totalfees;
                }



                if (ds.Tables[5].Rows.Count > 0)
                {
                    for (int y = 0; y < ds.Tables[5].Rows.Count; y++)
                    {
                        DateTime SD = Convert.ToDateTime(ds.Tables[5].Rows[y]["startdate"].ToString());
                        SD = new DateTime(objinvoice.fromdate.Year, SD.Month, SD.Day);
                        DateTime ED = Convert.ToDateTime(ds.Tables[5].Rows[y]["enddate"].ToString());
                        ED = new DateTime(objinvoice.todate.Year, ED.Month, ED.Day);
                        if (SD.Date == objinvoice.fromdate.Date && ED.Date == objinvoice.todate.Date)
                        {
                            objinvoice.Totalfees = Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString());
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees of Holiday Packages");
                        }
                        else if (objinvoice.fromdate.Date <= SD.Date && objinvoice.todate.Date >= ED.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = SD.Subtract(objinvoice.fromdate);
                            beforeamount = (h / new DateTime(objinvoice.fromdate.Year, objinvoice.fromdate.Month, 1).AddMonths(1).AddDays(-1).Day) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(ED);
                            afteramount = (h / new DateTime(objinvoice.todate.Year, objinvoice.todate.Month, 1).AddMonths(1).AddDays(-1).Day) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + ED.ToString("MMM dd, yyyy") + ": " + ds.Tables[5].Rows[y]["fees"].ToString() + "<br />");
                            msg.Append("Fees from " + ED.AddDays(+1).ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (SD.Date > objinvoice.fromdate.Date && SD.Date <= objinvoice.todate.Date && ED.Date >= objinvoice.todate.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = SD.Subtract(objinvoice.fromdate);
                            beforeamount = (h / new DateTime(objinvoice.fromdate.Year, objinvoice.fromdate.Month, 1).AddMonths(1).AddDays(-1).Day) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(SD);
                            afteramount = (Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) / new DateTime(objinvoice.todate.Year, objinvoice.todate.Month, 1).AddMonths(1).AddDays(-1).Day) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (ED.Date > objinvoice.fromdate.Date && ED.Date <= objinvoice.todate.Date && SD.Date <= objinvoice.fromdate.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = ED.Subtract(objinvoice.fromdate);
                            beforeamount = (Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) / new DateTime(objinvoice.fromdate.Year, objinvoice.fromdate.Month, 1).AddMonths(1).AddDays(-1).Day) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(ED);
                            afteramount = (h / new DateTime(objinvoice.todate.Year, objinvoice.todate.Month, 1).AddMonths(1).AddDays(-1).Day) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + ED.ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }

                    }

                }
            }
            else if (cycle[1] == "Weeks")
            {
                objinvoice.Monthrange = dtfdate.ToString("MMM dd, yyyy") + " to " + dtfdate.AddDays(Convert.ToInt32(cycle[0]) * 7).ToString("MMM dd, yyyy");
                objinvoice.fromdate = dtfdate;
                objinvoice.todate = dtfdate.AddDays(Convert.ToInt32(cycle[0]) * 7);
                dtfdate = dtfdate.AddDays(Convert.ToInt32(cycle[0]) * 7);
                if (dtfdate > DateTime.Now)
                {
                    break;
                }
                msg = new StringBuilder();
                decimal h = 0;
                decimal beforeamount = 0;
                decimal afteramount = 0;
                TimeSpan beforedays = new TimeSpan();
                TimeSpan afterdays = new TimeSpan();
                decimal Finalamount = 0;
                objstudent = new studentinvoice();
                prepackage = objstudent.Getpreviouspackages(objinvoice.fromdate.ToString(), objinvoice.todate.ToString(), ds.Tables[0].Rows[0]["ID"].ToString());
                if (prepackage.Rows.Count > 0)
                {
                    DateTime def = objinvoice.fromdate;
                    DateTime dee = objinvoice.todate;
                    for (int y1 = 0; y1 < prepackage.Rows.Count; y1++)
                    {
                        beforeamount = 0;
                        afteramount = 0;
                        DateTime SD = Convert.ToDateTime(prepackage.Rows[y1]["startday"].ToString());
                        DateTime ED = Convert.ToDateTime(prepackage.Rows[y1]["enddate"].ToString());
                        if (SD.Date < def.Date && ED.Date > dee.Date)
                        {
                            Finalamount += Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString());
                            msg.Append("Previous Package Fees<br />");
                            break;
                        }
                        if (SD.Date == def.Date && ED.Date == dee.Date)
                        {
                            Finalamount += Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString());
                            msg.Append("Previous Package Fees<br />");
                            break;
                        }
                        else if (def.Date <= SD.Date && dee.Date >= ED.Date)
                        {
                            /*time beach ma*/
                            if (Finalamount == 0)
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                                beforedays = SD.Subtract(def);
                                beforeamount = (h / 7) * beforedays.Days;
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            }
                            if (prepackage.Rows.Count == (y1 + 1))
                            {
                                h = netfee / Convert.ToDecimal(cycle[0]);
                            }
                            else if (prepackage.Rows.Count > (y1 + 1))
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1 + 1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            }
                            afterdays = dee.Subtract(ED);
                            afteramount = (h / 7) * afterdays.Days;
                            Finalamount += Math.Round(beforeamount) + Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) + Math.Round(afteramount);


                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + ED.ToString("MMM dd, yyyy") + ": " + prepackage.Rows[y1]["Totalamount"].ToString() + "<br />");
                            msg.Append("Fees from " + ED.AddDays(+1).ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (SD.Date > def.Date && SD.Date <= dee.Date && ED.Date >= dee.Date)
                        {
                            /*startday beach ma*/
                            if (prepackage.Rows.Count > 1 && y1 > 1)
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1 - 1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                                beforedays = SD.Subtract(def);
                                beforeamount = (h / 7) * beforedays.Days;
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            }
                            h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            afterdays = dee.Subtract(SD);
                            afteramount = (h / 7) * afterdays.Days;
                            Finalamount += Math.Round(beforeamount) + Math.Round(afteramount);


                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (ED.Date > def.Date && ED.Date <= dee.Date && SD.Date <= def.Date)
                        {
                            /*Enddate beach ma*/
                            h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            beforedays = ED.Subtract(def);
                            beforeamount = (h / ED.Subtract(SD).Days) * beforedays.Days;
                            if (prepackage.Rows.Count == 1 || prepackage.Rows.Count == (y1 + 1))
                            {
                                h = netfee / Convert.ToDecimal(cycle[0]);
                                afterdays = dee.Subtract(ED);
                                afteramount = (h / 7) * afterdays.Days;
                                Finalamount += Math.Round(beforeamount) + Math.Round(afteramount);

                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                                msg.Append("Fees from " + ED.ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                            }
                            else
                            {
                                Finalamount += Math.Round(beforeamount);
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                                def = ED;
                                continue;
                            }

                        }
                    }
                    netfee = Finalamount;
                    objinvoice.Totalfees = Finalamount;
                    objinvoice.TotalPackagefees = objinvoice.Totalfees;
                }


                if (ds.Tables[5].Rows.Count > 0)
                {
                    for (int y = 0; y < ds.Tables[5].Rows.Count; y++)
                    {
                        DateTime SD = Convert.ToDateTime(ds.Tables[5].Rows[y]["startdate"].ToString());
                        SD = new DateTime(objinvoice.fromdate.Year, SD.Month, SD.Day);
                        DateTime ED = Convert.ToDateTime(ds.Tables[5].Rows[y]["enddate"].ToString());
                        ED = new DateTime(objinvoice.todate.Year, ED.Month, ED.Day);
                        if (SD.Date == objinvoice.fromdate.Date && ED.Date == objinvoice.todate.Date)
                        {
                            objinvoice.Totalfees = Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString());
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees of Holiday Packages");
                        }
                        else if (objinvoice.fromdate.Date <= SD.Date && objinvoice.todate.Date >= ED.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = SD.Subtract(objinvoice.fromdate);
                            beforeamount = (h / 7) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(ED);
                            afteramount = (h / 7) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + ED.ToString("MMM dd, yyyy") + ": " + ds.Tables[5].Rows[y]["fees"].ToString() + "<br />");
                            msg.Append("Fees from " + ED.AddDays(+1).ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (SD.Date > objinvoice.fromdate.Date && SD.Date <= objinvoice.todate.Date && ED.Date >= objinvoice.todate.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = SD.Subtract(objinvoice.fromdate);
                            beforeamount = (h / 7) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(SD);
                            afteramount = (Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) / ED.Subtract(SD).Days) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (ED.Date > objinvoice.fromdate.Date && ED.Date <= objinvoice.todate.Date && SD.Date <= objinvoice.fromdate.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = ED.Subtract(objinvoice.fromdate);
                            beforeamount = (Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) / ED.Subtract(SD).Days) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(ED);
                            afteramount = (h / 7) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + ED.ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }

                    }

                }
            }
            else if (cycle[1] == "Years")
            {
                if (Convert.ToInt32(cyclestartdate) == dtfdate.Day || bit)
                {
                    objinvoice.Monthrange = dtfdate.ToString("MMM dd, yyyy") + " to " + dtfdate.AddYears(Convert.ToInt32(cycle[0])).AddDays(-1).ToString("MMM dd, yyyy");
                    objinvoice.fromdate = dtfdate;
                    objinvoice.todate = dtfdate.AddYears(Convert.ToInt32(cycle[0])).AddDays(-1);
                    dtfdate = dtfdate.AddYears(Convert.ToInt32(cycle[0]));
                }
                else if (!bit)
                {
                    while (dtfdate > Packagestartdate)
                    {
                        Packagestartdate = Packagestartdate.AddYears(Convert.ToInt32(cycle[0]));
                    }
                    objinvoice.Monthrange = dtfdate.ToString("MMM dd, yyyy") + " to " + Packagestartdate.AddDays(-1).ToString("MMM dd, yyyy");
                    objinvoice.fromdate = dtfdate;
                    objinvoice.todate = Packagestartdate.AddDays(-1);
                    TimeSpan da = Packagestartdate.AddDays(-1) - dtfdate;
                    dtfdate = Packagestartdate;
                    bit = true;
                    objinvoice.Totalfees = Math.Ceiling(((Convert.ToDecimal(netfee.ToString()) / Convert.ToDecimal(cycle[0])) / 365) * da.Days);
                    objinvoice.TotalPackagefees = objinvoice.Totalfees;
                }
                if (dtfdate > DateTime.Now)
                {
                    break;
                }
                msg = new StringBuilder();
                decimal h = 0;
                decimal beforeamount = 0;
                decimal afteramount = 0;
                TimeSpan beforedays = new TimeSpan();
                TimeSpan afterdays = new TimeSpan();
                decimal Finalamount = 0;
                objstudent = new studentinvoice();
                prepackage = objstudent.Getpreviouspackages(objinvoice.fromdate.ToString(), objinvoice.todate.ToString(), ds.Tables[0].Rows[0]["ID"].ToString());
                if (prepackage.Rows.Count > 0)
                {
                    DateTime def = objinvoice.fromdate;
                    DateTime dee = objinvoice.todate;
                    for (int y1 = 0; y1 < prepackage.Rows.Count; y1++)
                    {
                        beforeamount = 0;
                        afteramount = 0;
                        DateTime SD = Convert.ToDateTime(prepackage.Rows[y1]["startday"].ToString());
                        DateTime ED = Convert.ToDateTime(prepackage.Rows[y1]["enddate"].ToString());
                        if (SD.Date < def.Date && ED.Date > dee.Date)
                        {
                            Finalamount += Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString());
                            msg.Append("Previous Package Fees<br />");
                            if (!chw)
                            {
                                TimeSpan da = dee - def;
                                Finalamount = Math.Ceiling((((Finalamount / Convert.ToDecimal(cycle[0])) * 12) / 365) * da.Days);
                                chw = true;
                            }
                            break;
                        }
                        if (SD.Date == def.Date && ED.Date == dee.Date)
                        {
                            Finalamount += Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString());
                            msg.Append("Previous Package Fees<br />");
                            if (!chw)
                            {
                                TimeSpan da = dee - def;
                                Finalamount = Math.Ceiling((((Finalamount / Convert.ToDecimal(cycle[0])) * 12) / 365) * da.Days);
                                chw = true;
                            }
                            break;
                        }
                        else if (def.Date <= SD.Date && dee.Date >= ED.Date)
                        {
                            /*time beach ma*/
                            if (Finalamount == 0)
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                                beforedays = SD.Subtract(def);
                                beforeamount = (h / 365) * beforedays.Days;
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            }
                            if (prepackage.Rows.Count == (y1 + 1))
                            {
                                h = netfee / Convert.ToDecimal(cycle[0]);
                            }
                            else if (prepackage.Rows.Count > (y1 + 1))
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1 + 1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            }
                            afterdays = dee.Subtract(ED);
                            afteramount = (h / 365) * afterdays.Days;
                            Finalamount += Math.Round(beforeamount) + Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) + Math.Round(afteramount);


                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + ED.ToString("MMM dd, yyyy") + ": " + prepackage.Rows[y1]["Totalamount"].ToString() + "<br />");
                            msg.Append("Fees from " + ED.AddDays(+1).ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (SD.Date > def.Date && SD.Date <= dee.Date && ED.Date >= dee.Date)
                        {
                            /*startday beach ma*/
                            if (prepackage.Rows.Count > 1 && y1 > 1)
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1 - 1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                                beforedays = SD.Subtract(def);
                                beforeamount = (h / 365) * beforedays.Days;
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            }
                            h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            afterdays = dee.Subtract(SD);
                            afteramount = (h / ED.Subtract(SD).Days) * afterdays.Days;
                            Finalamount += Math.Round(beforeamount) + Math.Round(afteramount);


                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (ED.Date > def.Date && ED.Date <= dee.Date && SD.Date <= def.Date)
                        {
                            /*Enddate beach ma*/
                            h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            beforedays = ED.Subtract(def);
                            beforeamount = (h / ED.Subtract(SD).Days) * beforedays.Days;
                            if (prepackage.Rows.Count == 1 || prepackage.Rows.Count == (y1 + 1))
                            {
                                h = netfee / Convert.ToDecimal(cycle[0]);
                                afterdays = dee.Subtract(ED);
                                afteramount = (h / 365) * afterdays.Days;
                                Finalamount += Math.Round(beforeamount) + Math.Round(afteramount);

                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                                msg.Append("Fees from " + ED.ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                            }
                            else
                            {
                                Finalamount += Math.Round(beforeamount);
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                                def = ED;
                                continue;
                            }

                        }
                    }
                    netfee = Finalamount;
                    objinvoice.Totalfees = Finalamount;
                    objinvoice.TotalPackagefees = objinvoice.Totalfees;
                }

                if (ds.Tables[5].Rows.Count > 0)
                {
                    for (int y = 0; y < ds.Tables[5].Rows.Count; y++)
                    {
                        DateTime SD = Convert.ToDateTime(ds.Tables[5].Rows[y]["startdate"].ToString());
                        SD = new DateTime(objinvoice.fromdate.Year, SD.Month, SD.Day);
                        DateTime ED = Convert.ToDateTime(ds.Tables[5].Rows[y]["enddate"].ToString());
                        ED = new DateTime(objinvoice.todate.Year, ED.Month, ED.Day);
                        if (SD.Date == objinvoice.fromdate.Date && ED.Date == objinvoice.todate.Date)
                        {
                            objinvoice.Totalfees = Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString());
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees of Holiday Packages");
                        }
                        else if (objinvoice.fromdate.Date <= SD.Date && objinvoice.todate.Date >= ED.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = SD.Subtract(objinvoice.fromdate);
                            beforeamount = (h / 365) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(ED);
                            afteramount = (h / 365) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + ED.ToString("MMM dd, yyyy") + ": " + ds.Tables[5].Rows[y]["fees"].ToString() + "<br />");
                            msg.Append("Fees from " + ED.AddDays(+1).ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (SD.Date > objinvoice.fromdate.Date && SD.Date <= objinvoice.todate.Date && ED.Date >= objinvoice.todate.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = SD.Subtract(objinvoice.fromdate);
                            beforeamount = (h / 365) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(SD);
                            afteramount = (Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) / ED.Subtract(SD).Days) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (ED.Date > objinvoice.fromdate.Date && ED.Date <= objinvoice.todate.Date && SD.Date <= objinvoice.fromdate.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = ED.Subtract(objinvoice.fromdate);
                            beforeamount = (Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) / ED.Subtract(SD).Days) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(ED);
                            afteramount = (h / 365) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + ED.ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }

                    }

                }
            }
            else if (cycle[1] == "Day")
            {
                objinvoice.Monthrange = dtfdate.ToString("MMM dd, yyyy") + " to " + dtfdate.AddDays(Convert.ToInt32(cycle[0]) * 1).ToString("MMM dd, yyyy");
                objinvoice.fromdate = dtfdate;
                objinvoice.todate = dtfdate.AddDays(Convert.ToInt32(cycle[0]) * 1);
                dtfdate = dtfdate.AddDays(Convert.ToInt32(cycle[0]) * 1);

                if (dtfdate > DateTime.Now)
                {
                    break;
                }
            }




            objinvoice.feespaid = 0;

            //objinvoice.Totalfees = objinvoice.Totalfees - Convert.ToDecimal(Totaldiscount);
            //objinvoice.Totalfees = objinvoice.Totalfees + Convert.ToDecimal(Totalcharges);
            if (ds.Tables[0].Rows[0]["Istax"].ToString() == "True")
            {
                if (ds.Tables[0].Rows[0]["Isfixed"].ToString() == "0") // %age     ,Amount
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["Amount"])))
                    {
                        objinvoice.Totalfees = (objinvoice.Totalfees * ((100 + Convert.ToDecimal(ds.Tables[0].Rows[0]["Amount"])) / 100));
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["Amount"])))
                    {
                        objinvoice.Totalfees = objinvoice.Totalfees + Convert.ToDecimal(ds.Tables[0].Rows[0]["Amount"]);
                    }
                }
            }

            objinvoice.feesbalance = objinvoice.Totalfees - objinvoice.feespaid;
            //objinvoice.Totalcharges = Convert.ToDecimal(Totalcharges);
            //objinvoice.Totaldiscount = Convert.ToDecimal(Totaldiscount);
            objinvoice.feescomments = msg.ToString();
            objstudent = new studentinvoice();
            int idd = objstudent.insertinvoice(objinvoice);

            calculatenetfees(studentid, idd.ToString());
            if (ds.Tables[8].Rows.Count > 0)
            {
                for (int dds = 0; dds < ds.Tables[8].Rows.Count; dds++)
                {
                    if (!Convert.ToBoolean(ds.Tables[8].Rows[dds]["isdiscount"].ToString()))
                    {
                        objinvoice.Insertinvoiceajustments(ds.Tables[8].Rows[dds]["DetaiilTitle"].ToString(), ds.Tables[8].Rows[dds]["DetailAmount"].ToString(), idd.ToString(), "2");
                    }
                    else
                    {
                        objinvoice.Insertinvoiceajustments(ds.Tables[8].Rows[dds]["DetaiilTitle"].ToString(), ds.Tables[8].Rows[dds]["DetailAmount"].ToString(), idd.ToString(), "1");
                    }
                }
            }

            if (ds.Tables[6].Rows.Count > 0)
            {
                for (int annual = 0; annual < ds.Tables[6].Rows.Count; annual++)
                {
                    DateTime dtannula = Convert.ToDateTime(ds.Tables[6].Rows[annual]["Month"].ToString() + " " + DateTime.Now.Year.ToString());
                    if (dtannula >= objinvoice.fromdate && dtannula <= objinvoice.todate)
                    {
                        objinvoice.Insertinvoiceajustments(String.IsNullOrEmpty(ds.Tables[6].Rows[annual]["title"].ToString()) ? "Annual Fee" : ds.Tables[6].Rows[annual]["title"].ToString(), ds.Tables[6].Rows[annual]["fee"].ToString(), idd.ToString(), "2");
                    }
                }
            }



            if (!string.IsNullOrEmpty(parentemail))
                SendInvoice(studentid, instanceid, pid, parentemail, schoolname);
            //dtincoice.Rows.Add(new object[] { objinvoice.Monthrange, objinvoice.TotalPackagefees, Totalcharges, Totaldiscount, objinvoice.feespaid.ToString(), objinvoice.feesbalance.ToString(), mon, objinvoice.Totalfees.ToString() });
        }
    }
    private int NumberOfWeeks(DateTime dateFrom, DateTime dateTo)
    {
        TimeSpan Span = dateTo.Subtract(dateFrom);

        if (Span.Days <= 7)
        {
            return 1;
        }

        int weeks = Span.Days / 7;

        return weeks;
    }
    public void calculatenetfees(string Studentid, string invoiceid)
    {
        studentparameter objinvoice = new studentparameter();
        DataSet ds = new DataSet();
        try
        {

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_getallchargediscountdetails", con))
                {
                    if (con.State.ToString() != "Open")
                        con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@StudentID", Studentid);
                    ds = new DataSet();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);

                }
            }
            if (ds.Tables[1].Rows.Count > 0)
            {
                //Totalcharges = ds.Tables[1].Rows[0]["count"].ToString();
                for (int aa = 0; aa < ds.Tables[1].Rows.Count; aa++)
                {
                    objinvoice.Insertinvoiceajustments(ds.Tables[1].Rows[aa]["title"].ToString(), ds.Tables[1].Rows[aa]["count"].ToString(), invoiceid, "2");
                }
            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int aa = 0; aa < ds.Tables[0].Rows.Count; aa++)
                {
                    objinvoice.Insertinvoiceajustments(ds.Tables[0].Rows[aa]["title"].ToString(), ds.Tables[0].Rows[aa]["count"].ToString(), invoiceid, "1");
                }
            }
        }

        catch (Exception ex)
        {

        }

    }

    private void SendInvoice(string Studentid, string InstanceID, string PID, string parentemail, string schoolname)
    {
        DataSet ds = new DataSet();
        AvaimaEmailAPI objemail = new AvaimaEmailAPI();
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            string query = "feesvoucher";
            using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
            {
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@instanceid", InstanceID);
                da.SelectCommand.Parameters.AddWithValue("@studentid", Studentid);
                da.SelectCommand.Parameters.AddWithValue("@PID", PID);
                da.Fill(ds);
            }
        }
        string headerfile = "";
        string Footer = "";
        string lblclass = "";
        string lblissuedate = "";
        string lblregno = "";
        string lblstudentname = "";
        string lblfathername = "";
        string lblslipno = "";
        string lblsession = "";
        string lblperiod = "";
        string lblduedate = "";
        string lbldueamount = "";
        string fees = "0";
        string charges = "0";
        string Discount = "0";
        string ecomment = "";
        string ecommentamount = "0";
        string extradiscount = "";
        string extradiscountamount = "0";
        string adjustmentcomment = "";
        string adjustmentcommentamount = "0";
        decimal balanceval = 0;
        if (ds.Tables[0].Rows.Count > 0)
        {
            objuploader = new AvaimaStorageUploader();
            headerfile = objuploader.Singlefilelink(InstanceID, ds.Tables[0].Rows[0]["headerfile"].ToString());
            Footer = ds.Tables[0].Rows[0]["footer"].ToString();
        }
        if (ds.Tables[2].Rows.Count > 0)
        {
            lblclass = ds.Tables[2].Rows[0]["title"].ToString();
        }
        if (ds.Tables[1].Rows.Count > 0)
        {
            lblissuedate = Convert.ToDateTime(ds.Tables[1].Rows[0]["issuedate"].ToString()).ToString("dd-MMMM-yyyy");
            lblregno = ds.Tables[1].Rows[0]["Regno"].ToString();
            lblstudentname = ds.Tables[1].Rows[0]["studentname"].ToString();
            lblfathername = ds.Tables[1].Rows[0]["ParentName"].ToString();
        }
        if (ds.Tables[3].Rows.Count > 0)
        {
            lblslipno = ds.Tables[3].Rows[0]["recid"].ToString();

            DateTime dtstart = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
            if (ds.Tables[0].Rows.Count > 0 && dtstart.Month >= Convert.ToInt32(ds.Tables[0].Rows[0]["month"].ToString()))
            {
                lblsession = dtstart.Year + "-" + (dtstart.Year + 1);
            }
            else
            {
                lblsession = (dtstart.Year - 1) + "-" + dtstart.Year;
            }

            lblperiod = dtstart.ToString("MMM yyyy");

            fees = ds.Tables[3].Rows[0]["TotalPackagefees"].ToString();
            balanceval = Convert.ToDecimal(ds.Tables[4].Rows[0]["balance"].ToString()) - Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalPackagefees"].ToString());
            totalfees = Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalPackagefees"].ToString());
            if (Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString()) > 0)
            {
                charges = ds.Tables[3].Rows[0]["TotalCharges"].ToString();
                totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString());
            }
            if (Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString()) > 0)
            {
                Discount = ds.Tables[3].Rows[0]["Totaldiscount"].ToString();
                totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString());
            }
            if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString()) > 0)
            {
                ecomment = ds.Tables[3].Rows[0]["chargescomment"].ToString();
                ecommentamount = ds.Tables[3].Rows[0]["Totaldiscount"].ToString();
                totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString());
            }
            if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString()) > 0)
            {
                extradiscount = ds.Tables[3].Rows[0]["discountcomment"].ToString();
                extradiscountamount = ds.Tables[3].Rows[0]["extradiscount"].ToString();
                totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString());
            }
            if (Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString()) > 0)
            {
                adjustmentcomment = ds.Tables[3].Rows[0]["adjustmentcomment"].ToString();
                adjustmentcommentamount = ds.Tables[3].Rows[0]["adjustment"].ToString();
                totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString());
            }
            totalfees = totalfees + balanceval;
        }
        if (ds.Tables[5].Rows.Count > 0)
        {
            DateTime enddate = Convert.ToDateTime(ds.Tables[3].Rows[0]["todate"].ToString());
            string[] duedate = ds.Tables[5].Rows[0]["Fee_DueDate"].ToString().Split('#');
            string[] afterduedate = ds.Tables[5].Rows[0]["LateFeesStartDate"].ToString().Split('#');
            enddate = enddate.AddDays(Convert.ToDouble(duedate[0]));
            enddate = enddate.AddDays(Convert.ToDouble(afterduedate[0]));
            lblduedate = enddate.ToString("dd-MMM-yyyy");

            decimal latefee = 0;
            try
            {
                latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["LateFeeCharges"].ToString());
            }
            catch
            {
                latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["latefees"].ToString());
            }
            latefee = latefee + totalfees;
            lbldueamount = latefee.ToString();
        }


        StringBuilder HTML = new StringBuilder();
        HTML.AppendLine("<div><div><img src=\"" + headerfile + "\" alt=\"img\" /></div>");
        HTML.AppendLine("<div><h2><span>Slip. ID </span><span>" + lblslipno + "</span></h2><h2>Parent COPY</h2>");
        HTML.AppendLine("<div></div><table width=\"100%\" border=\"0\"><tr><td scope=\"col\"><b>Class:</b>" + lblclass + "<br />");
        HTML.AppendLine("<b>Issue Date:</b> " + lblissuedate + "<br /><b>Reg. No.:</b> " + lblregno + "<br />");
        HTML.AppendLine("<b>Session:</b> " + lblsession + "<br /><b>G. R. No.:</b> " + lblregno + "<br /><b>Period:</b> " + lblperiod + "<br />");
        HTML.AppendLine("</td></tr></table><table width=\"100%\" border=\"0\"><tr>");
        HTML.AppendLine("<td colspan=\"2\">Student&acute;s Name: <b>" + lblstudentname + "</b></td></tr>");
        HTML.AppendLine("<tr><td colspan=\"2\">Father&acute;s Name: " + lblfathername + "</td></tr></table></div>");
        HTML.AppendLine("<div><h2><span>Fee Details</span></h2></div>");
        HTML.AppendLine("<table width=\"100%\" border=\"0\" cellspacing=\"0\">");
        HTML.AppendLine("<tr><td>Tuition  Fee " + lblperiod + "</td><td>" + fees + "</td></tr>");

        if (charges != "0")
            HTML.AppendLine("<tr><td>Total Individual Charges</td><td>" + charges + "</td></tr>");
        if (Discount != "0")
            HTML.AppendLine("<tr><td>Total Individual Discount</td><td>" + Discount + "</td></tr>");
        if (ecommentamount != "0")
            HTML.AppendLine("<tr><td>" + ecomment + "</td><td>" + ecommentamount + "</td></tr>");
        if (extradiscountamount != "0")
            HTML.AppendLine("<tr><td>" + extradiscount + "</td><td>" + extradiscountamount + "</td></tr>");
        if (adjustmentcommentamount != "0")
            HTML.AppendLine("<tr><td>" + adjustmentcomment + "</td><td>" + adjustmentcommentamount + "</td></tr>");

        HTML.AppendLine("<tr><td>Previous Balance</td><td>" + balanceval.ToString() + "</td></tr>");
        HTML.AppendLine("<tr><td><b>Total Due</b></td><td><b>" + totalfees.ToString() + "</b></td></tr>");
        HTML.AppendLine("</table>");
        HTML.AppendLine("<table width=\"100%\" border=\"0\">");
        HTML.AppendLine("<tr><td colspan=\"2\">Due Date: <b>" + lblduedate + "</b></td></tr>");
        HTML.AppendLine("<tr><td>Amount payable after due date</td><td>" + lbldueamount + "</td></tr></table>");
        HTML.AppendLine("<br /><br /><br /><table width=\"100%\" border=\"0\"");
        HTML.AppendLine("<tr><td>Cashier</td><td>Officer</td></tr></table>");
        HTML.AppendLine("<div style=\"font-size: 11px; color: #b5b5b5; font-weight: lighter; padding: 11px 5px 0px 5px;\">" + Footer + "</div></div>");

        if (!string.IsNullOrEmpty(parentemail))
            objemail.send_email(parentemail, "Fee Management(" + schoolname + ")", "", HTML.ToString(), "Fee Invoice");
    }
}