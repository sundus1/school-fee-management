﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="feevouchertemplate.ascx.cs" Inherits="feevouchertemplate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head >
    <title></title>
     <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
</head>
<body>
    
        <div style="color:green;" runat="server" id="Divmsg">

        </div>
        <div id="mainContentDiv">
            <div class="formDiv">
                <table>
                    <tr>
                        <td colspan="2">
                            <div class="formTitleDiv">Multiple Fee Vouchers</div>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Template 1</td>
                        <td class="formFieldTd">
                            <asp:RadioButton ID="rbtntemp" runat="server" GroupName="template" Checked="true" />
                            <a href="images/2.png" target="_blank">View Template 1</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Template 2</td>
                        <td class="formFieldTd">
                            <asp:RadioButton ID="rbtntemp1" runat="server" GroupName="template" />
                            <a href="images/1.png" target="_blank">View Template 2</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Template 3</td>
                        <td class="formFieldTd">
                            <asp:RadioButton ID="rbtntemp2" runat="server" GroupName="template" />
                            <a href="images/3.png" target="_blank">View Template 3</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">&nbsp;</td>
                        <td class="formFieldTd">
                            <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="standardFormButton" OnClick="btnsave_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
</body>
</html>