﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="feevouchertemp1.aspx.cs" Inherits="feevouchertemp1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   <link href="_assets/jquery-ui.css" rel="stylesheet" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <script type="text/javascript">
        $(window).load(function () {
            resizeiframepage();
            setTimeout(function () { resizeiframepage(); }, 300);
        });
        function resizeiframepage() {
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
            if (innerDoc.body.offsetHeight) {
                iframe.height = innerDoc.body.offsetHeight + 700 + "px";
            }
            else if (innerDoc && innerDoc.body.scrollHeight) {
                iframe.style.height = innerDoc.body.scrollHeight + "px";
            }
        }
        function printout() {
            //$('#imgpinter').attr("src", "");
            //$('#ankedit').text('');
            window.print();
            //$('#imgpinter').attr("src", "images/1410871087_Print_32x32.png");
            //$('#ankedit').text('Edit');
        }
        $(document).ready(function () {
            $(document).on('click', '.ankdiscount', function () {

                var dlg = $("#divDiscount").dialog({
                    width: 416,
                    show: {

                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });

            $(document).on('click', '.ankcharges', function () {

                var dlg = $("#div2").dialog({
                    width: 416,
                    show: {

                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });

            $(document).on('click', '.ankadjustment', function () {

                var dlg = $("#div3").dialog({
                    width: 416,
                    show: {

                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });

            $(document).on('click', '#btnpayment', function () {
                var txtval = $('#txtpayment').val();
                if (txtval == "") {
                    $('#paymenterrmsg').text('Required Amount')
                    return false;
                }
                else {
                    var filter = /(?:^\d{1,3}(?:\.?\d{3})*(?:,\d{2})?$)|(?:^\d{1,3}(?:,?\d{3})*(?:\.\d{2})?$)/;
                    if (filter.test(txtval)) {
                        $('#paymenterrmsg').text('');
                        var dlg = $("#div1").dialog({
                            width: 416,
                            show: {

                            },
                            modal: true,
                            hide: {
                                effect: "blind",
                            }
                        });
                        dlg.parent().appendTo($("form:first"));
                        dlg.parent().css("z-index", "1000");
                        return false;
                    }
                    else {
                        $('#paymenterrmsg').text('Invalid Amount')
                        return false;
                    }
                }
            });

            $(document).on('click', '#addPaymentMethods', function () {

                var dlg = $("#additionalPaymentMethods").dialog({
                    width: 416,
                    show: {

                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;


            });

            $("#txtpaiddate").datepicker({
                dateFormat: 'dd MM yy',
                changeMonth: true,
                changeYear: true,
            });
        });
    </script>
    <style>
        @media print
        {
            .tag
            {
                background-color: #ccc;
            }
            .printdiv
            {
                display:none;
            }
        }
        .Tabletr
        {
            height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;
        }
          .errorMsg {
        color:red;
        }
        .lnkTxt {
        color:blue !important;
        cursor:pointer !important;
        }
    </style>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #000;">
    <form id="form1" runat="server">
        
        <div style="width: 285px; border: 1px solid #000; float: left; margin-right: 7px">
            <div style="border-bottom: 1px solid #000; height: 63px; width: 100%; position: relative">
                <div style="position: absolute; right: 5px; top: 3px;">BANK COPY</div>
                <div style="font-size: 22px; margin: 13px 0 0 0; text-align: center; display: inline-block; width: 100%; line-height: 40px;">
                    <img src="images/header.jpg" alt="img" id="img_header" runat="server" />
                </div>
            </div>
            <div style="border-bottom: 1px solid #000; width: 275px; padding: 3px 5px;">
                <div style="width: 87px; height: 12px; float: left; margin: 4px 0;">GR. No<span style="float: right; text-align: right; line-height: 12px; font-weight: bold;">:</span></div>
                <div style="width: 86px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;">
                    <asp:Label ID="lblgrno" runat="server"></asp:Label>
                </div>
                <div style="float: right; height: 18px; width: 90px; text-align: center; line-height: 18px; border: 1px solid #000; background: #ccc;">Orignal Voucher</div>
                <div style="clear: both;"></div>
                <div style="width: 87px; height: 12px; float: left; margin: 4px 0;">Father Name<span style="float: right; text-align: right; line-height: 12px; font-weight: bold;">:</span></div>
                <div style="width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;">
                    <asp:Label ID="lblfathername" runat="server"></asp:Label>
                </div>
                <div style="clear: both;"></div>
                <div style="width: 87px; height: 12px; float: left; margin: 4px 0;">Student Name<span style="float: right; text-align: right; line-height: 12px; font-weight: bold;">:</span></div>
                <div style="width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;">
                    <asp:Label ID="lblstudentname" runat="server"></asp:Label>
                </div>
                <div style="clear: both;"></div>
                <div style="width: 87px; height: 12px; float: left; margin: 4px 0;">Class<span style="float: right; text-align: right; line-height: 12px; font-weight: bold;">:</span></div>
                <div style="width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;">
                    <asp:Label ID="lblclass" runat="server"></asp:Label>
                </div>
                <div style="clear: both;"></div>
                <div style="width: 87px; height: 12px; float: left; margin: 4px 0;">Class Fee<span style="float: right; text-align: right; line-height: 12px; font-weight: bold;">:</span></div>
                <div style="width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;">
                    <asp:Label ID="lblclassfee" runat="server"></asp:Label>
                </div>
                <div style="clear: both;"></div>
                <h2 style="text-align: center; padding: 0px; margin: 4px 0 0 0;">Fee Voucher 
                    <asp:Label ID="lblperiod" runat="server"></asp:Label></h2>
            </div>
            <div style="border-bottom: 1px solid #000; width: 100%;">
                <table width="50%" border="0" cellpadding="0" cellspacing="0" style="float:left;">
                    <tr>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Month</th>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Vch. #</th>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Amount</th>
                    </tr>

                    <asp:Literal ID="lt_history" runat="server"></asp:Literal>

                </table>
                <table width="50%" border="0" cellpadding="0" cellspacing="0" style="float:right;">
                    <tr>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Month</th>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Vch. #</th>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Amount</th>
                    </tr>

                    <asp:Literal ID="Literal1" runat="server"></asp:Literal>

                </table>
                <div style="clear:both"></div>
                <asp:Table ID="tbl" runat="server" Width="100%" border="0" CellSpacing="0" Style="padding: 5px; margin: 5px 0 0 0">
                </asp:Table>
                <table width="100%" border="0" class="validtotal">
                    <tr>
                        <td colspan="2">Due Date: <b>
                            <asp:Label ID="lblduedate" runat="server"></asp:Label>
                        </b></td>
                    </tr>
                    <tr>
                        <td>Amount payable after due date</td>
                        <td>
                            <asp:Label ID="lbldueamount" runat="server"></asp:Label></td>
                    </tr>
                </table>
                <br />
                <br />
                <br />
                <p style="top: 43px; left: 17px; text-align: center;"><b>Receiver Signature &amp; Stamp</b></p>
            </div>
            <div id="Footer1" runat="server">
            </div>
        </div>
        <div style="width: 285px; border: 1px solid #000; float: left; margin-right: 7px">
            <div style="border-bottom: 1px solid #000; height: 63px; width: 100%; position: relative">
                <div style="position: absolute; right: 5px; top: 3px;">SCHOOL COPY</div>
                <div style="font-size: 22px; margin: 13px 0 0 0; text-align: center; display: inline-block; width: 100%; line-height: 40px;">
                    <img src="images/header.jpg" alt="img" id="img_header1" runat="server" />
                </div>
            </div>
            <div style="border-bottom: 1px solid #000; width: 275px; padding: 3px 5px;">
                <div style="width: 87px; height: 12px; float: left; margin: 4px 0;">GR. No<span style="float: right; text-align: right; line-height: 12px; font-weight: bold;">:</span></div>
                <div style="width: 86px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;">
                    <asp:Label ID="lblgrno1" runat="server"></asp:Label>
                </div>
                <div style="float: right; height: 18px; width: 90px; text-align: center; line-height: 18px; border: 1px solid #000; background: #ccc;">Orignal Voucher</div>
                <div style="clear: both;"></div>
                <div style="width: 87px; height: 12px; float: left; margin: 4px 0;">Father Name<span style="float: right; text-align: right; line-height: 12px; font-weight: bold;">:</span></div>
                <div style="width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;">
                    <asp:Label ID="lblfathername1" runat="server"></asp:Label>
                </div>
                <div style="clear: both;"></div>
                <div style="width: 87px; height: 12px; float: left; margin: 4px 0;">Student Name<span style="float: right; text-align: right; line-height: 12px; font-weight: bold;">:</span></div>
                <div style="width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;">
                    <asp:Label ID="lblstudentname1" runat="server"></asp:Label>
                </div>
                <div style="clear: both;"></div>
                <div style="width: 87px; height: 12px; float: left; margin: 4px 0;">Class<span style="float: right; text-align: right; line-height: 12px; font-weight: bold;">:</span></div>
                <div style="width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;">
                    <asp:Label ID="lblclass1" runat="server"></asp:Label>
                </div>
                <div style="clear: both;"></div>
                <div style="width: 87px; height: 12px; float: left; margin: 4px 0;">Class Fee<span style="float: right; text-align: right; line-height: 12px; font-weight: bold;">:</span></div>
                <div style="width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;">
                    <asp:Label ID="lblclassfee1" runat="server"></asp:Label>
                </div>
                <div style="clear: both;"></div>
                <h2 style="text-align: center; padding: 0px; margin: 4px 0 0 0;">Fee Voucher 
                    <asp:Label ID="lblperiod1" runat="server"></asp:Label></h2>
            </div>
            <div style="border-bottom: 1px solid #000; width: 100%;">
                <table width="50%" border="0" cellpadding="0" cellspacing="0" style="float:left;">
                    <tr>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Month</th>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Vch. #</th>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Amount</th>
                    </tr>
                    <asp:Literal ID="lt_history1" runat="server"></asp:Literal>
                </table>
                <table width="50%" border="0" cellpadding="0" cellspacing="0" style="float:right;">
                    <tr>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Month</th>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Vch. #</th>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Amount</th>
                    </tr>

                    <asp:Literal ID="Literal2" runat="server"></asp:Literal>

                </table>
                <div style="clear:both"></div>
                <asp:Table ID="tbl1" runat="server" Width="100%" border="0" CellSpacing="0" Style="padding: 5px; margin: 5px 0 0 0">
                </asp:Table>
                <table width="100%" border="0" class="validtotal">
                    <tr>
                        <td colspan="2">Due Date: <b>
                            <asp:Label ID="lblduedate1" runat="server"></asp:Label>
                        </b></td>
                    </tr>
                    <tr>
                        <td>Amount payable after due date</td>
                        <td>
                            <asp:Label ID="lbldueamount1" runat="server"></asp:Label></td>
                    </tr>
                </table>
                <br />
                <br />
                <br />
                <p style="top: 43px; left: 17px; text-align: center;"><b>Receiver Signature &amp; Stamp</b></p>
            </div>
            <div id="Footer2" runat="server">
            </div>
        </div>
        <div style="width: 285px; border: 1px solid #000; float: left; margin-right: 7px">
            <div style="border-bottom: 1px solid #000; height: 63px; width: 100%; position: relative">
                <div style="position: absolute; right: 5px; top: 3px;">STUDENT'S COPY</div>
                <div style="font-size: 22px; margin: 13px 0 0 0; text-align: center; display: inline-block; width: 100%; line-height: 40px;">
                    <img src="images/header.jpg" alt="img" id="img_header2" runat="server" />
                </div>
            </div>
            <div style="border-bottom: 1px solid #000; width: 275px; padding: 3px 5px;">
                <div style="width: 87px; height: 12px; float: left; margin: 4px 0;">GR. No<span style="float: right; text-align: right; line-height: 12px; font-weight: bold;">:</span></div>
                <div style="width: 86px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;">
                    <asp:Label ID="lblgrno2" runat="server"></asp:Label>
                </div>
                <div style="float: right; height: 18px; width: 90px; text-align: center; line-height: 18px; border: 1px solid #000; background: #ccc;">Orignal Voucher</div>
                <div style="clear: both;"></div>
                <div style="width: 87px; height: 12px; float: left; margin: 4px 0;">Father Name<span style="float: right; text-align: right; line-height: 12px; font-weight: bold;">:</span></div>
                <div style="width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;">
                    <asp:Label ID="lblfathername2" runat="server"></asp:Label>
                </div>
                <div style="clear: both;"></div>
                <div style="width: 87px; height: 12px; float: left; margin: 4px 0;">Student Name<span style="float: right; text-align: right; line-height: 12px; font-weight: bold;">:</span></div>
                <div style="width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;">
                    <asp:Label ID="lblstudentname2" runat="server"></asp:Label>
                </div>
                <div style="clear: both;"></div>
                <div style="width: 87px; height: 12px; float: left; margin: 4px 0;">Class<span style="float: right; text-align: right; line-height: 12px; font-weight: bold;">:</span></div>
                <div style="width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;">
                    <asp:Label ID="lblclass2" runat="server"></asp:Label>
                </div>
                <div style="clear: both;"></div>
                <div style="width: 87px; height: 12px; float: left; margin: 4px 0;">Class Fee<span style="float: right; text-align: right; line-height: 12px; font-weight: bold;">:</span></div>
                <div style="width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;">
                    <asp:Label ID="lblclassfee2" runat="server"></asp:Label>
                </div>
                <div style="clear: both;"></div>
                <h2 style="text-align: center; padding: 0px; margin: 4px 0 0 0;">Fee Voucher 
                    <asp:Label ID="lblperiod2" runat="server"></asp:Label></h2>
            </div>
            <div style="border-bottom: 1px solid #000; width: 100%;">

                <table width="50%" border="0" cellpadding="0" cellspacing="0" style="float:left;">
                    <tr>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Month</th>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Vch. #</th>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Amount</th>
                    </tr>
                    <asp:Literal ID="lt_history2" runat="server"></asp:Literal>
                </table>
                <table width="50%" border="0" cellpadding="0" cellspacing="0"  style="float:right;">
                    <tr>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Month</th>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Vch. #</th>
                        <th style="height: 24px; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;" scope="col">Amount</th>
                    </tr>

                    <asp:Literal ID="Literal3" runat="server"></asp:Literal>

                </table>
                <div style="clear:both"></div>
                <asp:Table ID="tbl2" runat="server" Width="100%" border="0" CellSpacing="0" Style="padding: 5px; margin: 5px 0 0 0">
                </asp:Table>
                <table width="100%" border="0" class="validtotal">
                    <tr>
                        <td colspan="2">Due Date: <b>
                            <asp:Label ID="lblduedate2" runat="server"></asp:Label>
                        </b></td>
                    </tr>
                    <tr>
                        <td>Amount payable after due date</td>
                        <td>
                            <asp:Label ID="lbldueamount2" runat="server"></asp:Label></td>
                    </tr>
                </table>
                <br />
                <br />
                <br />
                <p style="top: 43px; left: 17px; text-align: center;"><b>Receiver Signature &amp; Stamp</b></p>
            </div>
            <div id="Footer3" runat="server">
            </div>
        </div>

        
         <div style="float: right;line-height: 25px;font-size: 14px;font-family: arial; margin-right:100px" class="printdiv">
            <h2>Fee Options</h2>
             <a href="javascript:printout();" id="ankprint">
                <img src="images/1410871087_Print_32x32.png" id="imgpinter" /> Print</a>
            <br />
            <a id="ankedit" href="Invoice.aspx?<%= Request.QueryString.ToString().Replace("Studentid","studid") %>">View monthly details</a><br />
            <a href="#" id="ankdiscount" class="menuitem ankdiscount">Add Discount</a><br />
            <a href="#" id="ankcharges" class="menuitem ankcharges">Add Charges</a><br />
            <a href="#" id="ankadjustment" class="menuitem ankadjustment">Add Adjustment</a>
            <div>
                <h2>Payment Collection</h2>
                <h4>Total Due:
                    <asp:Label ID="lbltotladue" runat="server"></asp:Label></h4>
                 <asp:TextBox ID="txtpayment" runat="server" ClientIDMode="Static" CssClass="standardField" placeholder="Payment Amount"></asp:TextBox>&nbsp;<asp:Button ID="btnpayment" runat="server" Text="Collect Payment" CssClass="standardFormButton" /><br /><asp:Label ID="paymenterrmsg" runat="server" ClientIDMode="Static" ForeColor="Red"></asp:Label>
            </div>
        </div>
        <div id="div1" title="Payment Method" style="display: none;">
            <asp:Panel ID="Panel1" DefaultButton="btnprocessed" runat="server">
                <table>
                    <tr>
                        <td>
                            Paid Date:
                        </td>
                        <td>
                            <asp:TextBox ID="txtpaiddate" runat="server" CssClass="standardField" ClientIDMode="Static"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Paid By:
                        </td>
                        <td>
                            <asp:TextBox ID="txtpaidby" runat="server" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Payment Method:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlpayment" runat="server" AppendDataBoundItems="True">
                            </asp:DropDownList><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="ddlpayment" ForeColor="Red" InitialValue="0" ValidationGroup="payment"></asp:RequiredFieldValidator>
                        </td>
                         <td><asp:HyperLink runat="server" ID="addPaymentMethods" Text="Add Payment Method" CssClass="lnkTxt"></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td>Comments:
                        </td>
                        <td>
                            <asp:TextBox ID="txtpaycomment" runat="server" TextMode="MultiLine" Rows="3" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btnprocessed" runat="server" Text="Pay" CssClass="standardFormButton" ValidationGroup="payment" OnClick="btnprocessed_Click"/>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
        <div id="divDiscount" title="Discount" style="display: none">
            <asp:Panel ID="Panel2" DefaultButton="btndisamount" runat="server">
                <table>
                    <tr>
                        <td>Discount Amount:
                        </td>
                        <td>
                            <asp:TextBox ID="txtdisamount" runat="server" CssClass="standardField"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ValidationGroup="asd" ControlToValidate="txtdisamount" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid" ValidationGroup="asd" ValidationExpression="\d+(\.\d{1,2})?" ControlToValidate="txtdisamount" ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Comments:
                        </td>
                        <td>
                            <asp:TextBox ID="txtdiscountcomment" runat="server" TextMode="MultiLine" Rows="3" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btndisamount" runat="server" Text="Add Amount" ValidationGroup="asd" OnClick="btndisamount_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>

        <div id="div2" title="Charges" style="display: none">
            <asp:Panel ID="Panel3" DefaultButton="btnchargeamount" runat="server">
                <table>
                    <tr>
                        <td>Charges Amount:
                        </td>
                        <td>
                            <asp:TextBox ID="txtchargeamount" runat="server" CssClass="standardField"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ValidationGroup="asd1" ControlToValidate="txtchargeamount" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Invalid" ValidationGroup="asd1" ValidationExpression="\d+(\.\d{1,2})?" ControlToValidate="txtchargeamount" ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>

                    </tr>
                    <tr>
                        <td>Comments:
                        </td>
                        <td>
                            <asp:TextBox ID="txtchargescomment" runat="server" TextMode="MultiLine" Rows="3" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btnchargeamount" runat="server" Text="Add Amount" ValidationGroup="asd1" OnClick="btnchargeamount_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
        <div id="div3" title="Adjustment" style="display: none">
            <asp:Panel ID="Panel4" DefaultButton="btnadjustamount" runat="server">
                <table>
                    <tr>
                        <td>Adjustment Amount:
                        </td>
                        <td>
                            <asp:TextBox ID="txtadjustamount" runat="server" CssClass="standardField"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ValidationGroup="asd2" ControlToValidate="txtadjustamount" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Invalid" ValidationGroup="asd2" ValidationExpression="\d+(\.\d{1,2})?" ControlToValidate="txtadjustamount" ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Comments:
                        </td>
                        <td>
                            <asp:TextBox ID="txtajdustmentcomment" runat="server" TextMode="MultiLine" Rows="3" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Add/Subtract:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlAtype" runat="server">
                                <asp:ListItem Value="0" Selected>Add Amount</asp:ListItem>
                                <asp:ListItem Value="1">Subtract Amount</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btnadjustamount" runat="server" Text="Add Amount" ValidationGroup="asd2" OnClick="btnadjustamount_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
          <div id="additionalPaymentMethods" title="Payment Method" style="display: none">
            <asp:Panel ID="Panel5" DefaultButton="btnaddmethod" runat="server">
                <table>
                    <tr>
                        <td>Title:
                        </td>
                        <td>
                            <asp:TextBox ID="txtMethodName" runat="server" CssClass="standardField" ClientIDMode="Static"></asp:TextBox>
                        </td>
                        <td><asp:RequiredFieldValidator runat="server" ControlToValidate="txtMethodName" ValidationGroup="paymentmethod" ErrorMessage="Required" InitialValue="" CssClass="errorMsg" ></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btnaddmethod" runat="server" Text="Save Payment Method" CssClass="standardFormButton" ValidationGroup="paymentmethod" OnClick="btnaddmethod_Click" />
                        </td>
                    </tr>                    
                </table>
            </asp:Panel>
        </div>

         <asp:HiddenField ID="hfid" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hffeebalance" runat="server" ClientIDMode="Static" Value="0" />
         <asp:HiddenField ID="hffeepaid" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hffees" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hfdiscount" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hfcharge" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hfadjustment" runat="server" ClientIDMode="Static" Value="0" />

    </form>
</body>
</html>
