﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.4984
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

// 
// This source code was auto-generated by wsdl, Version=2.0.50727.3038.
// 


/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Web.Services.WebServiceBindingAttribute(Name="AvaimaStorageUploaderSoap", Namespace="http://tempuri.org/")]
public partial class AvaimaStorageUploader : System.Web.Services.Protocols.SoapHttpClientProtocol {
    
    private System.Threading.SendOrPostCallback fileuploaderOperationCompleted;
    
    private System.Threading.SendOrPostCallback filedeleteOperationCompleted;
    
    private System.Threading.SendOrPostCallback SinglefilelinkOperationCompleted;
    
    private System.Threading.SendOrPostCallback insertfileuploadedOperationCompleted;
    
    private System.Threading.SendOrPostCallback GetuploadedfileinfoOperationCompleted;
    
    private System.Threading.SendOrPostCallback DeleteuploadedfileOperationCompleted;
    
    private System.Threading.SendOrPostCallback InsertLinksOperationCompleted;
    
    private System.Threading.SendOrPostCallback GetlinksOperationCompleted;
    
    private System.Threading.SendOrPostCallback DeletelinksOperationCompleted;
    
    /// <remarks/>
    public AvaimaStorageUploader() {
        this.Url = "http://www.avaima.com/AvaimaStorageUploader.asmx";
    }
    
    /// <remarks/>
    public event fileuploaderCompletedEventHandler fileuploaderCompleted;
    
    /// <remarks/>
    public event filedeleteCompletedEventHandler filedeleteCompleted;
    
    /// <remarks/>
    public event SinglefilelinkCompletedEventHandler SinglefilelinkCompleted;
    
    /// <remarks/>
    public event insertfileuploadedCompletedEventHandler insertfileuploadedCompleted;
    
    /// <remarks/>
    public event GetuploadedfileinfoCompletedEventHandler GetuploadedfileinfoCompleted;
    
    /// <remarks/>
    public event DeleteuploadedfileCompletedEventHandler DeleteuploadedfileCompleted;
    
    /// <remarks/>
    public event InsertLinksCompletedEventHandler InsertLinksCompleted;
    
    /// <remarks/>
    public event GetlinksCompletedEventHandler GetlinksCompleted;
    
    /// <remarks/>
    public event DeletelinksCompletedEventHandler DeletelinksCompleted;
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/fileuploader", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    public void fileuploader(string instanceid, string filename, [System.Xml.Serialization.XmlElementAttribute(DataType="base64Binary")] byte[] streamdata) {
        this.Invoke("fileuploader", new object[] {
                    instanceid,
                    filename,
                    streamdata});
    }
    
    /// <remarks/>
    public System.IAsyncResult Beginfileuploader(string instanceid, string filename, byte[] streamdata, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("fileuploader", new object[] {
                    instanceid,
                    filename,
                    streamdata}, callback, asyncState);
    }
    
    /// <remarks/>
    public void Endfileuploader(System.IAsyncResult asyncResult) {
        this.EndInvoke(asyncResult);
    }
    
    /// <remarks/>
    public void fileuploaderAsync(string instanceid, string filename, byte[] streamdata) {
        this.fileuploaderAsync(instanceid, filename, streamdata, null);
    }
    
    /// <remarks/>
    public void fileuploaderAsync(string instanceid, string filename, byte[] streamdata, object userState) {
        if ((this.fileuploaderOperationCompleted == null)) {
            this.fileuploaderOperationCompleted = new System.Threading.SendOrPostCallback(this.OnfileuploaderOperationCompleted);
        }
        this.InvokeAsync("fileuploader", new object[] {
                    instanceid,
                    filename,
                    streamdata}, this.fileuploaderOperationCompleted, userState);
    }
    
    private void OnfileuploaderOperationCompleted(object arg) {
        if ((this.fileuploaderCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.fileuploaderCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/filedelete", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    public void filedelete(string instanceid, string filename) {
        this.Invoke("filedelete", new object[] {
                    instanceid,
                    filename});
    }
    
    /// <remarks/>
    public System.IAsyncResult Beginfiledelete(string instanceid, string filename, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("filedelete", new object[] {
                    instanceid,
                    filename}, callback, asyncState);
    }
    
    /// <remarks/>
    public void Endfiledelete(System.IAsyncResult asyncResult) {
        this.EndInvoke(asyncResult);
    }
    
    /// <remarks/>
    public void filedeleteAsync(string instanceid, string filename) {
        this.filedeleteAsync(instanceid, filename, null);
    }
    
    /// <remarks/>
    public void filedeleteAsync(string instanceid, string filename, object userState) {
        if ((this.filedeleteOperationCompleted == null)) {
            this.filedeleteOperationCompleted = new System.Threading.SendOrPostCallback(this.OnfiledeleteOperationCompleted);
        }
        this.InvokeAsync("filedelete", new object[] {
                    instanceid,
                    filename}, this.filedeleteOperationCompleted, userState);
    }
    
    private void OnfiledeleteOperationCompleted(object arg) {
        if ((this.filedeleteCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.filedeleteCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Singlefilelink", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    public string Singlefilelink(string instanceid, string filename) {
        object[] results = this.Invoke("Singlefilelink", new object[] {
                    instanceid,
                    filename});
        return ((string)(results[0]));
    }
    
    /// <remarks/>
    public System.IAsyncResult BeginSinglefilelink(string instanceid, string filename, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("Singlefilelink", new object[] {
                    instanceid,
                    filename}, callback, asyncState);
    }
    
    /// <remarks/>
    public string EndSinglefilelink(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((string)(results[0]));
    }
    
    /// <remarks/>
    public void SinglefilelinkAsync(string instanceid, string filename) {
        this.SinglefilelinkAsync(instanceid, filename, null);
    }
    
    /// <remarks/>
    public void SinglefilelinkAsync(string instanceid, string filename, object userState) {
        if ((this.SinglefilelinkOperationCompleted == null)) {
            this.SinglefilelinkOperationCompleted = new System.Threading.SendOrPostCallback(this.OnSinglefilelinkOperationCompleted);
        }
        this.InvokeAsync("Singlefilelink", new object[] {
                    instanceid,
                    filename}, this.SinglefilelinkOperationCompleted, userState);
    }
    
    private void OnSinglefilelinkOperationCompleted(object arg) {
        if ((this.SinglefilelinkCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.SinglefilelinkCompleted(this, new SinglefilelinkCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/insertfileuploaded", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    public void insertfileuploaded(string Filename, string FileExtension, string appid, string userid, string fileuserid, string instanceid, [System.Xml.Serialization.XmlElementAttribute(DataType="base64Binary")] byte[] streamdata) {
        this.Invoke("insertfileuploaded", new object[] {
                    Filename,
                    FileExtension,
                    appid,
                    userid,
                    fileuserid,
                    instanceid,
                    streamdata});
    }
    
    /// <remarks/>
    public System.IAsyncResult Begininsertfileuploaded(string Filename, string FileExtension, string appid, string userid, string fileuserid, string instanceid, byte[] streamdata, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("insertfileuploaded", new object[] {
                    Filename,
                    FileExtension,
                    appid,
                    userid,
                    fileuserid,
                    instanceid,
                    streamdata}, callback, asyncState);
    }
    
    /// <remarks/>
    public void Endinsertfileuploaded(System.IAsyncResult asyncResult) {
        this.EndInvoke(asyncResult);
    }
    
    /// <remarks/>
    public void insertfileuploadedAsync(string Filename, string FileExtension, string appid, string userid, string fileuserid, string instanceid, byte[] streamdata) {
        this.insertfileuploadedAsync(Filename, FileExtension, appid, userid, fileuserid, instanceid, streamdata, null);
    }
    
    /// <remarks/>
    public void insertfileuploadedAsync(string Filename, string FileExtension, string appid, string userid, string fileuserid, string instanceid, byte[] streamdata, object userState) {
        if ((this.insertfileuploadedOperationCompleted == null)) {
            this.insertfileuploadedOperationCompleted = new System.Threading.SendOrPostCallback(this.OninsertfileuploadedOperationCompleted);
        }
        this.InvokeAsync("insertfileuploaded", new object[] {
                    Filename,
                    FileExtension,
                    appid,
                    userid,
                    fileuserid,
                    instanceid,
                    streamdata}, this.insertfileuploadedOperationCompleted, userState);
    }
    
    private void OninsertfileuploadedOperationCompleted(object arg) {
        if ((this.insertfileuploadedCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.insertfileuploadedCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Getuploadedfileinfo", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    public System.Data.DataTable Getuploadedfileinfo(string appid, string fuserid) {
        object[] results = this.Invoke("Getuploadedfileinfo", new object[] {
                    appid,
                    fuserid});
        return ((System.Data.DataTable)(results[0]));
    }
    
    /// <remarks/>
    public System.IAsyncResult BeginGetuploadedfileinfo(string appid, string fuserid, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("Getuploadedfileinfo", new object[] {
                    appid,
                    fuserid}, callback, asyncState);
    }
    
    /// <remarks/>
    public System.Data.DataTable EndGetuploadedfileinfo(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((System.Data.DataTable)(results[0]));
    }
    
    /// <remarks/>
    public void GetuploadedfileinfoAsync(string appid, string fuserid) {
        this.GetuploadedfileinfoAsync(appid, fuserid, null);
    }
    
    /// <remarks/>
    public void GetuploadedfileinfoAsync(string appid, string fuserid, object userState) {
        if ((this.GetuploadedfileinfoOperationCompleted == null)) {
            this.GetuploadedfileinfoOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetuploadedfileinfoOperationCompleted);
        }
        this.InvokeAsync("Getuploadedfileinfo", new object[] {
                    appid,
                    fuserid}, this.GetuploadedfileinfoOperationCompleted, userState);
    }
    
    private void OnGetuploadedfileinfoOperationCompleted(object arg) {
        if ((this.GetuploadedfileinfoCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.GetuploadedfileinfoCompleted(this, new GetuploadedfileinfoCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Deleteuploadedfile", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    public void Deleteuploadedfile(string recordid) {
        this.Invoke("Deleteuploadedfile", new object[] {
                    recordid});
    }
    
    /// <remarks/>
    public System.IAsyncResult BeginDeleteuploadedfile(string recordid, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("Deleteuploadedfile", new object[] {
                    recordid}, callback, asyncState);
    }
    
    /// <remarks/>
    public void EndDeleteuploadedfile(System.IAsyncResult asyncResult) {
        this.EndInvoke(asyncResult);
    }
    
    /// <remarks/>
    public void DeleteuploadedfileAsync(string recordid) {
        this.DeleteuploadedfileAsync(recordid, null);
    }
    
    /// <remarks/>
    public void DeleteuploadedfileAsync(string recordid, object userState) {
        if ((this.DeleteuploadedfileOperationCompleted == null)) {
            this.DeleteuploadedfileOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDeleteuploadedfileOperationCompleted);
        }
        this.InvokeAsync("Deleteuploadedfile", new object[] {
                    recordid}, this.DeleteuploadedfileOperationCompleted, userState);
    }
    
    private void OnDeleteuploadedfileOperationCompleted(object arg) {
        if ((this.DeleteuploadedfileCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.DeleteuploadedfileCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/InsertLinks", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    public void InsertLinks(string title, string link, string instanceid, string ID) {
        this.Invoke("InsertLinks", new object[] {
                    title,
                    link,
                    instanceid,
                    ID});
    }
    
    /// <remarks/>
    public System.IAsyncResult BeginInsertLinks(string title, string link, string instanceid, string ID, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("InsertLinks", new object[] {
                    title,
                    link,
                    instanceid,
                    ID}, callback, asyncState);
    }
    
    /// <remarks/>
    public void EndInsertLinks(System.IAsyncResult asyncResult) {
        this.EndInvoke(asyncResult);
    }
    
    /// <remarks/>
    public void InsertLinksAsync(string title, string link, string instanceid, string ID) {
        this.InsertLinksAsync(title, link, instanceid, ID, null);
    }
    
    /// <remarks/>
    public void InsertLinksAsync(string title, string link, string instanceid, string ID, object userState) {
        if ((this.InsertLinksOperationCompleted == null)) {
            this.InsertLinksOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInsertLinksOperationCompleted);
        }
        this.InvokeAsync("InsertLinks", new object[] {
                    title,
                    link,
                    instanceid,
                    ID}, this.InsertLinksOperationCompleted, userState);
    }
    
    private void OnInsertLinksOperationCompleted(object arg) {
        if ((this.InsertLinksCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.InsertLinksCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Getlinks", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    public System.Data.DataTable Getlinks(string instanceid, string id) {
        object[] results = this.Invoke("Getlinks", new object[] {
                    instanceid,
                    id});
        return ((System.Data.DataTable)(results[0]));
    }
    
    /// <remarks/>
    public System.IAsyncResult BeginGetlinks(string instanceid, string id, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("Getlinks", new object[] {
                    instanceid,
                    id}, callback, asyncState);
    }
    
    /// <remarks/>
    public System.Data.DataTable EndGetlinks(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((System.Data.DataTable)(results[0]));
    }
    
    /// <remarks/>
    public void GetlinksAsync(string instanceid, string id) {
        this.GetlinksAsync(instanceid, id, null);
    }
    
    /// <remarks/>
    public void GetlinksAsync(string instanceid, string id, object userState) {
        if ((this.GetlinksOperationCompleted == null)) {
            this.GetlinksOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetlinksOperationCompleted);
        }
        this.InvokeAsync("Getlinks", new object[] {
                    instanceid,
                    id}, this.GetlinksOperationCompleted, userState);
    }
    
    private void OnGetlinksOperationCompleted(object arg) {
        if ((this.GetlinksCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.GetlinksCompleted(this, new GetlinksCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Deletelinks", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    public void Deletelinks(string recordid) {
        this.Invoke("Deletelinks", new object[] {
                    recordid});
    }
    
    /// <remarks/>
    public System.IAsyncResult BeginDeletelinks(string recordid, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("Deletelinks", new object[] {
                    recordid}, callback, asyncState);
    }
    
    /// <remarks/>
    public void EndDeletelinks(System.IAsyncResult asyncResult) {
        this.EndInvoke(asyncResult);
    }
    
    /// <remarks/>
    public void DeletelinksAsync(string recordid) {
        this.DeletelinksAsync(recordid, null);
    }
    
    /// <remarks/>
    public void DeletelinksAsync(string recordid, object userState) {
        if ((this.DeletelinksOperationCompleted == null)) {
            this.DeletelinksOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDeletelinksOperationCompleted);
        }
        this.InvokeAsync("Deletelinks", new object[] {
                    recordid}, this.DeletelinksOperationCompleted, userState);
    }
    
    private void OnDeletelinksOperationCompleted(object arg) {
        if ((this.DeletelinksCompleted != null)) {
            System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
            this.DeletelinksCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
        }
    }
    
    /// <remarks/>
    public new void CancelAsync(object userState) {
        base.CancelAsync(userState);
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void fileuploaderCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void filedeleteCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void SinglefilelinkCompletedEventHandler(object sender, SinglefilelinkCompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public partial class SinglefilelinkCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
    
    private object[] results;
    
    internal SinglefilelinkCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
            base(exception, cancelled, userState) {
        this.results = results;
    }
    
    /// <remarks/>
    public string Result {
        get {
            this.RaiseExceptionIfNecessary();
            return ((string)(this.results[0]));
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void insertfileuploadedCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void GetuploadedfileinfoCompletedEventHandler(object sender, GetuploadedfileinfoCompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public partial class GetuploadedfileinfoCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
    
    private object[] results;
    
    internal GetuploadedfileinfoCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
            base(exception, cancelled, userState) {
        this.results = results;
    }
    
    /// <remarks/>
    public System.Data.DataTable Result {
        get {
            this.RaiseExceptionIfNecessary();
            return ((System.Data.DataTable)(this.results[0]));
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void DeleteuploadedfileCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void InsertLinksCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void GetlinksCompletedEventHandler(object sender, GetlinksCompletedEventArgs e);

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public partial class GetlinksCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
    
    private object[] results;
    
    internal GetlinksCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
            base(exception, cancelled, userState) {
        this.results = results;
    }
    
    /// <remarks/>
    public System.Data.DataTable Result {
        get {
            this.RaiseExceptionIfNecessary();
            return ((System.Data.DataTable)(this.results[0]));
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
public delegate void DeletelinksCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
