﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for Student
/// </summary>
public class Student
{
	public Student()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public void InsertHolidaypackage(string fees, DateTime startdate, DateTime enddate, string packageid, string comments, string StudentId)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("insertholidaypackage", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@fees", fees);
        cmd.Parameters.AddWithValue("@startdate", startdate);
        cmd.Parameters.AddWithValue("@enddate", enddate);
        cmd.Parameters.AddWithValue("@packageid", packageid);
        cmd.Parameters.AddWithValue("@comments", comments);
        cmd.Parameters.AddWithValue("@StudentId", StudentId);
        
        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }
    public DataSet GetHolidaypackage(string packageid)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("select * from studentsubpackage where packageid=@packageid;select * from Annual_Fees where packageid=@packageid", conn);
        try
        {
            DataSet ds = new DataSet();

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@packageid", packageid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(ds);
            return ds;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }
    public void deleteHolidaypackage(string id)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("delete studentsubpackage where recid=@id", conn);
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.AddWithValue("@id", id);
        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }
    public void Insertprevpackage(string PID, string userid, string enddate, string startdate)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("Insertprevpackage", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@PID", PID);
        cmd.Parameters.AddWithValue("@userid", userid);
        cmd.Parameters.AddWithValue("@enddate", enddate);
        cmd.Parameters.AddWithValue("@startdate", startdate);

        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    public DataTable Getpreviouspackages(string packageid)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("select * from previouspackages where PID=@packageid", conn);
        try
        {
            DataTable dt = new DataTable();

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@packageid", packageid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }
    public void insertAnnual_Fees(string fees, string Month, string packageid, string title)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("insertAnnual_Fees", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@fees", fees);
        cmd.Parameters.AddWithValue("@Month", Month);
        cmd.Parameters.AddWithValue("@title", title);
        cmd.Parameters.AddWithValue("@packageid", packageid);
        

        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }
    public void deleteAnnual_Fees(string id)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("delete Annual_Fees where recid=@id", conn);
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.AddWithValue("@id", id);
        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

}