﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for studentinvoice
/// </summary>
public class studentparameter
{
    public int ID { get; set; }
    public string Monthrange { get; set; } //instance id
    public decimal TotalPackagefees { get; set; } //sectionname
    public decimal Totalcharges { get; set; }
    public decimal Totaldiscount { get; set; }
    public decimal feespaid { get; set; }
    public decimal feesbalance { get; set; }
    public decimal Totalfees { get; set; }
    public decimal extradiscount { get; set; }
    public decimal extracharges { get; set; }
    public decimal adjustment { get; set; }
    public string studentid { get; set; }
    public string paidcomplete { get; set; }
    public DateTime fromdate { get; set; }
    public DateTime todate { get; set; }
    public string discountcomments { get; set; }
    public string chargescomments { get; set; }
    public string adjustmentcomment { get; set; }
    public string adjustmentflag { get; set; }
    public string feescomments { get; set; }
    public studentparameter()
    {
        Monthrange = ""; //instance id
        TotalPackagefees = 0; //sectionname
        Totalcharges = 0;
        Totaldiscount = 0;
        feespaid = 0;
        feesbalance = 0;
        Totalfees = 0;
        extradiscount = 0;
        extracharges = 0;
        adjustment = 0;
        studentid = "";
        paidcomplete = "0";
        fromdate = DateTime.Now;
        todate = DateTime.Now;
        discountcomments = "";
        chargescomments = "";
        adjustmentcomment = "";
        adjustmentflag = "";
        feescomments = "";
    }

    public void Insertinvoiceajustments(string comments, string amount, string Invoiceid, string type)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("Insertinvoiceajustments", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@comments", comments);
        cmd.Parameters.AddWithValue("@amount", amount);
        cmd.Parameters.AddWithValue("@Invoiceid", Invoiceid);
        cmd.Parameters.AddWithValue("@type", type);


        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    public DataTable getstudentinvoiceadjust(string Invoiceid)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("SELECT * FROM InvoiceAjustment WHERE Invoiceid=@Invoiceid", conn);
        try
        {
            DataTable dt = new DataTable();

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@Invoiceid", Invoiceid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    public DataTable DELETEInvoiceAjustment(string Invoiceid, string val, string ajdval, string recid)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("DELETEInvoiceAjustment", conn);
        try
        {
            DataTable dt = new DataTable();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@val", val);
            cmd.Parameters.AddWithValue("@ajdval", ajdval);
            cmd.Parameters.AddWithValue("@recid", recid);
            cmd.Parameters.AddWithValue("@Invoiceid", Invoiceid);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }
}
public class studentinvoice
{
    public studentinvoice()
    {
    }
    public int insertinvoice(studentparameter objinvoice)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("insertinvoice", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@Monthrange", objinvoice.Monthrange);
        cmd.Parameters.AddWithValue("@TotalPackagefees", objinvoice.TotalPackagefees);
        cmd.Parameters.AddWithValue("@Totalcharges", objinvoice.Totalcharges);
        cmd.Parameters.AddWithValue("@Totaldiscount", objinvoice.Totaldiscount);
        cmd.Parameters.AddWithValue("@feespaid", objinvoice.feespaid);
        cmd.Parameters.AddWithValue("@feesbalance", objinvoice.feesbalance);
        cmd.Parameters.AddWithValue("@Totalfees", objinvoice.Totalfees);
        cmd.Parameters.AddWithValue("@extradiscount", objinvoice.extradiscount);
        cmd.Parameters.AddWithValue("@extracharges", objinvoice.extracharges);
        cmd.Parameters.AddWithValue("@adjustment", objinvoice.adjustment);
        cmd.Parameters.AddWithValue("@studentid", objinvoice.studentid);
        cmd.Parameters.AddWithValue("@fromdate", objinvoice.fromdate);
        cmd.Parameters.AddWithValue("@todate", objinvoice.todate);
        cmd.Parameters.AddWithValue("@feescomments", objinvoice.feescomments);
        cmd.Parameters.AddWithValue("@paidcomplete", objinvoice.paidcomplete);
        cmd.Parameters.AddWithValue("@chargescomment", objinvoice.chargescomments);
        cmd.Parameters.AddWithValue("@discountcomment", objinvoice.discountcomments);

        int id = 0;
        try
        {
            conn.Open();
            id = Convert.ToInt32(cmd.ExecuteScalar());
            return id;

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    public DataTable getstudentinvoice(string studentid)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("select * from Studentinvoice where studentid=@studentid and paidcomplete=0 order by fromdate", conn);
        try
        {
            DataTable dt = new DataTable();
           
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@studentid", studentid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    public DataTable getLastpaymentdate(string studentid)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("select top 1 fc.*,p.paymentmethod as PMethod from tbl_feecollection fc join paymentmethod p on p.recid=fc.paymentmethod where fc.Studentid=@studentid order by fc.paymentdate desc", conn);
        try
        {
            DataTable dt = new DataTable();

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@studentid", studentid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    public void adddiscount(studentparameter objinvoice)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("update Studentinvoice set  extradiscount=@extradiscount, discountcomment=@discountcomment,Totalfees=@Totalfees,feesbalance=@feesbalance where recid=@recid", conn);
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.AddWithValue("@feesbalance", objinvoice.feesbalance);
        cmd.Parameters.AddWithValue("@extradiscount", objinvoice.extradiscount);
        cmd.Parameters.AddWithValue("@discountcomment", objinvoice.discountcomments);
        cmd.Parameters.AddWithValue("@Totalfees", objinvoice.Totalfees);
        cmd.Parameters.AddWithValue("@recid", objinvoice.ID);
      
        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    public void addcharges(studentparameter objinvoice)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("update Studentinvoice set  extracharges=@extracharges, chargescomment=@chargescomment,Totalfees=@Totalfees,feesbalance=@feesbalance where recid=@recid", conn);
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.AddWithValue("@feesbalance", objinvoice.feesbalance);
        cmd.Parameters.AddWithValue("@extracharges", objinvoice.extracharges);
        cmd.Parameters.AddWithValue("@chargescomment", objinvoice.chargescomments);
        cmd.Parameters.AddWithValue("@Totalfees", objinvoice.Totalfees);
        cmd.Parameters.AddWithValue("@recid", objinvoice.ID);

        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    public void addadjustment(studentparameter objinvoice)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("update Studentinvoice set  adjustment=@adjustment, adjustmentcomment=@adjustmentcomment,Totalfees=@Totalfees,feesbalance=@feesbalance,adjustmentflag=@adjustmentflag where recid=@recid", conn);
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.AddWithValue("@feesbalance", objinvoice.feesbalance);
        cmd.Parameters.AddWithValue("@adjustment", objinvoice.adjustment);
        cmd.Parameters.AddWithValue("@adjustmentcomment", objinvoice.adjustmentcomment);
        cmd.Parameters.AddWithValue("@Totalfees", objinvoice.Totalfees);
        cmd.Parameters.AddWithValue("@recid", objinvoice.ID);
        cmd.Parameters.AddWithValue("@adjustmentflag", objinvoice.adjustmentflag);

        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    public void updatepayments(studentparameter objinvoice)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("update Studentinvoice set  feespaid=@feespaid, feesbalance=@feesbalance,paidcomplete=@paidcomplete where recid=@recid", conn);
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.AddWithValue("@feespaid", objinvoice.feespaid);
        cmd.Parameters.AddWithValue("@feesbalance", objinvoice.feesbalance);
        cmd.Parameters.AddWithValue("@paidcomplete", objinvoice.paidcomplete);
        cmd.Parameters.AddWithValue("@recid", objinvoice.ID);

        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    public void deleteinvoice(string recid)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("delete Studentinvoice where recid=@recid", conn);
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.AddWithValue("@recid", recid);

        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    public void insertfeecollection(string Studentid, string Feesubmit, DateTime SubmitDate, string InstanceId, string Paymentmethod, string paymentcomments, DateTime paymentdate,string userid,string paidby)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("insertfeecollection", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@Studentid", Studentid);
        cmd.Parameters.AddWithValue("@Feesubmit", Feesubmit);
        cmd.Parameters.AddWithValue("@SubmitDate", SubmitDate);
        cmd.Parameters.AddWithValue("@InstanceId", InstanceId);
        cmd.Parameters.AddWithValue("@Paymentmethod", Paymentmethod);
        cmd.Parameters.AddWithValue("@paymentcomments", paymentcomments);
        cmd.Parameters.AddWithValue("@paymentdate", paymentdate);
        cmd.Parameters.AddWithValue("@userid", userid);
        cmd.Parameters.AddWithValue("@paidby", paidby);
        
        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    public DataSet GetPaymentHistory(string studentid)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("GetPaymentHistory", conn);
        try
        {
            DataSet ds = new DataSet();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@studentid", studentid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(ds);
            return ds;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    public DataTable Getpreviouspackages(string startdate, string enddate, string PID)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("Getpreviouspackages", conn);
        try
        {
            DataTable dt = new DataTable();

            cmd.CommandType = CommandType.StoredProcedure;
            
            cmd.Parameters.AddWithValue("@PID", PID);
            cmd.Parameters.AddWithValue("@fromdate", startdate);
            cmd.Parameters.AddWithValue("@todate", enddate);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    public DataTable GetClasses(string instanceid)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("Getclasses", conn);
        try
        {
            DataTable dt = new DataTable();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@instanceid", instanceid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }
    public DataTable GetStudents(string ClassId)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("GetStudent", conn);
        try
        {
            DataTable dt = new DataTable();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@classid", ClassId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }
    public DataTable GetDefaulter(string instanceid, string def = "Defaulter")
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("GetDefaulter", conn);
        try
        {
            DataTable dt = new DataTable();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@instanceid", instanceid);
            cmd.Parameters.AddWithValue("@Defaulter", def);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }
    public DataTable GetFeePaid(string instanceid, string PID, string date)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("GetFeePaid", conn);
        try
        {
            DataTable dt = new DataTable();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@instanceid", instanceid);
            cmd.Parameters.AddWithValue("@classId", PID);
            cmd.Parameters.AddWithValue("@date", date);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }
    public DataTable GetDefaulterbyclass(string instanceid, string PID, string date)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("GetDefaulterbyclass", conn);
        try
        {
            DataTable dt = new DataTable();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@instanceid", instanceid);
            cmd.Parameters.AddWithValue("@classId", PID);
            cmd.Parameters.AddWithValue("@date", date);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }
    public DataTable GetAllStudent(string instanceid, string PID, string date)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("GetAllStudent", conn);
        try
        {
            DataTable dt = new DataTable();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@instanceid", instanceid);
            cmd.Parameters.AddWithValue("@PID", PID);
            cmd.Parameters.AddWithValue("@date", date);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }
    public DataTable GetStudentsbyinstanceid(string instanceid)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("GetAllStudentbyinstanceid", conn);
        try
        {
            DataTable dt = new DataTable();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@instanceid", instanceid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    public void SavefeeVoucher(string instanceid, string templateid)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("SavefeeVoucher", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@templateid", templateid);
        cmd.Parameters.AddWithValue("@instanceid", instanceid);

        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }
    public DataTable Getfeevouchertemplate(string instanceid)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("select templateid from feevouchertemplate where instanceid=@instanceid", conn);
        try
        {
            DataTable dt = new DataTable();

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@instanceid", instanceid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    public DataTable Getunpaidinvoice(string studentid)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("Getunpaidinvoice", conn);
        try
        {
            DataTable dt = new DataTable();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@studentid", studentid);
            
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }


    //Fee Reports
    public DataSet GetFeerangestatics(string instanceid, DateTime fromdate, DateTime todate)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("GetFeerangestatics", conn);
        try
        {
            DataSet ds = new DataSet();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@instanceid", instanceid);
            cmd.Parameters.AddWithValue("@todate", todate);
            cmd.Parameters.AddWithValue("@fromdate", fromdate);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(ds);
            return ds;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

    public void addPaymentMethod(string PaymentMethod, string InstanceID)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("insert into paymentmethod values(@paymentmethod,@instanceid)", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@paymentmethod", PaymentMethod);
                cmd.Parameters.AddWithValue("@instanceid", InstanceID);

                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
            }
            con.Close();
        }
    }
}