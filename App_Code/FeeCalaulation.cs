﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for FeeCalaulation
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class FeeCalaulation : System.Web.Services.WebService
{

    public FeeCalaulation()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public void AddLatefee()
    {
        if (DateTime.UtcNow.AddHours(5).ToString("h tt") == "6 AM")
        {
            DataTable dtdata = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                string query = "getstudentslate";
                using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
                {
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(dtdata);
                }
            }
            if (dtdata.Rows.Count > 0)
            {
                for (int a = 0; a < dtdata.Rows.Count; a++)
                {
                    calculatefees(dtdata.Rows[a]["StudendId"].ToString(), dtdata.Rows[a]["PID"].ToString());
                }
            }
        }
    }

    public void calculatefees(string studentid, string pid)
    {
        DataSet ds = new DataSet();

        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            string query = "Getstudenttoaddlatefee";
            using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
            {
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@studentid", studentid);
                da.SelectCommand.Parameters.AddWithValue("@PID", pid);
                da.Fill(ds);
            }
        }
        if (ds.Tables[0].Rows.Count > 0)
        {
            decimal latefeecharge = Convert.ToDecimal(ds.Tables[0].Rows[0]["LateFeeCharges"].ToString()); // pick up from db
            string[] feeduedatearray = ds.Tables[0].Rows[0]["Fee_DueDate"].ToString().Split('#');
            double feeduedate = Convert.ToDouble(feeduedatearray[0]);
            DateTime dtnow = DateTime.Now;
            decimal fees = 0;
            decimal balfees = 0;
            if (ds.Tables[1].Rows.Count > 0)
            {
                for (int rec = 0; rec < ds.Tables[1].Rows.Count; rec++)
                {
                    string asd = "";
                    DateTime SD = Convert.ToDateTime(ds.Tables[1].Rows[rec]["fromdate"].ToString());
                    DateTime ED = Convert.ToDateTime(ds.Tables[1].Rows[rec]["todate"].ToString());

                    if (!string.IsNullOrEmpty(ds.Tables[1].Rows[rec]["latedate"].ToString()))
                    {
                        string[] dates = ds.Tables[1].Rows[rec]["latedate"].ToString().Split('-');
                        if (Convert.ToInt32(dates[0].Trim(' ')) == 1)
                        {
                            dates[0] = "12";
                            dates[1] = (Convert.ToInt32(dates[1].Trim(' ')) - 1).ToString();
                        }
                        else
                        {
                            dates[0] = (Convert.ToInt32(dates[0].Trim(' ')) - 1).ToString();
                        }
                        asd = dates[0] + "-01-" + dates[1];
                    }
                    else
                    {
                        asd = ED.ToString();
                    }


                    DateTime lastadd = Convert.ToDateTime(asd);
                    if (lastadd.Month <= DateTime.Now.Month)
                    {
                        string latedate = "";
                        if (DateTime.Now.Month == 12)
                            latedate = "01-" + (DateTime.Now.Year + 1).ToString();
                        else
                            latedate = (DateTime.Now.Month + 1).ToString() + "-" + DateTime.Now.Year.ToString();


                        int mondiff = Math.Abs((DateTime.Now.Month - lastadd.Month) + 12 * (DateTime.Now.Year - lastadd.Year));
                        studentparameter objinvoice = new studentparameter();
                        if (mondiff > 0)
                        {
                            for (int m = 0; m < mondiff; m++)
                            {
                                objinvoice.Insertinvoiceajustments("Late Fee - " + (lastadd.Month + m) + " - " + lastadd.Year, latefeecharge.ToString(), ds.Tables[1].Rows[rec]["recid"].ToString(), "2");
                            }
                            updateinvoice(fees.ToString(), balfees.ToString(), ds.Tables[1].Rows[rec]["recid"].ToString(), latedate);
                        }
                        else if (mondiff == 0)
                        {
                            if (SD.AddDays(feeduedate + 1) < dtnow)
                            {
                                objinvoice.Insertinvoiceajustments("Late Fee - " + (lastadd.Month) + " - " + lastadd.Year, latefeecharge.ToString(), ds.Tables[1].Rows[rec]["recid"].ToString(), "2");
                                updateinvoice(fees.ToString(), balfees.ToString(), ds.Tables[1].Rows[rec]["recid"].ToString(), latedate);
                            }
                        }
                    }
                }
            }
        }
    }

    public void updateinvoice(string Totalfees, string feesbalance, string id, string latedate)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString());

        SqlCommand cmd = new SqlCommand("updateinvoice1", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        //cmd.Parameters.AddWithValue("@Totalfees", Totalfees);
        //cmd.Parameters.AddWithValue("@feesbalance", feesbalance);
        cmd.Parameters.AddWithValue("@id", id);
        cmd.Parameters.AddWithValue("@latedate", latedate);

        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Close();
        }
    }

}
