﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using Microsoft.Office.Interop.Excel;

/// <summary>
/// Summary description for uploaddata
/// </summary>
public class uploaddata
{
    public uploaddata()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataSet GetExcel(string fileName)
    {
        Application oXL;
        Workbook oWB;
        Worksheet oSheet;
        Range oRng;
        try
        {
            //  creat a Application object
            oXL = new ApplicationClass();
            //   get   WorkBook  object
            oWB = oXL.Workbooks.Open(fileName, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                    Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                    Missing.Value, Missing.Value);


            //   get   WorkSheet object 
            oSheet = (Microsoft.Office.Interop.Excel.Worksheet)oWB.Sheets[1];
            System.Data.DataTable dt = new System.Data.DataTable("dtExcel");
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            DataRow dr;


            StringBuilder sb = new StringBuilder();
            int jValue = oSheet.UsedRange.Cells.Columns.Count;
            int iValue = oSheet.UsedRange.Cells.Rows.Count;
            //  get data columns
            for (int j = 1; j <= jValue; j++)
            {
                dt.Columns.Add("column" + j, System.Type.GetType("System.String"));
            }


            //string colString = sb.ToString().Trim();
            //string[] colArray = colString.Split(':');


            //  get data in cell
            for (int i = 1; i <= iValue; i++)
            {
                dr = ds.Tables["dtExcel"].NewRow();
                for (int j = 1; j <= jValue; j++)
                {
                    oRng = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[i, j];
                    string strValue = oRng.Text.ToString();
                    dr["column" + j] = strValue;
                }
                ds.Tables["dtExcel"].Rows.Add(dr);
            }
            oXL.Workbooks.Close();
            return ds;
        }
        catch (Exception ex)
        {
            //Label1.Text = "Error: ";
            //Label1.Text += ex.Message.ToString();
            return null;
        }
        finally
        {
           
        }

    }
}