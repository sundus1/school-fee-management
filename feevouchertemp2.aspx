﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="feevouchertemp2.aspx.cs" Inherits="feevouchertemp2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="_assets/jquery-ui.css" rel="stylesheet" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <script type="text/javascript">
        $(window).load(function () {
            resizeiframepage();
            setTimeout(function () { resizeiframepage(); }, 300);
        });
        function resizeiframepage() {
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
            if (innerDoc.body.offsetHeight) {
                iframe.height = innerDoc.body.offsetHeight + 700 + "px";
            }
            else if (innerDoc && innerDoc.body.scrollHeight) {
                iframe.style.height = innerDoc.body.scrollHeight + "px";
            }
        }
        function printout() {
            //$('#imgpinter').attr("src", "");
            //$('#ankedit').text('');
            window.print();
            //$('#imgpinter').attr("src", "images/1410871087_Print_32x32.png");
            //$('#ankedit').text('Edit');
        }
        $(document).ready(function () {
            $(document).on('click', '.ankdiscount', function () {
               
                var dlg = $("#divDiscount").dialog({
                    width: 416,
                    show: {

                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });

            $(document).on('click', '.ankcharges', function () {
               
                var dlg = $("#div2").dialog({
                    width: 416,
                    show: {

                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });
            
            $(document).on('click', '.ankadjustment', function () {
                
                var dlg = $("#div3").dialog({
                    width: 416,
                    show: {

                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });

            $(document).on('click', '#btnpayment', function () {
                var txtval = $('#txtpayment').val();
                if (txtval == "") {
                    $('#paymenterrmsg').text('Required Amount')
                    return false;
                }
                else {
                    var filter = /(?:^\d{1,3}(?:\.?\d{3})*(?:,\d{2})?$)|(?:^\d{1,3}(?:,?\d{3})*(?:\.\d{2})?$)/;
                    if (filter.test(txtval)) {
                        $('#paymenterrmsg').text('');
                        var dlg = $("#div1").dialog({
                            width: 416,
                            show: {

                            },
                            modal: true,
                            hide: {
                                effect: "blind",
                            }
                        });
                        dlg.parent().appendTo($("form:first"));
                        dlg.parent().css("z-index", "1000");
                        return false;
                    }
                    else {
                        $('#paymenterrmsg').text('Invalid Amount')
                        return false;
                    }
                }
            });

            $(document).on('click', '#addPaymentMethods', function () {

                var dlg = $("#additionalPaymentMethods").dialog({
                    width: 416,
                    show: {

                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;


            });

            $("#txtpaiddate").datepicker({
                dateFormat: 'dd MM yy',
                changeMonth: true,
                changeYear: true,
            });
        });
    </script>
    <style>
        @media print
        {
            table
            {
                background-color: #000 !important;
                -webkit-print-color-adjust: exact;
            }

            .printdiv
            {
                display: none;
            }
        }

        .td_class
        {
            background: #FFF;
            padding: 5px;
        }

        .printdiv a
        {
            font:12px;

        } 
        .errorMsg {
        color:red;
        }
        .lnkTxt {
        color:blue !important;
        cursor:pointer !important;
        }
    </style>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #000;">
    <form id="form1" runat="server">
       
        <div style="height: auto; width: 290px; border: 1px solid #000; float: left; margin-right: 5px">
            <div style="border-bottom: 1px solid #000; height: 114px; width: 100%; position: relative">
                <div style="width: 110px; float: left; padding: 13px 0 0 13px;">
                    <img src="images/header.jpg" alt="img" id="img_header" runat="server" />
                </div>
                <div style="width: 166px; float: left;">
                    <div style="border: 1px solid #000; height: 32px; width: 147px; padding: 1px; margin: 5px 8px 0 0px; float: right">
                        <div style="width: 145px; height: 30px; line-height: 30px; border: 1px solid #000; font-size: 15px; text-align: center">BANK COPY</div>
                    </div>
                    <div style="clear: both;"></div>
                    <div style="font-size: 15px; font-weight: bold; line-height: 28px; padding: 10px 0 0 0;">
                        <asp:Label ID="lblschoolname" runat="server"></asp:Label>
                        <br />
                        Monthly Fee Challan
                    </div>
                </div>
            </div>
            <div style="width: 100%; height: 103px; border-bottom: 1px solid #000; font-size: 13px; font-weight: bold; line-height: 20px;">
                <div id="Footer1" runat="server">
                </div>
            </div>
            <div style="width: 100%;">
                <div style="padding: 10px 0 0 10px; font-size: 13px; font-weight: bold; line-height: 21px;">
                    Challan No. <span>
                        <asp:Label ID="lblslipno" runat="server"></asp:Label></span><br />
                    Name of Student: <span>
                        <asp:Label ID="lblstudentname" runat="server"></asp:Label></span><br />
                    Roll No. <span>
                        <asp:Label ID="lblregno" runat="server"></asp:Label></span><br />
                    <br />
                    Billing Month: <span>
                        <asp:Label ID="lblperiod" runat="server"></asp:Label></span><br />
                    Issuance Date: <span>
                        <asp:Label ID="lblissuedate" runat="server"></asp:Label></span><br />
                    Due Date: <span>
                        <asp:Label ID="lblduedate" runat="server"></asp:Label></span><br />
                </div>
                <asp:Table ID="tbl" runat="server" Width="96%" border="0" CellSpacing="0" CellPadding="0" Style="background-color: #000; font-size: 13px; font-weight: bold; margin: 20px 0 0 5px;">
                </asp:Table>
            </div>
            <p style="font-size: 13px; font-weight: bold; text-align: center;">Late Payment Surcharge: Rs. 10/- Per day.</p>
            <p style="font-size: 13px; font-weight: bold; padding: 15px 0 0 10px; margin: 25px 0 0 0;">
                <div style="font-size: 11px; color: #dfdfdf; font-weight: lighter;text-align:center;margin-bottom:25px;" id="Div5" runat="server">
                    CASH ONLY
                </div>
                Bank Stamp<br />
                &amp; Signature: <span>_________________________</span>
                <br /><br />
            </p>
        </div>
        <div style="height: auto; width: 290px; border: 1px solid #000; float: left; margin-right: 5px">
            <div style="border-bottom: 1px solid #000; height: 114px; width: 100%; position: relative">
                <div style="width: 110px; float: left; padding: 13px 0 0 13px;">
                    <img src="images/header.jpg" alt="img" id="img_header1" runat="server" />
                </div>
                <div style="width: 166px; float: left;">
                    <div style="border: 1px solid #000; height: 32px; width: 147px; padding: 1px; margin: 5px 8px 0 0px; float: right">
                        <div style="width: 145px; height: 30px; line-height: 30px; border: 1px solid #000; font-size: 15px; text-align: center">SCHOOL COPY</div>
                    </div>
                    <div style="clear: both;"></div>
                    <div style="font-size: 15px; font-weight: bold; line-height: 28px; padding: 10px 0 0 0;">
                        <asp:Label ID="lblschoolname1" runat="server"></asp:Label>
                        <br />
                        Monthly Fee Challan
                    </div>
                </div>
            </div>
            <div style="width: 100%; height: 103px; border-bottom: 1px solid #000; font-size: 13px; font-weight: bold; line-height: 20px;">
                <div id="Footer2" runat="server">
                </div>
            </div>
            <div style="width: 100%;">
                <div style="padding: 10px 0 0 10px; font-size: 13px; font-weight: bold; line-height: 21px;">
                    Challan No. <span>
                        <asp:Label ID="lblslipno1" runat="server"></asp:Label></span><br />
                    Name of Student: <span>
                        <asp:Label ID="lblstudentname1" runat="server"></asp:Label></span><br />
                    Roll No. <span>
                        <asp:Label ID="lblregno1" runat="server"></asp:Label></span><br />
                    <br />
                    Billing Month: <span>
                        <asp:Label ID="lblperiod1" runat="server"></asp:Label></span><br />
                    Issuance Date: <span>
                        <asp:Label ID="lblissuedate1" runat="server"></asp:Label></span><br />
                    Due Date: <span>
                        <asp:Label ID="lblduedate1" runat="server"></asp:Label></span><br />
                </div>
                <asp:Table ID="tbl1" runat="server" Width="96%" border="0" CellSpacing="0" CellPadding="0" Style="background-color: #000; font-size: 13px; font-weight: bold; margin: 20px 0 0 5px;">
                </asp:Table>
            </div>
            <p style="font-size: 13px; font-weight: bold; text-align: center;">Late Payment Surcharge: Rs. 10/- Per day.</p>
            <p style="font-size: 13px; font-weight: bold; padding: 15px 0 0 10px; margin: 25px 0 0 0;">
                <div style="font-size: 11px; color: #dfdfdf; font-weight: lighter;text-align:center;margin-bottom:25px;" id="Div6" runat="server">
                    CASH ONLY
                </div>
                Bank Stamp<br />
                &amp; Signature: <span>_________________________</span><br /><br />
                
            </p>
        </div>
        <div style="height: auto; width: 290px; border: 1px solid #000; float: left; margin-right: 5px">
            <div style="border-bottom: 1px solid #000; height: 114px; width: 100%; position: relative">
                <div style="width: 110px; float: left; padding: 13px 0 0 13px;">
                    <img src="images/header.jpg" alt="img" id="img_header2" runat="server" />
                </div>
                <div style="width: 166px; float: left;">
                    <div style="border: 1px solid #000; height: 32px; width: 147px; padding: 1px; margin: 5px 8px 0 0px; float: right">
                        <div style="width: 145px; height: 30px; line-height: 30px; border: 1px solid #000; font-size: 15px; text-align: center">STUDENT'S COPY</div>
                    </div>
                    <div style="clear: both;"></div>
                    <div style="font-size: 15px; font-weight: bold; line-height: 28px; padding: 10px 0 0 0;">
                        <asp:Label ID="lblschoolname2" runat="server"></asp:Label>
                        <br />
                        Monthly Fee Challan
                    </div>
                </div>
            </div>
            <div style="width: 100%; height: 103px; border-bottom: 1px solid #000; font-size: 13px; font-weight: bold; line-height: 20px;">
                <div id="Footer3" runat="server">
                </div>
            </div>
            <div style="width: 100%;">
                <div style="padding: 10px 0 0 10px; font-size: 13px; font-weight: bold; line-height: 21px;">
                    Challan No. <span>
                        <asp:Label ID="lblslipno2" runat="server"></asp:Label></span><br />
                    Name of Student: <span>
                        <asp:Label ID="lblstudentname2" runat="server"></asp:Label></span><br />
                    Roll No. <span>
                        <asp:Label ID="lblregno2" runat="server"></asp:Label></span><br />
                    <br />
                    Billing Month: <span>
                        <asp:Label ID="lblperiod2" runat="server"></asp:Label></span><br />
                    Issuance Date: <span>
                        <asp:Label ID="lblissuedate2" runat="server"></asp:Label></span><br />
                    Due Date: <span>
                        <asp:Label ID="lblduedate2" runat="server"></asp:Label></span><br />
                </div>
                <asp:Table ID="tbl2" runat="server" Width="96%" border="0" CellSpacing="0" CellPadding="0" Style="background-color: #000; font-size: 13px; font-weight: bold; margin: 20px 0 0 5px;">
                </asp:Table>
            </div>
            <p style="font-size: 13px; font-weight: bold; text-align: center;">Late Payment Surcharge: Rs. 10/- Per day.</p>
            <p style="font-size: 13px; font-weight: bold; padding: 15px 0 0 10px; margin: 25px 0 0 0;">
                 <div style="font-size: 11px; color: #dfdfdf; font-weight: lighter;text-align:center;margin-bottom:25px;" id="Div4" runat="server">
                    CASH ONLY
                </div>
                Bank Stamp<br />
                &amp; Signature: <span>_________________________</span>
                <br /><br />
            </p>
        </div>

        <div style="float: right;line-height: 25px;font-size: 14px;font-family: arial; margin-right:100px" class="printdiv">
            <h2>Fee Options</h2>
             <a href="javascript:printout();" id="ankprint">
                <img src="images/1410871087_Print_32x32.png" id="imgpinter" /> Print</a>
            <br />
            <a id="ankedit" href="Invoice.aspx?<%= Request.QueryString.ToString().Replace("Studentid","studid") %>">View monthly details</a><br />
            <a href="#" id="ankdiscount" class="menuitem ankdiscount">Add Discount</a><br />
            <a href="#" id="ankcharges" class="menuitem ankcharges">Add Charges</a><br />
            <a href="#" id="ankadjustment" class="menuitem ankadjustment">Add Adjustment</a>
            <div>
                <h2>Payment Collection</h2>
                <h4>Total Due:
                    <asp:Label ID="lbltotladue" runat="server"></asp:Label></h4>
                 <asp:TextBox ID="txtpayment" runat="server" ClientIDMode="Static" CssClass="standardField" placeholder="Payment Amount"></asp:TextBox>&nbsp;<asp:Button ID="btnpayment" runat="server" Text="Collect Payment" CssClass="standardFormButton" /><br /><asp:Label ID="paymenterrmsg" runat="server" ClientIDMode="Static" ForeColor="Red"></asp:Label>
            </div>
        </div>
        <div id="divDiscount" title="Discount" style="display: none">
            <asp:Panel ID="Panel2" DefaultButton="btndisamount" runat="server">
                <table>
                    <tr>
                        <td>Discount Amount:
                        </td>
                        <td>
                            <asp:TextBox ID="txtdisamount" runat="server" CssClass="standardField"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ValidationGroup="asd" ControlToValidate="txtdisamount" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid" ValidationGroup="asd" ValidationExpression="\d+(\.\d{1,2})?" ControlToValidate="txtdisamount" ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Comments:
                        </td>
                        <td>
                            <asp:TextBox ID="txtdiscountcomment" runat="server" TextMode="MultiLine" Rows="3" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btndisamount" runat="server" Text="Add Amount" ValidationGroup="asd" OnClick="btndisamount_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>

        <div id="div2" title="Charges" style="display: none">
            <asp:Panel ID="Panel3" DefaultButton="btnchargeamount" runat="server">
                <table>
                    <tr>
                        <td>Charges Amount:
                        </td>
                        <td>
                            <asp:TextBox ID="txtchargeamount" runat="server" CssClass="standardField"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ValidationGroup="asd1" ControlToValidate="txtchargeamount" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Invalid" ValidationGroup="asd1" ValidationExpression="\d+(\.\d{1,2})?" ControlToValidate="txtchargeamount" ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>

                    </tr>
                    <tr>
                        <td>Comments:
                        </td>
                        <td>
                            <asp:TextBox ID="txtchargescomment" runat="server" TextMode="MultiLine" Rows="3" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btnchargeamount" runat="server" Text="Add Amount" ValidationGroup="asd1" OnClick="btnchargeamount_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
        <div id="div3" title="Adjustment" style="display: none">
            <asp:Panel ID="Panel4" DefaultButton="btnadjustamount" runat="server">
                <table>
                    <tr>
                        <td>Adjustment Amount:
                        </td>
                        <td>
                            <asp:TextBox ID="txtadjustamount" runat="server" CssClass="standardField"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ValidationGroup="asd2" ControlToValidate="txtadjustamount" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Invalid" ValidationGroup="asd2" ValidationExpression="\d+(\.\d{1,2})?" ControlToValidate="txtadjustamount" ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Comments:
                        </td>
                        <td>
                            <asp:TextBox ID="txtajdustmentcomment" runat="server" TextMode="MultiLine" Rows="3" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Add/Subtract:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlAtype" runat="server">
                                <asp:ListItem Value="0" Selected>Add Amount</asp:ListItem>
                                <asp:ListItem Value="1">Subtract Amount</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btnadjustamount" runat="server" Text="Add Amount" ValidationGroup="asd2" OnClick="btnadjustamount_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
        <div id="div1" title="Payment Method" style="display: none;">
            <asp:Panel ID="Panel1" DefaultButton="btnprocessed" runat="server">
                <table>
                    <tr>
                        <td>
                            Paid Date:
                        </td>
                        <td>
                            <asp:TextBox ID="txtpaiddate" runat="server" CssClass="standardField" ClientIDMode="Static"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Paid By:
                        </td>
                        <td>
                            <asp:TextBox ID="txtpaidby" runat="server" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Payment Method:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlpayment" runat="server" AppendDataBoundItems="True">
                            </asp:DropDownList><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="ddlpayment" ForeColor="Red" InitialValue="0" ValidationGroup="payment"></asp:RequiredFieldValidator>
                        </td> 
                        <td><asp:HyperLink runat="server" ID="addPaymentMethods" Text="Add Payment Method" CssClass="lnkTxt"></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td>Comments:
                        </td>
                        <td>
                            <asp:TextBox ID="txtpaycomment" runat="server" TextMode="MultiLine" Rows="3" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btnprocessed" runat="server" Text="Pay" CssClass="standardFormButton" ValidationGroup="payment" OnClick="btnprocessed_Click"/>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>

          <div id="additionalPaymentMethods" title="Payment Method" style="display: none">
            <asp:Panel ID="Panel5" DefaultButton="btnaddmethod" runat="server">
                <table>
                    <tr>
                        <td>Title:
                        </td>
                        <td>
                            <asp:TextBox ID="txtMethodName" runat="server" CssClass="standardField" ClientIDMode="Static"></asp:TextBox>
                        </td>
                        <td><asp:RequiredFieldValidator runat="server" ControlToValidate="txtMethodName" ValidationGroup="paymentmethod" ErrorMessage="Required" InitialValue="" CssClass="errorMsg" ></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btnaddmethod" runat="server" Text="Save Payment Method" CssClass="standardFormButton" ValidationGroup="paymentmethod" OnClick="btnaddmethod_Click" />
                        </td>
                    </tr>                    
                </table>
            </asp:Panel>
        </div>
         <asp:HiddenField ID="hfid" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hffeebalance" runat="server" ClientIDMode="Static" Value="0" />
         <asp:HiddenField ID="hffeepaid" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hffees" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hfdiscount" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hfcharge" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hfadjustment" runat="server" ClientIDMode="Static" Value="0" />

    </form>
</body>
</html>
