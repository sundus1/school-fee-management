﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AllReports.aspx.cs" Inherits="AllReports" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <h3>All Reports</h3>
        <ul>
            <li>
                <a href='Reports.aspx?instanceid=<%= Request["instanceid"] %>'>Class Wise Defaulter Report</a>
            </li>
            <li>
                <a href='feeReports.aspx?instanceid=<%= Request["instanceid"] %>'> Fee Collection Report</a>
            </li>
        </ul>
    </div>
    </form>
</body>
</html>
