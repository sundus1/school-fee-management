﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using AvaimaWS;
using System.Globalization;

public partial class MultipleVochures : AvaimaThirdpartyTool.AvaimaWebPage
{
    FeeApplicationwebservice_New feeswebservice = new FeeApplicationwebservice_New();
    DataTable dttable = new DataTable();
    string Totalcharges = "";
    string Totaldiscount = "";
    studentinvoice objstudent;
    studentparameter objinvoice;
    AvaimaStorageUploader objuploader;
    decimal totalfees = 0;
    decimal totaldue = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        BindMonths();
        if (!IsPostBack)
        {
            binddata();
            AvaimaStorageUploader objuploader = new AvaimaStorageUploader();
            string path = objuploader.Singlefilelink(this.InstanceID, "Feevoucher.html");
            ankdownload.HRef = path;

            //  Button1.Enabled = false;
        }
    }
    private StringBuilder SendInvoice(string Studentid, string InstanceID, string PID, string parentemail, string schoolname)
    {
        try
        {
            DataSet ds = new DataSet();
            AvaimaEmailAPI objemail = new AvaimaEmailAPI();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                string query = "feesvoucher";
                using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
                {
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.AddWithValue("@instanceid", InstanceID);
                    da.SelectCommand.Parameters.AddWithValue("@studentid", Studentid);
                    da.SelectCommand.Parameters.AddWithValue("@PID", PID);
                    da.Fill(ds);
                }
            }
            string headerfile = "";
            string Footer = "";
            string lblclass = "";
            string lblissuedate = "";
            string lblregno = "";
            string lblstudentname = "";
            string lblfathername = "";
            string lblslipno = "";
            string lblsession = "";
            string lblperiod = "";
            string lblduedate = "";
            string lbldueamount = "";
            string fees = "0";
            string charges = "0";
            string Discount = "0";
            string ecomment = "";
            string ecommentamount = "0";
            string extradiscount = "";
            string extradiscountamount = "0";
            string adjustmentcomment = "";
            string adjustmentcommentamount = "0";
            decimal balanceval = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                objuploader = new AvaimaStorageUploader();
                headerfile = objuploader.Singlefilelink(InstanceID, ds.Tables[0].Rows[0]["headerfile"].ToString());
                Footer = ds.Tables[0].Rows[0]["footer"].ToString();
            }
            if (ds.Tables[2].Rows.Count > 0)
            {
                lblclass = ds.Tables[2].Rows[0]["title"].ToString();
            }
            if (ds.Tables[1].Rows.Count > 0)
            {
                lblissuedate = Convert.ToDateTime(ds.Tables[1].Rows[0]["issuedate"].ToString()).ToString("dd-MMMM-yyyy");

                lblregno = ds.Tables[1].Rows[0]["Regno"].ToString();
                lblstudentname = ds.Tables[1].Rows[0]["studentname"].ToString();
                lblfathername = ds.Tables[1].Rows[0]["ParentName"].ToString();
            }
            if (ds.Tables[3].Rows.Count > 0)
            {
                objinvoice = new studentparameter();
                dttable = objinvoice.getstudentinvoiceadjust(ds.Tables[3].Rows[0]["recid"].ToString());

                lblslipno = ds.Tables[3].Rows[0]["recid"].ToString();

                DateTime dtstart = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
                if (ds.Tables[0].Rows.Count > 0 && dtstart.Month >= Convert.ToInt32(ds.Tables[0].Rows[0]["month"].ToString()))
                {
                    lblsession = dtstart.Year + "-" + (dtstart.Year + 1);
                }
                else
                {
                    lblsession = (dtstart.Year - 1) + "-" + dtstart.Year;
                }

                lblperiod = dtstart.ToString("MMM yyyy");

                fees = ds.Tables[3].Rows[0]["TotalPackagefees"].ToString();
                decimal vale = Convert.ToDecimal(ds.Tables[4].Rows[0]["Totalfees"].ToString()) - Convert.ToDecimal(ds.Tables[4].Rows[0]["feespaid"].ToString()) + Convert.ToDecimal(ds.Tables[4].Rows[0]["charges"].ToString()) - Convert.ToDecimal(ds.Tables[4].Rows[0]["discount"].ToString());
                balanceval = vale - Convert.ToDecimal(ds.Tables[3].Rows[0]["Totalfees"].ToString());
                totalfees = Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalPackagefees"].ToString());
                if (Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString()) > 0)
                {
                    charges = ds.Tables[3].Rows[0]["TotalCharges"].ToString();
                    totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString());
                }
                if (Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString()) > 0)
                {
                    Discount = ds.Tables[3].Rows[0]["Totaldiscount"].ToString();
                    totalfees -= Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString());
                }
                if (dttable.Rows.Count > 0)
                {
                    for (int asd = 0; asd < dttable.Rows.Count; asd++)
                    {
                        if (dttable.Rows[asd]["type"].ToString() == "2")
                        {
                            totalfees += Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                            balanceval = balanceval - Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                        }
                        else if (dttable.Rows[asd]["type"].ToString() == "1")
                        {
                            totalfees -= Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                            balanceval = balanceval + Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                        }
                    }
                }
                else
                {
                    if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString()) > 0)
                    {
                        ecomment = ds.Tables[3].Rows[0]["chargescomment"].ToString();
                        ecommentamount = ds.Tables[3].Rows[0]["extracharges"].ToString();
                        totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString());
                    }

                    if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString()) > 0)
                    {
                        extradiscount = ds.Tables[3].Rows[0]["discountcomment"].ToString();
                        extradiscountamount = ds.Tables[3].Rows[0]["extradiscount"].ToString();
                        totalfees -= Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString());
                    }
                }
                if (Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString()) > 0)
                {
                    adjustmentcomment = ds.Tables[3].Rows[0]["adjustmentcomment"].ToString();
                    adjustmentcommentamount = ds.Tables[3].Rows[0]["adjustment"].ToString();
                    totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString());
                }
                totalfees = totalfees + balanceval;
            }
            if (ds.Tables[5].Rows.Count > 0)
            {
                DateTime enddate = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
                string[] duedate = ds.Tables[5].Rows[0]["Fee_DueDate"].ToString().Split('#');
                string[] afterduedate = ds.Tables[5].Rows[0]["LateFeesStartDate"].ToString().Split('#');
                enddate = enddate.AddDays(Convert.ToDouble(duedate[0]));
                enddate = enddate.AddDays(Convert.ToDouble(afterduedate[0]));
                lblduedate = enddate.ToString("dd-MMM-yyyy");

                decimal latefee = 0;
                try
                {
                    if (enddate > DateTime.Now)
                        latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["LateFeeCharges"].ToString());
                }
                catch
                {
                    if (enddate > DateTime.Now)
                        latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["latefees"].ToString());
                }
                latefee = latefee + totalfees;
                lbldueamount = latefee.ToString();
            }


            StringBuilder HTML = new StringBuilder();
            HTML.AppendLine("<div style=\" width: 285px; min-height: 300px; border-right: 1px dotted #000; float: left; padding: 0 5px \"><div style=\"height: auto;\"><img src=\"" + headerfile + "\" alt=\"img\" style=\"width:100%;\" /></div>");
            HTML.AppendLine("<div style=\"height: 25px; border-top: 2px solid #000; border-bottom: 1px solid #000; padding: 0 8px; \"><h2 style=\"float: left; font-size: 15px; padding: 0px; margin: 0px; line-height: 27px; color: #666;\"><span>Slip. ID </span><span>" + lblslipno + "</span></h2><h2 style=\"float: right; font-size: 14px; padding: 0px; margin: 0px; line-height: 26px; text-align: center; color: #666;\">BANK COPY</h2>");
            HTML.AppendLine("<div style=\" clear: both;\"></div><table width=\"100%\" border=\"0\" style=\"margin: 7px 0 0 0; font-size: 14px; float: left;\"><tr><td style=\"text-align: left; width: 82px;\" scope=\"col\"><b>Class:</b>" + lblclass + "<br />");
            HTML.AppendLine("<b>Issue Date:</b> " + lblissuedate + "<br /><b>Reg. No.:</b> " + lblregno + "<br />");
            HTML.AppendLine("<b>Session:</b> " + lblsession + "<br /><b>G. R. No.:</b> " + lblregno + "<br /><b>Period:</b> " + lblperiod + "<br />");
            HTML.AppendLine("</td></tr></table><table width=\"100%\" border=\"0\" style=\"margin: 7px 0 0 0; font-size: 14px; float: left;\"><tr>");
            HTML.AppendLine("<td style=\"width: 100% !important; margin: 10px 0 0 0 !important; padding: 10px 3px 0 !important; font-size: 13px;\" colspan=\"2\">Student&acute;s Name: " + lblstudentname + "</td></tr>");
            HTML.AppendLine("<tr><td style=\"width: 100% !important; margin: 10px 0 0 0 !important; padding: 10px 3px 0 !important; font-size: 13px;\" colspan=\"2\">Father&acute;s Name: " + lblfathername + "</td></tr></table></div>");
            HTML.AppendLine("<div style=\" clear: both;\"></div><div style=\" height: 25px; border-top: 2px solid #000; padding: 0 8px; \"><h2 style=\"float: left; font-size: 15px; padding: 0px; margin: 0px; line-height: 27px;\"><span>Fee Details</span></h2></div>");
            HTML.AppendLine("<table width=\"100% \" border=\"0\" cellspacing=\"0\" style=\"font-size: 14px;\">");
            HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Tuition  Fee " + lblperiod + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px; \">" + fees + "</td></tr>");
            if (charges != "0")
                HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Total Individual Charges</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + charges + "</td></tr>");
            if (Discount != "0")
                HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Total Individual Discount</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + Discount + "</td></tr>");
            if (dttable.Rows.Count > 0)
            {
                for (int asd = 0; asd < dttable.Rows.Count; asd++)
                {
                    HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + dttable.Rows[asd]["comments"].ToString() + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + dttable.Rows[asd]["amount"].ToString() + "</td></tr>");
                }
            }
            else
            {
                if (ecommentamount != "0")
                    HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + ecomment + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + ecommentamount + "</td></tr>");
                if (extradiscountamount != "0")
                    HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + extradiscount + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + extradiscountamount + "</td></tr>");
            }
            if (adjustmentcommentamount != "0")
                HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + adjustmentcomment + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + adjustmentcommentamount + "</td></tr>");

            HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Previous Balance</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + balanceval.ToString() + "</td></tr>");
            HTML.AppendLine("<tr style=\"background: #d9d9d9;\"><td style=\" border-bottom: 2px solid #000; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\"><b>Total Due</b></td><td style=\" border-bottom: 2px solid #000; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\"><b>" + totalfees.ToString() + "</b></td></tr>");
            HTML.AppendLine("</table>");
            HTML.AppendLine("<table style=\" font-size: 13px; margin: 10px 0 0 0; padding: 0 3px;\" width=\"100%\" border=\"0\">");
            HTML.AppendLine("<tr><td colspan=\"2\">Due Date: <b>" + lblduedate + "</b></td></tr>");
            HTML.AppendLine("<tr><td>Amount payable after due date</td><td style=\"text-align: right;\">" + lbldueamount + "</td></tr></table>");
            HTML.AppendLine("<br /><br /><br /><table width=\"100%\" border=\"0\" style=\"font-size: 14px; font-weight: bold;\"");
            HTML.AppendLine("<tr><td style=\"border-top: 1px solid #000;\">Cashier</td><td style=\"border-top: 1px solid #000;\">Officer</td></tr></table>");
            HTML.AppendLine("<div style=\"font-size: 11px; color: #b5b5b5; font-weight: lighter; padding: 11px 5px 0px 5px;\">" + Footer + "</div></div>");

            /***********/

            HTML.AppendLine("<div style=\" width: 285px; min-height: 300px; border-right: 1px dotted #000; float: left; padding: 0 5px \"><div style=\"height: auto;\"><img src=\"" + headerfile + "\" alt=\"img\" style=\"width:100%;\" /></div>");
            HTML.AppendLine("<div style=\"height: 25px; border-top: 2px solid #000; border-bottom: 1px solid #000; padding: 0 8px; \"><h2 style=\"float: left; font-size: 15px; padding: 0px; margin: 0px; line-height: 27px; color: #666;\"><span>Slip. ID </span><span>" + lblslipno + "</span></h2><h2 style=\"float: right; font-size: 14px; padding: 0px; margin: 0px; line-height: 26px; text-align: center; color: #666;\">SCHOOL COPY</h2>");
            HTML.AppendLine("<div style=\" clear: both;\"></div><table width=\"100%\" border=\"0\" style=\"margin: 7px 0 0 0; font-size: 14px; float: left;\"><tr><td style=\"text-align: left; width: 82px;\" scope=\"col\"><b>Class:</b>" + lblclass + "<br />");
            HTML.AppendLine("<b>Issue Date:</b> " + lblissuedate + "<br /><b>Reg. No.:</b> " + lblregno + "<br />");
            HTML.AppendLine("<b>Session:</b> " + lblsession + "<br /><b>G. R. No.:</b> " + lblregno + "<br /><b>Period:</b> " + lblperiod + "<br />");
            HTML.AppendLine("</td></tr></table><table width=\"100%\" border=\"0\" style=\"margin: 7px 0 0 0; font-size: 14px; float: left;\"><tr>");
            HTML.AppendLine("<td style=\"width: 100% !important; margin: 10px 0 0 0 !important; padding: 10px 3px 0 !important; font-size: 13px;\" colspan=\"2\">Student&acute;s Name: " + lblstudentname + "</td></tr>");
            HTML.AppendLine("<tr><td style=\"width: 100% !important; margin: 10px 0 0 0 !important; padding: 10px 3px 0 !important; font-size: 13px;\" colspan=\"2\">Father&acute;s Name: " + lblfathername + "</td></tr></table></div>");
            HTML.AppendLine("<div style=\" clear: both;\"></div><div style=\" height: 25px; border-top: 2px solid #000; padding: 0 8px; \"><h2 style=\"float: left; font-size: 15px; padding: 0px; margin: 0px; line-height: 27px;\"><span>Fee Details</span></h2></div>");
            HTML.AppendLine("<table width=\"100% \" border=\"0\" cellspacing=\"0\" style=\"font-size: 14px;\">");
            HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Tuition  Fee " + lblperiod + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px; \">" + fees + "</td></tr>");
            if (charges != "0")
                HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Total Individual Charges</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + charges + "</td></tr>");
            if (Discount != "0")
                HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Total Individual Discount</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + Discount + "</td></tr>");
            if (dttable.Rows.Count > 0)
            {
                for (int asd = 0; asd < dttable.Rows.Count; asd++)
                {
                    HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + dttable.Rows[asd]["comments"].ToString() + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + dttable.Rows[asd]["amount"].ToString() + "</td></tr>");
                }
            }
            else
            {
                if (ecommentamount != "0")
                    HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + ecomment + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + ecommentamount + "</td></tr>");
                if (extradiscountamount != "0")
                    HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + extradiscount + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + extradiscountamount + "</td></tr>");
            }
            if (adjustmentcommentamount != "0")
                HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + adjustmentcomment + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + adjustmentcommentamount + "</td></tr>");

            HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Previous Balance</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + balanceval.ToString() + "</td></tr>");
            HTML.AppendLine("<tr style=\"background: #d9d9d9;\"><td style=\" border-bottom: 2px solid #000; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\"><b>Total Due</b></td><td style=\" border-bottom: 2px solid #000; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\"><b>" + totalfees.ToString() + "</b></td></tr>");
            HTML.AppendLine("</table>");
            HTML.AppendLine("<table style=\" font-size: 13px; margin: 10px 0 0 0; padding: 0 3px;\" width=\"100%\" border=\"0\">");
            HTML.AppendLine("<tr><td colspan=\"2\">Due Date: <b>" + lblduedate + "</b></td></tr>");
            HTML.AppendLine("<tr><td>Amount payable after due date</td><td style=\"text-align: right;\">" + lbldueamount + "</td></tr></table>");
            HTML.AppendLine("<br /><br /><br /><table width=\"100%\" border=\"0\" style=\"font-size: 14px; font-weight: bold;\"");
            HTML.AppendLine("<tr><td style=\"border-top: 1px solid #000;\">Cashier</td><td style=\"border-top: 1px solid #000;\">Officer</td></tr></table>");
            HTML.AppendLine("<div style=\"font-size: 11px; color: #b5b5b5; font-weight: lighter; padding: 11px 5px 0px 5px;\">" + Footer + "</div></div>");


            /***********/

            HTML.AppendLine("<div style=\" width: 285px; min-height: 300px; border-left: 1px dotted #000;  float: left; padding: 0 5px \"><div style=\"height: auto;\"><img src=\"" + headerfile + "\" alt=\"img\" style=\"width:100%;\" /></div>");
            HTML.AppendLine("<div style=\"height: 25px; border-top: 2px solid #000; border-bottom: 1px solid #000; padding: 0 8px; \"><h2 style=\"float: left; font-size: 15px; padding: 0px; margin: 0px; line-height: 27px; color: #666;\"><span>Slip. ID </span><span>" + lblslipno + "</span></h2><h2 style=\"float: right; font-size: 14px; padding: 0px; margin: 0px; line-height: 26px; text-align: center; color: #666;\">STUDENT'S COPY</h2>");
            HTML.AppendLine("<div style=\" clear: both;\"></div><table width=\"100%\" border=\"0\" style=\"margin: 7px 0 0 0; font-size: 14px; float: left;\"><tr><td style=\"text-align: left; width: 82px;\" scope=\"col\"><b>Class:</b>" + lblclass + "<br />");
            HTML.AppendLine("<b>Issue Date:</b> " + lblissuedate + "<br /><b>Reg. No.:</b> " + lblregno + "<br />");
            HTML.AppendLine("<b>Session:</b> " + lblsession + "<br /><b>G. R. No.:</b> " + lblregno + "<br /><b>Period:</b> " + lblperiod + "<br />");
            HTML.AppendLine("</td></tr></table><table width=\"100%\" border=\"0\" style=\"margin: 7px 0 0 0; font-size: 14px; float: left;\"><tr>");
            HTML.AppendLine("<td style=\"width: 100% !important; margin: 10px 0 0 0 !important; padding: 10px 3px 0 !important; font-size: 13px;\" colspan=\"2\">Student&acute;s Name:" + lblstudentname + "</td></tr>");
            HTML.AppendLine("<tr><td style=\"width: 100% !important; margin: 10px 0 0 0 !important; padding: 10px 3px 0 !important; font-size: 13px;\" colspan=\"2\">Father&acute;s Name: " + lblfathername + "</td></tr></table></div>");
            HTML.AppendLine("<div style=\" clear: both;\"></div><div style=\" height: 25px; border-top: 2px solid #000; padding: 0 8px; \"><h2 style=\"float: left; font-size: 15px; padding: 0px; margin: 0px; line-height: 27px;\"><span>Fee Details</span></h2></div>");
            HTML.AppendLine("<table width=\"100% \" border=\"0\" cellspacing=\"0\" style=\"font-size: 14px;\">");
            HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Tuition  Fee " + lblperiod + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px; \">" + fees + "</td></tr>");
            if (charges != "0")
                HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Total Individual Charges</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + charges + "</td></tr>");
            if (Discount != "0")
                HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Total Individual Discount</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + Discount + "</td></tr>");
            if (dttable.Rows.Count > 0)
            {
                for (int asd = 0; asd < dttable.Rows.Count; asd++)
                {
                    HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + dttable.Rows[asd]["comments"].ToString() + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + dttable.Rows[asd]["amount"].ToString() + "</td></tr>");
                }
            }
            else
            {
                if (ecommentamount != "0")
                    HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + ecomment + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + ecommentamount + "</td></tr>");
                if (extradiscountamount != "0")
                    HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + extradiscount + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + extradiscountamount + "</td></tr>");
            }
            if (adjustmentcommentamount != "0")
                HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + adjustmentcomment + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + adjustmentcommentamount + "</td></tr>");

            HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Previous Balance</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + balanceval.ToString() + "</td></tr>");
            HTML.AppendLine("<tr style=\"background: #d9d9d9;\"><td style=\" border-bottom: 2px solid #000; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\"><b>Total Due</b></td><td style=\" border-bottom: 2px solid #000; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\"><b>" + totalfees.ToString() + "</b></td></tr>");
            HTML.AppendLine("</table>");
            HTML.AppendLine("<table style=\" font-size: 13px; margin: 10px 0 0 0; padding: 0 3px;\" width=\"100%\" border=\"0\">");
            HTML.AppendLine("<tr><td colspan=\"2\">Due Date: <b>" + lblduedate + "</b></td></tr>");
            HTML.AppendLine("<tr><td>Amount payable after due date</td><td style=\"text-align: right;\">" + lbldueamount + "</td></tr></table>");
            HTML.AppendLine("<br /><br /><br /><table width=\"100%\" border=\"0\" style=\"font-size: 14px; font-weight: bold;\"");
            HTML.AppendLine("<tr><td style=\"border-top: 1px solid #000;\">Cashier</td><td style=\"border-top: 1px solid #000;\">Officer</td></tr></table>");
            HTML.AppendLine("<div style=\"font-size: 11px; color: #b5b5b5; font-weight: lighter; padding: 11px 5px 0px 5px;\">" + Footer + "</div></div>");
            HTML.AppendLine("<div class=\"pageBreak\"></div> <br />");
            return HTML;
        }
        catch (Exception ex)
        {
            StringBuilder HTML = new StringBuilder();
            return HTML;
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //Create Vouchers & than generate downloadable link
        CreateVouchers();

        StringBuilder strHTMLContent = new StringBuilder();
        strHTMLContent.AppendLine("<style>@media print {.tablesdiv td{border:1px solid;} .pageBreak {page-break-before: always; page-break-after:always;}} .Tabletr{height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;}</style>");
        DataTable dtstudent = new DataTable();
        objstudent = new studentinvoice();
        if (rbtnclass.Checked)
            dtstudent = objstudent.GetStudents(ddlclass.SelectedValue);
        else
            dtstudent = objstudent.GetStudentsbyinstanceid(this.InstanceID);
        if (dtstudent.Rows.Count > 0)
        {
            if (hftemplate.Value == "2")
            {
                strHTMLContent.AppendLine("<body style=\"font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #000;\">");
            }
            else if (hftemplate.Value == "1")
                strHTMLContent.AppendLine("<body style=\"font-size: 11px !important; color: #000;\">");
            else if (hftemplate.Value == "99")
            {
                strHTMLContent.AppendLine("<style> .td_class{background: #FFF;padding: 5px;} .tablesdiv td{border:1px solid;} .printdiv a{font:12px;}</style>");
                strHTMLContent.AppendLine("<body style=\"font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #000;\">");
            }
            string htmll = "";
            for (int st = 0; st < dtstudent.Rows.Count; st++)
            {
                if (hftemplate.Value == "1")
                {
                    htmll = SendInvoicetemp1(dtstudent.Rows[st]["StudendId"].ToString(), this.InstanceID, dtstudent.Rows[st]["PID"].ToString(), "", "").ToString();
                    if (!String.IsNullOrEmpty(htmll))
                        strHTMLContent.AppendLine(htmll);
                }
                else if (hftemplate.Value == "2")
                {
                    htmll = SendInvoicetemp2(dtstudent.Rows[st]["StudendId"].ToString(), this.InstanceID, dtstudent.Rows[st]["PID"].ToString(), "", "").ToString();
                    if (!String.IsNullOrEmpty(htmll))
                        strHTMLContent.AppendLine(htmll);
                }
                else if (hftemplate.Value == "99")
                {
                    htmll = SendInvoicetemp3(dtstudent.Rows[st]["StudendId"].ToString(), this.InstanceID, dtstudent.Rows[st]["PID"].ToString(), "", "THE CITY SCHOLARS").ToString();
                    if (!String.IsNullOrEmpty(htmll))
                        strHTMLContent.AppendLine(htmll);
                }
                else
                {
                    htmll = SendInvoice(dtstudent.Rows[st]["StudendId"].ToString(), this.InstanceID, dtstudent.Rows[st]["PID"].ToString(), "", "").ToString();
                    if (!String.IsNullOrEmpty(htmll))
                        strHTMLContent.AppendLine(htmll);
                }
            }
            if (hftemplate.Value == "1" || hftemplate.Value == "2" || hftemplate.Value == "99")
            {
                strHTMLContent.AppendLine("</body>");
            }
        }
        AvaimaStorageUploader objuploader = new AvaimaStorageUploader();
        objuploader.insertfileuploaded("Feevoucher", "html", "8ee08d08-68ab-4415-8b50-9fa722151856", HttpContext.Current.User.Identity.Name, this.InstanceID, this.InstanceID, Encoding.ASCII.GetBytes(strHTMLContent.ToString()));
        string path = objuploader.Singlefilelink(this.InstanceID, "Feevoucher.html");
        ankdownload.HRef = path;
        ankdownload.Visible = true;
        ScriptManager.RegisterStartupScript(this, GetType(), "savedrecords", "alert('Save Successfully!!')", true);
        //ICDMComm cdm = new ICDMComm();
        //cdm.SaveFile(Server.MapPath("~\\Excelfile\\") + FileUpload1.FileName, FileUpload1.FileBytes, "aaaara1982");
    }
    private void binddata()
    {
        objstudent = new studentinvoice();
        DataTable dtclass = new DataTable();
        dtclass = objstudent.GetClasses(this.InstanceID);
        if (dtclass.Rows.Count > 0)
        {
            ddlclass.DataSource = dtclass;
            ddlclass.DataValueField = "ID";
            ddlclass.DataTextField = "Title";
            ddlclass.DataBind();
            ddlclass.Items.Insert(0, new ListItem("Select", "0"));
        }

        objstudent = new studentinvoice();
        DataTable dtvoucher = new DataTable();
        dtvoucher = objstudent.Getfeevouchertemplate(this.InstanceID);
        if (dtvoucher.Rows.Count > 0)
        {
            hftemplate.Value = dtvoucher.Rows[0]["templateid"].ToString();
        }
    }


    private StringBuilder SendInvoicetemp1(string Studentid, string InstanceID, string PID, string parentemail, string schoolname)
    {
        try
        {
            DataSet ds = new DataSet();
            AvaimaEmailAPI objemail = new AvaimaEmailAPI();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                string query = "feesvoucher";
                using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
                {
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.AddWithValue("@instanceid", InstanceID);
                    da.SelectCommand.Parameters.AddWithValue("@studentid", Studentid);
                    da.SelectCommand.Parameters.AddWithValue("@PID", PID);
                    da.Fill(ds);
                }
            }
            StringBuilder HTML = new StringBuilder();
            string headerfile = "";
            string Footer = "";
            string lblclass = "";
            string lblissuedate = "";
            string lblregno = "";
            string lblstudentname = "";
            string lblfathername = "";
            string lblslipno = "";
            int lblsession = 0;
            string lblperiod = "";
            string lblduedate = "";
            string lbldueamount = "";
            string fees = "0";
            string charges = "0";
            string Discount = "0";
            string ecomment = "";
            string ecommentamount = "0";
            string extradiscount = "";
            string extradiscountamount = "0";
            string adjustmentcomment = "";
            string adjustmentcommentamount = "0";
            decimal balanceval = 0;
            decimal classfees = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                objuploader = new AvaimaStorageUploader();
                headerfile = objuploader.Singlefilelink(InstanceID, ds.Tables[0].Rows[0]["headerfile"].ToString());
                Footer = ds.Tables[0].Rows[0]["footer"].ToString();
            }
            if (ds.Tables[2].Rows.Count > 0)
            {
                lblclass = ds.Tables[2].Rows[0]["title"].ToString();
            }
            if (ds.Tables[1].Rows.Count > 0)
            {
                lblissuedate = Convert.ToDateTime(ds.Tables[1].Rows[0]["issuedate"].ToString()).ToString("dd-MMMM-yyyy");
                lblregno = ds.Tables[1].Rows[0]["Regno"].ToString();
                lblstudentname = ds.Tables[1].Rows[0]["studentname"].ToString();
                lblfathername = ds.Tables[1].Rows[0]["ParentName"].ToString();
            }
            if (ds.Tables[3].Rows.Count > 0)
            {
                lblslipno = ds.Tables[3].Rows[0]["recid"].ToString();
                objinvoice = new studentparameter();
                dttable = objinvoice.getstudentinvoiceadjust(ds.Tables[3].Rows[0]["recid"].ToString());

                DateTime dtstart = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
                if (ds.Tables[0].Rows.Count > 0 && dtstart.Month >= Convert.ToInt32(ds.Tables[0].Rows[0]["month"].ToString()))
                {
                    lblsession = dtstart.Year;
                }
                else
                {
                    lblsession = (dtstart.Year - 1);
                }

                lblperiod = dtstart.ToString("MMMM - yyyy");

                fees = ds.Tables[3].Rows[0]["TotalPackagefees"].ToString();
                classfees = Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalPackagefees"].ToString());
                decimal vale = Convert.ToDecimal(ds.Tables[4].Rows[0]["Totalfees"].ToString()) - Convert.ToDecimal(ds.Tables[4].Rows[0]["feespaid"].ToString()) + Convert.ToDecimal(ds.Tables[4].Rows[0]["charges"].ToString()) - Convert.ToDecimal(ds.Tables[4].Rows[0]["discount"].ToString());

                balanceval = vale - Convert.ToDecimal(ds.Tables[3].Rows[0]["Totalfees"].ToString());
                totalfees = Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalPackagefees"].ToString());
                if (Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString()) > 0)
                {
                    charges = ds.Tables[3].Rows[0]["TotalCharges"].ToString();
                    totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString());
                }
                if (Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString()) > 0)
                {
                    Discount = ds.Tables[3].Rows[0]["Totaldiscount"].ToString();
                    totalfees -= Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString());
                }
                if (dttable.Rows.Count > 0)
                {
                    for (int asd = 0; asd < dttable.Rows.Count; asd++)
                    {
                        if (dttable.Rows[asd]["type"].ToString() == "2")
                        {
                            totalfees += Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                            balanceval = balanceval - Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                        }
                        else if (dttable.Rows[asd]["type"].ToString() == "1")
                        {
                            totalfees -= Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                            balanceval = balanceval + Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                        }

                    }
                }
                else
                {
                    if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString()) > 0)
                    {
                        ecomment = ds.Tables[3].Rows[0]["chargescomment"].ToString();
                        ecommentamount = ds.Tables[3].Rows[0]["extracharges"].ToString();
                        totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString());
                    }
                    if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString()) > 0)
                    {
                        extradiscount = ds.Tables[3].Rows[0]["discountcomment"].ToString();
                        extradiscountamount = ds.Tables[3].Rows[0]["extradiscount"].ToString();
                        totalfees -= Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString());
                    }
                }
                if (Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString()) > 0)
                {
                    adjustmentcomment = ds.Tables[3].Rows[0]["adjustmentcomment"].ToString();
                    adjustmentcommentamount = ds.Tables[3].Rows[0]["adjustment"].ToString();
                    totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString());
                }
                totalfees = totalfees + balanceval;
                if (ds.Tables[5].Rows.Count > 0)
                {
                    DateTime enddate = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
                    string[] duedate = ds.Tables[5].Rows[0]["Fee_DueDate"].ToString().Split('#');
                    string[] afterduedate = ds.Tables[5].Rows[0]["LateFeesStartDate"].ToString().Split('#');
                    enddate = enddate.AddDays(Convert.ToDouble(duedate[0]));
                    enddate = enddate.AddDays(Convert.ToDouble(afterduedate[0]));
                    lblduedate = enddate.ToString("dd-MMM-yyyy");

                    decimal latefee = 0;
                    try
                    {
                        if (enddate > DateTime.Now)
                            latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["LateFeeCharges"].ToString());
                    }
                    catch
                    {
                        if (enddate > DateTime.Now)
                            latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["latefees"].ToString());
                    }
                    latefee = latefee + totalfees;
                    lbldueamount = latefee.ToString();
                }

                /******************************/

                HTML.AppendLine("<div style=\"width: 285px; border: 1px solid #000; float: left; margin-right: 7px\">");
                HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; height: 63px; width: 100%; position: relative\"><div style=\"position: absolute; right: 5px; top: 3px;\">BANK COPY</div>");
                HTML.AppendLine("<div style=\"font-size: 22px; margin: 13px 0 0 0; text-align: center; display: inline-block; width: 100%; line-height: 40px;\">");
                HTML.AppendLine("<img src=\"" + headerfile + "\" alt=\"img\"/></div> </div>");
                HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; width: 275px; padding: 3px 5px;\">");
                HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">GR. No<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
                HTML.AppendLine("<div style=\"width: 86px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblregno + "</div>");
                HTML.AppendLine("<div style=\"float: right; height: 18px; width: 90px; text-align: center; line-height: 18px; border: 1px solid #000; background: #ccc;\">Orignal Voucher</div>");
                HTML.AppendLine("<div style=\"clear: both;\"></div><div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Father Name<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
                HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblfathername + "</div><div style=\"clear: both;\"></div>");
                HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Student Name<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
                HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblstudentname + "</div><div style=\"clear: both;\"></div>");
                HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Class<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
                HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblclass + "</div><div style=\"clear: both;\"></div>");
                HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Class Fee<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
                HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + classfees + "</div> <div style=\"clear: both;\"></div>");
                HTML.AppendLine("<h2 style=\"text-align: center; padding: 0px; margin: 4px 0 0 0;\">Fee Voucher " + lblperiod + "</h2></div>");
                HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; width: 100%;\">");
                HTML.AppendLine("<table width=\"50%\" style=\"float:left; font-size: 10px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><th style=\"height: 24px; width: 25%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Month</th>");
                HTML.AppendLine("<th style=\"height: 24px; width: 10%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Vch#</th><th style=\"height: 24px; width: 15%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Amount</th></tr>");
                string ss = ggs(ds, lblsession);
                HTML.AppendLine(ss);
                HTML.AppendLine("<table width=\"100% \" border=\"0\" cellspacing=\"0\" Style=\"padding: 5px; margin: 5px 0 0 0; font-size: 10px;\">");
                HTML.AppendLine("<tr><td class=\"Tabletr\">Tuition  Fee " + lblperiod + "</td><td class=\"Tabletr\">Vch#" + lblslipno + " </td><td class=\"Tabletr\">" + fees + "</td></tr>");
                if (charges != "0")
                    HTML.AppendLine("<tr><td class=\"Tabletr\">Total Individual Charges</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\">" + charges + "</td></tr>");
                if (Discount != "0")
                    HTML.AppendLine("<tr><td class=\"Tabletr\">Total Individual Discount</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\">" + Discount + "</td></tr>");
                if (dttable.Rows.Count > 0)
                {
                    for (int asd = 0; asd < dttable.Rows.Count; asd++)
                    {
                        HTML.AppendLine("<tr><td class=\"Tabletr\">" + dttable.Rows[asd]["comments"].ToString() + "</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\">" + dttable.Rows[asd]["amount"].ToString() + "</td></tr>");
                    }
                }
                else
                {
                    if (ecommentamount != "0")
                        HTML.AppendLine("<tr><td class=\"Tabletr\">" + ecomment + "</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + ecommentamount + "</td></tr>");
                    if (extradiscountamount != "0")
                        HTML.AppendLine("<tr><td class=\"Tabletr\">" + extradiscount + "</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\">" + extradiscountamount + "</td></tr>");
                }
                if (adjustmentcommentamount != "0")
                    HTML.AppendLine("<tr><td class=\"Tabletr\">" + adjustmentcomment + "</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + adjustmentcommentamount + "</td></tr>");

                HTML.AppendLine("<tr><td class=\"Tabletr\">Previous Balance</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + balanceval.ToString() + "</td></tr>");
                HTML.AppendLine("<tr><td class=\"Tabletr\">Admission Fee</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\"></td></tr>");
                HTML.AppendLine("<tr><td class=\"Tabletr\">Annual Fee</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\"></td></tr>");
                HTML.AppendLine("<tr><td class=\"Tabletr\">Examination Fee Fee</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\"></td></tr>");
                HTML.AppendLine("<tr><td class=\"Tabletr\"><b>Total Due</b></td><td class=\"Tabletr\"> </td><td class=\"Tabletr\"><b>" + totalfees.ToString() + "</b></td></tr>");
                HTML.AppendLine("</table>");
                HTML.AppendLine("<table style=\"font-size: 10px;\">");
                HTML.AppendLine("<tr><td colspan=\"2\">Due Date: <b>" + lblduedate + "</b></td></tr>");
                HTML.AppendLine("<tr><td>Amount payable after due date</td><td style=\"text-align: right;\">" + lbldueamount + "</td></tr></table><br /><br /><br />");
                HTML.AppendLine("<p style=\"top: 43px; left: 17px; text-align: center;\"><b>Receiver Signature &amp; Stamp</b></p></div>");
                HTML.AppendLine("<div>" + Footer + "</div></div>");

                /********************************/
                HTML.AppendLine("<div style=\"width: 285px; border: 1px solid #000; float: left; margin-right: 7px\">");
                HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; height: 63px; width: 100%; position: relative\"><div style=\"position: absolute; right: 5px; top: 3px;\">SCHOOL COPY</div>");
                HTML.AppendLine("<div style=\"font-size: 22px; margin: 13px 0 0 0; text-align: center; display: inline-block; width: 100%; line-height: 40px;\">");
                HTML.AppendLine("<img src=\"" + headerfile + "\" alt=\"img\"/></div> </div>");
                HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; width: 275px; padding: 3px 5px;\">");
                HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">GR. No<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
                HTML.AppendLine("<div style=\"width: 86px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblregno + "</div>");
                HTML.AppendLine("<div style=\"float: right; height: 18px; width: 90px; text-align: center; line-height: 18px; border: 1px solid #000; background: #ccc;\">Orignal Voucher</div>");
                HTML.AppendLine("<div style=\"clear: both;\"></div><div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Father Name<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
                HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblfathername + "</div><div style=\"clear: both;\"></div>");
                HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Student Name<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
                HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblstudentname + "</div><div style=\"clear: both;\"></div>");
                HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Class<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
                HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblclass + "</div><div style=\"clear: both;\"></div>");
                HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Class Fee<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
                HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + classfees + "</div> <div style=\"clear: both;\"></div>");
                HTML.AppendLine("<h2 style=\"text-align: center; padding: 0px; margin: 4px 0 0 0;\">Fee Voucher " + lblperiod + "</h2></div>");
                HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; width: 100%;\">");
                HTML.AppendLine("<table width=\"50%\" style=\"float:left; font-size: 10px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><th style=\"height: 24px; width: 25%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Month</th>");
                HTML.AppendLine("<th style=\"height: 24px; width: 10%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Vch#</th><th style=\"height: 24px; width: 15%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Amount</th></tr>");
                HTML.AppendLine(ss);
                HTML.AppendLine("<table width=\"100% \" border=\"0\" cellspacing=\"0\" Style=\"padding: 5px; margin: 5px 0 0 0;font-size: 10px;\">");
                HTML.AppendLine("<tr><td class=\"Tabletr\">Tuition  Fee " + lblperiod + "</td><td class=\"Tabletr\">Vch#" + lblslipno + "      </td><td class=\"Tabletr\">" + fees + "</td></tr>");
                if (charges != "0")
                    HTML.AppendLine("<tr><td class=\"Tabletr\">Total Individual Charges</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + charges + "</td></tr>");
                if (Discount != "0")
                    HTML.AppendLine("<tr><td class=\"Tabletr\">Total Individual Discount</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + Discount + "</td></tr>");
                if (dttable.Rows.Count > 0)
                {
                    for (int asd = 0; asd < dttable.Rows.Count; asd++)
                    {
                        HTML.AppendLine("<tr><td class=\"Tabletr\">" + dttable.Rows[asd]["comments"].ToString() + "</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\">" + dttable.Rows[asd]["amount"].ToString() + "</td></tr>");
                    }
                }
                else
                {
                    if (ecommentamount != "0")
                        HTML.AppendLine("<tr><td class=\"Tabletr\">" + ecomment + "</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + ecommentamount + "</td></tr>");
                    if (extradiscountamount != "0")
                        HTML.AppendLine("<tr><td class=\"Tabletr\">" + extradiscount + "</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\">" + extradiscountamount + "</td></tr>");
                }
                if (adjustmentcommentamount != "0")
                    HTML.AppendLine("<tr><td class=\"Tabletr\">" + adjustmentcomment + "</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\">" + adjustmentcommentamount + "</td></tr>");

                HTML.AppendLine("<tr><td class=\"Tabletr\">Previous Balance</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + balanceval.ToString() + "</td></tr>");
                HTML.AppendLine("<tr><td class=\"Tabletr\">Admission Fee</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\"></td></tr>");
                HTML.AppendLine("<tr><td class=\"Tabletr\">Annual Fee</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\"></td></tr>");
                HTML.AppendLine("<tr><td class=\"Tabletr\">Examination Fee Fee</td><td class=\"Tabletr\">Vch#____________   </td><td class=\"Tabletr\"></td></tr>");
                HTML.AppendLine("<tr><td class=\"Tabletr\"><b>Total Due</b></td><td class=\"Tabletr\"></td><td class=\"Tabletr\"><b>" + totalfees.ToString() + "</b></td></tr>");
                HTML.AppendLine("</table>");
                HTML.AppendLine("<table style=\"font-size: 10px;\">");
                HTML.AppendLine("<tr><td colspan=\"2\">Due Date: <b>" + lblduedate + "</b></td></tr>");
                HTML.AppendLine("<tr><td>Amount payable after due date</td><td style=\"text-align: right;\">" + lbldueamount + "</td></tr></table><br /><br /><br />");
                HTML.AppendLine("<p style=\"top: 43px; left: 17px; text-align: center;\"><b>Receiver Signature &amp; Stamp</b></p></div>");
                HTML.AppendLine("<div>" + Footer + "</div></div>");

                /****************************/
                HTML.AppendLine("<div style=\"width: 285px; border: 1px solid #000; float: left; margin-right: 7px\">");
                HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; height: 63px; width: 100%; position: relative\"><div style=\"position: absolute; right: 5px; top: 3px;\">STUDENT'S COPY</div>");
                HTML.AppendLine("<div style=\"font-size: 22px; margin: 13px 0 0 0; text-align: center; display: inline-block; width: 100%; line-height: 40px;\">");
                HTML.AppendLine("<img src=\"" + headerfile + "\" alt=\"img\"/></div> </div>");
                HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; width: 275px; padding: 3px 5px;\">");
                HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">GR. No<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
                HTML.AppendLine("<div style=\"width: 86px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblregno + "</div>");
                HTML.AppendLine("<div style=\"float: right; height: 18px; width: 90px; text-align: center; line-height: 18px; border: 1px solid #000; background: #ccc;\">Orignal Voucher</div>");
                HTML.AppendLine("<div style=\"clear: both;\"></div><div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Father Name<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
                HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblfathername + "</div><div style=\"clear: both;\"></div>");
                HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Student Name<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
                HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblstudentname + "</div><div style=\"clear: both;\"></div>");
                HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Class<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
                HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblclass + "</div><div style=\"clear: both;\"></div>");
                HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Class Fee<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
                HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + classfees + "</div> <div style=\"clear: both;\"></div>");
                HTML.AppendLine("<h2 style=\"text-align: center; padding: 0px; margin: 4px 0 0 0;\">Fee Voucher " + lblperiod + "</h2></div>");
                HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; width: 100%;\">");
                HTML.AppendLine("<table width=\"50%\" style=\"float:left; font-size: 10px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><th style=\"height: 24px; width: 25%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Month</th>");
                HTML.AppendLine("<th style=\"height: 24px; width: 10%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Vch#</th><th style=\"height: 24px; width: 15%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Amount</th></tr>");
                HTML.AppendLine(ss);
                HTML.AppendLine("<table width=\"100% \" border=\"0\" cellspacing=\"0\" Style=\"padding: 5px; margin: 5px 0 0 0;font-size: 10px;\">");
                HTML.AppendLine("<tr><td class=\"Tabletr\">Tuition  Fee " + lblperiod + "</td><td class=\"Tabletr\">Vch# " + lblslipno + "    </td><td class=\"Tabletr\">" + fees + "</td></tr>");
                if (charges != "0")
                    HTML.AppendLine("<tr><td class=\"Tabletr\">Total Individual Charges</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + charges + "</td></tr>");
                if (Discount != "0")
                    HTML.AppendLine("<tr><td class=\"Tabletr\">Total Individual Discount</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + Discount + "</td></tr>");
                if (dttable.Rows.Count > 0)
                {
                    for (int asd = 0; asd < dttable.Rows.Count; asd++)
                    {
                        HTML.AppendLine("<tr><td class=\"Tabletr\">" + dttable.Rows[asd]["comments"].ToString() + "</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\">" + dttable.Rows[asd]["amount"].ToString() + "</td></tr>");
                    }
                }
                else
                {
                    if (ecommentamount != "0")
                        HTML.AppendLine("<tr><td class=\"Tabletr\">" + ecomment + "</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + ecommentamount + "</td></tr>");
                    if (extradiscountamount != "0")
                        HTML.AppendLine("<tr><td class=\"Tabletr\">" + extradiscount + "</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\">" + extradiscountamount + "</td></tr>");
                }
                if (adjustmentcommentamount != "0")
                    HTML.AppendLine("<tr><td class=\"Tabletr\">" + adjustmentcomment + "</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + adjustmentcommentamount + "</td></tr>");

                HTML.AppendLine("<tr><td class=\"Tabletr\">Previous Balance</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + balanceval.ToString() + "</td></tr>");
                HTML.AppendLine("<tr><td class=\"Tabletr\">Admission Fee</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\"></td></tr>");
                HTML.AppendLine("<tr><td class=\"Tabletr\">Annual Fee</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\"></td></tr>");
                HTML.AppendLine("<tr><td class=\"Tabletr\">Examination Fee Fee</td><td class=\"Tabletr\">Vch #     </td><td class=\"Tabletr\"></td></tr>");
                HTML.AppendLine("<tr><td class=\"Tabletr\"><b>Total Due</b></td><td class=\"Tabletr\"></td><td class=\"Tabletr\"><b>" + totalfees.ToString() + "</b></td></tr>");
                HTML.AppendLine("</table>");
                HTML.AppendLine("<table style=\"font-size: 10px;\">");
                HTML.AppendLine("<tr><td colspan=\"2\">Due Date: <b>" + lblduedate + "</b></td></tr>");
                HTML.AppendLine("<tr><td>Amount payable after due date</td><td style=\"text-align: right;\">" + lbldueamount + "</td></tr></table><br /><br /><br />");
                HTML.AppendLine("<p style=\"top: 43px; left: 17px; text-align: center;\"><b>Receiver Signature &amp; Stamp</b></p></div>");
                HTML.AppendLine("<div>" + Footer + "</div></div>");
                HTML.AppendLine("<div class=\"pageBreak\"></div> <br />");

            }
            return HTML;

        }
        catch (Exception ex)
        {
            StringBuilder HTML = new StringBuilder();
            return HTML;
        }
    }

    private string ggs(DataSet ds, int lblsession)
    {
        StringBuilder HTML = new StringBuilder();
        DataTable dthis = new DataTable();
        dthis = ds.Tables[6];
        for (int cell = 0; cell < 6; cell++)
        {
            string ss = getmonth(cell, lblsession);
            bool chdt = false;
            if (dthis.Rows.Count > 0)
            {
                for (int aasd = 0; aasd < dthis.Rows.Count; aasd++)
                {
                    if (ss == dthis.Rows[aasd]["months"].ToString())
                    {
                        HTML.AppendLine("<tr><td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dthis.Rows[aasd]["months"].ToString() + "</td>");
                        HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dthis.Rows[aasd]["recid"].ToString() + "</td>");
                        HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dthis.Rows[aasd]["totalfees"].ToString() + "</td></tr>");
                        dthis.Rows.RemoveAt(aasd);
                        chdt = true;
                    }
                }
            }
            if (!chdt)
            {
                HTML.AppendLine("<tr><td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + ss + "</td>");
                HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\"> </td>");
                HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\"> </td></tr>");
            }
        }
        HTML.AppendLine("</table>");
        HTML.AppendLine("<table width=\"50%\" style=\"float:right; font-size: 10px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><th style=\"height: 24px; width: 25%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Month</th>");
        HTML.AppendLine("<th style=\"height: 24px;width: 10%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Vch#</th><th style=\"height: 24px; width: 15%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Amount</th></tr>");

        for (int cell = 6; cell < 12; cell++)
        {
            string ss = getmonth(cell, lblsession);
            bool chdt1 = false;
            if (dthis.Rows.Count > 0)
            {
                for (int assd = 0; assd < dthis.Rows.Count; assd++)
                {
                    if (ss == dthis.Rows[assd]["months"].ToString())
                    {
                        HTML.AppendLine("<tr><td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dthis.Rows[assd]["months"].ToString() + "</td>");
                        HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dthis.Rows[assd]["recid"].ToString() + "</td>");
                        HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dthis.Rows[assd]["totalfees"].ToString() + "</td></tr>");
                        dthis.Rows.RemoveAt(assd);
                        chdt1 = true;
                    }
                }
            }
            if (!chdt1)
            {
                HTML.AppendLine("<tr><td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + ss + "</td>");
                HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\"> </td>");
                HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\"> </td></tr>");
            }
        }
        HTML.AppendLine("</table>");

        return HTML.ToString();
    }
    private string getmonth(int month, int year)
    {
        int mon = month + 3;
        if (mon > 12)
        {
            mon = mon - 12;
            year = year + 1;
        }
        DateTime dd = Convert.ToDateTime(mon + "/1/" + year);
        return dd.ToString("MMM - yyyy");
    }
    private StringBuilder SendInvoicetemp2(string Studentid, string InstanceID, string PID, string parentemail, string schoolname)
    {
        try
        {
            DataSet ds = new DataSet();
            AvaimaEmailAPI objemail = new AvaimaEmailAPI();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                string query = "feesvoucher";
                using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
                {
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.AddWithValue("@instanceid", InstanceID);
                    da.SelectCommand.Parameters.AddWithValue("@studentid", Studentid);
                    da.SelectCommand.Parameters.AddWithValue("@PID", PID);
                    da.Fill(ds);
                }
            }
            string headerfile = "";
            string Footer = "";
            string lblclass = "";
            string lblissuedate = "";
            string lblregno = "";
            string lblstudentname = "";
            string lblfathername = "";
            string lblslipno = "";
            string lblsession = "";
            string lblperiod = "";
            string lblduedate = "";
            string lbldueamount = "";
            string fees = "0";
            string charges = "0";
            string Discount = "0";
            string ecomment = "";
            string ecommentamount = "0";
            string extradiscount = "";
            string extradiscountamount = "0";
            string adjustmentcomment = "";
            string adjustmentcommentamount = "0";
            decimal balanceval = 0;
            decimal classfees = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                objuploader = new AvaimaStorageUploader();
                headerfile = objuploader.Singlefilelink(InstanceID, ds.Tables[0].Rows[0]["headerfile"].ToString());
                Footer = ds.Tables[0].Rows[0]["footer"].ToString();
            }
            if (ds.Tables[2].Rows.Count > 0)
            {
                lblclass = ds.Tables[2].Rows[0]["title"].ToString();
            }
            if (ds.Tables[1].Rows.Count > 0)
            {
                lblissuedate = Convert.ToDateTime(ds.Tables[1].Rows[0]["issuedate"].ToString()).ToString("dd MMMM, yyyy");
                lblregno = ds.Tables[1].Rows[0]["Regno"].ToString();
                lblstudentname = ds.Tables[1].Rows[0]["studentname"].ToString();
                lblfathername = ds.Tables[1].Rows[0]["ParentName"].ToString();
            }
            if (ds.Tables[3].Rows.Count > 0)
            {
                lblslipno = ds.Tables[3].Rows[0]["recid"].ToString();
                objinvoice = new studentparameter();
                dttable = objinvoice.getstudentinvoiceadjust(ds.Tables[3].Rows[0]["recid"].ToString());

                DateTime dtstart = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
                if (ds.Tables[0].Rows.Count > 0 && dtstart.Month >= Convert.ToInt32(ds.Tables[0].Rows[0]["month"].ToString()))
                {
                    lblsession = dtstart.Year + "-" + (dtstart.Year + 1);
                }
                else
                {
                    lblsession = (dtstart.Year - 1) + "-" + dtstart.Year;
                }

                lblperiod = dtstart.ToString("MMMM - yyyy");

                fees = ds.Tables[3].Rows[0]["TotalPackagefees"].ToString();

                classfees = Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalPackagefees"].ToString());
                decimal vale = Convert.ToDecimal(ds.Tables[4].Rows[0]["Totalfees"].ToString()) - Convert.ToDecimal(ds.Tables[4].Rows[0]["feespaid"].ToString()) + Convert.ToDecimal(ds.Tables[4].Rows[0]["charges"].ToString()) - Convert.ToDecimal(ds.Tables[4].Rows[0]["discount"].ToString());

                balanceval = vale - Convert.ToDecimal(ds.Tables[3].Rows[0]["Totalfees"].ToString());
                totalfees = Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalPackagefees"].ToString());
                if (Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString()) > 0)
                {
                    charges = ds.Tables[3].Rows[0]["TotalCharges"].ToString();
                    totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString());
                }
                if (Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString()) > 0)
                {
                    Discount = ds.Tables[3].Rows[0]["Totaldiscount"].ToString();
                    totalfees -= Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString());
                }
                if (dttable.Rows.Count > 0)
                {
                    for (int asd = 0; asd < dttable.Rows.Count; asd++)
                    {
                        if (dttable.Rows[asd]["type"].ToString() == "2")
                        {
                            totalfees += Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                            balanceval = balanceval - Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                        }
                        else if (dttable.Rows[asd]["type"].ToString() == "1")
                        {
                            totalfees -= Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                            balanceval = balanceval + Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                        }
                    }
                }
                else
                {
                    if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString()) > 0)
                    {
                        ecomment = ds.Tables[3].Rows[0]["chargescomment"].ToString();
                        ecommentamount = ds.Tables[3].Rows[0]["extracharges"].ToString();
                        totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString());
                    }
                    if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString()) > 0)
                    {
                        extradiscount = ds.Tables[3].Rows[0]["discountcomment"].ToString();
                        extradiscountamount = ds.Tables[3].Rows[0]["extradiscount"].ToString();
                        totalfees -= Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString());
                    }
                }
                if (Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString()) > 0)
                {
                    adjustmentcomment = ds.Tables[3].Rows[0]["adjustmentcomment"].ToString();
                    adjustmentcommentamount = ds.Tables[3].Rows[0]["adjustment"].ToString();
                    if (InstanceID != "9b36e01e-9d84-45d2-9e53-c4830ef3c145")
                        totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString());
                }
                totalfees = totalfees + balanceval;
            }
            if (ds.Tables[5].Rows.Count > 0)
            {
                DateTime enddate = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
                string[] duedate = ds.Tables[5].Rows[0]["Fee_DueDate"].ToString().Split('#');
                string[] afterduedate = ds.Tables[5].Rows[0]["LateFeesStartDate"].ToString().Split('#');
                enddate = enddate.AddDays(Convert.ToDouble(duedate[0]));
                enddate = enddate.AddDays(Convert.ToDouble(afterduedate[0]));
                lblduedate = enddate.ToString("dd MMMM, yyyy");

                decimal latefee = 0;
                try
                {
                    if (enddate > DateTime.Now)
                        latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["LateFeeCharges"].ToString());
                }
                catch
                {
                    if (enddate > DateTime.Now)
                        latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["latefees"].ToString());
                }
                latefee = latefee + totalfees;
                lbldueamount = latefee.ToString();
            }

            /******************************/
            StringBuilder HTML = new StringBuilder();
            HTML.AppendLine("<div style=\"height: auto; width: 290px; border: 1px solid #000; float: left; margin-right: 5px\">");
            HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; height: 114px; width: 100%; position: relative\">");
            HTML.AppendLine("<div style=\"width: 110px; float: left; padding: 13px 0 0 13px;\"><img src=\"" + headerfile + "\" alt=\"img\"/></div>");
            HTML.AppendLine("<div style=\"width: 166px; float: left;\"><div style=\"border: 1px solid #000; height: 32px; width: 147px; padding: 1px; margin: 5px 8px 0 0px; float: right\">");
            HTML.AppendLine("<div style=\"width: 145px; height: 30px; line-height: 30px; border: 1px solid #000; font-size: 15px; text-align: center\">BANK COPY</div></div>");
            HTML.AppendLine("<div style=\"clear: both;\"></div><div style=\"font-size: 15px; font-weight: bold; line-height: 28px; padding: 10px 0 0 0;\">" + schoolname + "<br />Monthly Fee Challan</div></div></div>");
            HTML.AppendLine("<div style=\"width: 100%; height: 103px; border-bottom: 1px solid #000; font-size: 13px; font-weight: bold; line-height: 20px;\">" + Footer + " </div>");
            HTML.AppendLine("<div style=\"width: 100%;\"><div style=\"padding: 10px 0 0 10px; font-size: 13px; font-weight: bold; line-height: 21px;\">");
            HTML.AppendLine("Challan No. <span>" + lblslipno + "</span><br />");
            HTML.AppendLine("Name of Student: <span>" + lblstudentname + "</span><br />");
            HTML.AppendLine("Roll No. <span>" + lblregno + "</span><br /><br />");
            HTML.AppendLine("Billing Month: <span>" + lblperiod + "</span><br />");
            HTML.AppendLine("Issuance Date: <span>" + lblissuedate + "</span><br />");
            HTML.AppendLine("Due Date: <span>" + lblduedate + "</span><br /> </div>");
            HTML.AppendLine("<table width=\"96%\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" style=\"background-color: #000; font-size: 13px; font-weight: bold; margin: 20px 0 0 5px;\">");
            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Tuition  Fee </td><td style=\"background: #FFF; padding: 5px\">" + fees + "</td></tr>");
            if (charges != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Charges</td><td style=\"background: #FFF; padding: 5px\">" + charges + "</td></tr>");
            if (Discount != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Discount</td><td style=\"background: #FFF; padding: 5px\">" + Discount + "</td></tr>");
            if (dttable.Rows.Count > 0)
            {
                for (int asd = 0; asd < dttable.Rows.Count; asd++)
                {
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + dttable.Rows[asd]["comments"].ToString() + "</td><td style=\"background: #FFF; padding: 5px\">" + dttable.Rows[asd]["amount"].ToString() + "</td></tr>");
                }
            }
            else
            {
                if (ecommentamount != "0")
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + ecomment + "</td><td style=\"background: #FFF; padding: 5px\">" + ecommentamount + "</td></tr>");
                if (extradiscountamount != "0")
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + extradiscount + "</td><td style=\"background: #FFF; padding: 5px\">" + extradiscountamount + "</td></tr>");
            }
            if (adjustmentcommentamount != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + adjustmentcomment + "</td><td style=\"background: #FFF; padding: 5px\">" + adjustmentcommentamount + "</td></tr>");

            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Arrears</td><td style=\"background: #FFF; padding: 5px\">" + balanceval.ToString() + "</td></tr>");
            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\"><b>Total Dues</b></td><td style=\"background: #FFF; padding: 5px\"><b>" + totalfees.ToString() + "</b></td></tr>");
            HTML.AppendLine("</table></div>");
            HTML.AppendLine("<p style=\"font-size: 13px; font-weight: bold; text-align: center;\">Late Payment Surcharge: Rs. 10/- Per day.</p>");
            HTML.AppendLine("<p style=\"font-size: 13px; font-weight: bold; padding: 15px 0 0 10px; margin: 25px 0 0 0;\"><div style=\"font-size: 11px; color: #dfdfdf; font-weight: lighter;text-align:center;margin-bottom:25px;\">CASH ONLY</div>Bank Stamp<br />&amp; Signature: <span>_________________________</span><br /><br /></p></div>");
            /****************************/

            HTML.AppendLine("<div style=\"height: auto; width: 290px; border: 1px solid #000; float: left; margin-right: 5px\">");
            HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; height: 114px; width: 100%; position: relative\">");
            HTML.AppendLine("<div style=\"width: 110px; float: left; padding: 13px 0 0 13px;\"><img src=\"" + headerfile + "\" alt=\"img\"/></div>");
            HTML.AppendLine("<div style=\"width: 166px; float: left;\"><div style=\"border: 1px solid #000; height: 32px; width: 147px; padding: 1px; margin: 5px 8px 0 0px; float: right\">");
            HTML.AppendLine("<div style=\"width: 145px; height: 30px; line-height: 30px; border: 1px solid #000; font-size: 15px; text-align: center\">SCHOOL COPY</div></div>");
            HTML.AppendLine("<div style=\"clear: both;\"></div><div style=\"font-size: 15px; font-weight: bold; line-height: 28px; padding: 10px 0 0 0;\">" + schoolname + "<br />Monthly Fee Challan</div></div></div>");
            HTML.AppendLine("<div style=\"width: 100%; height: 103px; border-bottom: 1px solid #000; font-size: 13px; font-weight: bold; line-height: 20px;\">" + Footer + " </div>");
            HTML.AppendLine("<div style=\"width: 100%;\"><div style=\"padding: 10px 0 0 10px; font-size: 13px; font-weight: bold; line-height: 21px;\">");
            HTML.AppendLine("Challan No. <span>" + lblslipno + "</span><br />");
            HTML.AppendLine("Name of Student: <span>" + lblstudentname + "</span><br />");
            HTML.AppendLine("Roll No. <span>" + lblregno + "</span><br /><br />");
            HTML.AppendLine("Billing Month: <span>" + lblperiod + "</span><br />");
            HTML.AppendLine("Issuance Date: <span>" + lblissuedate + "</span><br />");
            HTML.AppendLine("Due Date: <span>" + lblduedate + "</span><br /> </div>");
            HTML.AppendLine("<table width=\"96%\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" style=\"background-color: #000; font-size: 13px; font-weight: bold; margin: 20px 0 0 5px;\">");
            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Tuition  Fee </td><td style=\"background: #FFF; padding: 5px\">" + fees + "</td></tr>");
            if (charges != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Charges</td><td style=\"background: #FFF; padding: 5px\">" + charges + "</td></tr>");
            if (Discount != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Discount</td><td style=\"background: #FFF; padding: 5px\">" + Discount + "</td></tr>");
            if (dttable.Rows.Count > 0)
            {
                for (int asd = 0; asd < dttable.Rows.Count; asd++)
                {
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + dttable.Rows[asd]["comments"].ToString() + "</td><td style=\"background: #FFF; padding: 5px\">" + dttable.Rows[asd]["amount"].ToString() + "</td></tr>");
                }
            }
            else
            {
                if (ecommentamount != "0")
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + ecomment + "</td><td style=\"background: #FFF; padding: 5px\">" + ecommentamount + "</td></tr>");
                if (extradiscountamount != "0")
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + extradiscount + "</td><td style=\"background: #FFF; padding: 5px\">" + extradiscountamount + "</td></tr>");
            }
            if (adjustmentcommentamount != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + adjustmentcomment + "</td><td style=\"background: #FFF; padding: 5px\">" + adjustmentcommentamount + "</td></tr>");

            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Arrears</td><td style=\"background: #FFF; padding: 5px\">" + balanceval.ToString() + "</td></tr>");
            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\"><b>Total Dues</b></td><td style=\"background: #FFF; padding: 5px\"><b>" + totalfees.ToString() + "</b></td></tr>");
            HTML.AppendLine("</table></div>");
            HTML.AppendLine("<p style=\"font-size: 13px; font-weight: bold; text-align: center;\">Late Payment Surcharge: Rs. 10/- Per day.</p>");
            HTML.AppendLine("<p style=\"font-size: 13px; font-weight: bold; padding: 15px 0 0 10px; margin: 25px 0 0 0;\"><div style=\"font-size: 11px; color: #dfdfdf; font-weight: lighter;text-align:center;margin-bottom:25px;\">CASH ONLY</div>Bank Stamp<br />&amp; Signature: <span>_________________________</span><br /><br /></p></div>");
            /****************************/

            HTML.AppendLine("<div style=\"height: auto; width: 290px; border: 1px solid #000; float: left; margin-right: 5px\">");
            HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; height: 114px; width: 100%; position: relative\">");
            HTML.AppendLine("<div style=\"width: 110px; float: left; padding: 13px 0 0 13px;\"><img src=\"" + headerfile + "\" alt=\"img\"/></div>");
            HTML.AppendLine("<div style=\"width: 166px; float: left;\"><div style=\"border: 1px solid #000; height: 32px; width: 147px; padding: 1px; margin: 5px 8px 0 0px; float: right\">");
            HTML.AppendLine("<div style=\"width: 145px; height: 30px; line-height: 30px; border: 1px solid #000; font-size: 15px; text-align: center\">STUDENT'S COPY</div></div>");
            HTML.AppendLine("<div style=\"clear: both;\"></div><div style=\"font-size: 15px; font-weight: bold; line-height: 28px; padding: 10px 0 0 0;\">" + schoolname + "<br />Monthly Fee Challan</div></div></div>");
            HTML.AppendLine("<div style=\"width: 100%; height: 103px; border-bottom: 1px solid #000; font-size: 13px; font-weight: bold; line-height: 20px;\">" + Footer + " </div>");
            HTML.AppendLine("<div style=\"width: 100%;\"><div style=\"padding: 10px 0 0 10px; font-size: 13px; font-weight: bold; line-height: 21px;\">");
            HTML.AppendLine("Challan No. <span>" + lblslipno + "</span><br />");
            HTML.AppendLine("Name of Student: <span>" + lblstudentname + "</span><br />");
            HTML.AppendLine("Roll No. <span>" + lblregno + "</span><br /><br />");
            HTML.AppendLine("Billing Month: <span>" + lblperiod + "</span><br />");
            HTML.AppendLine("Issuance Date: <span>" + lblissuedate + "</span><br />");
            HTML.AppendLine("Due Date: <span>" + lblduedate + "</span><br /> </div>");
            HTML.AppendLine("<table width=\"96%\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" style=\"background-color: #000; font-size: 13px; font-weight: bold; margin: 20px 0 0 5px;\">");
            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Tuition  Fee </td><td style=\"background: #FFF; padding: 5px\">" + fees + "</td></tr>");
            if (charges != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Charges</td><td style=\"background: #FFF; padding: 5px\">" + charges + "</td></tr>");
            if (Discount != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Discount</td><td style=\"background: #FFF; padding: 5px\">" + Discount + "</td></tr>");
            if (dttable.Rows.Count > 0)
            {
                for (int asd = 0; asd < dttable.Rows.Count; asd++)
                {
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + dttable.Rows[asd]["comments"].ToString() + "</td><td style=\"background: #FFF; padding: 5px\">" + dttable.Rows[asd]["amount"].ToString() + "</td></tr>");
                }
            }
            else
            {
                if (ecommentamount != "0")
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + ecomment + "</td><td style=\"background: #FFF; padding: 5px\">" + ecommentamount + "</td></tr>");
                if (extradiscountamount != "0")
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + extradiscount + "</td><td style=\"background: #FFF; padding: 5px\">" + extradiscountamount + "</td></tr>");
            }
            if (adjustmentcommentamount != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + adjustmentcomment + "</td><td style=\"background: #FFF; padding: 5px\">" + adjustmentcommentamount + "</td></tr>");

            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Arrears</td><td style=\"background: #FFF; padding: 5px\">" + balanceval.ToString() + "</td></tr>");
            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\"><b>Total Dues</b></td><td style=\"background: #FFF; padding: 5px\"><b>" + totalfees.ToString() + "</b></td></tr>");
            HTML.AppendLine("</table></div>");
            HTML.AppendLine("<p style=\"font-size: 13px; font-weight: bold; text-align: center;\">Late Payment Surcharge: Rs. 10/- Per day.</p>");
            HTML.AppendLine("<p style=\"font-size: 13px; font-weight: bold; padding: 15px 0 0 10px; margin: 25px 0 0 0;\"><div style=\"font-size: 11px; color: #dfdfdf; font-weight: lighter;text-align:center;margin-bottom:25px;\">CASH ONLY</div>Bank Stamp<br />&amp; Signature: <span>_________________________</span><br /><br /></p></div>");
            HTML.AppendLine("<div class=\"pageBreak\"></div> <br />");
            /****************************/

            return HTML;
        }
        catch (Exception ex)
        {
            StringBuilder HTML = new StringBuilder();
            return HTML;
        }
    }

    private StringBuilder SendInvoicetemp3(string Studentid, string InstanceID, string PID, string parentemail, string schoolname)
    {
        try
        {
            DataSet ds = new DataSet();
            AvaimaEmailAPI objemail = new AvaimaEmailAPI();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                string query = "feesvoucher";
                using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
                {
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.AddWithValue("@instanceid", InstanceID);
                    da.SelectCommand.Parameters.AddWithValue("@studentid", Studentid);
                    da.SelectCommand.Parameters.AddWithValue("@PID", PID);
                    da.Fill(ds);
                }
            }
            string headerfile = "";
            string Footer = "";
            string lblclass = "";
            string lblissuedate = "";
            string lblregno = "";
            string lblstudentname = "";
            string lblfathername = "";
            string lblslipno = "";
            string lblsession = "";
            string lblperiod = "";
            string lblduedate = "";
            string lbldueamount = "";
            string fees = "0";
            string charges = "0";
            string Discount = "0";
            string ecomment = "";
            string ecommentamount = "0";
            string extradiscount = "";
            string extradiscountamount = "0";
            string adjustmentcomment = "";
            string adjustmentcommentamount = "0";
            decimal balanceval = 0;
            decimal classfees = 0;
            string Label3 = "";
            if (ds.Tables[0].Rows.Count > 0)
            {
                objuploader = new AvaimaStorageUploader();
                headerfile = objuploader.Singlefilelink(InstanceID, ds.Tables[0].Rows[0]["headerfile"].ToString());
                Footer = ds.Tables[0].Rows[0]["footer"].ToString();
            }
            if (ds.Tables[2].Rows.Count > 0)
            {
                lblclass = ds.Tables[2].Rows[0]["title"].ToString();
            }
            if (ds.Tables[1].Rows.Count > 0)
            {

                //lblissuedate = Convert.ToDateTime(ds.Tables[1].Rows[0]["issuedate"].ToString()).ToString("dd MMMM, yyyy");
                lblregno = ds.Tables[1].Rows[0]["Regno"].ToString();
                lblstudentname = ds.Tables[1].Rows[0]["studentname"].ToString();
                lblfathername = ds.Tables[1].Rows[0]["ParentName"].ToString();
            }
            if (ds.Tables[3].Rows.Count > 0)
            {
                lblslipno = ds.Tables[3].Rows[0]["recid"].ToString();
                objinvoice = new studentparameter();
                dttable = objinvoice.getstudentinvoiceadjust(ds.Tables[3].Rows[0]["recid"].ToString());

                if (DateTime.Now.Day < 21 || DateTime.Now.Month < Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString()).Month)
                {
                    lblissuedate = Convert.ToDateTime(new DateTime(Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString()).Year, Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString()).Month, 1)).ToString("dd MMMM, yyyy");
                    //lblissuedate = Convert.ToDateTime(new DateTime(2016, 5, 28)).ToString("dd MMMM, yyyy");
                    Label3 = Convert.ToDateTime(new DateTime(Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString()).Year, Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString()).Month, 20)).ToString("dd MMMM, yyyy");
                }
                else
                {
                    lblissuedate = DateTime.Now.ToString("dd MMMM, yyyy");
                    //lblissuedate = Convert.ToDateTime(new DateTime(2016, 5, 28)).ToString("dd MMMM, yyyy");
                    Label3 = Convert.ToDateTime(new DateTime(Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString()).Year, Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString()).Month, 25)).ToString("dd MMMM, yyyy");
                }


                DateTime dtstart = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
                if (ds.Tables[0].Rows.Count > 0 && dtstart.Month >= Convert.ToInt32(ds.Tables[0].Rows[0]["month"].ToString()))
                {
                    lblsession = dtstart.Year + "-" + (dtstart.Year + 1);
                }
                else
                {
                    lblsession = (dtstart.Year - 1) + "-" + dtstart.Year;
                }

                lblperiod = dtstart.ToString("MMMM, yyyy");

                fees = ds.Tables[3].Rows[0]["TotalPackagefees"].ToString();

                classfees = Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalPackagefees"].ToString());
                decimal vale = Convert.ToDecimal(ds.Tables[4].Rows[0]["Totalfees"].ToString()) - Convert.ToDecimal(ds.Tables[4].Rows[0]["feespaid"].ToString()) + Convert.ToDecimal(ds.Tables[4].Rows[0]["charges"].ToString()) - Convert.ToDecimal(ds.Tables[4].Rows[0]["discount"].ToString());
                balanceval = vale - Convert.ToDecimal(ds.Tables[3].Rows[0]["Totalfees"].ToString());
                totalfees = Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalPackagefees"].ToString());
                if (Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString()) > 0)
                {
                    charges = ds.Tables[3].Rows[0]["TotalCharges"].ToString();
                    totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString());
                }
                if (Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString()) > 0)
                {
                    Discount = ds.Tables[3].Rows[0]["Totaldiscount"].ToString();
                    totalfees -= Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString());
                }
                if (dttable.Rows.Count > 0)
                {
                    for (int asd = 0; asd < dttable.Rows.Count; asd++)
                    {
                        if (dttable.Rows[asd]["type"].ToString() == "2")
                        {
                            totalfees += Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                            balanceval = balanceval - Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                        }
                        else if (dttable.Rows[asd]["type"].ToString() == "1")
                        {
                            totalfees -= Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                            balanceval = balanceval + Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                        }
                    }
                }
                else
                {
                    if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString()) > 0)
                    {
                        ecomment = ds.Tables[3].Rows[0]["chargescomment"].ToString();
                        ecommentamount = ds.Tables[3].Rows[0]["extracharges"].ToString();
                        totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString());
                    }
                    if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString()) > 0)
                    {
                        extradiscount = ds.Tables[3].Rows[0]["discountcomment"].ToString();
                        extradiscountamount = ds.Tables[3].Rows[0]["extradiscount"].ToString();
                        totalfees -= Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString());
                    }
                }
                if (Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString()) > 0)
                {
                    adjustmentcomment = ds.Tables[3].Rows[0]["adjustmentcomment"].ToString();
                    adjustmentcommentamount = ds.Tables[3].Rows[0]["adjustment"].ToString();
                    if (InstanceID != "9b36e01e-9d84-45d2-9e53-c4830ef3c145")
                        totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString());
                }
                totalfees = totalfees + balanceval;
            }
            if (ds.Tables[5].Rows.Count > 0)
            {
                DateTime enddate = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
                string[] duedate = ds.Tables[5].Rows[0]["Fee_DueDate"].ToString().Split('#');
                string[] afterduedate = ds.Tables[5].Rows[0]["LateFeesStartDate"].ToString().Split('#');
                enddate = enddate.AddDays(Convert.ToDouble(duedate[0]));
                enddate = enddate.AddDays(Convert.ToDouble(afterduedate[0]));
                lblduedate = enddate.ToString("dd MMMM, yyyy");

                decimal latefee = 0;
                try
                {
                    if (enddate > DateTime.Now)
                        latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["LateFeeCharges"].ToString());
                }
                catch
                {
                    if (enddate > DateTime.Now)
                        latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["latefees"].ToString());
                }
                if (latefee == 0)
                    latefee = 100;
                latefee = latefee + totalfees;
                lbldueamount = latefee.ToString();
            }
            string image = @"<img alt="""" src=""http://avaima.blob.core.windows.net/data/apps/b2303cf1-32f3-439d-9753-4a1a0748a376/1146c337-2de0-45a0-af97-99674ceedb73/user_data/uploads/bank-logo.jpg"" />";
            /******************************/
            StringBuilder HTML = new StringBuilder();
            HTML.AppendLine("<div style=\"height: auto; width: 290px; border: 1px solid #000; float: left; margin-right: 5px\">");
            HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; height: 114px; width: 100%; position: relative\">");
            HTML.AppendLine("<div style=\"width: 110px; float: left; padding: 13px 0 0 13px;\"><img src=\"" + headerfile + "\" alt=\"img\"/></div>");
            HTML.AppendLine("<div style=\"width: 166px; float: left;\">");
            //HTML.AppendLine(image + "</div>");
            HTML.AppendLine("<div style=\"clear: both;\"></div><div style=\"font-size: 15px; font-weight: bold; line-height: 17px; padding: 35px 0 0 5px;\">" + schoolname + "<br />Monthly Fee Challan</div></div></div>");
            HTML.AppendLine("<div style=\"width: 100%; height: 103px; border-bottom: 1px solid #000; font-size: 13px; font-weight: lighter; line-height: 20px;\">" + Footer + " </div>");
            HTML.AppendLine("<div style=\"width: 100%;\"><div style=\"padding: 10px 0 0 10px; font-size: 13px; font-weight: lighter; line-height: 21px;\">");
            HTML.AppendLine("Name of Student: <span style=\"text-decoration:underline;\">" + lblstudentname + "</span><br />");
            HTML.AppendLine("Roll No. <span>" + lblregno + "</span><br />");
            HTML.AppendLine("Class: <span>" + lblclass + "</span><br /><br />");
            HTML.AppendLine("Billing Month: <span>" + lblperiod + "</span><br />");
            HTML.AppendLine("Issuance Date: <span>" + lblissuedate + "</span><br />");
            HTML.AppendLine("Due Date: <span>" + lblduedate + "</span><br />");
            HTML.AppendLine("Challan Validity Date: <span>" + Label3 + "</span><br /> </div>");
            HTML.AppendLine("<table width=\"96%\" class=\"tablesdiv\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:96%;border-collapse:collapse;background-color: #000; font-size: 13px; font-weight: lighter; margin: 20px 0 0 5px;\">");
            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Tuition  Fee </td><td style=\"background: #FFF; padding: 5px\">" + fees + "</td></tr>");
            if (charges != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Charges</td><td style=\"background: #FFF; padding: 5px\">" + charges + "</td></tr>");
            if (Discount != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Discount</td><td style=\"background: #FFF; padding: 5px\">" + Discount + "</td></tr>");
            if (dttable.Rows.Count > 0)
            {
                for (int asd = 0; asd < dttable.Rows.Count; asd++)
                {
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + dttable.Rows[asd]["comments"].ToString() + "</td><td style=\"background: #FFF; padding: 5px\">" + dttable.Rows[asd]["amount"].ToString() + "</td></tr>");
                }
            }
            else
            {
                if (ecommentamount != "0")
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + ecomment + "</td><td style=\"background: #FFF; padding: 5px\">" + ecommentamount + "</td></tr>");
                if (extradiscountamount != "0")
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + extradiscount + "</td><td style=\"background: #FFF; padding: 5px\">" + extradiscountamount + "</td></tr>");
            }
            if (adjustmentcommentamount != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + adjustmentcomment + "</td><td style=\"background: #FFF; padding: 5px\">" + adjustmentcommentamount + "</td></tr>");

            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Arrears</td><td style=\"background: #FFF; padding: 5px\">" + balanceval.ToString() + "</td></tr>");
            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\"><b>Total Dues</b></td><td style=\"background: #FFF; padding: 5px; font-size=15x\"><b>" + totalfees.ToString() + "</b></td></tr>");
            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\"><b>Total Dues After Due Date</b></td><td style=\"background: #FFF; padding: 5px; font-size=15x\"><b>" + lbldueamount + "</b></td></tr>");
            HTML.AppendLine("</table></div>");
            HTML.AppendLine("<div style=\"font-size: 25px; color:lightgray; font-weight: lighter;text-align:center;margin-top:25px;\">CASH ONLY</div><p style=\"font-size: 13px; font-weight: lighter; padding: 15px 0 0 10px; margin: 0 0 0 0;\">Bank Stamp<br />&amp; Signature: <span>_________________________</span></p>");
            HTML.AppendLine("<div style=\"border: 1px solid #000; height: 32px; width: 147px; padding: 1px; margin: 5px 8px 0 0px; float: right\"><div style=\"width: 145px; height: 30px; line-height: 30px; border: 1px solid #000; font-size: 15px; text-align: center\">BANK COPY</div></div></div>");
            /****************************/

            HTML.AppendLine("<div style=\"height: auto; width: 290px; border: 1px solid #000; float: left; margin-right: 5px\">");
            HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; height: 114px; width: 100%; position: relative\">");
            HTML.AppendLine("<div style=\"width: 110px; float: left; padding: 13px 0 0 13px;\"><img src=\"" + headerfile + "\" alt=\"img\"/></div>");
            HTML.AppendLine("<div style=\"width: 166px; float: left;\">");
            //HTML.AppendLine(image + "</div>");
            HTML.AppendLine("<div style=\"clear: both;\"></div><div style=\"font-size: 15px; font-weight: bold; line-height: 17px; padding: 35px 0 0 5px;\">" + schoolname + "<br />Monthly Fee Challan</div></div></div>");
            HTML.AppendLine("<div style=\"width: 100%; height: 103px; border-bottom: 1px solid #000; font-size: 13px; font-weight: lighter; line-height: 20px;\">" + Footer + " </div>");
            HTML.AppendLine("<div style=\"width: 100%;\"><div style=\"padding: 10px 0 0 10px; font-size: 13px; font-weight: lighter; line-height: 21px;\">");
            HTML.AppendLine("Name of Student: <span style=\"text-decoration:underline;\">" + lblstudentname + "</span><br />");
            HTML.AppendLine("Roll No. <span>" + lblregno + "</span><br />");
            HTML.AppendLine("Class: <span>" + lblclass + "</span><br /><br />");
            HTML.AppendLine("Billing Month: <span>" + lblperiod + "</span><br />");
            HTML.AppendLine("Issuance Date: <span>" + lblissuedate + "</span><br />");
            HTML.AppendLine("Due Date: <span>" + lblduedate + "</span><br />");
            HTML.AppendLine("Challan Validity Date: <span>" + Label3 + "</span><br /> </div>");
            HTML.AppendLine("<table width=\"96%\" class=\"tablesdiv\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:96%;border-collapse:collapse;background-color: #000; font-size: 13px; font-weight: lighter; margin: 20px 0 0 5px;\">");
            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Tuition  Fee </td><td style=\"background: #FFF; padding: 5px\">" + fees + "</td></tr>");
            if (charges != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Charges</td><td style=\"background: #FFF; padding: 5px\">" + charges + "</td></tr>");
            if (Discount != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Discount</td><td style=\"background: #FFF; padding: 5px\">" + Discount + "</td></tr>");
            if (dttable.Rows.Count > 0)
            {
                for (int asd = 0; asd < dttable.Rows.Count; asd++)
                {
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + dttable.Rows[asd]["comments"].ToString() + "</td><td style=\"background: #FFF; padding: 5px\">" + dttable.Rows[asd]["amount"].ToString() + "</td></tr>");
                }
            }
            else
            {
                if (ecommentamount != "0")
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + ecomment + "</td><td style=\"background: #FFF; padding: 5px\">" + ecommentamount + "</td></tr>");
                if (extradiscountamount != "0")
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + extradiscount + "</td><td style=\"background: #FFF; padding: 5px\">" + extradiscountamount + "</td></tr>");
            }
            if (adjustmentcommentamount != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + adjustmentcomment + "</td><td style=\"background: #FFF; padding: 5px\">" + adjustmentcommentamount + "</td></tr>");

            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Arrears</td><td style=\"background: #FFF; padding: 5px\">" + balanceval.ToString() + "</td></tr>");
            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\"><b>Total Dues</b></td><td style=\"background: #FFF; padding: 5px; font-size=15x\"><b>" + totalfees.ToString() + "</b></td></tr>");
            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\"><b>Total Dues After Due Date</b></td><td style=\"background: #FFF; padding: 5px; font-size=15x\"><b>" + lbldueamount + "</b></td></tr>");
            HTML.AppendLine("</table></div>");
            HTML.AppendLine("<div style=\"font-size: 25px; color:lightgray; font-weight: lighter;text-align:center;margin-top:25px;\">CASH ONLY</div><p style=\"font-size: 13px; font-weight: lighter; padding: 15px 0 0 10px; margin: 0 0 0 0;\">Bank Stamp<br />&amp; Signature: <span>_________________________</span></p>");
            HTML.AppendLine("<div style=\"border: 1px solid #000; height: 32px; width: 147px; padding: 1px; margin: 5px 8px 0 0px; float: right\"><div style=\"width: 145px; height: 30px; line-height: 30px; border: 1px solid #000; font-size: 15px; text-align: center\">SCHOOL COPY</div></div></div>");
            /****************************/

            HTML.AppendLine("<div style=\"height: auto; width: 290px; border: 1px solid #000; float: left; margin-right: 5px\">");
            HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; height: 114px; width: 100%; position: relative\">");
            HTML.AppendLine("<div style=\"width: 110px; float: left; padding: 13px 0 0 13px;\"><img src=\"" + headerfile + "\" alt=\"img\"/></div>");
            HTML.AppendLine("<div style=\"width: 166px; float: left;\">");
            //HTML.AppendLine(image + "</div>");
            HTML.AppendLine("<div style=\"clear: both;\"></div><div style=\"font-size: 15px; font-weight: bold; line-height: 17px; padding: 35px 0 0 5px;\">" + schoolname + "<br />Monthly Fee Challan</div></div></div>");
            HTML.AppendLine("<div style=\"width: 100%; height: 103px; border-bottom: 1px solid #000; font-size: 13px; font-weight: lighter; line-height: 20px;\">" + Footer + " </div>");
            HTML.AppendLine("<div style=\"width: 100%;\"><div style=\"padding: 10px 0 0 10px; font-size: 13px; font-weight: lighter; line-height: 21px;\">");
            HTML.AppendLine("Name of Student: <span style=\"text-decoration:underline;\">" + lblstudentname + "</span><br />");
            HTML.AppendLine("Roll No. <span>" + lblregno + "</span><br />");
            HTML.AppendLine("Class: <span>" + lblclass + "</span><br /><br />");
            HTML.AppendLine("Billing Month: <span>" + lblperiod + "</span><br />");
            HTML.AppendLine("Issuance Date: <span>" + lblissuedate + "</span><br />");
            HTML.AppendLine("Due Date: <span>" + lblduedate + "</span><br />");
            HTML.AppendLine("Challan Validity Date: <span>" + Label3 + "</span><br /> </div>");
            HTML.AppendLine("<table width=\"96%\" class=\"tablesdiv\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:96%;border-collapse:collapse;background-color: #000; font-size: 13px; font-weight: lighter; margin: 20px 0 0 5px;\">");
            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Tuition  Fee </td><td style=\"background: #FFF; padding: 5px\">" + fees + "</td></tr>");
            if (charges != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Charges</td><td style=\"background: #FFF; padding: 5px\">" + charges + "</td></tr>");
            if (Discount != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Discount</td><td style=\"background: #FFF; padding: 5px\">" + Discount + "</td></tr>");
            if (dttable.Rows.Count > 0)
            {
                for (int asd = 0; asd < dttable.Rows.Count; asd++)
                {
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + dttable.Rows[asd]["comments"].ToString() + "</td><td style=\"background: #FFF; padding: 5px\">" + dttable.Rows[asd]["amount"].ToString() + "</td></tr>");
                }
            }
            else
            {
                if (ecommentamount != "0")
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + ecomment + "</td><td style=\"background: #FFF; padding: 5px\">" + ecommentamount + "</td></tr>");
                if (extradiscountamount != "0")
                    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + extradiscount + "</td><td style=\"background: #FFF; padding: 5px\">" + extradiscountamount + "</td></tr>");
            }
            if (adjustmentcommentamount != "0")
                HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + adjustmentcomment + "</td><td style=\"background: #FFF; padding: 5px\">" + adjustmentcommentamount + "</td></tr>");

            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Arrears</td><td style=\"background: #FFF; padding: 5px\">" + balanceval.ToString() + "</td></tr>");
            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\"><b>Total Dues</b></td><td style=\"background: #FFF; padding: 5px; font-size=15x\"><b>" + totalfees.ToString() + "</b></td></tr>");
            HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\"><b>Total Dues After Due Date</b></td><td style=\"background: #FFF; padding: 5px; font-size=15x\"><b>" + lbldueamount + "</b></td></tr>");
            HTML.AppendLine("</table></div>");
            HTML.AppendLine("<div style=\"font-size: 25px; color:lightgray; font-weight: lighter;text-align:center;margin-top:25px;\">CASH ONLY</div><p style=\"font-size: 13px; font-weight: lighter; padding: 15px 0 0 10px; margin: 0 0 0 0;\">Bank Stamp<br />&amp; Signature: <span>_________________________</span></p>");
            HTML.AppendLine("<div style=\"border: 1px solid #000; height: 32px; width: 147px; padding: 1px; margin: 5px 8px 0 0px; float: right\"><div style=\"width: 145px; height: 30px; line-height: 30px; border: 1px solid #000; font-size: 15px; text-align: center\">STUDENT COPY</div></div></div>");
            HTML.AppendLine("<div class=\"pageBreak\"></div> <br />");
            /****************************/

            return HTML;
        }
        catch (Exception ex)
        {
            StringBuilder HTML = new StringBuilder();
            return HTML;
        }
    }

    protected void CreateVoucher_Click(object sender, EventArgs e)
    {
       // CreateVoucher.Enabled = false;
        string year = DateTime.Now.Year.ToString();
        string date = year + "-" + ddlMonths.SelectedValue + "-01";
     //   feeswebservice.AutoGenerateInvoice(date, this.InstanceID);
       // lblmsg.Text = "Vouchers Created !";
        Button1.Enabled = true;
    }

    protected void CreateVouchers()
    {
        string year = DateTime.Now.Year.ToString();        
        string date = year + "-" + ddlMonths.SelectedValue + "-01";
        if (rbtnclass.Checked)
            feeswebservice.AutoGenerateInvoice(date, this.InstanceID, Convert.ToInt32(ddlclass.SelectedValue));
        else
            feeswebservice.AutoGenerateInvoice(date, this.InstanceID, 0);

        //feeswebservice.test();
    }

    //Bind Current Month & onwards 
    protected void BindMonths()
    {
        DateTime currentmonth = DateTime.Now;

        ddlMonths.Items.Add(new ListItem(currentmonth.ToString("MMMM") + " " + currentmonth.Year.ToString(), currentmonth.Month.ToString("00")));

        for (int i = 1; i < 12; i++)
        {
            currentmonth = currentmonth.AddMonths(1);
            ddlMonths.Items.Add(new ListItem(currentmonth.ToString("MMMM")+" "+currentmonth.Year.ToString(), currentmonth.Month.ToString("00")));
        }              
    }

}