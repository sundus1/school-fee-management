﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class InvoiceSetting : AvaimaThirdpartyTool.AvaimaWebPage
{
    AvaimaStorageUploader objuploader;
    protected void Page_Load(object sender, EventArgs e)
    {
        txtschool.Focus();
        if (!IsPostBack)
        {
            binddata();
        }
    }

    private void binddata()
    {
        DataTable dtuploadfile = new DataTable();
        objuploader = new AvaimaStorageUploader();
        dtuploadfile = getrecord();
        if (dtuploadfile.Rows.Count > 0)
        {
            string headerfile = objuploader.Singlefilelink(this.InstanceID, dtuploadfile.Rows[0]["headerfile"].ToString());
            string logofile = objuploader.Singlefilelink(this.InstanceID, dtuploadfile.Rows[0]["Logofile"].ToString());
            tr_header.Visible = true;
            tr_logo.Visible = true;
            img_header.ImageUrl = headerfile;
            ank_header.HRef = headerfile;
            ank_logo.HRef = logofile;
            img_logo.ImageUrl = logofile;
            btnsave.Visible = false;
            btnupdate.Visible = true;
            hfrecid.Value = dtuploadfile.Rows[0]["recid"].ToString();
            txtfooter.Text = dtuploadfile.Rows[0]["Footer"].ToString();
            txtschool.Text = dtuploadfile.Rows[0]["schoolname"].ToString();
            ddlmonth.SelectedValue = dtuploadfile.Rows[0]["bankname"].ToString();
            hfheader.Value = dtuploadfile.Rows[0]["headerfile"].ToString();
            hflogo.Value = dtuploadfile.Rows[0]["Logofile"].ToString();
            if (!String.IsNullOrEmpty(dtuploadfile.Rows[0]["headerfile"].ToString()) || !String.IsNullOrEmpty(dtuploadfile.Rows[0]["Logofile"].ToString()))
            {
                DataTable dtfiles = new DataTable();
                dtfiles = objuploader.Getuploadedfileinfo("8ee08d08-68ab-4415-8b50-9fa722151856", this.InstanceID);
                if (dtfiles.Rows.Count > 0)
                {
                    for (int sd = 0; sd < dtfiles.Rows.Count; sd++)
                    {
                        if (hfheader.Value == dtfiles.Rows[sd]["FilePath"].ToString())
                            hfheaderid.Value = dtfiles.Rows[sd]["recid"].ToString();
                        if (hflogo.Value == dtfiles.Rows[sd]["FilePath"].ToString())
                            hflogoid.Value = dtfiles.Rows[sd]["recid"].ToString();
                    }
                }
            }
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            objuploader = new AvaimaStorageUploader();
            string headername = "";
            string Logoname = "";
            if (fu_header.HasFile)
            {
                headername = fu_header.FileName;
                string[] header = fu_header.FileName.Split('.');
                objuploader.insertfileuploaded(header[0], header[1], "8ee08d08-68ab-4415-8b50-9fa722151856", HttpContext.Current.User.Identity.Name, this.InstanceID, this.InstanceID, fu_header.FileBytes);
            }
            if (fu_logo.HasFile)
            {
                Logoname = fu_logo.FileName;
                string[] logo = fu_logo.FileName.Split('.');
                objuploader.insertfileuploaded(logo[0], logo[1], "8ee08d08-68ab-4415-8b50-9fa722151856", HttpContext.Current.User.Identity.Name, this.InstanceID, this.InstanceID, fu_logo.FileBytes);
            }

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("insertInvoicesetting", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Footer", txtfooter.Text);
                    cmd.Parameters.AddWithValue("@headerfile", headername);
                    cmd.Parameters.AddWithValue("@Logofile", Logoname);
                    cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                    cmd.Parameters.AddWithValue("@bankname", ddlmonth.SelectedValue);
                    cmd.Parameters.AddWithValue("@schoolname", txtschool.Text);

                    cmd.ExecuteNonQuery();
                   
                    ScriptManager.RegisterStartupScript(this, GetType(), "updatealert", "alert('Setting Saved Sucessfully !')", true);
                }
               
                con.Close();
            }
            binddata();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private DataTable getrecord()
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT *  FROM SchoolFeesInvoicesetting WHERE instanceid=@id", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", this.InstanceID);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    return dt;
                }
            }
        }
        catch (Exception)
        {

            throw;
        }

    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            objuploader = new AvaimaStorageUploader();
            string headername = "";
            string Logoname = "";
            if (fu_header.HasFile)
            {
                if (hfheaderid.Value != "0")
                    objuploader.Deleteuploadedfile(hfheaderid.Value);

                objuploader.filedelete(this.InstanceID, hfheader.Value);
                headername = fu_header.FileName;
                string[] header = fu_header.FileName.Split('.');
                objuploader.insertfileuploaded(header[0], header[1], "8ee08d08-68ab-4415-8b50-9fa722151856", HttpContext.Current.User.Identity.Name, this.InstanceID, this.InstanceID, fu_header.FileBytes);
            }
            else
                headername = hfheader.Value;

            if (fu_logo.HasFile)
            {
                if (hflogoid.Value != "0")
                    objuploader.Deleteuploadedfile(hflogoid.Value);

                objuploader.filedelete(this.InstanceID, hflogo.Value);
                Logoname = fu_logo.FileName;
                string[] logo = fu_logo.FileName.Split('.');
                objuploader.insertfileuploaded(logo[0], logo[1], "8ee08d08-68ab-4415-8b50-9fa722151856", HttpContext.Current.User.Identity.Name, this.InstanceID, this.InstanceID, fu_logo.FileBytes);
            }
            else
                Logoname = hflogo.Value;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("updateInvoicesetting", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Footer", txtfooter.Text);
                    cmd.Parameters.AddWithValue("@headerfile", headername);
                    cmd.Parameters.AddWithValue("@Logofile", Logoname);
                    cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                    cmd.Parameters.AddWithValue("@bankname", ddlmonth.SelectedValue);
                    cmd.Parameters.AddWithValue("@schoolname", txtschool.Text);
                    cmd.Parameters.AddWithValue("@recid", hfrecid.Value);
                    

                    cmd.ExecuteNonQuery();

                    ScriptManager.RegisterStartupScript(this, GetType(), "updatealert", "alert('Setting Update Sucessfully !')", true);
                }

                con.Close();
            }
            binddata();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}