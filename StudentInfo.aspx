﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StudentInfo.aspx.cs" Inherits="StudentInfo" %>

<%--<%@ Register Src="~/WebApplication1/userprofile.ascx" TagPrefix="uc1" TagName="userprofile" %>--%>

<%--
<%@ Register Src="~/WebApplication1/userprofile.ascx" TagName="studentprofile" TagPrefix="uc1" %>
<%@ Register Src="~/WebApplication1/userprofile.ascx" TagName="parentprofile" TagPrefix="uc2" %>
<%@ Register Src="~/WebApplication1/userprofile.ascx" TagName="otherprofile" TagPrefix="uc3" %>

--%>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <%--<link href="_assets/css/style.css" rel="stylesheet" />--%>
    <link href="_assets/jquery-ui.css" rel="stylesheet" />
    <link href="_assets/jquery.datepick.css" rel="stylesheet" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <script src="_assets/jquery.datepick.js"></script>
    <style>
        #tr_charge1 ul li
        {
            padding: 0 0 0 0px;
            margin: 0px 0 6px 17px;
        }


        #tr_discount1 ul li
        {
            padding: 0 0 0 0px;
            margin: 0px 0 6px 17px;
        }



        .StudentDetailTable a
        {
            margin: 4px 0 !important;
            display: inline-block;
        }
    </style>
    <script>
        $(document).ready(function () {

            $('.cb_select_all input').change(function () {
                if (this.checked)
                    $($(this).parents("table")[0]).find(".cb_select input").each(function () {
                        this.checked = true;
                    });
                else {
                    $($(this).parents("table")[0]).find(".cb_select input").each(function () {
                        this.checked = false;
                    });
                }
            });

            $('#chknewuser').change(function () {
                if (this.checked) {
                    if ($('#ddlfee option').size() == 0) {
                        $('#tr_new1').hide();
                        $('#tr_new2').hide();
                        $('#tr_new4').hide();
                        $('#tr_new3').show();
                    }
                    else {
                        $('#tr_new1').show();
                        $('#tr_new2').show();
                        $('#tr_new4').show();
                        $('#tr_new3').hide();
                    }
                }
                else {
                    $('#tr_new1').hide();
                    $('#tr_new2').hide();
                    $('#tr_new3').hide();
                    $('#tr_new4').hide();
                }
            });

            $("#lnkshowhistory").click(function () {
                var dlg = $("#divstudenthistory").dialog({
                    height: 500,
                    width: 400,
                    show: {
                        height: 100
                        //effect: "blind",
                        //duration: 1000
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                        //duration: 1000
                    }
                });
                resizeiframepage();

                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
                //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
            });

            $("#lbtnchangepack").click(function () {
                var dlg = $("#divdepart").dialog({
                    width: 400,
                    show: {
                        height: 100
                        //effect: "blind",
                        //duration: 1000
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                        //duration: 1000
                    }
                });
                resizeiframepage();

                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
                //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
            });

            $("#ibtncharge").click(function () {
                var dlg = $("#divcharges").dialog({
                    width: 400,
                    show: {
                        height: 100
                        //effect: "blind",
                        //duration: 1000
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                        //duration: 1000
                    }
                });
                resizeiframepage();

                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
                //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
            });

            $("#ibtndiscount").click(function () {
                var dlg = $("#divDiscount").dialog({
                    width: 400,
                    show: {
                        height: 100
                        //effect: "blind",
                        //duration: 1000
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                        //duration: 1000
                    }
                });
                resizeiframepage();

                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
                //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
            });

            $("#lbtnCChrg").click(function () {
                var dlg = $("#divCC").dialog({
                    width: 400,
                    show: {
                        height: 100
                        //effect: "blind",
                        //duration: 1000
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                        //duration: 1000
                    }
                });
                resizeiframepage();

                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
                //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
            });
            $("#txtstartdate").datepicker({
                dateFormat: 'dd-MM-yy',
                changeMonth: true,
                changeYear: true
            });
            $("#txtppstartdate").datepicker({
                dateFormat: 'dd-MM-yy',
                changeMonth: true,
                changeYear: true
            });
            $("#lbtncdiscount").click(function () {
                var dlg = $("#divCD").dialog({
                    width: 400,
                    show: {
                        height: 100
                        //effect: "blind",
                        //duration: 1000
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                        //duration: 1000
                    }
                });
                resizeiframepage();

                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
                //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
            });
        });
        function resizeiframepage() {
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
            if (innerDoc.body.offsetHeight) {
                iframe.height = innerDoc.body.offsetHeight + 400 + "px";
            }
            else if (iframe.Document && iframe.Document.body.scrollHeight) {
                iframe.style.height = iframe.Document.body.scrollHeight + "px";
            }
        }
        function checknewuser() {
            if ($('#chknewuser').is(':checked')) {
                if ($('#ddlfee option').size() == 0) {
                    $('#tr_new1').hide();
                    $('#tr_new2').hide();
                    $('#tr_new3').show();
                }
                else {
                    $('#tr_new1').show();
                    $('#tr_new2').show();
                    $('#tr_new3').hide();
                }
            }
            else {
                $('#tr_new1').hide();
                $('#tr_new2').hide();
                $('#tr_new3').hide();
            }
        }

        function addrow(row, count, type) {
            var s = type;
            if (s.indexOf("/Guardian") > 0) {
                s = s.replace("/Guardian ", "");
            }

            var trrow = document.getElementById(row);
            var tr = "<tr><td class=\"formCaptionTd\">" + type + ":</td><td class=\"formFieldTd\"> <input type=\"text\" id=\"" + s + count + "\" name=\"" + s.replace("ParentPhone", "arenthone") + count + "\" class=\"standardField\" MaxLength=\"50\" />&nbsp;<a id=\"a" + s + count + "\" href=\"javascript:addrow('" + s + count + "','" + (parseInt(count) + 1) + "','" + type + "');\">Add Row</a>&nbsp;<a id=\"d" + s + count + "\" href=\"javascript:delrow('" + s + count + "','" + s + (parseInt(count) - 1) + "','" + (parseInt(count) - 1) + "','" + type + "');\">Delete Row</a></td></tr>";
            $(trrow).parent().children("a").remove();
            $(trrow).parent().parent().after(tr);
        }

        function delrow(row, prerow, count, type) {
            var s = type;
            if (s.indexOf("/Guardian") > 0) {
                s = s.replace("/Guardian ", "");
            }
            var trrow = document.getElementById(row);
            var trrow1 = document.getElementById(prerow);
            var tr = "";
            var dd = "";
            if (prerow.indexOf('0') > 0) {
                if (type == "Parent/Guardian Phone") {
                    prerow = "arenthone0";
                    dd = "arenthone";
                }
                else
                    dd = s;
                trrow1 = document.getElementById(prerow);
                tr = "<a id=\"a" + s + count + "\" href=\"javascript:addrow('" + dd + count + "','" + (parseInt(count) + 1) + "','" + type + "');\">Add Row</a>";
            }
            else
                tr = "<a id=\"a" + s + count + "\" href=\"javascript:addrow('" + s + count + "','" + (parseInt(count) + 1) + "','" + type + "');\">Add Row</a>&nbsp;<a id=\"d" + s + count + "\" href=\"javascript:delrow('" + s + count + "','" + s + (parseInt(count) - 1) + "','" + (parseInt(count) - 1) + "','" + type + "');\">Delete Row</a>";
            //$(trrow).parent().children("a").remove();
            $(trrow).parent().parent().remove();
            $(trrow1).parent().append(tr);
        }



        function delvalue(val) {
            $(val).find("input[type=text]").val("");
        }
    </script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <%--<asp:ScriptManager ID="sm_usercontraol" runats="server"></asp:ScriptManager>--%>

        <%-- <div id="studentcontrol" title="User Info" style="display: none">

            <uc1:studentprofile ID="studentprofile" runat="server" />

            <uc1:userprofile runat="server" ID="userprofile" />
        </div>
        <div id="parentcontrol" title="User Info" style="display: none">

            <uc2:parentprofile ID="parentprofile" runat="server" />

        </div>
        <div id="othercontrol" title="User Info" style="display: none">

            <uc3:otherprofile ID="otherprofile" runat="server" />

        </div>--%>
        <div id="mainContentDiv">
            <div id="divmenu" runat="server" style="display: none">
                <div id="divddlhide" runat="server">
                    <%--<asp:LinkButton ID="lnkadddateset" Text="Add Package" runat="server" ClientIDMode="Static"></asp:LinkButton>--%>

                    <asp:LinkButton ID="lnkadddepartment" Text="Add Setting" runat="server" OnClick="lnkadddepartment_Click"></asp:LinkButton>
                </div>

            </div>
            <div class="breadCrumb">
                Student Information
            </div>

            <asp:HiddenField runat="server" ID="hfparentid" />
            <table class="StudentDetailTable">
                <tr>
                    <td class="formCaptionTd"></td>
                    <td class="formFieldTd">
                        <asp:CheckBox ID="chknewuser" runat="server" Text="Generate Invoice" ClientIDMode="Static" />
                    </td>
                </tr>
                <tr id="tr_new1" runat="server" style="display: none;" clientidmode="Static">
                    <td class="formCaptionTd">Select Fee</td>
                    <td class="formFieldTd">
                        <asp:DropDownList ID="ddlfee" runat="server" ClientIDMode="Static"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="ddlfee" ForeColor="Red" InitialValue="0" ValidationGroup="ddlfee"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="tr_new2" runat="server" style="display: none;" clientidmode="Static">
                    <td class="formCaptionTd">Date
                    </td>
                    <td class="formFieldTd">
                        <asp:TextBox ID="txtfeedate" runat="server" CssClass="standardField"></asp:TextBox>
                    </td>
                </tr>
                <tr id="tr_new4" runat="server" style="display: none;" clientidmode="Static">
                    <td class="formCaptionTd"></td>
                    <td class="formFieldTd">
                        <asp:Button runat="server" ID="btngeninvoice" Text="Generate Invoice" CssClass="standardFormButton" OnClick="btngeninvoice_Click" ValidationGroup="ddlfee" />
                        <br />
                        <asp:Label ID="lblmsg" ForeColor="Red" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr id="tr_new3" runat="server" style="display: none;" clientidmode="Static">
                    <td class="formCaptionTd"></td>
                    <td class="formFieldTd">
                        <b>Note:</b> Go to setting and define fee. If you dont change addmission fee then leave this blank.<br />
                        <a href="Settings.aspx">Click to go settings</a>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd">Student Full Name </td>
                    <td class="formFieldTd">
                        <asp:TextBox runat="server" ID="txtStudentname" CssClass=" standardField" required="true"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="formCaptionTd">Student Roll Number
                    </td>
                    <td class="formFieldTd">
                        <asp:TextBox runat="server" ID="txtrollnum" CssClass=" standardField" required="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd">Student Email</td>
                    <td class="formFieldTd">
                        <asp:TextBox runat="server" ID="txtstudentemail" CssClass=" standardField" type="email"></asp:TextBox><br />
                        <a runat="server" id="viewstudent" target="_blank" visible="False">I want to provide information myself</a>
                        <asp:HiddenField runat="server" ID="hfstudentemail" />
                    </td>
                    <%--<td><a runat="server" id="viewstudent" target="_blank" visible="False">I want to provide information myself</a></td>--%>
                </tr>
                <tr>
                    <td class="formCaptionTd">Phone</td>
                    <td class="formFieldTd">
                        <asp:TextBox ID="Phone0" runat="server" CssClass="standardField" MaxLength="50" ClientIDMode="Static" />&nbsp;<a id="aPhone0" runat="server" href="javascript:addrow('Phone0','1','Phone');">Add Row</a>
                    </td>
                </tr>
                <asp:Literal ID="lt_SPhone" runat="server"> </asp:Literal>
                <tr>
                    <td class="formCaptionTd">Parent/Guardian Full Name </td>
                    <td class="formFieldTd">
                        <asp:TextBox runat="server" ID="txtParentname" CssClass=" standardField" required="true"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="formCaptionTd">Parent/Guardian Email</td>
                    <td class="formFieldTd">
                        <asp:TextBox runat="server" ID="txtparentsemail" CssClass=" standardField" type="email"></asp:TextBox><br />
                        <a runat="server" id="viewparent" target="_blank" visible="False">I want to provide information myself</a>
                        <asp:HiddenField runat="server" ID="hfparentemail" />
                    </td>
                    <%--<td><a runat="server" id="viewparent" target="_blank" visible="False">View</a></td>--%>
                </tr>
                <tr>
                    <td class="formCaptionTd">Parent/Guardian Phone</td>
                    <td class="formFieldTd">
                        <asp:TextBox ID="arenthone0" runat="server" CssClass="standardField" MaxLength="50" ClientIDMode="Static" />&nbsp;<a id="a1" runat="server" href="javascript:addrow('arenthone0','1','Parent/Guardian Phone');">Add Row</a>
                    </td>
                </tr>
                <asp:Literal ID="lt_PPhone" runat="server"> </asp:Literal>
                <%--<tr>
                    <td class="formCaptionTd">Others Email</td>
                    <td class="formFieldTd">
                        <asp:TextBox runat="server" ID="txtothersemail" CssClass=" standardField" type="email"></asp:TextBox>
                        <asp:HiddenField runat="server" ID="hfotheremail" />
                    </td>
                    <td><a runat="server" id="viewother" target="_blank" visible="False">View</a></td>

                </tr>--%>
                <tr>
                    <td class="formCaptionTd">Package
                    </td>
                    <td class="formFieldTd textOnly">
                        <asp:Label ID="lblpackage" runat="server"></asp:Label><br />
                        <asp:LinkButton ID="lbtnchangepack" runat="server" Text="Change Package" ClientIDMode="Static" OnClientClick="return false"></asp:LinkButton>

                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd">Package start Date
                    </td>
                    <td class="formFieldTd">
                        <asp:TextBox ID="txtstartdate" runat="server" ClientIDMode="Static" class=" standardField"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd"></td>
                    <td class="formFieldTd">
                        <asp:Button runat="server" ID="btnsave" Text="Save" OnClick="btnsave_Click" CssClass="standardFormButton" />
                        <%--<asp:LinkButton runat="server" ID="lnkshowhistory" Text="Show Student History"></asp:LinkButton>--%>
                     &nbsp;
                                <asp:Button runat="server" ID="btnSaveClose" Text="Save & Close" OnClick="btnSaveClose_Click" CssClass="standardFormButton" />
                     &nbsp;
                                <asp:Button runat="server" ID="btnAddNew" Text="Save & Add Another Student" OnClick="btnAddNew_Click" CssClass="standardFormButton" />
                    </td>

                </tr>
                <tr id="tr_charge" runat="server" style="display: none">
                    <td class="formCaptionTd">Add Additional Charges
                    </td>
                    <td class="formFieldTd textOnly">
                        <asp:LinkButton ID="ibtncharge" runat="server" Text="Add/Edit Charges" ClientIDMode="Static" OnClientClick="return false;"></asp:LinkButton>
                    </td>
                </tr>
                <tr id="tr_charge1" runat="server" style="display: none">
                    <td class="formCaptionTd"></td>
                    <td class="formFieldTd">
                        <asp:Repeater ID="reptcharges" runat="server">
                            <HeaderTemplate>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li>
                                    <asp:Label ID="lbllicharge" runat="server" Text='<%# Eval("Title") %>'></asp:Label></li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr id="tr_discount" runat="server" style="display: none">
                    <td class="formCaptionTd">Add Discount
                    </td>
                    <td class="formFieldTd textOnly">
                        <asp:LinkButton ID="ibtndiscount" runat="server" Text="Add/Edit Discounts" ClientIDMode="Static" OnClientClick="return false;"></asp:LinkButton>
                    </td>
                </tr>
                <tr id="tr_discount1" runat="server" style="display: none">
                    <td class="formCaptionTd"></td>
                    <td class="formFieldTd">
                        <asp:Repeater ID="reptadiscount" runat="server">
                            <HeaderTemplate>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li>
                                    <asp:Label ID="lbllicharge" runat="server" Text='<%# Eval("Title") %>'></asp:Label></li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </table>
            <asp:HiddenField runat="server" ID="hfstudentid" />
            <asp:HiddenField runat="server" ID="hfstudentpackage" />
            <asp:HiddenField runat="server" ID="hfdiscount" />
            <asp:HiddenField runat="server" ID="hfnewuser" Value="0" />

            <div id="divdepart" title="Class Info" style="display: none">
                <asp:Panel ID="pnladd" DefaultButton="btninsert" runat="server">
                    <table>
                        <tr>
                            <td class="formCaptionTd">Packages</td>
                            <td class="formFieldTd">
                                <asp:DropDownList runat="server" ID="ddldateset">
                                    <asp:ListItem Value="">Select</asp:ListItem>
                                </asp:DropDownList>
                            </td>

                        </tr>
                        <tr>
                            <td class="formCaptionTd">Package start Date
                            </td>
                            <td class="formFieldTd">
                                <asp:TextBox ID="txtppstartdate" runat="server" ClientIDMode="Static" class=" standardField" required></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="formCaptionTd"></td>
                            <td class="formFieldTd">
                                <asp:LinkButton ID="lbtnpackage" runat="server" Text="Create Package" OnClick="lnkadddepartment_Click"></asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="formCaptionTd"></td>
                            <td class="formFieldTd">
                                <asp:Button ID="btninsert" Text="submit" runat="server" CssClass="standardFormButton" OnClick="btninsert_Click" /></td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <div id="divcharges" title="Charges" style="display: none">
                <asp:Panel ID="Panel1" DefaultButton="btnsavecharges" runat="server">
                    <asp:Repeater ID="rptrfeepakages" runat="server" OnItemDataBound="rptrfeepackage_itemdatabound">
                        <HeaderTemplate>
                            <table class="smallGrid">
                                <tr class="smallGridHead">
                                    <td>
                                        <asp:CheckBox ID="chkselectall" runat="server" CssClass="cb_select_all" />
                                    </td>
                                    <td>Title</td>
                                    <td>Charge Type</td>
                                    <td>IsFixed</td>
                                    <td>Amount</td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="smallGrid">
                                <td>
                                    <asp:CheckBox ID="chkselecteditem" runat="server" CssClass="cb_select" />
                                    <asp:HiddenField runat="server" ID="hf_ID" Value='<%# Eval("ID") %>' />
                                </td>

                                <td>
                                    <asp:Label ID="lblgridtitle" Text='<%# Eval("Title") %>' runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblgridchargetype" Text='<%# Eval("ChargeType") %>' runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblgridisfixed" Text='<%# Eval("IsFixed") %>' runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblgridamount" Text='<%# Eval("Amount") %>' runat="server"></asp:Label></td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:LinkButton ID="lbtnCChrg" runat="server" Text="Create Charges" OnClientClick="return False;" ClientIDMode="Static"></asp:LinkButton><br />
                    <asp:Button ID="btnsavecharges" runat="server" Text="Save" CssClass="standardFormButton" OnClick="btnsavecharges_Click" />
                </asp:Panel>
            </div>
            <div id="divCC" title="Create Charges" style="display: none">
                <asp:Panel ID="Panel3" DefaultButton="Button1" runat="server">
                    <table>
                        <tr>
                            <td class="formCaptionTd">Title</td>
                            <td class="formFieldTd">
                                <asp:HiddenField ID="hfcharges" runat="server" Value="0" />
                                <asp:TextBox runat="server" ID="txtcharges" CssClass=" standardField"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtcharges" ErrorMessage="*" ForeColor="Red" ValidationGroup="sac"></asp:RequiredFieldValidator>
                            </td>

                        </tr>

                        <tr>
                            <td class="formCaptionTd">Amount</td>
                            <td class="formFieldTd">
                                <asp:TextBox runat="server" ID="txtChargesamount" CssClass=" standardField"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtChargesamount" ErrorMessage="*" ForeColor="Red" ValidationGroup="sac"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtChargesamount" ErrorMessage="Invalid" ForeColor="Red" ValidationExpression="^[0-9]+([.][0-9]{1,3})?$" ValidationGroup="sac"></asp:RegularExpressionValidator>
                            </td>


                        </tr>

                        <tr>
                            <td class="formCaptionTd"></td>
                            <td class="formFieldTd">
                                <asp:Button runat="server" ID="Button1" Text="Save Additional Charges" CssClass="standardFormButton" ValidationGroup="sac" OnClick="Button1_Click" /></td>


                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <div id="divCD" title="Create Discount" style="display: none">
                <asp:Panel ID="Panel4" DefaultButton="btnsubmit" runat="server">
                    <table>
                        <tr>
                            <td class="formCaptionTd">Title</td>
                            <td class="formFieldTd">
                                <asp:TextBox runat="server" ID="txtchargetitle" CssClass=" standardField"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtchargetitle" ErrorMessage="*" ForeColor="Red" ValidationGroup="sd"></asp:RequiredFieldValidator>
                            </td>

                        </tr>

                        <tr>
                            <td class="formCaptionTd">Amount</td>
                            <td class="formFieldTd">
                                <asp:TextBox runat="server" ID="txtamount" CssClass=" standardField"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtamount" ErrorMessage="*" ForeColor="Red" ValidationGroup="sd"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtamount" ErrorMessage="Invalid" ForeColor="Red" ValidationExpression="^[0-9]+([.][0-9]{1,3})?$" ValidationGroup="sd"></asp:RegularExpressionValidator>
                            </td>


                        </tr>

                        <tr>
                            <td class="formCaptionTd"></td>
                            <td class="formFieldTd">
                                <asp:Button runat="server" ID="btnsubmit" Text="Save Discount" OnClick="btnsubmit_Click" CssClass="standardFormButton" ValidationGroup="sd" />
                         </td>

                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <div id="divDiscount" title="Discount" style="display: none">
                <asp:Panel ID="Panel2" DefaultButton="btndiscount" runat="server">
                    <asp:Repeater ID="reptdiscount" runat="server" OnItemDataBound="rptrdiscount_itemdatabound">
                        <HeaderTemplate>
                            <table class="smallGrid">
                                <tr class="smallGridHead">
                                    <td>
                                        <asp:CheckBox ID="chkselectall" runat="server" CssClass="cb_select_all" />
                                    </td>
                                    <td>Title</td>
                                    <td>Charge Type</td>
                                    <td>IsFixed</td>
                                    <td>Amount</td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="smallGrid">
                                <td>
                                    <asp:CheckBox ID="chkselecteditem" runat="server" CssClass="cb_select" />
                                    <asp:HiddenField runat="server" ID="hf_ID" Value='<%# Eval("ID") %>' />
                                </td>

                                <td>
                                    <asp:Label ID="lblgridtitle" Text='<%# Eval("Title") %>' runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblgridchargetype" Text='<%# Eval("ChargeType") %>' runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblgridisfixed" Text='<%# Eval("IsFixed") %>' runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblgridamount" Text='<%# Eval("Amount") %>' runat="server"></asp:Label></td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:LinkButton ID="lbtncdiscount" runat="server" Text="Create Discount" OnClientClick="return False;" ClientIDMode="Static"></asp:LinkButton><br />
                    <asp:Button ID="btndiscount" runat="server" Text="Save" CssClass="standardFormButton" OnClick="btndiscount_Click" />
                </asp:Panel>
            </div>
            <div id="divstudenthistory" style="display: none" title="Student History">
                <asp:Repeater ID="rptrhistory" runat="server" OnItemDataBound="rptrhistory_itemdatabound">
                    <HeaderTemplate>
                        <table class="smallGrid">
                            <tr class="smallGridHead">
                                <td>Message</td>
                                <td>DateTime</td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="smallGrid" style="width: 200px">
                            <td>
                                <asp:Label ID="lblgridisfixed" Text='<%# Eval("Message") %>' runat="server"></asp:Label></td>
                            <td>
                                <asp:Label ID="lblgridamount" Text='<%# Eval("DateTime") %>' runat="server"></asp:Label></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <asp:HiddenField ID="hfpackageid" runat="server" />
                <asp:HiddenField ID="hfcurrency" runat="server" Value="0" />
            </div>
        </div>
    </form>
</body>
</html>
