﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class FeesPackage : AvaimaThirdpartyTool.AvaimaWebPage
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["studentid"]) || !String.IsNullOrEmpty(Request.QueryString["new"]))
                {
                    btnsavegoback.Visible = true;
                }
                else
                {
                    btnsavegoback.Visible = false;
                }
                binddata();
                if (!string.IsNullOrEmpty(Convert.ToString(Request["id"])))
                {
                    hfpakageid.Value = Request["id"];
                    rptrfill();
                    rptrfill1();
                    trEffecteddate.Visible = true;
                    Getpackagemasteritems(hfpakageid.Value);
                    ShowPackageHistory(hfpakageid.Value);
                    bindholidaypackage();
                    bindpackagehistory();
                    lnkshowhistory.Visible = true;
                    tr_addcharges.Visible = true;
                }
                else
                {
                    lbltxtmsg.Text = "Package start date: " + DateTime.Now.ToString("dd-MMMM-yyyy");
                    txtstartdate.Text = DateTime.Now.ToString("dd-MMMM-yyyy");
                    lnkshowhistory.Visible = false;
                    tr_addcharges.Visible = false;
                    rptrfeedetail.Visible = false;
                    dicCharges.Visible = false;
                    divsubfeesheading.Visible=false;
                    divsubfees.Visible = false;
                    divanualfeeheading.Visible = false;
                    divanualfee.Visible = false;
                    dicdiscount.Visible = false;
                    tr1.Visible = false;
                    //tr_1.Style.Add("display", "none");
                    //tr_2.Style.Add("display", "none");
                    //tr_3.Style.Add("display", "none");
                    //tr_4.Style.Add("display", "none");
                    //tr_5.Style.Add("display", "none");
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    private void binddata()
    {
        txttax.Enabled = false;
        ddltaxtype.Enabled = false;
        //currentinstance
        try
        {
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SELECT ID,Currency,latefees,Fee_DueDate,LateFeesStartDate,InstanceId,Istax,Isfixed,Amount FROM tbl_Settings WHERE   InstanceId = @InstanceId and gen=1", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                        DataTable dt = new DataTable();
                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        adp.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            //chksetting.Checked = true;
                            //hfcurrentinstance.Value = this.InstanceID;
                            if (!string.IsNullOrEmpty(dt.Rows[0]["Currency"].ToString()))
                                ddlcurrencyselect.SelectedValue = dt.Rows[0]["Currency"].ToString();

                            if (!string.IsNullOrEmpty(dt.Rows[0]["Fee_DueDate"].ToString()))
                            {
                                string[] duedate = dt.Rows[0]["Fee_DueDate"].ToString().Split('#');
                                ddlddate.SelectedValue = duedate[0];
                                ddlsdays.SelectedValue = duedate[1];
                            }

                            //if (!string.IsNullOrEmpty(dt.Rows[0]["startdate"].ToString()))
                            //    ddlSday.SelectedValue = dt.Rows[0]["startdate"].ToString();

                            if (!string.IsNullOrEmpty(dt.Rows[0]["LateFeesStartDate"].ToString()))
                            {
                                string[] duestartdate = dt.Rows[0]["LateFeesStartDate"].ToString().Split('#');
                                ddldsdate.SelectedValue = duestartdate[0];
                                ddlstype.SelectedValue = duestartdate[1];
                            }
                            txtlatefee.Value = dt.Rows[0]["latefees"].ToString();
                            hflatefee.Value = dt.Rows[0]["latefees"].ToString();
                            //if (!string.IsNullOrEmpty(dt.Rows[0]["LateFeesEndDate"].ToString()))
                            //    ddldedate.SelectedValue = dt.Rows[0]["LateFeesEndDate"].ToString();
                            if (!string.IsNullOrEmpty(dt.Rows[0]["Istax"].ToString()))
                            {
                                if (dt.Rows[0]["Istax"].ToString() == "False")
                                {
                                    txttax.Enabled = false;
                                    ddltaxtype.Enabled = false;
                                    chktax.Checked = false;

                                }
                                else
                                {
                                    txttax.Enabled = true;
                                    ddltaxtype.Enabled = true;
                                    chktax.Checked = true;

                                    if (!string.IsNullOrEmpty(dt.Rows[0]["Isfixed"].ToString()))
                                    {
                                        ddltaxtype.SelectedValue = dt.Rows[0]["Isfixed"].ToString();

                                    }
                                    if (!string.IsNullOrEmpty(dt.Rows[0]["Amount"].ToString()))
                                    {
                                        txttax.Text = dt.Rows[0]["Amount"].ToString();
                                        hftxttax.Value = dt.Rows[0]["Amount"].ToString();
                                    }
                                }
                            }

                        }

                    }
                    con.Close();
                }
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }
    protected void chktax_CheckedChanged(object sender, EventArgs e)
    {
        if (chktax.Checked)
        {
            txttax.Enabled = true;
            ddltaxtype.Enabled = true;
        }
        else
        {

            txttax.Enabled = false;
            ddltaxtype.Enabled = false;

        }
    }
    public void Getpackagemasteritems(string id)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("SELECT * FROM tbl_feesPakageMaster WHERE ID=@id", con))
            {
                if (con.State.ToString() != "Open")
                    con.Open();
                cmd.Parameters.AddWithValue("@id", id);
                DataTable dt =  new DataTable();
                SqlDataAdapter adp= new SqlDataAdapter(cmd);
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        txttitle.Value = dr["Title"].ToString();
                        txtfeeamount.Value = dr["Amount"].ToString();
                        hffeeamount.Value = dr["Amount"].ToString();
                        txtlatefee.Value = dr["LateFeeCharges"].ToString();
                        hflatefee.Value = dr["LateFeeCharges"].ToString();
                        hfeffdate.Value = string.IsNullOrEmpty(dr["effectedfrom"].ToString()) ? Convert.ToDateTime(dr["startday"].ToString()).ToString("dd-MMMM-yyyy") : Convert.ToDateTime(dr["effectedfrom"].ToString()).ToString("dd-MMMM-yyyy");
                        txteffecteddate.Text = string.IsNullOrEmpty(dr["effectedfrom"].ToString()) ? Convert.ToDateTime(dr["startday"].ToString()).ToString("dd-MMMM-yyyy") : Convert.ToDateTime(dr["effectedfrom"].ToString()).ToString("dd-MMMM-yyyy");
                        txtstartdate.Text = Convert.ToDateTime(dr["startday"].ToString()).ToString("dd-MMMM-yyyy");
                        string[] avalue = dr["paymentby"].ToString().Split('#');
                        ddlnum.SelectedValue = avalue[0];
                        ddltype.SelectedValue = avalue[1];
                        ddltype.Enabled = false;
                        ddlnum.Enabled = false;
                        if (avalue[1] == "Months")
                        {
                            lbltxtmsg.Text = "Fees will be due on \"Day " + dr["cyclestartday"].ToString() + "\" of every month.";
                            lbtnchange.Text = "[Change]";
                            //tr_month.Style.Add("display", "none");
                            //ddldays.SelectedValue = dr["cyclestartday"].ToString();
                        }
                        else if (avalue[1] == "Years")
                        {
                            DateTime dtdate = new DateTime(DateTime.Now.Year, 12, 1);
                            string[] vv = dr["cyclestartday"].ToString().Split('#');
                            lbltxtmsg.Text = "Fees will be due on \"" + dtdate.ToString("MMMM") + " " + vv[0] + "\" of every year.";
                            lbtnchange.Text = "[Change]";
                            //tr_month.Attributes.Remove("style");
                            //ddldays.SelectedValue = vv[0];
                            //ddlmonth.SelectedValue = vv[1];


                        }

                        if (!string.IsNullOrEmpty(dr["Currency"].ToString()) || !string.IsNullOrEmpty(dr["Fee_DueDate"].ToString()) ||
                             !string.IsNullOrEmpty(dr["LateFeesStartDate"].ToString()) || !string.IsNullOrEmpty(dr["Amount"].ToString()))
                        {
                            chksetting.Checked = dr["chksetting"].ToString() == "True" ? true : false;
                            if (!chksetting.Checked)
                            {
                                tr_1.Attributes.Remove("style");
                                tr_2.Attributes.Remove("style");
                                tr_3.Attributes.Remove("style");
                                tr_4.Attributes.Remove("style");
                                tr_5.Attributes.Remove("style");
                                //hfcurrentinstance.Value = this.InstanceID;
                                if (!string.IsNullOrEmpty(dr["Currency"].ToString()))
                                    ddlcurrencyselect.SelectedValue = dr["Currency"].ToString();

                                if (!string.IsNullOrEmpty(dr["Fee_DueDate"].ToString()))
                                {
                                    string[] duedate = dr["Fee_DueDate"].ToString().Split('#');
                                    ddlddate.SelectedValue = duedate[0];
                                    ddlsdays.SelectedValue = duedate[1];
                                }

                                //if (!string.IsNullOrEmpty(dr["startdate"].ToString()))
                                //    ddlSday.SelectedValue = dr["startdate"].ToString();

                                if (!string.IsNullOrEmpty(dr["LateFeesStartDate"].ToString()))
                                {
                                    string[] duestartdate = dr["LateFeesStartDate"].ToString().Split('#');
                                    ddldsdate.SelectedValue = duestartdate[0];
                                    ddlstype.SelectedValue = duestartdate[1];
                                }
                                txtlatefee.Value = dr["latefees"].ToString();
                                hflatefee.Value = dr["latefees"].ToString();
                                //if (!string.IsNullOrEmpty(dr["LateFeesEndDate"].ToString()))
                                //    ddldedate.SelectedValue = dr["LateFeesEndDate"].ToString();
                                if (!string.IsNullOrEmpty(dr["Istax"].ToString()))
                                {
                                    if (dr["Istax"].ToString() == "False")
                                    {
                                        txttax.Enabled = false;
                                        ddltaxtype.Enabled = false;
                                        chktax.Checked = false;

                                    }
                                    else
                                    {
                                        txttax.Enabled = true;
                                        ddltaxtype.Enabled = true;
                                        chktax.Checked = true;

                                        if (!string.IsNullOrEmpty(dr["Isfixed"].ToString()))
                                        {
                                            ddltaxtype.SelectedValue = dr["Isfixed"].ToString();

                                        }
                                        if (!string.IsNullOrEmpty(dr["Amount"].ToString()))
                                        {
                                            txttax.Text = dr["Amount"].ToString();
                                            hftxttax.Value = dr["Amount"].ToString();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                tr_1.Style.Add("display", "none");
                                tr_2.Style.Add("display", "none");
                                tr_3.Style.Add("display", "none");
                                tr_4.Style.Add("display", "none");
                                tr_5.Style.Add("display", "none");
                            }
                        }
                        
                    }
                    reptdetail.DataSource = dt;
                    reptdetail.DataBind();
                    rptrfeedetail.Visible = true;
                    dicCharges.Visible = true;
                    divsubfeesheading.Visible = true;
                    divsubfees.Visible = true;
                    divanualfeeheading.Visible = true;
                    divanualfee.Visible = true;
                    dicdiscount.Visible = true;
                    tr1.Visible = true;
                }
                else
                {
                    txtstartdate.Text = DateTime.Now.ToString("dd-MMMM-yyyy");
                    lbltxtmsg.Text = "Package start date: " + DateTime.Now.ToString("dd-MMMM-yyyy");
                    rptrfeedetail.Visible = false;
                    dicCharges.Visible = false;
                    divsubfeesheading.Visible = false;
                    divsubfees.Visible = false;
                    divanualfeeheading.Visible = false;
                    divanualfee.Visible = false;
                    dicdiscount.Visible = false;
                    tr1.Visible = false;
                    //txtfcday.Text = DateTime.Now.ToString("dd-MMMM-yyyy");
                }
                con.Close();
            }
        }
        catch (Exception ex)
        {
            
        }
    }
    private void bindholidaypackage()
    {
        DataSet dtholiday = new DataSet();
        Student package = new Student();
        dtholiday = package.GetHolidaypackage(hfpakageid.Value);
        if (dtholiday.Tables[0].Rows.Count > 0)
        {
            reptholidaypackage.DataSource = dtholiday.Tables[0];
            reptholidaypackage.DataBind();
        }
        else
        {
            reptholidaypackage.DataSource = null;
            reptholidaypackage.DataBind();
        }
        if (dtholiday.Tables[1].Rows.Count > 0)
        {
            Reptannual.DataSource = dtholiday.Tables[1];
            Reptannual.DataBind();
        }
        else
        {
            Reptannual.DataSource = null;
            Reptannual.DataBind();
        }

    }
    public void rptrfill()
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("select DID,PID,DetaiilTitle,DetailAmount,DetailDate from tbl_feesPakgaeDetail where PID=@pid and isdiscount=0", con))
            {
                if (con.State.ToString() != "Open")
                    con.Open();
                cmd.Parameters.AddWithValue("@pid", hfpakageid.Value);
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    rptrfeedetail.DataSource = dt;
                    rptrfeedetail.DataBind();
                    //rptrfeedetail.Visible = true;
                    //dicCharges.Visible = true;
                }
                else
                {
                    rptrfeedetail.DataSource = null;
                    rptrfeedetail.DataBind();
                    //rptrfeedetail.Visible = false;
                    //dicCharges.Visible = false;
                    
                }
               
            }
            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
        }
    }
    public void rptrfill1()
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("select DID,PID,DetaiilTitle,DetailAmount,DetailDate from tbl_feesPakgaeDetail where PID=@pid and isdiscount=1", con))
            {
                if (con.State.ToString() != "Open")
                    con.Open();
                cmd.Parameters.AddWithValue("@pid", hfpakageid.Value);
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    reptdiscount.DataSource = dt;
                    reptdiscount.DataBind();
                    //rptrfeedetail.Visible = true;
                    //dicCharges.Visible = true;
                }
                else
                {
                    reptdiscount.DataSource = null;
                    reptdiscount.DataBind();
                    //rptrfeedetail.Visible = false;
                    //dicCharges.Visible = false;

                }

            }
            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
        }
    }
    protected void rptrfeedetail_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //if (e.CommandName == "edit")
            //{
            //    string message = "Fees Package Updated on";
            //    Packagehistory(Convert.ToInt32(hfpakageid.Value), message);
            //}
            if (e.CommandName == "delete")
            {
                txteffecteddate.Text = string.IsNullOrEmpty(txteffecteddate.Text) ? DateTime.Now.ToString("dd-MMMM-yyyy") : txteffecteddate.Text;
                Student objstud = new Student();
                //objstud.Insertprevpackage(hfpakageid.Value, HttpContext.Current.User.Identity.Name, txteffecteddate.Text, hfeffdate.Value);
                using (SqlCommand cmd = new SqlCommand("Delete from tbl_feesPakgaeDetail where DID=@did", con))
                {
                    if (con.State.ToString() != "Open")
                        con.Open();
                    cmd.Parameters.AddWithValue("@did", e.CommandArgument);
                    cmd.ExecuteNonQuery();
                    string message = "Fees Package Deleted on";
                    Packagehistory(Convert.ToInt32(hfpakageid.Value), message);
                    rptrfill();
                    rptrfill1();
                    con.Close();
                    
                }
                
            }
            else if (e.CommandName == "edit")
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    if (e.CommandName == "edit")
                    {
                        LinkButton lnk = e.Item.FindControl("lnkedit") as LinkButton;

                        using (SqlDataAdapter adp = new SqlDataAdapter("SELECT DID,DetaiilTitle,DetailAmount FROM tbl_feesPakgaeDetail WHERE DID=" + e.CommandArgument, con))
                        {
                            if (con.State.ToString() != "Open")
                                con.Open();
                            DataTable dt = new DataTable();
                            adp.Fill(dt);
                            hfdetaileditid.Value = e.CommandArgument.ToString();
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    txtedittitle.Text = dr["DetaiilTitle"].ToString();
                                    txteditamount.Text = dr["DetailAmount"].ToString();
                                    hfrecamount.Value = dr["DetailAmount"].ToString(); ;

                                }
                            }
                        }

                    }
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "openedit", "opendetailedit()", true);
            }
        }
        //updatepackagemaster(false);
        Getpackagemasteritems(hfpakageid.Value);

    }
    protected void rptrfeedetail_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
             AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
            object container = e.Item.DataItem;

            Label lblmodeficationdate = e.Item.FindControl("lblgriddate") as Label;
            lblmodeficationdate.Text = objATZ.GetDate(DataBinder.Eval(container, "DetailDate").ToString(), HttpContext.Current.User.Identity.Name);

           
        }
    }
    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        try
        {
            string message = "";

            if (hfpakageid.Value == "")
            {
                #region insertocde
                //using (SqlCommand cmd = new SqlCommand("insertpakagemaster", con))
                //{
                //    if (con.State.ToString() != "Open")
                //        con.Open();
                //    cmd.CommandType = CommandType.StoredProcedure;
                //    cmd.Parameters.AddWithValue("@title", txttitle.Value);
                //    cmd.Parameters.AddWithValue("@amount", txtfeeamount.Value);
                //    cmd.Parameters.AddWithValue("@Latefeecharge", txtlatefee.Value);

                //    hfpakageid.Value = cmd.ExecuteScalar().ToString();
                //    con.Close();
                //} 
                #endregion
                using (SqlCommand cmd = new SqlCommand("insert into tbl_feesPakgaeDetail (PID,DetaiilTitle,DetailAmount,DetailDate ) values (@detailid,@detailtitle,@detaiilamount,GETDATE())", con))
                {
                    if (con.State.ToString() != "Open")
                        con.Open();
                    cmd.Parameters.AddWithValue("@detailid", hfpakageid.Value);
                    cmd.Parameters.AddWithValue("@detailtitle", txtfeeitemtitle.Value);
                    cmd.Parameters.AddWithValue("@detaiilamount", txtfeeitemamount.Value);
                    cmd.ExecuteNonQuery();
                    message = "Fees Package Created";
                    // Packagehistory(Convert.ToInt32(hfpakageid.Value), message);
                    rptrfill();
                    rptrfill1();
                    con.Close();
                }
            }

            else
            {
                Student objstud = new Student();
                //objstud.Insertprevpackage(hfpakageid.Value, HttpContext.Current.User.Identity.Name, txteffecteddate.Text, hfeffdate.Value);
                using (SqlCommand cmd = new SqlCommand("insert into tbl_feesPakgaeDetail (PID,DetaiilTitle,DetailAmount,DetailDate,isdiscount ) values (@detailid,@detailtitle,@detaiilamount,GETDATE(),@isdiscount)", con))
                {
                    if (con.State.ToString() != "Open")
                        con.Open();
                    cmd.Parameters.AddWithValue("@detailid", hfpakageid.Value);
                    cmd.Parameters.AddWithValue("@detailtitle", txtfeeitemtitle.Value);
                    cmd.Parameters.AddWithValue("@detaiilamount", txtfeeitemamount.Value);
                    cmd.Parameters.AddWithValue("@isdiscount", hfflag.Value);
                    cmd.ExecuteNonQuery();
                    message = "Fees Package Inserted on";
                    //Packagehistory(Convert.ToInt32(hfpakageid.Value), message);
                    //InsertPackageHistory(hfpakageid.Value, "Package Updated.");
                    rptrfill();
                    rptrfill1();
                    //txteffecteddate.Text = DateTime.Now.ToString("dd-MMMM-yyyy");
                    //updatepackagemaster(false);
                    con.Close();
                }
            }
            txtfeeitemtitle.Value = "";
            txtfeeitemamount.Value = "";
            Getpackagemasteritems(hfpakageid.Value);
        }
        catch (Exception ex) { }
    }
    public void Packagehistory(int id, string message)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("insert into tbl_Packagehistory (HistoryTitle,HisytoryDate) values (@message,@detailtitle,GETDATE())", con))
            {
                if (con.State.ToString() != "Open")
                    con.Open();
                cmd.Parameters.AddWithValue("@message", hfpakageid.Value);
                cmd.Parameters.AddWithValue("@detailtitle", txtfeeitemtitle.Value);
                cmd.ExecuteNonQuery();

                con.Close();
            }
        }
        catch (Exception ex) { }
    }

    protected void RptrPackageHistory_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
    protected void RptrPackageHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }

    protected void btnsavepakage_clicks(object sender, EventArgs e)
    {
        if (hfpakageid.Value == "")
        {
            insertpachakgemaster();
            if (!String.IsNullOrEmpty(Request.QueryString["studentid"]))
                updatestudentpackage();

            tr_addcharges.Visible = true;
        }
        else
        {
            updatepackagemaster();
           
        }
      
        Getpackagemasteritems(hfpakageid.Value);
        ScriptManager.RegisterStartupScript(this, GetType(), "savedrecords", "alert('Save Successfully!!')", true);
    }
    private void updatestudentpackage()
    {
        DateTime dttime = Convert.ToDateTime(txtstartdate.Text);
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("update tbl_feesManagement set PackageID=@PackageID,packagechangedate=@packagechangedate where studendId=@studendId", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@PackageID", hfpakageid.Value);
                cmd.Parameters.AddWithValue("@packagechangedate", "01-" + dttime.Month + "-" + dttime.Year);
                cmd.Parameters.AddWithValue("@studendId", Request.QueryString["studentid"]);
                cmd.CommandType = CommandType.Text;
                //getstudentpackage();
                cmd.ExecuteNonQuery();
            }
        }
    }
    public void insertpachakgemaster()
    {
        string cyf = "";
        string[] dates = txtstartdate.Text.Split('-');
        if (ddltype.SelectedValue == "Months")
        {
            cyf = dates[0];
        }
        else if (ddltype.SelectedValue == "Years")
        {
            cyf = dates[0] + "#" + dates[1];
        }
        txtstartdate.Text = txtstartdate.Text.Replace(dates[0], "01");
        string Query = "sp_insertpakagemaster";
        //if (!String.IsNullOrEmpty(Request.QueryString["studentid"]))
        //    Query = "sp_insertpakagemasterbystud";

        using (SqlCommand cmd = new SqlCommand(Query, con))
        {
            if (con.State.ToString() != "Open")
                con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@title", txttitle.Value);
            cmd.Parameters.AddWithValue("@amount", txtfeeamount.Value);
            cmd.Parameters.AddWithValue("@csday", "01");
            cmd.Parameters.AddWithValue("@paymentby", ddlnum.SelectedValue + "#" + ddltype.SelectedValue);
            cmd.Parameters.AddWithValue("@startday", txtstartdate.Text);
            cmd.Parameters.AddWithValue("@Latefeecharge", txtlatefee.Value);
            cmd.Parameters.AddWithValue("@Totalamount", txtfeeamount.Value);
            cmd.Parameters.AddWithValue("@Isntanceid", this.InstanceID);
            cmd.Parameters.AddWithValue("@currency", ddlcurrencyselect.SelectedValue);
            cmd.Parameters.AddWithValue("@duedate", ddlddate.SelectedValue + "#" + ddlsdays.SelectedValue);
            cmd.Parameters.AddWithValue("@lfeesstartdate", ddldsdate.SelectedValue + "#" + ddlstype.SelectedValue);
            if (chktax.Checked)
                cmd.Parameters.AddWithValue("@Istax", 1);
            else
            {
                cmd.Parameters.AddWithValue("@Istax", 0);
            }

            cmd.Parameters.AddWithValue("@Isfixed", ddltaxtype.SelectedValue);
            if (!string.IsNullOrEmpty(Convert.ToString(txttax.Text)))
                cmd.Parameters.AddWithValue("@taxAmount", txttax.Text);
            else
            {
                cmd.Parameters.AddWithValue("@taxAmount", 0);
            }
            if (chksetting.Checked)
                cmd.Parameters.AddWithValue("@chksetting", 1);
            else
            {
                cmd.Parameters.AddWithValue("@chksetting", 0);
            }
           

            hfpakageid.Value = cmd.ExecuteScalar().ToString();

            InsertPackageHistory(hfpakageid.Value, "Package " + txttitle.Value + " Created.");

            lnkshowhistory.Visible = true;

            con.Close();
        }
    }

    public void updatepackagemaster(bool v=false)
    {
        DateTime dttime = Convert.ToDateTime(txtstartdate.Text);
        bool cd = v;
        if (hffeeamount.Value != txtfeeamount.Value)
        {
            InsertPackageHistory(hfpakageid.Value, "Fees Update", hffeeamount.Value, txtfeeamount.Value);
            cd = true;
        }
        if (hflatefee.Value != txtlatefee.Value)
        {
            InsertPackageHistory(hfpakageid.Value, "Late Fees Update", hflatefee.Value, txtlatefee.Value);
            cd = true;
        }
        if (hftxttax.Value != txttax.Text)
        {
            InsertPackageHistory(hfpakageid.Value, "Tax Update", hftxttax.Value, txttax.Text);
            cd = true;
        }
        if (hfeffdate.Value == txteffecteddate.Text)
        {
            using (SqlCommand cmd = new SqlCommand("sp_updatepakagemaster", con))
            {
                if (con.State.ToString() != "Open")
                    con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", hfpakageid.Value);
                cmd.Parameters.AddWithValue("@title", txttitle.Value);
                cmd.Parameters.AddWithValue("@paymentby", ddlnum.SelectedValue + "#" + ddltype.SelectedValue);
                cmd.Parameters.AddWithValue("@startday", "01-" + dttime.Month + "-" + dttime.Year);
                cmd.Parameters.AddWithValue("@amount", txtfeeamount.Value);
                cmd.Parameters.AddWithValue("@Latefeecharge", txtlatefee.Value);
                cmd.Parameters.AddWithValue("@Totalamount", txtlatefee.Value);
                cmd.Parameters.AddWithValue("@effecteddate", txteffecteddate.Text);
                cmd.Parameters.AddWithValue("@currency", ddlcurrencyselect.SelectedValue);
                cmd.Parameters.AddWithValue("@duedate", ddlddate.SelectedValue + "#" + ddlsdays.SelectedValue);
                cmd.Parameters.AddWithValue("@lfeesstartdate", ddldsdate.SelectedValue + "#" + ddlstype.SelectedValue);
                if (chktax.Checked)
                    cmd.Parameters.AddWithValue("@Istax", 1);
                else
                {
                    cmd.Parameters.AddWithValue("@Istax", 0);
                }

                cmd.Parameters.AddWithValue("@Isfixed", ddltaxtype.SelectedValue);
                if (!string.IsNullOrEmpty(Convert.ToString(txttax.Text)))
                    cmd.Parameters.AddWithValue("@taxAmount", txttax.Text);
                else
                {
                    cmd.Parameters.AddWithValue("@taxAmount", 0);
                }
                if (chksetting.Checked)
                    cmd.Parameters.AddWithValue("@chksetting", 1);
                else
                {
                    cmd.Parameters.AddWithValue("@chksetting", 0);
                }
                cmd.ExecuteNonQuery();
                //hfpakageid.Value = cmd.ExecuteScalar().ToString();
                //InsertPackageHistory(hfpakageid.Value, "Package " + txttitle.Value + " Updated.", "", "");

                con.Close();
            }
            if (cd)
            {
                Student objstud = new Student();
                //objstud.Insertprevpackage(hfpakageid.Value, HttpContext.Current.User.Identity.Name, txteffecteddate.Text, hfeffdate.Value);
            }
        }
        else
        {
            if (cd)
            {
                Student objstud = new Student();
                //objstud.Insertprevpackage(hfpakageid.Value, HttpContext.Current.User.Identity.Name, txteffecteddate.Text, hfeffdate.Value);
            }
            using (SqlCommand cmd = new SqlCommand("sp_updatepakagemaster", con))
            {
                if (con.State.ToString() != "Open")
                    con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", hfpakageid.Value);
                cmd.Parameters.AddWithValue("@title", txttitle.Value);
                cmd.Parameters.AddWithValue("@paymentby", ddlnum.SelectedValue + "#" + ddltype.SelectedValue);
                cmd.Parameters.AddWithValue("@startday", "01-" + dttime.Month + "-" + dttime.Year);
                cmd.Parameters.AddWithValue("@amount", txtfeeamount.Value);
                cmd.Parameters.AddWithValue("@Latefeecharge", txtlatefee.Value);
                cmd.Parameters.AddWithValue("@effecteddate", txteffecteddate.Text);
                cmd.Parameters.AddWithValue("@Totalamount", txtlatefee.Value);
                cmd.Parameters.AddWithValue("@currency", ddlcurrencyselect.SelectedValue);
                cmd.Parameters.AddWithValue("@duedate", ddlddate.SelectedValue + "#" + ddlsdays.SelectedValue);
                cmd.Parameters.AddWithValue("@lfeesstartdate", ddldsdate.SelectedValue + "#" + ddlstype.SelectedValue);
                if (chktax.Checked)
                    cmd.Parameters.AddWithValue("@Istax", 1);
                else
                {
                    cmd.Parameters.AddWithValue("@Istax", 0);
                }

                cmd.Parameters.AddWithValue("@Isfixed", ddltaxtype.SelectedValue);
                if (!string.IsNullOrEmpty(Convert.ToString(txttax.Text)))
                    cmd.Parameters.AddWithValue("@taxAmount", txttax.Text);
                else
                {
                    cmd.Parameters.AddWithValue("@taxAmount", 0);
                }
                if (chksetting.Checked)
                    cmd.Parameters.AddWithValue("@chksetting", 1);
                else
                {
                    cmd.Parameters.AddWithValue("@chksetting", 0);
                }
                cmd.ExecuteNonQuery();
                //hfpakageid.Value = cmd.ExecuteScalar().ToString();
                //InsertPackageHistory(hfpakageid.Value, "Package " + txttitle.Value + " Updated.", "", "");

                con.Close();
            }
        }
       
    }
    public void updatedetail()
    {
        using (SqlCommand cmd = new SqlCommand("UPDATE tbl_feesPakgaeDetail SET DetaiilTitle=@detailtitle,DetailAmount=@detailamount,DetailDate=GETDATE() WHERE DID=@detailid", con))
        {
            if (con.State.ToString() != "Open")
                con.Open();

            //
        }
    }
    protected void btnsaveedit_Click(object sender, EventArgs e)
    {
        if (hfrecamount.Value != txteditamount.Text)
        {
            Student objstud = new Student();
            //objstud.Insertprevpackage(hfpakageid.Value, HttpContext.Current.User.Identity.Name, txteffecteddate.Text, hfeffdate.Value);
        }
        using (SqlCommand cmd = new SqlCommand("update tbl_feesPakgaeDetail set DetaiilTitle=@detailtitle,DetailAmount=@detailamount,DetailDate=GETDATE() where DID=@did", con))
        {
            if (con.State.ToString() != "Open")
                con.Open();
            cmd.Parameters.AddWithValue("@did", hfdetaileditid.Value);
            cmd.Parameters.AddWithValue("@detailtitle", txtedittitle.Text);
            cmd.Parameters.AddWithValue("@DetailAmount", txteditamount.Text);
            cmd.ExecuteNonQuery();
            con.Close();
            InsertPackageHistory(hfpakageid.Value,"Package "+txttitle.Value+" Updated.");
            rptrfill();
            rptrfill1();
            //txteffecteddate.Text = DateTime.Now.ToString("dd-MMMM-yyyy");
            updatepackagemaster(false);
        }

    }

    public void InsertPackageHistory(string sid, string message)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("INSERT INTO tbl_packagehistory ( PackageID, Message )VALUES  ( @sid, @message)", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@sid", sid);
                    cmd.Parameters.AddWithValue("@message", message);
                    cmd.ExecuteNonQuery();
                }
            }
            ShowPackageHistory(sid);
        }
        catch (Exception)
        {

            throw;
        }
    }

    public void InsertPackageHistory(string sid, string message, string oldvalue, string newvalue)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("INSERT INTO tbl_packagehistory ( PackageID, Message,oldvalue,newvalue )VALUES  ( @sid, @message,@oldvalue,@newvalue)", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@sid", sid);
                    cmd.Parameters.AddWithValue("@message", message);
                    cmd.Parameters.AddWithValue("@oldvalue", oldvalue);
                    cmd.Parameters.AddWithValue("@newvalue", newvalue);
                    cmd.ExecuteNonQuery();
                }
            }
            ShowPackageHistory(sid);
        }
        catch (Exception)
        {

            throw;
        }
    }

    public void ShowPackageHistory(string sid)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT ID,Message,DateTime  FROM tbl_packagehistory WHERE PackageID=@id", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", sid);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    rptrhistory.DataSource = dt;
                    rptrhistory.DataBind();
                }
            }
        }
        catch (Exception)
        {

            throw;
        }


    }

    protected void rptrhistory_itemdatabound(object sender, RepeaterItemEventArgs e)
    {
	if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
            object container = e.Item.DataItem;

            Label lblmodeficationdate = e.Item.FindControl("lblgridamount") as Label;
            lblmodeficationdate.Text = objATZ.GetDate(DataBinder.Eval(container, "DateTime").ToString(), HttpContext.Current.User.Identity.Name);
            lblmodeficationdate.Text +=" "+ objATZ.GetTime(DataBinder.Eval(container, "DateTime").ToString(), HttpContext.Current.User.Identity.Name);
        }
    }
    protected void reptdetail_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            object container = e.Item.DataItem;
            Label lblAC = e.Item.FindControl("lbladditioncharges") as Label;
            lblAC.Text = (Convert.ToDecimal(DataBinder.Eval(container, "Totalamount").ToString()) - Convert.ToDecimal(DataBinder.Eval(container, "Amount").ToString())).ToString();
        }
    }
    protected void btndatesave_Click(object sender, EventArgs e)
    {
        string cyf = "";
        string[] dates = txtstartdate.Text.Split('-');
        if (ddltype.SelectedValue == "Months")
        {
            cyf = dates[0];
        }
        else if (ddltype.SelectedValue == "Years")
        {
            cyf = dates[0] + "#" + dates[1];
        }
        txtstartdate.Text = txtstartdate.Text.Replace(dates[0], "01");
        using (SqlCommand cmd = new SqlCommand("update tbl_feesPakageMaster set startday=@startday, cyclestartday=@cyclestartday,paymentby=@paymentby where ID=@id ", con))
        {
            if (con.State.ToString() != "Open")
                con.Open();
            cmd.Parameters.AddWithValue("@cyclestartday ", cyf);
            cmd.Parameters.AddWithValue("@startday ", txtstartdate.Text);
            cmd.Parameters.AddWithValue("@paymentby ", ddlnum.SelectedValue + "#" + ddltype.SelectedValue);
            cmd.Parameters.AddWithValue("@id", hfpakageid.Value);
           
            cmd.ExecuteNonQuery();
            //message = "Fees Package Created";
            Getpackagemasteritems(hfpakageid.Value);
            // Packagehistory(Convert.ToInt32(hfpakageid.Value), message)
            con.Close();
        }
    }
    protected void btnsavegoback_Click(object sender, EventArgs e)
    {
        btnsavepakage_clicks(sender, e);
        if (!String.IsNullOrEmpty(Request.QueryString["studentid"]))
        {
            this.Redirect("StudentInfo.aspx?stid=" + Request.QueryString["studentid"] + "&pstate=" + Request.QueryString["pstate"]);
        }
        else
            this.Redirect("StudentInfo.aspx?new=" + Request.QueryString["new"] + "&pid=" + Request.QueryString["pid"]);
    }
    protected void btnsubfees_Click(object sender, EventArgs e)
    {
        Student objstudent = new Student();
        objstudent.InsertHolidaypackage(txtsubfees.Text, Convert.ToDateTime(txtsubfeesstartdate.Text), Convert.ToDateTime(txtsubfeesenddate.Text), hfpakageid.Value, txtsubfeescomments.Text, "");
        bindholidaypackage();
        txtsubfees.Text = "";
        txtsubfeesstartdate.Text = "";
        txtsubfeesenddate.Text = "";
        txtsubfeescomments.Text = "";
    }
    protected void reptholidaypackage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "delete")
        {
            Student objdel = new Student();
            objdel.deleteHolidaypackage(e.CommandArgument.ToString());
            bindholidaypackage();
        }
    }

    private void bindpackagehistory()
    {
        DataTable dtholiday = new DataTable();
        Student package = new Student();
        dtholiday = package.Getpreviouspackages(hfpakageid.Value);
        if (dtholiday.Rows.Count > 0)
        {
            reptpackhistory.DataSource = dtholiday;
            reptpackhistory.DataBind();
        }
        else
        {
            reptpackhistory.DataSource = null;
            reptpackhistory.DataBind();
        }
    }

    protected void reptpackhistory_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            using (SqlCommand cmd = new SqlCommand("Delete from previouspackages where recid=@did", con))
            {
                if (con.State.ToString() != "Open")
                    con.Open();
                cmd.Parameters.AddWithValue("@did", e.CommandArgument);
                cmd.ExecuteNonQuery();
                bindpackagehistory();
                con.Close();

            }

        }
    }
    protected void reptpackhistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hfuserid = e.Item.FindControl("hfuserid") as HiddenField;
            AvaimaUserProfile objuser = new AvaimaUserProfile();
            string[] user = objuser.Getonlineuserinfo(hfuserid.Value).Split('#');
            Label updateby = e.Item.FindControl("lblupdateby") as Label;
            updateby.Text = user[0] + " " + user[1];

        }
    }
    protected void btnannual_Click(object sender, EventArgs e)
    {
        Student objstudent = new Student();
        objstudent.insertAnnual_Fees(txtannualfee.Text, txtannualdate.Text, hfpakageid.Value, txtpacktitle.Text);
        bindholidaypackage();
        txtannualdate.Text = "";
        txtannualfee.Text = "";
    }
    protected void Reptannual_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "delete")
        {
            Student objdel = new Student();
            objdel.deleteAnnual_Fees(e.CommandArgument.ToString());
            bindholidaypackage();
        }
    }
}