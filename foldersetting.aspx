﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="foldersetting.aspx.cs" Inherits="foldersetting" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="_assets/css/style.css" rel="stylesheet" />
    <link href="_assets/jquery-ui.css" rel="stylesheet" />
    <link href="_assets/jquery.datepick.css" rel="stylesheet" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <script src="_assets/jquery.datepick.js"></script>
    <title></title>
    <script>
        $(document).ready(function () {
            // $('.tax').prop("disabled", true);


            //    $(".caldate").datepick({
            //    dateFormat: 'dd-MMM-yyyy'
            //});

            //$('#chktax').change(function () {
            //    if ($('#chktax').prop('checked')) {
            //        $('.tax').prop("disabled", false);
            //    } else {
            //        $('.tax').prop("disabled", true);
            //    }
            //});
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="mainContentDiv">
            <asp:HiddenField runat="server" ID="hfcurrentinstance" Value="0" />
            <table>
                <tr>
                    <td class="formCaptionTd" width="150px">
                        Title
                    </td>
                    <td>
                        <asp:TextBox ID="txtname" runat="server" CssClass="standardField" required></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd" width="150px" required>Select Currency</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlcurrencyselect" required>
                            <asp:ListItem Value="">Select </asp:ListItem>
                            <asp:ListItem Value="USD">USD - US Dollar</asp:ListItem>
                            <asp:ListItem Value="EUR">EUR - Euro</asp:ListItem>
                            <asp:ListItem Value="GBP">GBP - British Pound</asp:ListItem>
                            <asp:ListItem Value="INR">INR - Indian Rupee</asp:ListItem>
                            <asp:ListItem Value="AUD">AUD - Australian Dollar</asp:ListItem>
                            <asp:ListItem Value="CAD">CAD - Canadian Dollar</asp:ListItem>
                            <asp:ListItem Value="AED">AED - Emirati Dirham</asp:ListItem>
                            <asp:ListItem Value="AED">AED - Emirati Dirham</asp:ListItem>
                            <asp:ListItem Value="AFN">AFN - Afghan Afghani</asp:ListItem>
                            <asp:ListItem Value="ALL">ALL - Albanian Lek</asp:ListItem>
                            <asp:ListItem Value="AMD">AMD - Armenian Dram</asp:ListItem>
                            <asp:ListItem Value="ANG">ANG - Dutch Guilder</asp:ListItem>
                            <asp:ListItem Value="AOA">AOA - Angolan Kwanza</asp:ListItem>
                            <asp:ListItem Value="ARS">ARS - Argentine Peso</asp:ListItem>
                            <asp:ListItem Value="AUD">AUD - Australian Dollar</asp:ListItem>
                            <asp:ListItem Value="AWG">AWG - Aruban or Dutch Guilder</asp:ListItem>
                            <asp:ListItem Value="AZN">AZN - Azerbaijani New Manat</asp:ListItem>
                            <asp:ListItem Value="BAM">BAM - Bosnian Convertible Marka</asp:ListItem>
                            <asp:ListItem Value="BBD">BBD - Barbadian or Bajan Dollar</asp:ListItem>
                            <asp:ListItem Value="BDT">BDT - Bangladeshi Taka</asp:ListItem>
                            <asp:ListItem Value="BGN">BGN - Bulgarian Lev</asp:ListItem>
                            <asp:ListItem Value="BHD">BHD - Bahraini Dinar</asp:ListItem>
                            <asp:ListItem Value="BIF">BIF - Burundian Franc</asp:ListItem>
                            <asp:ListItem Value="BMD">BMD - Bermudian Dollar</asp:ListItem>
                            <asp:ListItem Value="BND">BND - Bruneian Dollar</asp:ListItem>
                            <asp:ListItem Value="BOB">BOB - Bolivian Boliviano</asp:ListItem>
                            <asp:ListItem Value="BRL">BRL - Brazilian Real</asp:ListItem>
                            <asp:ListItem Value="BSD">BSD - Bahamian Dollar</asp:ListItem>
                            <asp:ListItem Value="BTN">BTN - Bhutanese Ngultrum</asp:ListItem>
                            <asp:ListItem Value="BWP">BWP - Botswana Pula</asp:ListItem>
                            <asp:ListItem Value="BYR">BYR - Belarusian Ruble</asp:ListItem>
                            <asp:ListItem Value="BZD">BZD - Belizean Dollar</asp:ListItem>
                            <asp:ListItem Value="CAD">CAD - Canadian Dollar</asp:ListItem>
                            <asp:ListItem Value="CDF">CDF - Congolese Franc</asp:ListItem>
                            <asp:ListItem Value="CHF">CHF - Swiss Franc</asp:ListItem>
                            <asp:ListItem Value="CLP">CLP - Chilean Peso</asp:ListItem>
                            <asp:ListItem Value="CNY">CNY - Chinese Yuan Renminbi</asp:ListItem>
                            <asp:ListItem Value="COP">COP - Colombian Peso</asp:ListItem>
                            <asp:ListItem Value="CRC">CRC - Costa Rican Colon</asp:ListItem>
                            <asp:ListItem Value="CUC">CUC - Cuban Convertible Peso</asp:ListItem>
                            <asp:ListItem Value="CUP">CUP - Cuban Peso</asp:ListItem>
                            <asp:ListItem Value="CVE">CVE - Cape Verdean Escudo</asp:ListItem>
                            <asp:ListItem Value="CZK">CZK - Czech Koruna</asp:ListItem>
                            <asp:ListItem Value="DJF">DJF - Djiboutian Franc</asp:ListItem>
                            <asp:ListItem Value="DKK">DKK - Danish Krone</asp:ListItem>
                            <asp:ListItem Value="DOP">DOP - Dominican Peso</asp:ListItem>
                            <asp:ListItem Value="DZD">DZD - Algerian Dinar</asp:ListItem>
                            <asp:ListItem Value="EGP">EGP - Egyptian Pound</asp:ListItem>
                            <asp:ListItem Value="ERN">ERN - Eritrean Nakfa</asp:ListItem>
                            <asp:ListItem Value="ETB">ETB - Ethiopian Birr</asp:ListItem>
                            <asp:ListItem Value="EUR">EUR - Euro</asp:ListItem>
                            <asp:ListItem Value="FJD">FJD - Fijian Dollar</asp:ListItem>
                            <asp:ListItem Value="FKP">FKP - Falkland Island Pound</asp:ListItem>
                            <asp:ListItem Value="GBP">GBP - British Pound</asp:ListItem>
                            <asp:ListItem Value="GEL">GEL - Georgian Lari</asp:ListItem>
                            <asp:ListItem Value="GGP">GGP - Guernsey Pound</asp:ListItem>
                            <asp:ListItem Value="GHS">GHS - Ghanaian Cedi</asp:ListItem>
                            <asp:ListItem Value="GIP">GIP - Gibraltar Pound</asp:ListItem>
                            <asp:ListItem Value="GMD">GMD - Gambian Dalasi</asp:ListItem>
                            <asp:ListItem Value="GNF">GNF - Guinean Franc</asp:ListItem>
                            <asp:ListItem Value="GTQ">GTQ - Guatemalan Quetzal</asp:ListItem>
                            <asp:ListItem Value="GYD">GYD - Guyanese Dollar</asp:ListItem>
                            <asp:ListItem Value="HKD">HKD - Hong Kong Dollar</asp:ListItem>
                            <asp:ListItem Value="HNL">HNL - Honduran Lempira</asp:ListItem>
                            <asp:ListItem Value="HNL">HNL - Croatian Kuna</asp:ListItem>
                            <asp:ListItem Value="HTG">HTG - Haitian Gourde</asp:ListItem>
                            <asp:ListItem Value="HUF">HUF - Hungarian Forint</asp:ListItem>
                            <asp:ListItem Value="IDR">IDR - Indonesian Rupiah</asp:ListItem>
                            <asp:ListItem Value="ILS">ILS - Israeli Shekel</asp:ListItem>
                            <asp:ListItem Value="IMP">IMP - Isle of Man Pound</asp:ListItem>
                            <asp:ListItem Value="INR">INR - Indian Rupee</asp:ListItem>
                            <asp:ListItem Value="IQD">IQD - Iraqi Dinar</asp:ListItem>
                            <asp:ListItem Value="IRR">IRR - Iranian Rial</asp:ListItem>
                            <asp:ListItem Value="ISK">ISK - Icelandic Krona</asp:ListItem>
                            <asp:ListItem Value="JEP">JEP - Jersey Pound</asp:ListItem>
                            <asp:ListItem Value="JMD">JMD - Jamaican Dollar</asp:ListItem>
                            <asp:ListItem Value="JOD">JOD - Jordanian Dinar</asp:ListItem>
                            <asp:ListItem Value="JPY">JPY - Japanese Yen</asp:ListItem>
                            <asp:ListItem Value="KES">KES - Kenyan Shilling</asp:ListItem>
                            <asp:ListItem Value="KGS">KGS - Kyrgyzstani Som</asp:ListItem>
                            <asp:ListItem Value="KHR">KHR - Cambodian Riel</asp:ListItem>
                            <asp:ListItem Value="KMF">KMF - Comoran Franc</asp:ListItem>
                            <asp:ListItem Value="KPW">KPW - North Korean Won</asp:ListItem>
                            <asp:ListItem Value="KRW">KRW - South Korean Won</asp:ListItem>
                            <asp:ListItem Value="KWD">KWD - Kuwaiti Dinar</asp:ListItem>
                            <asp:ListItem Value="KYD">KYD - Caymanian Dollar</asp:ListItem>
                            <asp:ListItem Value="KZT">KZT - Kazakhstani Tenge</asp:ListItem>
                            <asp:ListItem Value="LAK">LAK - Lao or Laotian Kip</asp:ListItem>
                            <asp:ListItem Value="LBP">LBP - Lebanese Pound</asp:ListItem>
                            <asp:ListItem Value="LKR">LKR - Sri Lankan Rupee</asp:ListItem>
                            <asp:ListItem Value="LRD">LRD - Liberian Dollar</asp:ListItem>
                            <asp:ListItem Value="LSL">LSL - Basotho Loti</asp:ListItem>
                            <asp:ListItem Value="LTL">LTL - Lithuanian Litas</asp:ListItem>
                            <asp:ListItem Value="LVL">LVL - Latvian Lat</asp:ListItem>
                            <asp:ListItem Value="LYD">LYD - Libyan Dinar</asp:ListItem>
                            <asp:ListItem Value="MAD">MAD - Moroccan Dirham</asp:ListItem>
                            <asp:ListItem Value="MDL">MDL - Moldovan Leu</asp:ListItem>
                            <asp:ListItem Value="MGA">MGA - Malagasy Ariary</asp:ListItem>
                            <asp:ListItem Value="MKD">MKD - Macedonian Denar</asp:ListItem>
                            <asp:ListItem Value="MMK">MMK - Burmese Kyat</asp:ListItem>
                            <asp:ListItem Value="MNT">MNT - Mongolian Tughrik</asp:ListItem>
                            <asp:ListItem Value="MOP">MOP - Macau Pataca</asp:ListItem>
                            <asp:ListItem Value="MRO">MRO - Mauritanian Ouguiya</asp:ListItem>
                            <asp:ListItem Value="MUR">MUR - Mauritian Rupee</asp:ListItem>
                            <asp:ListItem Value="MVR">MVR - Maldivian Rufiyaa</asp:ListItem>
                            <asp:ListItem Value="MWK">MWK - Malawian Kwacha</asp:ListItem>
                            <asp:ListItem Value="MXN">MXN - Mexican Peso</asp:ListItem>
                            <asp:ListItem Value="MYR">MYR - Malaysian Ringgit</asp:ListItem>
                            <asp:ListItem Value="MZN">MZN - Mozambican Metical</asp:ListItem>
                            <asp:ListItem Value="NAD">NAD - Namibian Dollar</asp:ListItem>
                            <asp:ListItem Value="NGN">NGN - Nigerian Naira</asp:ListItem>
                            <asp:ListItem Value="NIO">NIO - Nicaraguan Cordoba</asp:ListItem>
                            <asp:ListItem Value="NOK">NOK - Norwegian Krone</asp:ListItem>
                            <asp:ListItem Value="NPR">NPR - Nepalese Rupee</asp:ListItem>
                            <asp:ListItem Value="NZD">NZD - New Zealand Dollar</asp:ListItem>
                            <asp:ListItem Value="OMR">OMR - Omani Rial</asp:ListItem>
                            <asp:ListItem Value="PAB">PAB - Panamanian Balboa</asp:ListItem>
                            <asp:ListItem Value="PEN">PEN - Peruvian Nuevo Sol</asp:ListItem>
                            <asp:ListItem Value="PGK">PGK - Papua New Guinean Kina</asp:ListItem>
                            <asp:ListItem Value="PHP">PHP - Philippine Peso</asp:ListItem>
                            <asp:ListItem Value="PKR">PKR - Pakistani Rupee</asp:ListItem>
                            <asp:ListItem Value="PLN">PLN - Polish Zloty</asp:ListItem>
                            <asp:ListItem Value="PYG">PYG - Paraguayan Guarani</asp:ListItem>
                            <asp:ListItem Value="QAR">QAR - Qatari Riyal</asp:ListItem>
                            <asp:ListItem Value="RON">RON - Romanian New Leu</asp:ListItem>
                            <asp:ListItem Value="RSD">RSD - Serbian Dinar</asp:ListItem>
                            <asp:ListItem Value="RUB">RUB - Russian Ruble</asp:ListItem>
                            <asp:ListItem Value="RWF">RWF - Rwandan Franc</asp:ListItem>
                            <asp:ListItem Value="SAR">SAR - Saudi Arabian Riyal</asp:ListItem>
                            <asp:ListItem Value="SBD">SBD - Solomon Islander Dollar</asp:ListItem>
                            <asp:ListItem Value="SCR">SCR - Seychellois Rupee</asp:ListItem>
                            <asp:ListItem Value="SDG">SDG - Sudanese Pound</asp:ListItem>
                            <asp:ListItem Value="SEK">SEK - Swedish Krona</asp:ListItem>
                            <asp:ListItem Value="SEK">SEK - Singapore Dollar</asp:ListItem>
                            <asp:ListItem Value="SHP">SHP - Saint Helenian Pound</asp:ListItem>
                            <asp:ListItem Value="SLL">SLL - Sierra Leonean Leone</asp:ListItem>
                            <asp:ListItem Value="SOS">SOS - Somali Shilling</asp:ListItem>
                            <asp:ListItem Value="SPL">SPL - Seborgan Luigino</asp:ListItem>
                            <asp:ListItem Value="SRD">SRD - Surinamese Dollar</asp:ListItem>
                            <asp:ListItem Value="STD">STD - Sao Tomean Dobra</asp:ListItem>
                            <asp:ListItem Value="SVC">SVC - Salvadoran Colon</asp:ListItem>
                            <asp:ListItem Value="SYP">SYP - Syrian Pound</asp:ListItem>
                            <asp:ListItem Value="SZL">SZL - Swazi Lilangeni</asp:ListItem>
                            <asp:ListItem Value="THB">THB - Thai Baht</asp:ListItem>
                            <asp:ListItem Value="TJS">TJS - Tajikistani Somoni</asp:ListItem>
                            <asp:ListItem Value="TMT">TMT - Turkmenistani Manat</asp:ListItem>
                            <asp:ListItem Value="TND">TND - Tunisian Dinar</asp:ListItem>
                            <asp:ListItem Value="TOP">TOP - Tongan Pa'anga</asp:ListItem>
                            <asp:ListItem Value="TRY">TRY - Turkish Lira</asp:ListItem>
                            <asp:ListItem Value="TTD">TTD - Trinidadian Dollar</asp:ListItem>
                            <asp:ListItem Value="TVD">TVD - Tuvaluan Dollar</asp:ListItem>
                            <asp:ListItem Value="TWD">TWD - Taiwan New Dollar</asp:ListItem>
                            <asp:ListItem Value="TZS">TZS - Tanzanian Shilling</asp:ListItem>
                            <asp:ListItem Value="UAH">UAH - Ukrainian Hryvna</asp:ListItem>
                            <asp:ListItem Value="UGX">UGX - Ugandan Shilling</asp:ListItem>
                            <asp:ListItem Value="USD">USD - US Dollar</asp:ListItem>
                            <asp:ListItem Value="UYU">UYU - Uruguayan Peso</asp:ListItem>
                            <asp:ListItem Value="UZS">UZS - Uzbekistani Som</asp:ListItem>
                            <asp:ListItem Value="VEF">VEF - Venezuelan Bolivar</asp:ListItem>
                            <asp:ListItem Value="VND">VND - Vietnamese Dong</asp:ListItem>
                            <asp:ListItem Value="VUV">VUV - Ni-Vanuatu Vatu</asp:ListItem>
                            <asp:ListItem Value="WST">WST - Samoan Tala</asp:ListItem>
                            <asp:ListItem Value="XAF">XAF - Central African CFA Franc BEAC</asp:ListItem>
                            <asp:ListItem Value="XAG">XAG - Silver Ounce</asp:ListItem>
                            <asp:ListItem Value="XAU">XAU - Gold Ounce</asp:ListItem>
                            <asp:ListItem Value="XBT">XBT - Bitcoin</asp:ListItem>
                            <asp:ListItem Value="XCD">XCD - East Caribbean Dollar</asp:ListItem>
                            <asp:ListItem Value="XDR">XDR - IMF Special Drawing Rights</asp:ListItem>
                            <asp:ListItem Value="XOF">XOF - CFA Franc</asp:ListItem>
                            <asp:ListItem Value="XPD">XPD - Palladium Ounce</asp:ListItem>
                            <asp:ListItem Value="XPF">XPF - CFP Franc</asp:ListItem>
                            <asp:ListItem Value="XPT">XPT - Platinum Ounce</asp:ListItem>
                            <asp:ListItem Value="YER">YER - Yemeni Rial</asp:ListItem>
                            <asp:ListItem Value="ZAR">ZAR - South African Rand</asp:ListItem>
                            <asp:ListItem Value="ZMW">ZMW - Zambian Kwacha</asp:ListItem>
                            <asp:ListItem Value="ZWD">ZWD - Zimbabwean Dollar</asp:ListItem>

                        </asp:DropDownList>
                    </td>

                </tr>
                <tr>
                    <td class="formCaptionTd" width="150px">Start Day of month:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlSday" required>
                            <asp:ListItem Value="">Select</asp:ListItem>
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                            <asp:ListItem Value="6">6</asp:ListItem>
                            <asp:ListItem Value="7">7</asp:ListItem>
                            <asp:ListItem Value="8">8</asp:ListItem>
                            <asp:ListItem Value="9">9</asp:ListItem>
                            <asp:ListItem Value="10">10</asp:ListItem>
                            <asp:ListItem Value="11">11</asp:ListItem>
                            <asp:ListItem Value="12">12</asp:ListItem>
                            <asp:ListItem Value="13">13</asp:ListItem>
                            <asp:ListItem Value="14">14</asp:ListItem>
                            <asp:ListItem Value="15">15</asp:ListItem>
                            <asp:ListItem Value="16">16</asp:ListItem>
                            <asp:ListItem Value="17">17</asp:ListItem>
                            <asp:ListItem Value="18">18</asp:ListItem>
                            <asp:ListItem Value="19">19</asp:ListItem>
                            <asp:ListItem Value="20">20</asp:ListItem>
                            <asp:ListItem Value="21">21</asp:ListItem>
                            <asp:ListItem Value="22">22</asp:ListItem>
                            <asp:ListItem Value="23">23</asp:ListItem>
                            <asp:ListItem Value="24">24</asp:ListItem>
                            <asp:ListItem Value="25">25</asp:ListItem>
                            <asp:ListItem Value="26">26</asp:ListItem>
                            <asp:ListItem Value="27">27</asp:ListItem>
                            <asp:ListItem Value="28">28</asp:ListItem>
                            <asp:ListItem Value="29">29</asp:ListItem>
                            <asp:ListItem Value="30">30</asp:ListItem>
                            <asp:ListItem Value="31">31</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd" width="150px">Fees Due Date</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlddate">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                            <asp:ListItem Value="6">6</asp:ListItem>
                            <asp:ListItem Value="7">7</asp:ListItem>
                            <asp:ListItem Value="8">8</asp:ListItem>
                            <asp:ListItem Value="9">9</asp:ListItem>
                            <asp:ListItem Value="10">10</asp:ListItem>
                            <asp:ListItem Value="11">11</asp:ListItem>
                            <asp:ListItem Value="12">12</asp:ListItem>
                            <asp:ListItem Value="13">13</asp:ListItem>
                            <asp:ListItem Value="14">14</asp:ListItem>
                            <asp:ListItem Value="15">15</asp:ListItem>
                            <asp:ListItem Value="16">16</asp:ListItem>
                            <asp:ListItem Value="17">17</asp:ListItem>
                            <asp:ListItem Value="18">18</asp:ListItem>
                            <asp:ListItem Value="19">19</asp:ListItem>
                            <asp:ListItem Value="20">20</asp:ListItem>
                            <asp:ListItem Value="21">21</asp:ListItem>
                            <asp:ListItem Value="22">22</asp:ListItem>
                            <asp:ListItem Value="23">23</asp:ListItem>
                            <asp:ListItem Value="24">24</asp:ListItem>
                            <asp:ListItem Value="25">25</asp:ListItem>
                            <asp:ListItem Value="26">26</asp:ListItem>
                            <asp:ListItem Value="27">27</asp:ListItem>
                            <asp:ListItem Value="28">28</asp:ListItem>
                            <asp:ListItem Value="29">29</asp:ListItem>
                            <asp:ListItem Value="30">30</asp:ListItem>
                            <asp:ListItem Value="31">31</asp:ListItem>
                        </asp:DropDownList>
                    </td>

                </tr>
                <tr>
                    <td class="formCaptionTd" width="150px" required>Late Fees Start Date</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddldsdate">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                            <asp:ListItem Value="6">6</asp:ListItem>
                            <asp:ListItem Value="7">7</asp:ListItem>
                            <asp:ListItem Value="8">8</asp:ListItem>
                            <asp:ListItem Value="9">9</asp:ListItem>
                            <asp:ListItem Value="10">10</asp:ListItem>
                            <asp:ListItem Value="11">11</asp:ListItem>
                            <asp:ListItem Value="12">12</asp:ListItem>
                            <asp:ListItem Value="13">13</asp:ListItem>
                            <asp:ListItem Value="14">14</asp:ListItem>
                            <asp:ListItem Value="15">15</asp:ListItem>
                            <asp:ListItem Value="16">16</asp:ListItem>
                            <asp:ListItem Value="17">17</asp:ListItem>
                            <asp:ListItem Value="18">18</asp:ListItem>
                            <asp:ListItem Value="19">19</asp:ListItem>
                            <asp:ListItem Value="20">20</asp:ListItem>
                            <asp:ListItem Value="21">21</asp:ListItem>
                            <asp:ListItem Value="22">22</asp:ListItem>
                            <asp:ListItem Value="23">23</asp:ListItem>
                            <asp:ListItem Value="24">24</asp:ListItem>
                            <asp:ListItem Value="25">25</asp:ListItem>
                            <asp:ListItem Value="26">26</asp:ListItem>
                            <asp:ListItem Value="27">27</asp:ListItem>
                            <asp:ListItem Value="28">28</asp:ListItem>
                            <asp:ListItem Value="29">29</asp:ListItem>
                            <asp:ListItem Value="30">30</asp:ListItem>
                            <asp:ListItem Value="31">31</asp:ListItem>
                        </asp:DropDownList>
                    </td>

                </tr>
                <tr>
                    <td class="formCaptionTd" width="150px">Late Fees End Date</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddldedate">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                            <asp:ListItem Value="6">6</asp:ListItem>
                            <asp:ListItem Value="7">7</asp:ListItem>
                            <asp:ListItem Value="8">8</asp:ListItem>
                            <asp:ListItem Value="9">9</asp:ListItem>
                            <asp:ListItem Value="10">10</asp:ListItem>
                            <asp:ListItem Value="11">11</asp:ListItem>
                            <asp:ListItem Value="12">12</asp:ListItem>
                            <asp:ListItem Value="13">13</asp:ListItem>
                            <asp:ListItem Value="14">14</asp:ListItem>
                            <asp:ListItem Value="15">15</asp:ListItem>
                            <asp:ListItem Value="16">16</asp:ListItem>
                            <asp:ListItem Value="17">17</asp:ListItem>
                            <asp:ListItem Value="18">18</asp:ListItem>
                            <asp:ListItem Value="19">19</asp:ListItem>
                            <asp:ListItem Value="20">20</asp:ListItem>
                            <asp:ListItem Value="21">21</asp:ListItem>
                            <asp:ListItem Value="22">22</asp:ListItem>
                            <asp:ListItem Value="23">23</asp:ListItem>
                            <asp:ListItem Value="24">24</asp:ListItem>
                            <asp:ListItem Value="25">25</asp:ListItem>
                            <asp:ListItem Value="26">26</asp:ListItem>
                            <asp:ListItem Value="27">27</asp:ListItem>
                            <asp:ListItem Value="28">28</asp:ListItem>
                            <asp:ListItem Value="29">29</asp:ListItem>
                            <asp:ListItem Value="30">30</asp:ListItem>
                            <asp:ListItem Value="31">31</asp:ListItem>
                        </asp:DropDownList></td>

                </tr>
                <tr>
                    <td class="formCaptionTd" width="150px">Tax &nbsp;
                <asp:CheckBox runat="server" ID="chktax" Style="float: right" AutoPostBack="True" OnCheckedChanged="chktax_CheckedChanged" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txttax" CssClass="standardField tax"></asp:TextBox>&nbsp;&nbsp;
                        <asp:DropDownList runat="server" ID="ddltaxtype" CssClass="tax">
                            <asp:ListItem Value="0" Text="% Age"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Fixed"></asp:ListItem>
                        </asp:DropDownList>
                    </td>

                </tr>
                <tr>
                    <td></td>
                    <td></td>

                </tr>
                <tr>
                    <td class="formCaptionTd" width="150px"></td>
                    <td>
                        <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" Text="Save" CssClass="standardFormButton" /></td>

                </tr>
            </table>
            <div>
                <asp:Repeater ID="RptrdateManagement" runat="server" OnItemCommand="RptrdateManagement_ItemCommand">
                    <HeaderTemplate>
                        <table id="tblconctract" class="smallGrid">
                            <tr class="smallGridHead">
                                <td>Title</td>
                                <td>Start Date(for every month)</td>
                                <td>Due Date</td>
                                <td></td>
                                <td></td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="smallGrid">
                            <td>
                                <asp:HiddenField ID="hfid" runat="server" Value='<%# Eval("ID") %>' />
                                <asp:Label ID="lblname" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblsday" runat="server" Text='<%# Eval("startdate") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblddate" runat="server" Text='<%# Eval("Fee_DueDate") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:LinkButton ID="lbtnedit" runat="server" Text="Edit" CommandName="Edit"></asp:LinkButton>
                            </td>
                            <td>
                                <asp:LinkButton ID="lbtndel" runat="server" Text="Delete" OnClientClick="return confirm('Are you sure want to delete');" CommandName="Delete"></asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </form>
</body>
</html>
