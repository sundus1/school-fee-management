﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    string Totalcharges = "";
    string Totaldiscount = "";
    studentinvoice objstudent;
    studentparameter objinvoice;
    AvaimaStorageUploader objuploader;
    decimal totalfees = 0;
    decimal totaldue = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        SendInvoice("student-124", "94c2459c-c346-4f9f-8507-e9ffb35d22a0", "1385", "muhammad_usman200887@yahoo.com", "Sacred Heart School");
    }

    private void SendInvoice(string Studentid, string InstanceID, string PID, string parentemail, string schoolname)
    {
        DataSet ds = new DataSet();
        AvaimaEmailAPI objemail = new AvaimaEmailAPI();
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            string query = "feesvoucher";
            using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
            {
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@instanceid", InstanceID);
                da.SelectCommand.Parameters.AddWithValue("@studentid", Studentid);
                da.SelectCommand.Parameters.AddWithValue("@PID", PID);
                da.Fill(ds);
            }
        }
        string headerfile = "";
        string Footer = "";
        string lblclass = "";
        string lblissuedate = "";
        string lblregno = "";
        string lblstudentname = "";
        string lblfathername = "";
        string lblslipno = "";
        string lblsession = "";
        string lblperiod = "";
        string lblduedate = "";
        string lbldueamount = "";
        string fees = "0";
        string charges = "0";
        string Discount = "0";
        string ecomment = "";
        string ecommentamount = "0";
        string extradiscount = "";
        string extradiscountamount = "0";
        string adjustmentcomment = "";
        string adjustmentcommentamount = "0";
        decimal balanceval = 0;
        if (ds.Tables[0].Rows.Count > 0)
        {
            objuploader = new AvaimaStorageUploader();
            headerfile = objuploader.Singlefilelink(InstanceID, ds.Tables[0].Rows[0]["headerfile"].ToString());
            Footer = ds.Tables[0].Rows[0]["footer"].ToString();
        }
        if (ds.Tables[2].Rows.Count > 0)
        {
            lblclass = ds.Tables[2].Rows[0]["title"].ToString();
        }
        if (ds.Tables[1].Rows.Count > 0)
        {
            lblissuedate = Convert.ToDateTime(ds.Tables[1].Rows[0]["issuedate"].ToString()).ToString("dd-MMMM-yyyy");
            lblregno = ds.Tables[1].Rows[0]["Regno"].ToString();
            lblstudentname = ds.Tables[1].Rows[0]["studentname"].ToString();
            lblfathername = ds.Tables[1].Rows[0]["ParentName"].ToString();
        }
        if (ds.Tables[3].Rows.Count > 0)
        {
            lblslipno = ds.Tables[3].Rows[0]["recid"].ToString();

            DateTime dtstart = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
            if (dtstart.Month >= Convert.ToInt32(ds.Tables[0].Rows[0]["month"].ToString()))
            {
                lblsession = dtstart.Year + "-" + (dtstart.Year + 1);
            }
            else
            {
                lblsession = (dtstart.Year - 1) + "-" + dtstart.Year;
            }

            lblperiod = dtstart.ToString("MMM yyyy");

            fees = ds.Tables[3].Rows[0]["TotalPackagefees"].ToString();
            balanceval = Convert.ToDecimal(ds.Tables[4].Rows[0]["balance"].ToString()) - Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalPackagefees"].ToString());
            totalfees = Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalPackagefees"].ToString());
            if (Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString()) > 0)
            {
                charges = ds.Tables[3].Rows[0]["TotalCharges"].ToString();
                totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString());
            }
            if (Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString()) > 0)
            {
                Discount = ds.Tables[3].Rows[0]["Totaldiscount"].ToString();
                totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString());
            }
            if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString()) > 0)
            {
                ecomment = ds.Tables[3].Rows[0]["chargescomment"].ToString();
                ecommentamount = ds.Tables[3].Rows[0]["Totaldiscount"].ToString();
                totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString());
            }
            if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString()) > 0)
            {
                extradiscount = ds.Tables[3].Rows[0]["discountcomment"].ToString();
                extradiscountamount = ds.Tables[3].Rows[0]["extradiscount"].ToString();
                totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString());
            }
            if (Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString()) > 0)
            {
                adjustmentcomment = ds.Tables[3].Rows[0]["adjustmentcomment"].ToString();
                adjustmentcommentamount = ds.Tables[3].Rows[0]["adjustment"].ToString();
                totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString());
            }
            totalfees = totalfees + balanceval;
        }
        if (ds.Tables[5].Rows.Count > 0)
        {
            DateTime enddate = Convert.ToDateTime(ds.Tables[3].Rows[0]["todate"].ToString());
            string[] duedate = ds.Tables[5].Rows[0]["Fee_DueDate"].ToString().Split('#');
            string[] afterduedate = ds.Tables[5].Rows[0]["LateFeesStartDate"].ToString().Split('#');
            enddate = enddate.AddDays(Convert.ToDouble(duedate[0]));
            enddate = enddate.AddDays(Convert.ToDouble(afterduedate[0]));
            lblduedate = enddate.ToString("dd-MMM-yyyy");

            decimal latefee = 0;
            try
            {
                latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["LateFeeCharges"].ToString());
            }
            catch
            {
                latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["latefees"].ToString());
            }
            latefee = latefee + totalfees;
            lbldueamount = latefee.ToString();
        }


        StringBuilder HTML = new StringBuilder();
        HTML.AppendLine("<div class=\"receiptCont\"><div class=\"header\"><img src=\"" + headerfile + "\" alt=\"img\" /></div>");
        HTML.AppendLine("<div class=\"tablehead\"><h2 class=\"color\"><span>Slip. ID </span><span>" + lblslipno + "</span></h2><h2 class=\"color\">Parent COPY</h2>");
        HTML.AppendLine("<div class=\"clear\"></div><table width=\"100%\" border=\"0\"><tr><td scope=\"col\"><b>Class:</b>" + lblclass + "<br />");
        HTML.AppendLine("<b>Issue Date:</b> " + lblissuedate + "<br /><b>Reg. No.:</b> " + lblregno + "<br />");
        HTML.AppendLine("<b>Session:</b> " + lblsession + "<br /><b>G. R. No.:</b> " + lblregno + "<br /><b>Period:</b> " + lblperiod + "<br />");
        HTML.AppendLine("</td></tr></table><table width=\"100%\" border=\"0\"><tr>");
        HTML.AppendLine("<td colspan=\"2\" class=\"name\">Student&acute;s Name: <b>" + lblstudentname + "</b></td></tr>");
        HTML.AppendLine("<tr><td colspan=\"2\" class=\"fname\">Father&acute;s Name: " + lblfathername + "</td></tr></table></div>");
        HTML.AppendLine("<div class=\"clear\"></div><div class=\"tablehead\"><h2><span>Fee Details</span></h2></div>");
        HTML.AppendLine("<table width=\"100%\" border=\"0\" cellspacing=\"0\" class=\"feedetail\">");
        HTML.AppendLine("<tr><td>Tuition  Fee " + lblperiod + "</td><td>" + fees + "</td></tr>");

        if (charges != "0")
            HTML.AppendLine("<tr><td>Total Individual Charges</td><td>" + charges + "</td></tr>");
        if (Discount != "0")
            HTML.AppendLine("<tr><td>Total Individual Discount</td><td>" + Discount + "</td></tr>");
        if (ecommentamount != "0")
            HTML.AppendLine("<tr><td>" + ecomment + "</td><td>" + ecommentamount + "</td></tr>");
        if (extradiscountamount != "0")
            HTML.AppendLine("<tr><td>" + extradiscount + "</td><td>" + extradiscountamount + "</td></tr>");
        if (adjustmentcommentamount != "0")
            HTML.AppendLine("<tr><td>" + adjustmentcomment + "</td><td>" + adjustmentcommentamount + "</td></tr>");

        HTML.AppendLine("<tr><td>Previous Balance</td><td>" + balanceval.ToString() + "</td></tr>");
        HTML.AppendLine("<tr class=\"totalAm\"><td><b>Total Due</b></td><td><b>" + totalfees.ToString() + "</b></td></tr>");
        HTML.AppendLine("</table>");
        HTML.AppendLine("<table width=\"100%\" border=\"0\" class=\"validtotal\">");
        HTML.AppendLine("<tr><td colspan=\"2\">Due Date: <b>" + lblduedate + "</b></td></tr>");
        HTML.AppendLine("<tr><td>Amount payable after due date</td><td>" + lbldueamount + "</td></tr></table>");
        HTML.AppendLine("<br /><br /><br /><table width=\"100%\" border=\"0\" class=\"signature\">");
        HTML.AppendLine("<tr><td>Cashier</td><td>Officer</td></tr></table>");
        HTML.AppendLine("<div style=\"font-size: 11px; color: #b5b5b5; font-weight: lighter; padding: 11px 5px 0px 5px;\">" + Footer + "</div></div>");

        if (!string.IsNullOrEmpty(parentemail))
            objemail.send_email(parentemail, "Fee Management(" + schoolname + ")", "", HTML.ToString(), "Fee Invoice");
    }
}