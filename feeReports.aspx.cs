﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class feeReports : AvaimaThirdpartyTool.AvaimaWebPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtto.Text = DateTime.Now.ToString("dd MMMM yyyy");
            txtfrom.Text = DateTime.Now.ToString("dd MMMM yyyy");
        }
    }
    protected void btnreport_Click(object sender, EventArgs e)
    {
        try
        {
            studentinvoice objfee = new studentinvoice();
            DataSet dsfee = new DataSet();
            dsfee = objfee.GetFeerangestatics(this.InstanceID, Convert.ToDateTime(txtfrom.Text), Convert.ToDateTime(txtto.Text));
            tabletr(dsfee);
            ScriptManager.RegisterStartupScript(this, GetType(), "resizeiframepageb", "resizeiframepage();", true);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "erroralert", "alert(" + ex.Message + ");", true);
        }
    }
    private void tabletr(DataSet ds)
    {
        decimal total = 0;
        TableRow row = new TableRow();
        if (ds.Tables[0].Rows.Count > 0)
        {
            row.Cells.Add(new TableCell() { Text = "How many Paid" });
            row.Cells.Add(new TableCell() { Text = ds.Tables[0].Rows[0]["fee"].ToString() });
            tbl.Rows.Add(row);
        }

        if (ds.Tables[1].Rows.Count > 0)
        {
            for (int r = 0; r < ds.Tables[1].Rows.Count; r++)
            {
                row = new TableRow();
                row.Cells.Add(new TableCell() { Text = ds.Tables[1].Rows[r]["paymentmethod"].ToString() });
                row.Cells.Add(new TableCell() { Text = ds.Tables[1].Rows[r]["fee"].ToString() });
                tbl.Rows.Add(row);
                total += Convert.ToDecimal(ds.Tables[1].Rows[r]["fee"].ToString());
            }
        }
        row = new TableRow();
        row.Cells.Add(new TableCell() { Text = "Total" });
        row.Cells.Add(new TableCell() { Text = total.ToString() });
        tbl.Rows.Add(row);

    }
}