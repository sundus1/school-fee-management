﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FeePakagelist.aspx.cs" Inherits="FeePakagelist" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="_assets/css/style.css" rel="stylesheet" />
    <link href="_assets/jquery-ui.css" rel="stylesheet" />
    <link href="_assets/jquery.datepick.css" rel="stylesheet" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <script src="_assets/jquery.datepick.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="mainContentDiv">
            <br/>
            <asp:LinkButton runat="server" Text="Add New Package" OnClick="Unnamed1_Click1"></asp:LinkButton>
            <br/>
            <br/>
            <div style="display:none">
            <asp:TextBox runat="server" ID="txtsearchtext" CssClass=" standardField"></asp:TextBox>
            <asp:Button runat="server" ID="btnsearch" Text="Search" OnClick="btnSearch_Click" CssClass="standardFormButton" />
                </div>
            <asp:Repeater ID="rptrfeepakages" runat="server" OnItemCommand="rptrfeepakages_ItemCommand" OnItemDataBound="rptrfeepakages_ItemDataBound">
                <HeaderTemplate>
                    <table class="smallGrid">
                        <tr class="smallGridHead">
                            <td style="display: none"></td>
                            <td>Title
                            </td>
                            <td>Fee Charege</td>
                            <td></td>
                            <td></td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td style="display: none">
                            <asp:Label ID="lblID" Text='<%# Eval("ID") %>' runat="server"></asp:Label></td>

                        <td>
                            <asp:Label ID="lblgridtitle" Text='<%# Eval("Title") %>' runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblgridamount" Text='<%# Eval("Amount") %>' runat="server"></asp:Label></td>
                        <td>
                            <asp:LinkButton ID="lnkedit" CommandName="select" Text="Select" CommandArgument='<%# Eval("ID") %>' runat="server"></asp:LinkButton></td>
                        <td>
                            <asp:LinkButton ID="lnkdelete" CommandName="delete" Text="Delete" CommandArgument='<%# Eval("ID") %>' runat="server" OnClientClick="return confirm('Are You Sure You Want to Delete. !')"></asp:LinkButton>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <asp:Label runat="server" ID="lblgriderror" Text="No record found" ForeColor="red"></asp:Label>
        </div>
    </form>
</body>
</html>
