﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class foldersetting : AvaimaThirdpartyTool.AvaimaWebPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        txttax.Enabled = false;
        ddltaxtype.Enabled = false;
        if (!IsPostBack)
        {
            bindrept();
        }



    }
    private void bindrept()
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM tbl_Settings WHERE InstanceId = @InstanceId and gen=0", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        RptrdateManagement.DataSource = dt;
                        RptrdateManagement.DataBind();
                    }
                    else
                    {
                        RptrdateManagement.DataSource = null;
                        RptrdateManagement.DataBind();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw;
        }

    }
    private void binddata(string id)
    {
        txttax.Enabled = false;
        ddltaxtype.Enabled = false;
        //currentinstance
        try
        {
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SELECT * FROM tbl_Settings WHERE ID=@id", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@id", id);
                        DataTable dt = new DataTable();
                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        adp.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            txtname.Text = dt.Rows[0]["name"].ToString();
                            hfcurrentinstance.Value = dt.Rows[0]["ID"].ToString();
                            if (!string.IsNullOrEmpty(dt.Rows[0]["Currency"].ToString()))
                                ddlcurrencyselect.SelectedValue = dt.Rows[0]["Currency"].ToString();

                            if (!string.IsNullOrEmpty(dt.Rows[0]["Fee_DueDate"].ToString()))
                                ddlddate.SelectedValue = dt.Rows[0]["Fee_DueDate"].ToString();

                            if (!string.IsNullOrEmpty(dt.Rows[0]["startdate"].ToString()))
                                ddlSday.SelectedValue = dt.Rows[0]["startdate"].ToString();

                            if (!string.IsNullOrEmpty(dt.Rows[0]["LateFeesStartDate"].ToString()))
                                ddldsdate.SelectedValue = dt.Rows[0]["LateFeesStartDate"].ToString();

                            if (!string.IsNullOrEmpty(dt.Rows[0]["LateFeesEndDate"].ToString()))
                                ddldedate.SelectedValue = dt.Rows[0]["LateFeesEndDate"].ToString();
                            if (!string.IsNullOrEmpty(dt.Rows[0]["Istax"].ToString()))
                            {
                                if (dt.Rows[0]["Istax"].ToString() == "False")
                                {
                                    txttax.Enabled = false;
                                    ddltaxtype.Enabled = false;
                                    chktax.Checked = false;

                                }
                                else
                                {
                                    txttax.Enabled = true;
                                    ddltaxtype.Enabled = true;
                                    chktax.Checked = true;

                                    if (!string.IsNullOrEmpty(dt.Rows[0]["Isfixed"].ToString()))
                                    {
                                        ddltaxtype.SelectedValue = dt.Rows[0]["Isfixed"].ToString();

                                    }
                                    if (!string.IsNullOrEmpty(dt.Rows[0]["Amount"].ToString()))
                                    {
                                        txttax.Text = dt.Rows[0]["Amount"].ToString();
                                    }
                                }
                            }

                        }

                    }
                    con.Close();
                }
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (Convert.ToInt32(ddlddate.SelectedValue) != 0 && Convert.ToInt32(ddldsdate.SelectedValue) != 0)
                if (Convert.ToInt32(ddlddate.SelectedValue) >= Convert.ToInt32(ddldsdate.SelectedValue))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "aa", "alert('Late fee Start Date must not be Less/Equal to fee due date')", true);
                    return;
                }
            if (hfcurrentinstance.Value != "0")
            {
                {
                    using (
                        SqlConnection con =
                            new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("sp_updatefoldersettings", con))
                        {
                            con.Open();
                            cmd.Parameters.AddWithValue("@currency", ddlcurrencyselect.SelectedValue);
                            cmd.Parameters.AddWithValue("@duedate", ddlddate.SelectedValue);
                            cmd.Parameters.AddWithValue("@startdate", ddlSday.SelectedValue);
                            cmd.Parameters.AddWithValue("@latefeestartdate", ddldsdate.SelectedValue);
                            cmd.Parameters.AddWithValue("@latefeeenddate", ddldedate.SelectedValue);
                            cmd.Parameters.AddWithValue("@id", hfcurrentinstance.Value);
                            cmd.Parameters.AddWithValue("@name", txtname.Text);

                            if (chktax.Checked)
                                cmd.Parameters.AddWithValue("@Istax", 1);
                            else
                            {
                                cmd.Parameters.AddWithValue("@Istax", 0);
                            }
                            cmd.Parameters.AddWithValue("@Isfixed", ddltaxtype.SelectedValue);
                            if (!string.IsNullOrEmpty(Convert.ToString(txttax.Text)))
                                cmd.Parameters.AddWithValue("@Amount", txttax.Text);
                            else
                            {
                                cmd.Parameters.AddWithValue("@Amount", 0);
                            }


                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.ExecuteNonQuery();
                            //DataTable dt = new DataTable();
                            //SqlDataAdapter adp = new SqlDataAdapter(cmd);
                            //adp.Fill(dt);
                            hfcurrentinstance.Value = "0";

                        }
                        con.Close();
                    }
                }
            }
            else
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("sp_insertfoldersettings", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@currency", ddlcurrencyselect.SelectedValue);
                        cmd.Parameters.AddWithValue("@duedate", ddlddate.SelectedValue);
                        cmd.Parameters.AddWithValue("@lfeesenddate", ddldsdate.SelectedValue);
                        cmd.Parameters.AddWithValue("@name", txtname.Text);
                        cmd.Parameters.AddWithValue("@lfeesstartdate", ddldedate.SelectedValue);
                        cmd.Parameters.AddWithValue("@startdate", ddlSday.SelectedValue);
                        cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                        if (chktax.Checked)
                            cmd.Parameters.AddWithValue("@Istax", 1);
                        else
                        {
                            cmd.Parameters.AddWithValue("@Istax", 0);
                        }
                        cmd.Parameters.AddWithValue("@Isfixed", ddltaxtype.SelectedValue);

                        if (!string.IsNullOrEmpty(Convert.ToString(txttax.Text)))
                            cmd.Parameters.AddWithValue("@Amount", txttax.Text);
                        else
                        {
                            cmd.Parameters.AddWithValue("@Amount", 0);
                        }

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.ExecuteNonQuery();
                        //DataTable dt = new DataTable();
                        //SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        //adp.Fill(dt);


                    }
                    con.Close();
                }
            }
            txtname.Text = "";
            ddlcurrencyselect.SelectedValue = "";
            ddlddate.SelectedValue = "0";
            ddldedate.SelectedValue = "0";
            ddldsdate.SelectedValue = "0";
            ddlSday.SelectedValue = "";
            bindrept();
            ScriptManager.RegisterStartupScript(this, GetType(), "savedd", "alert('Save Successfully!!')", true);
        }
        catch (Exception ex)
        {
            throw;

        }
    }
    protected void chktax_CheckedChanged(object sender, EventArgs e)
    {
        if (chktax.Checked)
        {
            txttax.Enabled = true;
            ddltaxtype.Enabled = true;
        }
        else
        {

            txttax.Enabled = false;
            ddltaxtype.Enabled = false;

        }
    }
    protected void RptrdateManagement_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        HiddenField id = e.Item.FindControl("hfid") as HiddenField;
        if (e.CommandName == "Edit")
        {
            binddata(id.Value);
        }
        if (e.CommandName == "Delete")
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Delete tbl_Settings where ID=@id", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", id.Value);
                    
                    cmd.CommandType = CommandType.Text;

                    cmd.ExecuteNonQuery();
                    //DataTable dt = new DataTable();
                    //SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    //adp.Fill(dt);

                    
                }
                con.Close();
            }
        }
        bindrept();
    }
}