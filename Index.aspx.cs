﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.IO;
using System.Collections;
using AvaimaWS;
using System.Text.RegularExpressions;
using System.Text;

public partial class Index : AvaimaThirdpartyTool.AvaimaWebPage
{
    Excel excel = new Excel();
    PagedDataSource pds = new PagedDataSource();
    private Dictionary<string, string> dict = new Dictionary<string, string>();
    List<int> delid = new List<int>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (hfdivstate.Value == "1")
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "script", "document.getElementById('searchdate').style.display='' ; document.getElementById('divmenu').style.display='none'", true);
        }
        PagerControl.OnPageIndex_Changed += pg_rptrcontract_OnPageIndex_Changed;
        if (!IsPostBack)
        {
            PagerControl.CurrentPage = 0;

            Fillcategorydropdown();
            Rptrfillfeesmanagement(0);
            hfparentid.Value = "0";
            TreeNode parentNode = new TreeNode(String.Format("<input type='radio' name='rbtncat' value='Root' onclick='setvalue(0)'/>&nbsp; Root"));
            FillTreeView(parentNode.ChildNodes, "0");
            TreeView1.Nodes.Add(parentNode);
         }



        //    try
        //    {
        //        if (!IsPostBack)
        //        {
        //            if (!string.IsNullOrEmpty(Convert.ToString(Request["id"])))
        //            {
        //                hfpakageid.Value = Request["id"];
        //                rptrfill();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}
    }

    public void Rptrfillfeesmanagement(int id)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("getallfeesrecord_new", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                   

                    pds.AllowPaging = true;
                    pds.DataSource = dt.DefaultView;
                    pds.CurrentPageIndex = PagerControl.CurrentPage;
                    pds.PageSize = PagerControl.PageSize;
                    PagerControl.TotalRecords = dt.Rows.Count;
                    RptrFeesManagement.DataSource = pds;
                    RptrFeesManagement.DataBind();
                    PagerControl.LoadStatus();

                    if (dt.Rows.Count > 0)
                    {
                        lblgriderror.Text = "";


                    }
                    else
                    {
                        lblgriderror.Text = "No Record Found";
                        RptrFeesManagement.DataSource = pds;
                        RptrFeesManagement.DataBind();
                    }
                }
                con.Close();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    //protected void rptrfeedetail_ItemCommand(object source, RepeaterCommandEventArgs e)
    //{
    //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
    //    {
    //        if (e.CommandName == "edit")
    //        {
    //        }
    //        else if (e.CommandName == "delete")
    //        {
    //            using (SqlCommand cmd = new SqlCommand("Delete from tbl_feesPakgaeDetail where DID=@did", con))
    //            {
    //                if (con.State.ToString() != "Open")
    //                    con.Open();
    //                cmd.Parameters.AddWithValue("@did", e.CommandArgument);
    //                cmd.ExecuteNonQuery();
    //                rptrfill();
    //            }
    //        }
    //        else if (e.CommandName == "update")
    //        {
    //            using (SqlCommand cmd = new SqlCommand("update tbl_feesPakgaeDetail set DetaiilTitle=@detailtitle,DetailAmount=@detailamount,DetailDate=GETDATE() where DID=@did", con))
    //            {
    //                if (con.State.ToString() != "Open")
    //                    con.Open();
    //                cmd.Parameters.AddWithValue("@did", e.CommandArgument);
    //                cmd.Parameters.AddWithValue("@did", e.CommandArgument);
    //                cmd.Parameters.AddWithValue("@did", e.CommandArgument);
    //                cmd.ExecuteNonQuery();
    //                rptrfill();
    //            }

    //        }
    //    }
    //}
    //protected void rptrfeedetail_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{
    //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
    //    {
    //        object container = e.Item.DataItem;
    //        Label lbldates = e.Item.FindControl("lblgriddate") as Label;
    //        lbldates.Text = Convert.ToDateTime(DataBinder.Eval(container, "DetailDate")).ToString("dd/MMM/yyyy");
    //    }
    //}
    //protected void btnsumbit_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        using (SqlCommand cmd = new SqlCommand("insert into tbl_feesPakgaeDetail (PID,DetaiilTitle,DetailAmount,DetailDate ) values (@detailid,@detailtitle,@detaiilamount,GETDATE())", con))
    //        {
    //            if (con.State.ToString() != "Open")
    //                con.Open();
    //            cmd.Parameters.AddWithValue("@detailid", hfpakageid.Value);
    //            cmd.Parameters.AddWithValue("@detailtitle", txtfeeitemtitle.Value);
    //            cmd.Parameters.AddWithValue("@detaiilamount", txtfeeitemamount.Value);
    //            cmd.ExecuteNonQuery();
    //            rptrfill();
    //            con.Close();
    //        }
    //    }
    //    catch (Exception ex) { }
    //}
    protected void RptrFeesManagement_itemcommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "openpopup")
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                this.Redirect("Studentinfo.aspx?stid=" + e.CommandArgument + "&pstate=" + hfparentid.Value);
            }
        }
        if (e.CommandName == "search")
        {
            try
            {
                PagerControl.CurrentPage = 0;
                LinkButton lnk = e.Item.FindControl("lnktitle") as LinkButton;

                // for saving value in hidden field for insertion.
                if (!string.IsNullOrEmpty(lnk.Text))
                    hfparentid.Value = e.CommandArgument.ToString();

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    breadcrum(Convert.ToInt32(e.CommandArgument));
                    Breadcrumgridfill();
                    Rptrfillfeesmanagement(Convert.ToInt32(Convert.ToString(e.CommandArgument)));

                    //BindRepeater(e.CommandArgument.ToString());
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        if (e.CommandName == "edit")
        {
            LinkButton lnk = e.Item.FindControl("lnkedit") as LinkButton;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlDataAdapter adp = new SqlDataAdapter("SELECT ID,Title,PackageID,foldersetting FROM tbl_feesManagement WHERE ID=" + e.CommandArgument, con))
                {
                    con.Open();
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    hfeditid.Value = e.CommandArgument.ToString();
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            txttitle.Text = dr["Title"].ToString();
                            ddlpackage.SelectedValue = dr["PackageID"].ToString();
                            ddldateset.SelectedValue = String.IsNullOrEmpty(dr["foldersetting"].ToString()) ? "0" : dr["foldersetting"].ToString();
                        }
                    }


                }
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "openedit", "openeditdiv()", true);
        }

        if (e.CommandName == "archive")
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                this.Redirect("Archived.aspx?id=" + e.CommandArgument.ToString());
            }
        }

    }

    protected void RptrFeesManagement_itemdatabound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
            object container = e.Item.DataItem;
            Label lblmodeficationdate = e.Item.FindControl("lblmodeficationdate") as Label;
            lblmodeficationdate.Text = objATZ.GetDate(DataBinder.Eval(container, "ModificationDate").ToString(), HttpContext.Current.User.Identity.Name);

            Label lblcategory = e.Item.FindControl("lblstudentcheck") as Label;
            if (lblcategory.Text == "1" || lblcategory.Text == "1")
            {
                LinkButton lnk = e.Item.FindControl("lnktitle") as LinkButton;
                lnk.CommandName = "openpopup";
                lnk.CommandArgument = DataBinder.Eval(container, "StudendId").ToString();
                //lnk.CommandArgument = Eval("ID").ToString();


                ImageButton imgedit = e.Item.FindControl("imgedit") as ImageButton;
                imgedit.Visible = false;

            }
            Label lbl = e.Item.FindControl("lblstudentcheck") as Label;
            if (lbl.Text == "0" || lbl.Text == "0")
            {
                LinkButton lnktitle = e.Item.FindControl("lnktitle") as LinkButton;
                int a = 0;
                Label lblcount = e.Item.FindControl("lblcount") as Label;
                lblcount.Text = "(" + DataBinder.Eval(container, "t_records").ToString() + ")";
            }
            else
            {
                Image img = e.Item.FindControl("imgfolder") as Image;
                img.ImageUrl = "";
            }


            //Label lbldate = e.Item.FindControl("lbldatefield") as Label;
            //lbldate.Text = objATZ.GetDate(DataBinder.Eval(container, "datefield").ToString(),
            //                              HttpContext.Current.User.Identity.Name);
            //Label lbltime = e.Item.FindControl("lbltimefield") as Label;
            //lbltime.Text = objATZ.GetTime(DataBinder.Eval(container, "datefield").ToString(),
            //                              HttpContext.Current.User.Identity.Name);

            //if (!string.IsNullOrEmpty(DataBinder.Eval(container, "startdate").ToString()))
            //{
            //    Label lblconstartdate = e.Item.FindControl("lblconstartdate") as Label;
            //    lblconstartdate.Text = objATZ.GetDate(DataBinder.Eval(container, "startdate").ToString(),
            //                                          HttpContext.Current.User.Identity.Name);

            //    Label lblconstarttime = e.Item.FindControl("lblconstarttime") as Label;
            //    lblconstarttime.Text = Convert.ToDateTime(DataBinder.Eval(container, "startdate"))
            //                                  .ToString("H:mm:ss tt");

        }
    }


    protected void btntreeview_Click(object sender, EventArgs e)
    {
        {
            //var ids = string.Join(",", (from r in rptrcontract.Items.Cast<RepeaterItem>()
            //                            let selected = (r.FindControl("chkselecteditem") as CheckBox).Checked
            //                            let id = (r.FindControl("hf_ID") as HiddenField).Value
            //                            where selected
            //                            select id).ToList());

            var ids = (from r in RptrFeesManagement.Items.Cast<RepeaterItem>()
                       let selected = (r.FindControl("chkselecteditem") as CheckBox).Checked
                       let id = (r.FindControl("hf_ID") as HiddenField).Value
                       where selected
                       select id).ToList();

            foreach (var id in ids)
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    int a = 0;
                    int result = 0;
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("SELECT IsStudent from tbl_feesManagement WHERE id=@id", con))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        a = Convert.ToInt32(cmd.ExecuteScalar());
                    }


                    if (a != 1 || (hftreenodevalue.Value != "0"))
                    {
                        using (SqlCommand cmd = new SqlCommand("SELECT t_records FROM tbl_feesManagement WHERE id=@id", con))
                        {
                            cmd.Parameters.AddWithValue("@id", id);
                            result = Convert.ToInt32(cmd.ExecuteScalar());
                        }
                        using (SqlCommand cmd = new SqlCommand("updaterecords_new", con))
                        {
                            cmd.Parameters.AddWithValue("@id", id);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.ExecuteNonQuery();
                        }
                        using (SqlCommand cmd = new SqlCommand("UPDATE tbl_feesManagement SET PID=@p_id WHERE ID IN (@upd_ids)", con))
                        {
                            cmd.Parameters.AddWithValue("@p_id", hftreenodevalue.Value);
                            cmd.Parameters.AddWithValue("@upd_ids", id);
                            cmd.ExecuteNonQuery();
                        }
                        using (SqlCommand cmd = new SqlCommand("Updaterecordall", con))
                        {
                            cmd.Parameters.AddWithValue("@id", id);
                            cmd.Parameters.AddWithValue("@coderesult", result);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            this.Redirect("Index.aspx");
            Rptrfillfeesmanagement(Convert.ToInt32(hftreenodevalue.Value));
            TreeView1.Nodes.Clear();
            TreeNode parentNode = new TreeNode(String.Format("<input type='radio' name='rbtncat' value='Root' onclick='setvalue(0)'/>&nbsp; Root"));
            FillTreeView(parentNode.ChildNodes, "0");
            TreeView1.Nodes.Add(parentNode);
        }
    }

    protected void lnkaddstudnet_Click(object sender, EventArgs e)
    {

        this.Redirect("StudentInfo.aspx?new=1&pid=" + hfparentid.Value);
    }

    protected void lnkaddpartment_Click(object sender, EventArgs e)
    {
    }

    protected void lbtndel_Click(object sender, EventArgs e)
    {
        lnkDelRecords.Enabled = false;
        var ids = (from r in RptrFeesManagement.Items.Cast<RepeaterItem>()
                   let selected = (r.FindControl("chkselecteditem") as CheckBox).Checked
                   let id = (r.FindControl("hf_ID") as HiddenField).Value
                   where selected
                   select id).ToList();

        string reason = ddlRemoveReason.SelectedValue;
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            con.Open();
            foreach (var id in ids)
            {
                using (SqlCommand cmd = new SqlCommand("updaterecords_new", con))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
                using (SqlCommand cmd = new SqlCommand("del_records_new", con))
                {
                    cmd.Parameters.AddWithValue("@Pid", id);
                    cmd.Parameters.AddWithValue("@Reason", reason);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }

        }
        Rptrfillfeesmanagement(Convert.ToInt32(hfparentid.Value));
        TreeView1.Nodes.Clear();
        TreeNode parentNode = new TreeNode(String.Format("<input type='radio' name='rbtncat' value='Root' onclick='setvalue(0)'/>&nbsp; Root"));
        FillTreeView(parentNode.ChildNodes, "0");
        TreeView1.Nodes.Add(parentNode);
        ddlRemoveReason.SelectedValue = "";
        lnkDelRecords.Enabled = true;
    }

    protected void root_Click(object sender, EventArgs e)
    {
        //hfdivstate.Value = "0";
        hfparentid.Value = "0";
        Rptrfillfeesmanagement(0);
        rptBreadCrum.DataBind();
        lbtnupload.Visible = false;
        lbluploadsep.Visible = false;

        div_upload.Attributes.Add("style", "background:#f5f5f5;display:none;width: 300px; padding: 10px");
    }

    protected void rptBreadCrum_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.DataItem != null)
        {
            LinkButton lbtnBreadCrum = e.Item.FindControl("lbtnBreadCrum") as LinkButton;
            lbtnBreadCrum.Text = DataBinder.Eval(e.Item.DataItem, "Value").ToString();
            lbtnBreadCrum.CommandArgument = DataBinder.Eval(e.Item.DataItem, "Key").ToString();
        }
    }

    protected void rptBreadCrum_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "breadcrum")
        {
            Rptrfillfeesmanagement(Convert.ToInt32(e.CommandArgument));
            //using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString()))
            //{
            //    // for saving value in hidden field for insertion.
            //    hfparentid.Value = e.CommandArgument.ToString();
            //    using (
            //        SqlCommand cmd = new SqlCommand("SELECT * FROM tbl_feesManagement fm LEFT JOIN tbl_student ts ON fm.ID=ts.SID WHERE fm.InstanceId=@InstanceId and PID=@id", con))
            //    {
            //        cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
            //        cmd.Parameters.AddWithValue("@id", e.CommandArgument);
            //        SqlDataAdapter da = new SqlDataAdapter(cmd);
            //        DataSet ds = new DataSet();
            //        da.Fill(ds);
            //        if (ds.Tables[0].Rows.Count > 0)
            //        {
            //            //lbltotalrecords.Text = ds.Tables[0].Rows.Count.ToString();
            //            RptrFeesManagement.DataSource = ds.Tables[0];
            //            RptrFeesManagement.DataBind();
            //            lblgriderror.Text = "";
            //        }
            //        else
            //        {
            //            // lbltotalrecords.Text = ds.Tables[0].Rows.Count.ToString();

            //            lblgriderror.Text = "No Record Found
            //        }
            //        divbreadcrum.Controls.Clear();";
            //        breadcrum(Convert.ToInt32(e.CommandArgument));

            //        Breadcrumgridfill();

            //    }
            //}
        }
    }
    public void breadcrum(int id)
    {

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString()))
        {
            using (SqlCommand cmd = new SqlCommand("SELECT ID,Title,PID FROM tbl_feesManagement WHERE InstanceId=@InstanceId and ID=@id and IsStudent =0", con))
            {
                cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    dict.Add(ds.Tables[0].Rows[0]["ID"].ToString(), ds.Tables[0].Rows[0]["Title"].ToString());
                    if (Convert.ToInt32(ds.Tables[0].Rows[0]["PID"]) != 0)
                    {

                        breadcrum(Convert.ToInt32(ds.Tables[0].Rows[0]["PID"]));
                    }

                    //rptrcontract.DataSource = ds.Tables[0];
                    //rptrcontract.DataBind();
                }
            }
        }
    }

    public void FillTreeView(TreeNodeCollection treenode, string parentid)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            using (SqlDataAdapter adp = new SqlDataAdapter("SELECT ID,Title,PID FROM tbl_feesManagement WHERE InstanceId=@InstanceId and PID=@ID and IsStudent =0", con))
            {
                con.Open();
                adp.SelectCommand.Parameters.AddWithValue("@id", parentid);
                adp.SelectCommand.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        TreeNode node = new TreeNode(String.Format("<input type='radio' name='rbtncat' value='{0}' onclick='setvalue({0})'/>&nbsp;{1}", dr["ID"].ToString(), dr["Title"].ToString()));
                        //TreeNode node = new TreeNode(dr["Title"].ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        FillTreeView(node.ChildNodes, dr["ID"].ToString());
                        treenode.Add(node);
                    }
                }
            }
        }
    }
    public void Breadcrumgridfill()
    {
        if (dict.Count > 0)
        {

            var reversedDict = dict.Reverse();
            rptBreadCrum.DataSource = reversedDict;
            rptBreadCrum.DataBind();
            lbtnupload.Visible = true;
            lbluploadsep.Visible = true;
            //dict =(Dictionary<string,string>) dict.Reverse();
            foreach (var vari in reversedDict)
            {
                //Label lbl = new Label();
                //lbl.Text = "->";
                //LinkButton lnk1 = new LinkButton();
                //lnk1.Text = vari.Value;
                //lnk1.CommandName = "";
                //lnk1.CommandArgument = vari.Key;
                //lnk1.Click += lnk1_Click;
                //divbreadcrum.Controls.Add(lnk1);
                //divbreadcrum.Controls.Add(lbl);
            }
        }
    }


    public void Fillcategorydropdown()
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("SELECT ID,Title FROM tbl_feesPakageMaster WHERE InstanceId=@InstanceId and studentflag=0", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                ddlpackage.DataSource = dt;
                ddlpackage.DataTextField = "Title";
                ddlpackage.DataValueField = "ID";
                ddlpackage.DataBind();
                ddlpackage.Items.Insert(0, new ListItem("Select", string.Empty));
            }

            using (SqlCommand cmd = new SqlCommand("SELECT ID,name FROM tbl_Settings WHERE InstanceId=@InstanceId and gen=0", con))
            {
                cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                DataTable dt = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                ddldateset.DataSource = dt;
                ddldateset.DataTextField = "name";
                ddldateset.DataValueField = "ID";
                ddldateset.DataBind();
                ddldateset.Items.Insert(0, new ListItem("Select", "0"));
            }
        }
    }


    protected void btninsert_Click(object sender, EventArgs e)
    {
        string s = hfparentid.Value;
        if (hfeditid.Value != "0")
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("sp_Updatedepartment", con);
                cmd.Parameters.AddWithValue("@ID", hfeditid.Value);
                cmd.Parameters.AddWithValue("@Title", txttitle.Text);
                cmd.Parameters.AddWithValue("@packageid", ddlpackage.SelectedValue);
                cmd.Parameters.AddWithValue("@foldersetting", ddldateset.SelectedValue);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                Rptrfillfeesmanagement(Convert.ToInt32(s));
                breadcrum(Convert.ToInt32(s));
                Breadcrumgridfill();
                txttitle.Text = "";
            }
        }
        else
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("sp_insertdepartment", con);
                cmd.Parameters.AddWithValue("@PID", s);
                cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                cmd.Parameters.AddWithValue("@Title", txttitle.Text);
                cmd.Parameters.AddWithValue("@packageid", ddlpackage.SelectedValue);
                cmd.Parameters.AddWithValue("@foldersetting", ddldateset.SelectedValue);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                Rptrfillfeesmanagement(Convert.ToInt32(s));
                breadcrum(Convert.ToInt32(s));
                Breadcrumgridfill();
                txttitle.Text = "";
            }
        }
        hfeditid.Value = "0";
        TreeView1.Nodes.Clear();
        TreeNode parentNode = new TreeNode(String.Format("<input type='radio' name='rbtncat' value='Root' onclick='setvalue(0)'/>&nbsp; Root"));
        FillTreeView(parentNode.ChildNodes, "0");
        TreeView1.Nodes.Add(parentNode);
        TreeView1.ExpandAll();
    }


    public void pg_rptrcontract_OnPageIndex_Changed(object sender, EventArgs e)
    {
        Rptrfillfeesmanagement(Convert.ToInt32(hfparentid.Value));

    }

    protected void btnsearch_click(object sender, EventArgs e)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_getallsearchrecord", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@searchstr", txtsearchtext.Text);
                    cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);

                    PagedDataSource pds = new PagedDataSource();
                    pds.AllowPaging = true;
                    pds.DataSource = dt.DefaultView;
                    pds.CurrentPageIndex = PagerControl.CurrentPage;
                    pds.PageSize = PagerControl.PageSize;
                    PagerControl.TotalRecords = dt.Rows.Count;
                    RptrFeesManagement.DataSource = pds;
                    RptrFeesManagement.DataBind();
                    PagerControl.LoadStatus();

                    if (dt.Rows.Count > 0)
                    {
                        lblgriderror.Text = "";


                    }
                    else
                    {
                        lblgriderror.Text = "No Record Found";
                        RptrFeesManagement.DataSource = pds;
                        RptrFeesManagement.DataBind();
                    }
                }
                con.Close();
            }
            divresult.Attributes.Add("style", "display : block");

        }
        catch (Exception ex)
        {

        }
    }

    protected void btn_cancel_click(object sender, EventArgs e)
    {
        this.Redirect("Index.aspx");
    }
    protected void btnupload_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable duplicateEntries = new DataTable();
            DataTable dt;
            if (FileUpload1.HasFile)
            {
                if (FileUpload1.FileName.Contains("xlsx") || FileUpload1.FileName.Contains("xls"))
                {
                    ICDMComm cdm = new ICDMComm();
                    uploaddata objupload = new uploaddata();
                    string ExcelFilePath = Server.MapPath("~\\Excelfile\\") + FileUpload1.FileName;
                    //cdm.SaveFile(Server.MapPath("~\\Excelfile\\") + FileUpload1.FileName, FileUpload1.FileBytes, "aaaara1982");
                    FileUpload1.SaveAs(ExcelFilePath);


                    //OLD METHOD
                    //DataSet ds = new DataSet();

                    //ds = objupload.GetExcel(Server.MapPath("~\\Excelfile\\") + FileUpload1.FileName);
                    //if (ds.Tables[0].Rows.Count > 0)
                    //{
                    //    for (int row = 1; row < ds.Tables[0].Rows.Count; row++)
                    //    {
                    //        if (!String.IsNullOrEmpty(ds.Tables[0].Rows[row][0].ToString()) && !String.IsNullOrEmpty(ds.Tables[0].Rows[row][1].ToString()))
                    //        {
                    //            savestudents(ds.Tables[0].Rows[row][0].ToString(), ds.Tables[0].Rows[row][1].ToString(), ds.Tables[0].Rows[row][2].ToString(), ds.Tables[0].Rows[row][4].ToString(), ds.Tables[0].Rows[row][3].ToString(), ds.Tables[0].Rows[row][5].ToString(), ds.Tables[0].Rows[row][6].ToString());
                    //        }
                    //    }
                    //}
                    //OLD METHOD ENDS


                    //NEW METHOD                   
                    dt = excel.ExceltoDatatable(ExcelFilePath, Path.GetExtension(FileUpload1.FileName), "YES");

                    if (!dt.Columns.Contains("IsDuplicate"))
                        dt.Columns.Add("IsDuplicate");

                    string Studentphone = "";
                    string Parentphone = "";
                    string StudentName = "";
                    string ParentName = "";
                    string RollNo = "";

                    if (dt.Rows.Count > 0)
                    {
                        for (int row = 0; row < dt.Rows.Count; row++)
                        {
                            Studentphone = FilterData(dt.Rows[row][5].ToString(), "phonenumber");
                            Parentphone = FilterData(dt.Rows[row][6].ToString(), "phonenumber");
                            // StudentName = FilterData(dt.Rows[row][0].ToString(), "name");
                            //ParentName = FilterData(dt.Rows[row][3].ToString(), "name");
                            StudentName = dt.Rows[row][0].ToString();
                            ParentName = dt.Rows[row][3].ToString();
                            RollNo = FilterData(dt.Rows[row][1].ToString(), "rollnumber");

                            if (!string.IsNullOrEmpty(StudentName) && !string.IsNullOrEmpty(RollNo))
                            {
                                string studentId = savestudents(StudentName, RollNo, dt.Rows[row][2].ToString(), dt.Rows[row][4].ToString(), ParentName, Studentphone, Parentphone);
                                if (studentId == "-1")
                                {
                                    dt.Rows[row]["IsDuplicate"] = "-1";
                                }

                            }
                        }
                        dt = new DataView(dt, "IsDuplicate='-1'", "", DataViewRowState.CurrentRows).ToTable();
                    }
                    //NEW METHODS END

                    File.Delete(Server.MapPath("~\\Excelfile\\") + FileUpload1.FileName);
                    //cdm.DeleteFile(Server.MapPath("~\\Excelfile\\") + FileUpload1.FileName, "aaaara1982");
                    Rptrfillfeesmanagement(Convert.ToInt32(hfparentid.Value));
                    TreeView1.Nodes.Clear();
                    TreeNode parentNode = new TreeNode(String.Format("<input type='radio' name='rbtncat' value='Root' onclick='setvalue(0)'/>&nbsp; Root"));
                    FillTreeView(parentNode.ChildNodes, "0");
                    TreeView1.Nodes.Add(parentNode);
                    //      ScriptManager.RegisterStartupScript(this, GetType(), "SuccessMessage", "alert('File Upload Sucessfully')", true);
                }
                else
                    throw new Exception("Invalid File Extension. Upload File in Excel Format. For Help See Simple File.");
            }
            else
                throw new Exception("First Select File than Press Update Button.");


            //Duplicate Entries
            if (dt.Rows.Count > 0)
            {
                string Data = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Data += dt.Rows[i][1];
                    if (i != (dt.Rows.Count - 1))
                        Data += ", ";
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "Duplicate Records", "alert('File Upload Sucessfully but Following Roll# already exist\\n" + Data + "')", true);
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "SuccessMessage", "alert('File Upload Sucessfully')", true);


        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "ErrorMessage", "alert('" + ex.Message + "')", true);
        }
    }


    private string savestudents(string name, string rollnumber, string studentemail, string parentsemail, string parentname, string studentphone, string parentphone)
    {
        string studentid = "";
        try
        {
            if (!String.IsNullOrEmpty(studentemail))
            {
                Sendemailtouser(studentemail, 1);
            }
            if (!String.IsNullOrEmpty(parentsemail))
            {
                Sendemailtouser(parentsemail, 2);
            }

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_insertstudent_new", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@PID", hfparentid.Value);
                    //txtStudentname
                    cmd.Parameters.AddWithValue("@title", name);
                    cmd.Parameters.AddWithValue("@StudentEmail", studentemail);
                    cmd.Parameters.AddWithValue("@StudentprimaryID", hfstudentemail.Value);
                    cmd.Parameters.AddWithValue("@studentrollnumber", rollnumber);
                    cmd.Parameters.AddWithValue("@packagechangedate", DateTime.Now.Month + "-1-" + DateTime.Now.Year);
                    cmd.Parameters.AddWithValue("@ParentEmail", parentsemail);
                    cmd.Parameters.AddWithValue("@ParentId", hfparentemail.Value);
                    cmd.Parameters.AddWithValue("@parentname", parentname);
                    cmd.Parameters.AddWithValue("@Parentphone", parentphone);
                    cmd.Parameters.AddWithValue("@studentphone", studentphone);
                    cmd.Parameters.AddWithValue("@feesbit", false);
                    cmd.Parameters.AddWithValue("@InstanceID", this.InstanceID);
                    //hfstudentpackage.Value = StudentPackagedetail();
                    //cmd.Parameters.AddWithValue("@package", hfstudentpackage.Value);
                    cmd.CommandType = CommandType.StoredProcedure;
                    //getstudentpackage();
                    object returnval = cmd.ExecuteScalar();
                    if (returnval != null)
                    {
                        studentid = returnval.ToString();
                    }
                    else
                        studentid = "-1";
                    // studentid = cmd.ExecuteScalar().ToString();

                }
                if (studentid != "-1")
                {
                    using (SqlCommand cmd = new SqlCommand("incrementall", con))
                    {
                        cmd.Parameters.AddWithValue("@id", hfparentid.Value);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
                con.Close();
            }
        }
        catch (Exception ex)
        { }

        return studentid;
    }
    public void Sendemailtouser(string useremail, int uid)
    {
        Hashtable objuser = new Hashtable();
        AvaimaUserProfile objwebser = new AvaimaUserProfile();
        objuser.Add("userid", Guid.NewGuid().ToString());
        objuser.Add("email", useremail == "" ? "" : useremail);

        // adding student info
        if (uid == 1)
            hfstudentemail.Value = objuser["userid"].ToString();

        //adding parentinfo
        if (uid == 2)
            hfparentemail.Value = objuser["userid"].ToString();


        objwebser.insertUser(objuser["userid"].ToString(), "", objuser["email"].ToString(), "", "", "",
                             "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");   // objuser["Contractid"].ToString(), "");

    }

    public string FilterData(string value, string type)
    {
        string result = "";
        switch (type)
        {
            case "rollnumber":
                result = new string(value.Where(c => char.IsNumber(c) || char.IsLetter(c) || c.Equals('(') || c.Equals(')') || c.Equals('-')).ToArray());
                break;
            case "name":
                result = new string(value.Where(c => char.IsLetter(c) || char.IsWhiteSpace(c)).ToArray());
                break;
            case "phonenumber":
                //skip extension
                result = new string(value.Where(c => char.IsNumber(c) || char.IsWhiteSpace(c) || c.Equals('(') || c.Equals(')') || c.Equals('-') || c.Equals('+')).ToArray());
                break;
            default:
                result = "";
                break;
        }

        return result;
    }

    protected void RptrFeesManagement_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        if (hfparentid.Value != "0" && hfparentid.Value != "")
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                Control th = e.Item.FindControl("thHeadArchive");
                th.Visible = false;
            }
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Control td = e.Item.FindControl("tdArchive");
                td.Visible = false;
            }
        }

    }
}
