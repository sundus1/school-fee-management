﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Settings : AvaimaThirdpartyTool.AvaimaWebPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            binddata();
            Fillrptrfeepakages();
            Fillreptcharges();
            bindpaymentmethod();
            Addtionalfee();
            Onetimefee();
        }
    }

    private void binddata()
    {
        txttax.Enabled = false;
        ddltaxtype.Enabled = false;
        //currentinstance
        try
        {

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT ID,Currency,latefees,Fee_DueDate,LateFeesStartDate,InstanceId,Istax,Isfixed,Amount FROM tbl_Settings WHERE   InstanceId = @InstanceId and gen=1", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        hfcurrentinstance.Value = this.InstanceID;
                        if (!string.IsNullOrEmpty(dt.Rows[0]["Currency"].ToString()))
                            ddlcurrencyselect.SelectedValue = dt.Rows[0]["Currency"].ToString();

                        if (!string.IsNullOrEmpty(dt.Rows[0]["Fee_DueDate"].ToString()))
                        {
                            string[] duedate = dt.Rows[0]["Fee_DueDate"].ToString().Split('#');
                            ddlddate.SelectedValue = duedate[0];
                            ddltype.SelectedValue = duedate[1];
                        }

                        //if (!string.IsNullOrEmpty(dt.Rows[0]["startdate"].ToString()))
                        //    ddlSday.SelectedValue = dt.Rows[0]["startdate"].ToString();

                        if (!string.IsNullOrEmpty(dt.Rows[0]["LateFeesStartDate"].ToString()))
                        {
                            string[] duestartdate = dt.Rows[0]["LateFeesStartDate"].ToString().Split('#');
                            ddldsdate.SelectedValue = duestartdate[0];
                            ddlstype.SelectedValue = duestartdate[1];
                        }
                        txtlatefee.Value = dt.Rows[0]["latefees"].ToString();
                        //if (!string.IsNullOrEmpty(dt.Rows[0]["LateFeesEndDate"].ToString()))
                        //    ddldedate.SelectedValue = dt.Rows[0]["LateFeesEndDate"].ToString();
                        if (!string.IsNullOrEmpty(dt.Rows[0]["Istax"].ToString()))
                        {
                            if (dt.Rows[0]["Istax"].ToString() == "False")
                            {
                                txttax.Enabled = false;
                                ddltaxtype.Enabled = false;
                                chktax.Checked = false;

                            }
                            else
                            {
                                txttax.Enabled = true;
                                ddltaxtype.Enabled = true;
                                chktax.Checked = true;

                                if (!string.IsNullOrEmpty(dt.Rows[0]["Isfixed"].ToString()))
                                {
                                    ddltaxtype.SelectedValue = dt.Rows[0]["Isfixed"].ToString();

                                }
                                if (!string.IsNullOrEmpty(dt.Rows[0]["Amount"].ToString()))
                                {
                                    txttax.Text = dt.Rows[0]["Amount"].ToString();
                                }
                            }
                        }

                    }

                }
                con.Close();
            }
            DataTable dtuploadfile = new DataTable();
            AvaimaStorageUploader objuploader = new AvaimaStorageUploader();
            dtuploadfile = getrecord();
            if (dtuploadfile.Rows.Count > 0)
            {
                string headerfile = objuploader.Singlefilelink(this.InstanceID, dtuploadfile.Rows[0]["headerfile"].ToString());
                string logofile = objuploader.Singlefilelink(this.InstanceID, dtuploadfile.Rows[0]["Logofile"].ToString());
                tr_header.Visible = true;
                tr_logo.Visible = true;
                img_header.ImageUrl = headerfile;
                ank_header.HRef = headerfile;
                ank_logo.HRef = logofile;
                img_logo.ImageUrl = logofile;
               
                hfrecid.Value = dtuploadfile.Rows[0]["recid"].ToString();
                txtfooter.Text = dtuploadfile.Rows[0]["Footer"].ToString();
                txtschool.Text = dtuploadfile.Rows[0]["schoolname"].ToString();
                ddlmonth.SelectedValue = dtuploadfile.Rows[0]["bankname"].ToString();
                hfheader.Value = dtuploadfile.Rows[0]["headerfile"].ToString();
                hflogo.Value = dtuploadfile.Rows[0]["Logofile"].ToString();
                if (!String.IsNullOrEmpty(dtuploadfile.Rows[0]["headerfile"].ToString()) || !String.IsNullOrEmpty(dtuploadfile.Rows[0]["Logofile"].ToString()))
                {
                    DataTable dtfiles = new DataTable();
                    dtfiles = objuploader.Getuploadedfileinfo("8ee08d08-68ab-4415-8b50-9fa722151856", this.InstanceID);
                    if (dtfiles.Rows.Count > 0)
                    {
                        for (int sd = 0; sd < dtfiles.Rows.Count; sd++)
                        {
                            if (hfheader.Value == dtfiles.Rows[sd]["FilePath"].ToString())
                                hfheaderid.Value = dtfiles.Rows[sd]["recid"].ToString();
                            if (hflogo.Value == dtfiles.Rows[sd]["FilePath"].ToString())
                                hflogoid.Value = dtfiles.Rows[sd]["recid"].ToString();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }
    private DataTable getrecord()
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT *  FROM SchoolFeesInvoicesetting WHERE instanceid=@id", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", this.InstanceID);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    return dt;
                }
            }
        }
        catch (Exception)
        {

            throw;
        }

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            //if (Convert.ToInt32(ddlddate.SelectedValue) != 0 && Convert.ToInt32(ddldsdate.SelectedValue) != 0)
            //    if (Convert.ToInt32(ddlddate.SelectedValue) >= Convert.ToInt32(ddldsdate.SelectedValue))
            //    {
            //        ScriptManager.RegisterStartupScript(this, GetType(), "aa", "alert('Late fee Start Date must not be Less/Equal to fee due date')", true);
            //        return;
            //    }
            if (hfcurrentinstance.Value != "-")
                {
                    {
                        using (
                            SqlConnection con =
                                new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                        {
                            using (SqlCommand cmd = new SqlCommand("sp_updatesettings", con))
                            {
                                con.Open();
                                cmd.Parameters.AddWithValue("@currency", ddlcurrencyselect.SelectedValue);
                                cmd.Parameters.AddWithValue("@duedate", ddlddate.SelectedValue + "#" + ddltype.SelectedValue);
                                cmd.Parameters.AddWithValue("@latefees", txtlatefee.Value);
                                cmd.Parameters.AddWithValue("@latefeestartdate", ddldsdate.SelectedValue + "#" + ddlstype.SelectedValue);
                                //cmd.Parameters.AddWithValue("@latefeeenddate", ddldedate.SelectedValue);
                                cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);

                                if (chktax.Checked)
                                    cmd.Parameters.AddWithValue("@Istax", 1);
                                else
                                {
                                    cmd.Parameters.AddWithValue("@Istax", 0);
                                }
                                cmd.Parameters.AddWithValue("@Isfixed", ddltaxtype.SelectedValue);
                                if (!string.IsNullOrEmpty(Convert.ToString(txttax.Text)))
                                    cmd.Parameters.AddWithValue("@Amount", txttax.Text);
                                else
                                {
                                    cmd.Parameters.AddWithValue("@Amount", 0);
                                }


                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd.ExecuteNonQuery();
                                //DataTable dt = new DataTable();
                                //SqlDataAdapter adp = new SqlDataAdapter(cmd);
                                //adp.Fill(dt);


                            }
                            con.Close();
                        }
                    }
                    saveschoolinfo(false);
                }
                else
                {
                    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("sp_insertsettings", con))
                        {
                            con.Open();
                            cmd.Parameters.AddWithValue("@currency", ddlcurrencyselect.SelectedValue);
                            cmd.Parameters.AddWithValue("@duedate", ddlddate.SelectedValue + "#" + ddltype.SelectedValue);
                            //cmd.Parameters.AddWithValue("@lfeesenddate", ddldsdate.SelectedValue);
                            cmd.Parameters.AddWithValue("@lfeesstartdate", ddldsdate.SelectedValue + "#" + ddlstype.SelectedValue);
                            cmd.Parameters.AddWithValue("@latefees", txtlatefee.Value);
                            cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                            if (chktax.Checked)
                                cmd.Parameters.AddWithValue("@Istax", 1);
                            else
                            {
                                cmd.Parameters.AddWithValue("@Istax", 0);
                            }
                            cmd.Parameters.AddWithValue("@Isfixed", ddltaxtype.SelectedValue);

                            if (!string.IsNullOrEmpty(Convert.ToString(txttax.Text)))
                                cmd.Parameters.AddWithValue("@Amount", txttax.Text);
                            else
                            {
                                cmd.Parameters.AddWithValue("@Amount", 0);
                            }

                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.ExecuteNonQuery();
                            //DataTable dt = new DataTable();
                            //SqlDataAdapter adp = new SqlDataAdapter(cmd);
                            //adp.Fill(dt);


                        }
                        con.Close();
                    }
                    saveschoolinfo(true);
                }
            binddata();
                       
            ScriptManager.RegisterStartupScript(this, GetType(), "savedd", "alert('Save Successfully!!')", true);
        }
        catch (Exception ex)
        {
            throw;

        }
    }

    protected void PaymentMethodExist()
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("select * from paymentmethod WHERE instanceid = @InstanceId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    if (dt.Rows.Count == 0)
                    {
                        
                        addPaymentMethod("By Cash",this.InstanceID);
                        addPaymentMethod("By Bank", this.InstanceID);
                    }
                   
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void saveschoolinfo(bool ch)
    {
        try
        {
            AvaimaStorageUploader objuploader = new AvaimaStorageUploader();
            if (ch)
            {
                string headername = "";
                string Logoname = "";
                if (fu_logo.HasFile)
                {
                    Logoname = fu_logo.FileName;
                    string[] logo = fu_logo.FileName.Split('.');
                    objuploader.insertfileuploaded(logo[0], logo[1], "8ee08d08-68ab-4415-8b50-9fa722151856", HttpContext.Current.User.Identity.Name, this.InstanceID, this.InstanceID, fu_logo.FileBytes);
                }
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("insertInvoicesetting1", con))
                    {
                        con.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Logofile", Logoname);
                        cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                        cmd.Parameters.AddWithValue("@bankname", ddlmonth.SelectedValue);
                        cmd.Parameters.AddWithValue("@schoolname", txtschool.Text);
                        cmd.Parameters.AddWithValue("@generatedate", ddlissuedate.SelectedValue);

                        cmd.ExecuteNonQuery();

                        //ScriptManager.RegisterStartupScript(this, GetType(), "updatealert", "alert('Setting Saved Sucessfully !')", true);
                    }

                    con.Close();
                }
            }
            else
            {
                string Logoname = "";
                if (fu_logo.HasFile)
                {
                    if (hflogoid.Value != "0")
                        objuploader.Deleteuploadedfile(hflogoid.Value);

                    objuploader.filedelete(this.InstanceID, hflogo.Value);
                    Logoname = fu_logo.FileName;
                    string[] logo = fu_logo.FileName.Split('.');
                    objuploader.insertfileuploaded(logo[0], logo[1], "8ee08d08-68ab-4415-8b50-9fa722151856", HttpContext.Current.User.Identity.Name, this.InstanceID, this.InstanceID, fu_logo.FileBytes);
                }
                else
                    Logoname = hflogo.Value;

                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("updateInvoicesetting1", con))
                    {
                        con.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Logofile", Logoname);
                        cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                        cmd.Parameters.AddWithValue("@bankname", ddlmonth.SelectedValue);
                        cmd.Parameters.AddWithValue("@schoolname", txtschool.Text);
                        cmd.Parameters.AddWithValue("@generatedate", ddlissuedate.SelectedValue);
                        cmd.Parameters.AddWithValue("@recid", hfrecid.Value);


                        cmd.ExecuteNonQuery();

                        //ScriptManager.RegisterStartupScript(this, GetType(), "updatealert", "alert('Setting Update Sucessfully !')", true);
                    }

                    con.Close();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void chktax_CheckedChanged(object sender, EventArgs e)
    {
        if (chktax.Checked)
        {
            txttax.Enabled = true;
            ddltaxtype.Enabled = true;
        }
        else
        {

            txttax.Enabled = false;
            ddltaxtype.Enabled = false;

        }
    }
    private void bindpaymentmethod()
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("select * from paymentmethod WHERE instanceid = @InstanceId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        reptpayment.DataSource = dt;
                        reptpayment.DataBind();
                    }
                    else
                    {
                        reptpayment.DataSource = null;
                        reptpayment.DataBind();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        try
        {
            if (hfdiscount.Value != "0")
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("updateChargeDiscount", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@title", txtchargetitle.Text);
                        cmd.Parameters.AddWithValue("@amount", txtamount.Text);
                        cmd.Parameters.AddWithValue("@id", hfdiscount.Value);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                    con.Close();
                    hfdiscount.Value = "0";
                    txtchargetitle.Text = "";
                    txtamount.Text = "";
                }            
            }
            else
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("sp_insertchargediscount", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@title", txtchargetitle.Text);
                        cmd.Parameters.AddWithValue("@chargetype", "Discount");
                        cmd.Parameters.AddWithValue("@isfixed", "Fixed");
                        cmd.Parameters.AddWithValue("@amount", txtamount.Text);
                        cmd.Parameters.AddWithValue("@InstanceID", this.InstanceID);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                    con.Close();
                    txtchargetitle.Text = "";
                    txtamount.Text = "";
                }
            }
            Fillrptrfeepakages();

        }
        catch (Exception ex)
        {

        }

    }
    public void Fillrptrfeepakages()
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT ID,Title, ChargeType, IsFixed, Amount ,InstanceID FROM tbl_ChargeDiscount WHERE InstanceId=@InstanceId and ChargeType='Discount'", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        rptrpakagegrid.DataSource = dt;
                        rptrpakagegrid.DataBind();
                    }
                    else
                    {
                        rptrpakagegrid.DataSource = null;
                        rptrpakagegrid.DataBind();
                    }

                }
                con.Close();
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void rptrpakagegrid_itemcommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.CommandName == "delete")
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("DELETE FROM tbl_ChargeDiscount WHERE ID=@id", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@id", e.CommandArgument);
                        cmd.ExecuteNonQuery();
                        string message = "Fees Package Deleted on";
                        //Packagehistory(Convert.ToInt32(hfpakageid.Value), message);
                        Fillrptrfeepakages();
                    }
                }
            }
            else if (e.CommandName == "edit")
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    if (e.CommandName == "edit")
                    {
                        LinkButton lnk = e.Item.FindControl("lnkedit") as LinkButton;

                        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                        {
                            using (SqlDataAdapter adp = new SqlDataAdapter("SELECT ID,Title,ChargeType,IsFixed,Amount FROM tbl_ChargeDiscount WHERE ID=" + e.CommandArgument, con))
                            {
                                con.Open();
                                DataTable dt = new DataTable();
                                adp.Fill(dt);
                                //hfeditpackageid.Value = e.CommandArgument.ToString();
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        txtchargetitle.Text = dr["Title"].ToString();
                                        txtamount.Text = dr["Amount"].ToString();
                                        hfdiscount.Value = dr["ID"].ToString();
                                    }
                                }
                            }
                        }
                    }
                }
            }
           
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            if (hfcharges.Value != "0")
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("updateChargeDiscount", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@title", txtcharges.Text);
                        cmd.Parameters.AddWithValue("@amount", txtChargesamount.Text);
                        cmd.Parameters.AddWithValue("@id", hfcharges.Value);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                    con.Close();
                    hfcharges.Value = "0";
                    txtcharges.Text = "";
                    txtChargesamount.Text = "";
                }
            }
            else
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("sp_insertchargediscount", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@title", txtcharges.Text);
                        cmd.Parameters.AddWithValue("@chargetype", "Charge");
                        cmd.Parameters.AddWithValue("@isfixed", "Fixed");
                        cmd.Parameters.AddWithValue("@amount", txtChargesamount.Text);
                        cmd.Parameters.AddWithValue("@InstanceID", this.InstanceID);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                    con.Close();
                    txtcharges.Text = "";
                    txtChargesamount.Text = "";
                }
            }
            Fillreptcharges();
            
        }
        catch (Exception ex)
        {

        }
    }
    public void Fillreptcharges()
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT ID,Title, ChargeType, IsFixed, Amount ,InstanceID FROM tbl_ChargeDiscount WHERE InstanceId=@InstanceId and ChargeType='Charge'", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        reptcharges.DataSource = dt;
                        reptcharges.DataBind();
                    }
                    else
                    {
                        reptcharges.DataSource = null;
                        reptcharges.DataBind();
                    }

                }
                con.Close();
            }
        }
        catch (Exception ex)
        {

        }
    }
    public void Addtionalfee()
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Addtionalfee WHERE InstanceId=@InstanceId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        reptfees.DataSource = dt;
                        reptfees.DataBind();
                    }
                    else
                    {
                        reptfees.DataSource = null;
                        reptfees.DataBind();
                    }

                }
                con.Close();
            }
        }
        catch (Exception ex)
        {

        }
    }
    public void Onetimefee()
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Onetimefee WHERE InstanceId=@InstanceId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        reptOTfee.DataSource = dt;
                        reptOTfee.DataBind();
                    }
                    else
                    {
                        reptOTfee.DataSource = null;
                        reptOTfee.DataBind();
                    }

                }
                con.Close();
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void reptcharges_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.CommandName == "delete")
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("DELETE FROM tbl_ChargeDiscount WHERE ID=@id", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@id", e.CommandArgument);
                        cmd.ExecuteNonQuery();
                        string message = "Fees Package Deleted on";
                        //Packagehistory(Convert.ToInt32(hfpakageid.Value), message);
                        Fillreptcharges();
                    }
                }
            }
            else if (e.CommandName == "edit")
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    if (e.CommandName == "edit")
                    {
                        LinkButton lnk = e.Item.FindControl("lnkedit") as LinkButton;

                        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                        {
                            using (SqlDataAdapter adp = new SqlDataAdapter("SELECT ID,Title,ChargeType,IsFixed,Amount FROM tbl_ChargeDiscount WHERE ID=" + e.CommandArgument, con))
                            {
                                con.Open();
                                DataTable dt = new DataTable();
                                adp.Fill(dt);
                                //hfeditpackageid.Value = e.CommandArgument.ToString();
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        txtcharges.Text = dr["Title"].ToString();
                                        txtChargesamount.Text = dr["Amount"].ToString();
                                        hfcharges.Value = dr["ID"].ToString();
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    }
    protected void btnpaymentmethod_Click(object sender, EventArgs e)
    {
        //using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        //{
        //    using (SqlCommand cmd = new SqlCommand("insert into paymentmethod values(@paymentmethod,@instanceid)", con))
        //    {
        //        con.Open();
        //        cmd.Parameters.AddWithValue("@paymentmethod", txtPaymethod.Text);
        //        cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);

        //        cmd.CommandType = CommandType.Text;
        //        cmd.ExecuteNonQuery();
        //    }
        //    con.Close();
        //}

        addPaymentMethod(txtPaymethod.Text,this.InstanceID);
        bindpaymentmethod();
    }

    protected void addPaymentMethod(string PaymentMethod, string InstanceID)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("insert into paymentmethod values(@paymentmethod,@instanceid)", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@paymentmethod", PaymentMethod);
                cmd.Parameters.AddWithValue("@instanceid", InstanceID);

                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
            }
            con.Close();
        }
    }

    protected void reptpayment_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "delete")
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("delete paymentmethod where recid=@id", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", e.CommandArgument.ToString());
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
                con.Close();
            }
            bindpaymentmethod();
        }
    }
    protected void btnfeesave_Click(object sender, EventArgs e)
    {
        try
        {
            if (hffees.Value != "0")
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("updateadditionalfee", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@feetitle", txtfeetitle.Text);
                        cmd.Parameters.AddWithValue("@Feeamount", txtfeeamount.Text);
                        cmd.Parameters.AddWithValue("@id", hffees.Value);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                    con.Close();
                    hffees.Value = "0";
                    txtfeeamount.Text = "";
                    txtfeetitle.Text = "";
                    btnfeesave.Text = "Save Fee";
                }
            }
            else
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("sp_insertAddtionalfee", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@title", txtfeetitle.Text);
                        cmd.Parameters.AddWithValue("@amount", txtfeeamount.Text);
                        cmd.Parameters.AddWithValue("@InstanceID", this.InstanceID);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                    con.Close();
                    txtfeeamount.Text = "";
                    txtfeetitle.Text = "";
                }
            }
            Addtionalfee();

        }
        catch (Exception ex)
        {

        }
    }
    protected void reptfees_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.CommandName == "delete")
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("DELETE FROM Addtionalfee WHERE recid=@id", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@id", e.CommandArgument);
                        cmd.ExecuteNonQuery();

                        Addtionalfee();
                    }
                }
            }
        }
    }
    protected void btnOTfee_Click(object sender, EventArgs e)
    {
        try
        {
            if (hfOTfee.Value != "0")
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("updateOnetimefee", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@feetitle", txtOTfeetitle.Text);
                        cmd.Parameters.AddWithValue("@Feeamount", txtOTfeeamount.Text);
                        cmd.Parameters.AddWithValue("@id", hfOTfee.Value);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                    con.Close();
                    hfOTfee.Value = "0";
                    txtOTfeeamount.Text = "";
                    txtOTfeetitle.Text = "";
                    btnOTfee.Text = "Save Fee";
                }
            }
            else
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("sp_insertOnetimefee", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@title", txtOTfeetitle.Text);
                        cmd.Parameters.AddWithValue("@amount", txtOTfeeamount.Text);
                        cmd.Parameters.AddWithValue("@InstanceID", this.InstanceID);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                    con.Close();
                    txtOTfeeamount.Text = "";
                    txtOTfeetitle.Text = "";
                }
            }
            Onetimefee();
           
        }
        catch (Exception ex)
        {

        }
    }
    protected void reptOTfee_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.CommandName == "delete")
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("DELETE FROM Onetimefee WHERE recid=@id", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@id", e.CommandArgument);
                        cmd.ExecuteNonQuery();

                        Onetimefee();
                    }
                }
            }
        }
    }
    protected void lbtnInvoiceset_Click(object sender, EventArgs e)
    {
        this.Redirect("InvoiceSetting.aspx");
    }
    protected void btninvoice_Click(object sender, EventArgs e)
    {
        try
        {
            AvaimaStorageUploader objuploader = new AvaimaStorageUploader();

            string headername = "";

            if (hfrecid.Value == "0")
            {
                if (fu_header.HasFile)
                {
                    headername = fu_header.FileName;
                    string[] header = fu_header.FileName.Split('.');
                    objuploader.insertfileuploaded(header[0], header[1], "8ee08d08-68ab-4415-8b50-9fa722151856", HttpContext.Current.User.Identity.Name, this.InstanceID, this.InstanceID, fu_header.FileBytes);
                }
            }
            else
            {
                if (fu_header.HasFile)
                {
                    if (hfheaderid.Value != "0")
                        objuploader.Deleteuploadedfile(hfheaderid.Value);

                    objuploader.filedelete(this.InstanceID, hfheader.Value);
                    headername = fu_header.FileName;
                    string[] header = fu_header.FileName.Split('.');
                    objuploader.insertfileuploaded(header[0], header[1], "8ee08d08-68ab-4415-8b50-9fa722151856", HttpContext.Current.User.Identity.Name, this.InstanceID, this.InstanceID, fu_header.FileBytes);
                }
                else
                    headername = hfheader.Value;
            }

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("insertInvoicesetting2", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Footer", txtfooter.Text);
                    cmd.Parameters.AddWithValue("@headerfile", headername);
                    cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);

                    cmd.ExecuteNonQuery();

                    ScriptManager.RegisterStartupScript(this, GetType(), "updatealert", "alert('Invoice Setting Saved Sucessfully !')", true);
                }

                con.Close();
            }
            binddata();
           

        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
}