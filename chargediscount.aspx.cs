﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class chargediscount : AvaimaThirdpartyTool.AvaimaWebPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Fillrptrfeepakages();
    }

    protected void btnsubmit_click(object sender, EventArgs e)
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_insertchargediscount", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@title", txtchargetitle.Text);
                    cmd.Parameters.AddWithValue("@chargetype", ddlchargetype.SelectedValue);
                    cmd.Parameters.AddWithValue("@isfixed", ddlamounttype.SelectedValue);
                    cmd.Parameters.AddWithValue("@amount", txtamount.Text);
                    cmd.Parameters.AddWithValue("@InstanceID", this.InstanceID);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
                con.Close();
            }
            Fillrptrfeepakages();

        }
        catch (Exception ex)
        {

        }

    }

    public void Fillrptrfeepakages()
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT ID,Title, ChargeType, IsFixed, Amount ,InstanceID FROM tbl_ChargeDiscount WHERE InstanceId=@InstanceId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    rptrpakagegrid.DataSource = dt;
                    rptrpakagegrid.DataBind();

                }
                con.Close();
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void rptrpakagegrid_itemdatabound(object sender, RepeaterItemEventArgs e)
    {

    }

    protected void rptrpakagegrid_itemcommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.CommandName == "delete")
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("DELETE FROM tbl_ChargeDiscount WHERE ID=@id", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@id", e.CommandArgument);
                        cmd.ExecuteNonQuery();
                        string message = "Fees Package Deleted on";
                        //Packagehistory(Convert.ToInt32(hfpakageid.Value), message);
                        Fillrptrfeepakages();
                    }
                }
            }
            else if (e.CommandName == "edit")
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    if (e.CommandName == "edit")
                    {
                        LinkButton lnk = e.Item.FindControl("lnkedit") as LinkButton;

                        using (SqlConnection con =new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
                        {
                            using (SqlDataAdapter adp = new SqlDataAdapter("SELECT ID,Title,ChargeType,IsFixed,Amount FROM tbl_ChargeDiscount WHERE ID=" + e.CommandArgument, con))
                            {
                                con.Open();
                                DataTable dt = new DataTable();
                                adp.Fill(dt);
                                hfeditpackageid.Value = e.CommandArgument.ToString();
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        txtedittitle.Text = dr["Title"].ToString();
                                        ddlchargetype.SelectedValue = dr["ChargeType"].ToString();
                                        ddleditamounttype.SelectedValue = dr["IsFixed"].ToString();
                                        txteditamount.Text = dr["Amount"].ToString();
                                    }
                                }
                            }
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "openedit", "opendetailedit()", true);
            }
        }
    }

    protected void btnupdate_click(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }
}