﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="feevoucher.aspx.cs" Inherits="feevoucher" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="_assets/css/voucher.css" rel="stylesheet" type="text/css" />
     <link href="_assets/jquery-ui.css" rel="stylesheet" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <script type="text/javascript">
        $(window).load(function () {
            //resizeiframepage();
            //setTimeout(function () { resizeiframepage(); }, 300);
        });
        function resizeiframepage() {
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
            if (innerDoc.body.offsetHeight) {
                iframe.height = innerDoc.body.offsetHeight + 400 + "px";
            }
            else if (innerDoc && innerDoc.body.scrollHeight) {
                iframe.style.height = innerDoc.body.scrollHeight + "px";
            }
        }
        function printout() {
            //$('#imgpinter').attr("src", "");
            //$('#ankedit').text('');
            window.print();
            //$('#imgpinter').attr("src", "images/1410871087_Print_32x32.png");
            //$('#ankedit').text('Edit');
        }
        $(document).ready(function () {
            $(document).on('click', '.ankdiscount', function () {

                var dlg = $("#divDiscount").dialog({
                    width: 416,
                    show: {

                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });

            $(document).on('click', '.ankcharges', function () {

                var dlg = $("#div2").dialog({
                    width: 416,
                    show: {

                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });

            $(document).on('click', '.ankadjustment', function () {

                var dlg = $("#div3").dialog({
                    width: 416,
                    show: {

                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
            });

            $(document).on('click', '#btnpayment', function () {
                var txtval = $('#txtpayment').val();
                if (txtval == "") {
                    $('#paymenterrmsg').text('Required Amount')
                    return false;
                }
                else {
                    var filter = /(?:^\d{1,3}(?:\.?\d{3})*(?:,\d{2})?$)|(?:^\d{1,3}(?:,?\d{3})*(?:\.\d{2})?$)/;
                    if (filter.test(txtval)) {
                        $('#paymenterrmsg').text('');
                        var dlg = $("#div1").dialog({
                            width: 416,
                            show: {

                            },
                            modal: true,
                            hide: {
                                effect: "blind",
                            }
                        });
                        dlg.parent().appendTo($("form:first"));
                        dlg.parent().css("z-index", "1000");
                        return false;
                    }
                    else {
                        $('#paymenterrmsg').text('Invalid Amount')
                        return false;
                    }
                }
            });

            $(document).on('click', '#addPaymentMethods', function () {

                var dlg = $("#additionalPaymentMethods").dialog({
                    width: 416,
                    show: {

                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                return false;


            });


            $("#txtpaiddate").datepicker({
                dateFormat: 'dd MM yy',
                changeMonth: true,
                changeYear: true,
            });
        });
    </script>
    <style>
        @media print
        {
            .printdiv
            {
                display:none;
            }
        }
         .errorMsg {
            color: red;
        }

        .lnkTxt {
            color: blue !important;
            cursor: pointer !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
       
        <div>
            <div class="receiptCont">
                <div class="header">
                    <img src="images/header.jpg" alt="img" id="img_header" runat="server" />
                </div>
                <div class="tablehead">
                    <h2 class="color">BANK COPY</h2>
                    <h2 class="color"><span>Invoice ID&nbsp;</span><span><asp:Label ID="lblslipno" runat="server"></asp:Label>
                    </span></h2>
                    <div class="clear"></div>
                    <table width="100%" border="0">
                        <tr>
                            <td scope="col">
                                <b>Class:</b>
                                <asp:Label ID="lblclass" runat="server"></asp:Label><br />
                                <b>Issue Date:</b>
                                <asp:Label ID="lblissuedate" runat="server"></asp:Label><br />
                                <b>Reg. No.:</b>
                                <asp:Label ID="lblregno" runat="server"></asp:Label><br />
                                <b>Session:</b>
                                <asp:Label ID="lblsession" runat="server"></asp:Label><br />
                                <b>G. R. No.:</b>
                                <asp:Label ID="lblgrno" runat="server"></asp:Label><br />
                                <b>Period:</b>
                                <asp:Label ID="lblperiod" runat="server"></asp:Label><br />
                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0">
                        <tr>
                            <td colspan="2" class="name">Student&acute;s Name: <span>&nbsp;<asp:Label ID="lblstudentname" runat="server"></asp:Label></span></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="fname">Father&acute;s Name: <span>&nbsp;<asp:Label ID="lblfathername" runat="server"></asp:Label></span></td>
                        </tr>
                    </table>

                </div>
                <div class="clear"></div>
                <div class="tablehead">
                    <h2><span>Fee Details</span></h2>
                </div>
                <asp:Table ID="tbl" runat="server" Width="100%" border="0" CellSpacing="0" class="feedetail">
                </asp:Table>
                <table width="100%" border="0" class="validtotal">
                    <tr>
                        <td colspan="2">Due Date: <b>
                            <asp:Label ID="lblduedate" runat="server"></asp:Label>
                        </b></td>
                    </tr>
                    <tr>
                        <td>Amount payable after due date</td>
                        <td>
                            <asp:Label ID="lbldueamount" runat="server"></asp:Label></td>
                    </tr>
                </table>
                <br />
                <br />
                <br />
                <table width="100%" border="0" class="signature">
                    <tr>
                        <td>Cashier</td>
                        <td>Officer</td>
                    </tr>
                </table>
                <div style="font-size: 11px; color: #b5b5b5; font-weight: lighter; padding: 11px 5px 0px 5px;" id="Footer2" runat="server">
                </div>
            </div>
            <div class="receiptCont">
                <div class="header">
                    <img src="images/header.jpg" alt="img" id="img_header1" runat="server" />
                </div>
                <div class="tablehead">
                    <h2 class="color">SCHOOL COPY</h2>
                    <h2 class="color"><span>Invoice ID&nbsp;</span><span><asp:Label ID="lblslipno1" runat="server"></asp:Label>
                    </span></h2>
                    <div class="clear"></div>
                    <table width="100%" border="0">
                        <tr>
                            <td scope="col">
                                <b>Class:</b>
                                <asp:Label ID="lblclass1" runat="server"></asp:Label><br />
                                <b>Issue Date:</b>
                                <asp:Label ID="lblissuedate1" runat="server"></asp:Label><br />
                                <b>Reg. No.:</b>
                                <asp:Label ID="lblregno1" runat="server"></asp:Label><br />
                                <b>Session:</b>
                                <asp:Label ID="lblsession1" runat="server"></asp:Label><br />
                                <b>G. R. No.:</b>
                                <asp:Label ID="lblgrno1" runat="server"></asp:Label><br />
                                <b>Period:</b>
                                <asp:Label ID="lblperiod1" runat="server"></asp:Label><br />
                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0">
                        <tr>
                            <td colspan="2" class="name">Student&acute;s Name: <span>&nbsp;<asp:Label ID="lblstudentname1" runat="server"></asp:Label></span></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="fname">Father&acute;s Name: <span>&nbsp;<asp:Label ID="lblfathername1" runat="server"></asp:Label></span></td>
                        </tr>
                    </table>

                </div>
                <div class="clear"></div>
                <div class="tablehead">
                    <h2><span>Fee Details</span></h2>
                </div>
                <asp:Table ID="tbl1" runat="server" Width="100%" border="0" CellSpacing="0" class="feedetail">
                </asp:Table>

                <table width="100%" border="0" class="validtotal">
                    <tr>
                        <td colspan="2">Due Date: <b>
                            <asp:Label ID="lblduedate1" runat="server"></asp:Label></b></td>
                    </tr>
                    <tr>
                        <td>Amount payable after due date</td>
                        <td>
                            <asp:Label ID="lbldueamount1" runat="server"></asp:Label></td>
                    </tr>
                </table>
                <br />
                <br />
                <br />
                <table width="100%" border="0" class="signature">
                    <tr>
                        <td>Cashier</td>
                        <td>Officer</td>
                    </tr>
                </table>
                <div style="font-size: 11px; color: #b5b5b5; font-weight: lighter; padding: 11px 5px 0px 5px;" id="Footer1" runat="server">
                </div>
            </div>
            <div class="receiptCont noboarder">
                <div class="header">
                    <img src="images/header.jpg" alt="img" id="img_header2" runat="server" />
                </div>
                <div class="tablehead">
                    <h2 class="color">STUDENT'S COPY</h2>
                    <h2 class="color"><span>Invoice ID&nbsp;</span><span><asp:Label ID="lblslipno2" runat="server"></asp:Label>
                    </span></h2>
                    <div class="clear"></div>
                    <table width="100%" border="0">
                        <tr>
                            <td scope="col">
                                <b>Class:</b>
                                <asp:Label ID="lblclass2" runat="server"></asp:Label><br />
                                <b>Issue Date:</b>
                                <asp:Label ID="lblissuedate2" runat="server"></asp:Label><br />
                                <b>Reg. No.:</b>
                                <asp:Label ID="lblregno2" runat="server"></asp:Label><br />
                                <b>Session:</b>
                                <asp:Label ID="lblsession2" runat="server"></asp:Label><br />
                                <b>G. R. No.:</b>
                                <asp:Label ID="lblgrno2" runat="server"></asp:Label><br />
                                <b>Period:</b>
                                <asp:Label ID="lblperiod2" runat="server"></asp:Label><br />
                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0">
                        <tr>
                            <td colspan="2" class="name">Student&acute;s Name: <span>&nbsp;<asp:Label ID="lblstudentname2" runat="server"></asp:Label></span></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="fname">Father&acute;s Name: <span>&nbsp;<asp:Label ID="lblfathername2" runat="server"></asp:Label></span></td>
                        </tr>
                    </table>

                </div>
                <div class="clear"></div>
                <div class="tablehead">
                    <h2><span>Fee Details</span></h2>
                </div>
                <asp:Table ID="tbl2" runat="server" Width="100%" border="0" CellSpacing="0" class="feedetail">
                </asp:Table>

                <table width="100%" border="0" class="validtotal">
                    <tr>
                        <td colspan="2">Due Date: <b>
                            <asp:Label ID="lblduedate2" runat="server"></asp:Label></b></td>
                    </tr>
                    <tr>
                        <td>Amount payable after due date</td>
                        <td>
                            <asp:Label ID="lbldueamount2" runat="server"></asp:Label></td>
                    </tr>
                </table>
                <br />
                <br />
                <br />
                <table width="100%" border="0" class="signature">
                    <tr>
                        <td>Cashier</td>
                        <td>Officer</td>
                    </tr>
                </table>
                <div style="font-size: 11px; color: #b5b5b5; font-weight: lighter; padding: 11px 5px 0px 5px;" id="Footer" runat="server">
                </div>
            </div>
        </div>
         <div style="float: right;line-height: 25px;font-size: 14px;font-family: arial; margin-right:100px" class="printdiv">
            <h2>Fee Options</h2>
             <a href="javascript:printout();" id="ankprint">
                <img src="images/1410871087_Print_32x32.png" id="imgpinter" /> Print</a>
            <br />
            <a id="ankedit" href="Invoice.aspx?<%= Request.QueryString.ToString().Replace("Studentid","studid") %>">View monthly details</a><br />
            <a href="#" id="ankdiscount" class="menuitem ankdiscount">Add Discount</a><br />
            <a href="#" id="ankcharges" class="menuitem ankcharges">Add Charges</a><br />
            <a href="#" id="ankadjustment" class="menuitem ankadjustment">Add Adjustment</a>
            <div>
                <h2>Payment Collection</h2>
                <h4>Total Due:
                    <asp:Label ID="lbltotladue" runat="server"></asp:Label></h4>
                 <asp:TextBox ID="txtpayment" runat="server" ClientIDMode="Static" CssClass="standardField" placeholder="Payment Amount"></asp:TextBox>&nbsp;<asp:Button ID="btnpayment" runat="server" Text="Collect Payment" CssClass="standardFormButton" /><br /><asp:Label ID="paymenterrmsg" runat="server" ClientIDMode="Static" ForeColor="Red"></asp:Label>
            </div>
        </div>
         <div id="div1" title="Payment Method" style="display: none;">
            <asp:Panel ID="Panel1" DefaultButton="btnprocessed" runat="server">
                <table>
                    <tr>
                        <td>
                            Paid Date:
                        </td>
                        <td>
                            <asp:TextBox ID="txtpaiddate" runat="server" CssClass="standardField" ClientIDMode="Static"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Paid By:
                        </td>
                        <td>
                            <asp:TextBox ID="txtpaidby" runat="server" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Payment Method:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlpayment" runat="server" AppendDataBoundItems="True">
                            </asp:DropDownList><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="ddlpayment" ForeColor="Red" InitialValue="0" ValidationGroup="payment"></asp:RequiredFieldValidator>
                        </td>
                          <td><asp:HyperLink runat="server" ID="addPaymentMethods" Text="Add Payment Method" CssClass="lnkTxt"></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td>Comments:
                        </td>
                        <td>
                            <asp:TextBox ID="txtpaycomment" runat="server" TextMode="MultiLine" Rows="3" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btnprocessed" runat="server" Text="Pay" CssClass="standardFormButton" ValidationGroup="payment" OnClick="btnprocessed_Click"/>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
        <div id="divDiscount" title="Discount" style="display: none">
            <asp:Panel ID="Panel2" DefaultButton="btndisamount" runat="server">
                <table>
                    <tr>
                        <td>Discount Amount:
                        </td>
                        <td>
                            <asp:TextBox ID="txtdisamount" runat="server" CssClass="standardField"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ValidationGroup="asd" ControlToValidate="txtdisamount" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid" ValidationGroup="asd" ValidationExpression="\d+(\.\d{1,2})?" ControlToValidate="txtdisamount" ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Comments:
                        </td>
                        <td>
                            <asp:TextBox ID="txtdiscountcomment" runat="server" TextMode="MultiLine" Rows="3" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btndisamount" runat="server" Text="Add Amount" ValidationGroup="asd" OnClick="btndisamount_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>

        <div id="div2" title="Charges" style="display: none">
            <asp:Panel ID="Panel3" DefaultButton="btnchargeamount" runat="server">
                <table>
                    <tr>
                        <td>Charges Amount:
                        </td>
                        <td>
                            <asp:TextBox ID="txtchargeamount" runat="server" CssClass="standardField"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ValidationGroup="asd1" ControlToValidate="txtchargeamount" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Invalid" ValidationGroup="asd1" ValidationExpression="\d+(\.\d{1,2})?" ControlToValidate="txtchargeamount" ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>

                    </tr>
                    <tr>
                        <td>Comments:
                        </td>
                        <td>
                            <asp:TextBox ID="txtchargescomment" runat="server" TextMode="MultiLine" Rows="3" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btnchargeamount" runat="server" Text="Add Amount" ValidationGroup="asd1" OnClick="btnchargeamount_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
        <div id="div3" title="Adjustment" style="display: none">
            <asp:Panel ID="Panel4" DefaultButton="btnadjustamount" runat="server">
                <table>
                    <tr>
                        <td>Adjustment Amount:
                        </td>
                        <td>
                            <asp:TextBox ID="txtadjustamount" runat="server" CssClass="standardField"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ValidationGroup="asd2" ControlToValidate="txtadjustamount" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Invalid" ValidationGroup="asd2" ValidationExpression="\d+(\.\d{1,2})?" ControlToValidate="txtadjustamount" ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Comments:
                        </td>
                        <td>
                            <asp:TextBox ID="txtajdustmentcomment" runat="server" TextMode="MultiLine" Rows="3" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Add/Subtract:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlAtype" runat="server">
                                <asp:ListItem Value="0" Selected>Add Amount</asp:ListItem>
                                <asp:ListItem Value="1">Subtract Amount</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btnadjustamount" runat="server" Text="Add Amount" ValidationGroup="asd2" OnClick="btnadjustamount_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
          <div id="additionalPaymentMethods" title="Payment Method" style="display: none">
            <asp:Panel ID="Panel5" DefaultButton="btnaddmethod" runat="server">
                <table>
                    <tr>
                        <td>Title:
                        </td>
                        <td>
                            <asp:TextBox ID="txtMethodName" runat="server" CssClass="standardField" ClientIDMode="Static"></asp:TextBox>
                        </td>
                        <td><asp:RequiredFieldValidator runat="server" ControlToValidate="txtMethodName" ValidationGroup="paymentmethod" ErrorMessage="Required" InitialValue="" CssClass="errorMsg" ></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btnaddmethod" runat="server" Text="Save Payment Method" CssClass="standardFormButton" ValidationGroup="paymentmethod" OnClick="btnaddmethod_Click" />
                        </td>
                    </tr>                    
                </table>
            </asp:Panel>
        </div>
         <asp:HiddenField ID="hfid" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hffeebalance" runat="server" ClientIDMode="Static" Value="0" />
         <asp:HiddenField ID="hffeepaid" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hffees" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hfdiscount" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hfcharge" runat="server" ClientIDMode="Static" Value="0" />
        <asp:HiddenField ID="hfadjustment" runat="server" ClientIDMode="Static" Value="0" />
    </form>
</body>
</html>
