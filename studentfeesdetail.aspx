﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="studentfeesdetail.aspx.cs" Inherits="studentfeesdetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        .bordercell {
            border: 1px solid black;
        }

        .paymentrow {
            margin: 2px;
            padding: 1px;
        }
    </style>
    <link href="_assets/css/style.css" rel="stylesheet" />
    <link href="_assets/jquery-ui.css" rel="stylesheet" />
    <link href="_assets/jquery.datepick.css" rel="stylesheet" />
    <%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>--%>

    <%--<script src="_assets/jquery-1.9.1.js"></script>--%>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <%--<script src="_assets/jquery.datepick.js"></script>--%>
    <title></title>


    <script>
        //for add value popup
        function checkdecimal(e) {
            var a = e.value;
            var type = typeof a;

            var intRegex = /^\d+$/;
            var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;

            var str = a;
            if ($('#' + e.id + '').val() != "") {
                if (intRegex.test(str) || floatRegex.test(str)) {
                } else {
                    alert('Not a number');
                    $('#' + e.id + '').val("");

                }
            }

        }

        $(function () {
            $("#lnkhistory").click(function () {
                //$('#lblerror').text("");
                var dlg = $("#divstudentfeehistory").dialog({
                    height: 550,
                    width: 400,
                    show: {
                        //effect: "blind",
                        //duration: 1000
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                        //duration: 1000
                    }
                });
                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");
                resizeiframepage();
                return false;
                //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
            });
        });
        $(document).ready(function () {
            $("#step2").hide();
            resizeiframepage();
        });



        function btnclick() {
            if ($("#txtpayamount").val() != "") {
                var intRegex = /^(0|[1-9][0-9]*)$/;
                if (intRegex.test($("#txtpayamount").val())) {
                    $("#validateamount").text("");
                    return true;
                }
                else {
                    $("#validateamount").text("Invalid amount");
                    return false;
                }
            }
            else {
                $("#validateamount").text("Required");
                return false;
            }
            //if ($("#hf_TotalAmount").val() != "0") {

            //    alert('Amount Paid');
            //    return true;
            //} else {
            //    alert("Fees Already Paid");
            //    return false;
            //}


        }

        function Addmore(btnID) {
            $('.feehide').show();
            var div = $(btnID).parent().parent();
            var count = $(div).find("input[id*='txtChargeType']").size();
            var id = $(div).find("input[id*='txtChargeType']").attr("id"); // to make the function dynamic, concatenate this id
            var id2 = $(div).find("input[id*='txtAmount']").attr("id"); // same as above comment.
            var newrow = $('<p class="paymentrow"><input  id="txtChargeType' + count + '" type="text"  />&nbsp;'
                + '&nbsp;<input id="txtAmount' + count + '" type="text" onblur="checkdecimal(this);" />'
                + '<a href="#" class="RemoveRow">Remove</a>'
                + '</p>');
            $(div).append(newrow);
            resizeiframepage();
            return false;
        }
        $(document).ready(function () {
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            iframe.height = "600px";
        });
        function resizeiframepage() {
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
            if (innerDoc.body.offsetHeight) {
                iframe.height = innerDoc.body.offsetHeight + 400 + "px";
            }
            else if (iframe.Document && iframe.Document.body.scrollHeight) {
                iframe.style.height = iframe.Document.body.scrollHeight + "px";
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="mainContentDiv">
            <asp:HiddenField ID="hfpackagefeeduedate" runat="server" />
            <asp:HiddenField ID="hfpackageletfeesamount" runat="server" />
            <asp:HiddenField ID="hfbalamount" runat="server" Value="0" />
            <asp:HiddenField ID="hfpackagetotalamount" runat="server" />
            <asp:HiddenField ID="hf_TotalAmount" runat="server" />
            <br />
            &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
            <h2>
                <asp:Label ID="lbltitle" runat="server" Text="Student Fee Package Detail"></asp:Label></h2>
            <br />
            <table>
                <tr>
                    <td class="formCaptionTd" width="150px">Title</td>
                    <td>
                        <asp:HiddenField runat="server" ID="hfstudentid" />
                        <input type="text" id="txttitle" runat="server" required disabled="True" />
                        <td></td>
                </tr>
                <tr>
                    <td class="formCaptionTd" width="150px">Fee Amount</td>
                    <td>
                        <input type="text" id="txtfeeamount" runat="server" required disabled="True" />
                        <td></td>
                </tr>
                <tr>
                    <td class="formCaptionTd" width="150px">Late Fee Charges</td>
                    <td>
                        <input type="text" id="txtlatefee" runat="server" required disabled="True" />

                        <td>
                            <asp:LinkButton runat="server" ID="lnkhistory" Text="Fee History"></asp:LinkButton></td>
                </tr>
            </table>

            <br />
            <table>
                <tr id="tr_package" runat="server">
                    <td class="formCaptionTd" width="150px">Package Items : </td>
                    <td>
                        <asp:Repeater ID="rptrfeedetail" runat="server" OnItemDataBound="rptrfeedetail_itemdatabound">
                            <HeaderTemplate>
                                <table class="smallGrid">
                                    <tr class="smallGridHead">
                                        <td>Title
                                        </td>
                                        <td>Amount</td>
                                        <td>Modified</td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="smallGrid">
                                    <td>
                                        <asp:Label ID="lblgridtitle" Text='<%# Eval("DetaiilTitle") %>' runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblgridamount" Text='<%# Eval("DetailAmount") %>' runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblgriddate" Text='<%# Eval("DetailDate") %>' runat="server"></asp:Label></td>

                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr id="tr_charge" runat="server">
                    <td class="formCaptionTd" width="150px">Charge /Discount :</td>
                    <td>
                        <asp:Repeater ID="rptrfeechargediscount" runat="server">
                            <HeaderTemplate>
                                <table class="smallGrid">
                                    <tr class="smallGridHead">

                                        <td>Title</td>
                                        <td>ChargeType</td>
                                        <td>IsFixe</td>
                                        <td>Amount</td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="smallGrid">
                                    <td>
                                        <asp:Label ID="lblgridtitle" Text='<%# Eval("Title") %>' runat="server"></asp:Label></td>
                                    <asp:HiddenField runat="server" ID="hf_ID" Value='<%# Eval("ID") %>' />

                                    <td>
                                        <asp:Label ID="lblgridchargetype" Text='<%# Eval("ChargeType") %>' runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblgridisfixed" Text='<%# Eval("IsFixed") %>' runat="server"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblgridamount" Text='<%# Eval("Amount") %>' runat="server"></asp:Label></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>

                </tr>
                <tr>
                    <td class="formCaptionTd" width="150px"></td>

                    <td></td>
                    <%--<asp:Button runat="server" ID="calc" OnClick="btnclick" Text="calculate" />--%>
                </tr>
            </table>
            <div id="extracharges_div" style="border: solid 1px; width: 600px">
                <div style="float: left; border: 1px solid">
                    <div>
                        <asp:Label runat="server" ID="lbltitletext" CssClass="feehide" Text="Title" Style="margin-left: 5px"></asp:Label>
                        <asp:Label runat="server" ID="lblamounetext" CssClass="feehide" Text="Amount" Style="margin-left: 130px"></asp:Label>
                    </div>
                    <div id="step1">

                        <p class="paymentrow">
                            <asp:TextBox ID="txtChargeType" runat="server"></asp:TextBox>&nbsp;
                        <asp:TextBox ID="txtAmount" runat="server" Height="20px" onblur="checkdecimal(this);"></asp:TextBox>&nbsp;&nbsp;<asp:Button ID="btnAddmore" runat="server" Text=" Add more + " OnClientClick="return Addmore(this)" />
                        </p>

                    </div>
                    <input id="btnproceed" value="Proceed" type="button" onclick="return ProceedToStep2(this)" />
                    <div id="step2">
                        <div id="divMessage" runat="server">
                            <asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
                            
                        </div>
                        Payment Amount: <asp:TextBox ID="txtpayamount" runat="server" CssClass="standardField" ClientIDMode="Static"></asp:TextBox><asp:Label runat="server" ID="validateamount" ClientIDMode="Static" ForeColor="Red"></asp:Label><br /><br />
                        <asp:Button Style="width: 200px" ID="btnPay" runat="server" Text="Pay" OnClick="btnPay_Click" OnClientClick="return btnclick();"/>
                    </div>
                </div>
                <div style="float: right; border: 1px solid">
                    <table>
                        <thead>
                            <th>Charge Type</th>
                            <th>Amount</th>
                        </thead>
                        <tbody id="tbody">
                        </tbody>
                    </table>
                </div>
            </div>
            <div style="height: 100px"></div>

        </div>
        <div id="divstudentfeehistory" title="Fees History" style="display: none">
            <asp:Repeater ID="rptrhistory" runat="server" OnItemDataBound="rptrhistory_itemdatabound">
                <HeaderTemplate>
                    <table class="smallGrid">
                        <tr class="smallGridHead">
                            <td>Message</td>
                            <td>Amount </td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="smallGrid" style="width: 220px">
                        <td>
                            <asp:Label ID="lblgridisfixed" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblgridamount" Text='<%# Eval("Feesubmit") %>' runat="server"></asp:Label></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <asp:HiddenField ID="hfpackageid" runat="server" />
            <asp:HiddenField ID="hfsetting" runat="server" />
        </div>
    </form>
</body>
</html>
<script>
    $(".RemoveRow").live('click', function () {
        $(this).parent().remove();
    });

    function ShowChargesInTable() {
        var string = "";
        var lbltxt = parseInt($("#hf_TotalAmount").val());
        var count = 0;

        if ($("#txtAmount").val() != "")
            $("#step1").find("p").each(function () {
                var txtamountid = "txtAmount";
                var txtchrgtype = $(this).find("input[id*='txtChargeType']").val();
                var txtamount = $(this).find("input[id*='txtAmount']").val();

                string += '<tr>'
                    + '<td>' + txtchrgtype + '</td>'
                    + '<td>' + txtamount + '</td>'
                    + '</tr>';
                if (count == 0) {
                    if ($("input[id*='" + txtamountid + "']").val() != "undefined" && $("input[id*='" + txtamountid + "']").val() != "")

                        lbltxt = lbltxt + parseFloat($("input[id*='" + txtamountid + "']").val());
                    count++;
                } else {
                    txtamountid += count;
                    if ($("input[id*='" + txtamountid + "']").val() != "undefined" && $("input[id*='" + txtamountid + "']").val() != "")
                        lbltxt = lbltxt + parseFloat($("input[id*='" + txtamountid + "']").val());
                    count++;

                }

                //if (typeof $(this).find("input[id*='txtChargeType']").val() != "undefined")
            });
        $("#tbody").empty().append(string);
        $("#lblTotalAmount").text(lbltxt);
        var dds = lbltxt;
        if (parseFloat(lbltxt) < 0)
            dds = 0;
        $("#divMessage").html("</br><h2>Your Total Amount to be submitted is : " + dds + "  </h2></br>");
        $("#hf_TotalAmount").val(lbltxt);
        resizeiframepage();
    }

    function ProceedToStep2(id) {
        $('.feehide').hide();
        var step1 = $("#step1");
        var step2 = $("#step2");
        if ($(id).val() == "Proceed") {

            $(id).val("Add More +");
            $(step1).fadeOut();
            $(step2).fadeIn();
            ShowChargesInTable();
            resizeiframepage();
        }
        else {
            $('.feehide').show();
            $(id).val("Proceed");
            $(step2).fadeOut();
            $(step1).fadeIn();
            resizeiframepage();
        }
    }

    $(document).ready(function () {

    });
</script>
