﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ExcelReader : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        //  dt = ExceltoDatatable("D:/AVAIB PROJECTS/8ee08d08-68ab-4415-8b50-9fa722151856/Excelfile/SimpleExeclFile.xlsx", ".xlsx", "YES");
        dt = ExceltoDatatable(Server.MapPath("~/Excelfile/SimpleExeclFile.xlsx"), ".xlsx", "YES");
        

        if (dt.Rows.Count > 0)
        {
            for (int row = 1; row < dt.Rows.Count; row++)
            {
                if (!String.IsNullOrEmpty(dt.Rows[row][0].ToString()) && !String.IsNullOrEmpty(dt.Rows[row][1].ToString()))
                {
                   // savestudents(dt.Rows[row][0].ToString(), dt.Rows[row][1].ToString(), dt.Rows[row][2].ToString(), dt.Rows[row][4].ToString(), dt.Rows[row][3].ToString(), dt.Rows[row][5].ToString(), dt.Rows[row][6].ToString());
                }
            }
        }        

    }

    protected DataTable ExceltoDatatable(string FilePath, string Extension, string isHDR)
    {
        string conStr = "";
        switch (Extension)
        {
            case ".xls": //Excel 97-03
                conStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1}'";
                break;
            case ".xlsx": //Excel 07
                conStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1}'";
                break;
        }
        conStr = String.Format(conStr, FilePath, isHDR);
        OleDbConnection connExcel = new OleDbConnection(conStr);
        OleDbCommand cmdExcel = new OleDbCommand();
        OleDbDataAdapter oda = new OleDbDataAdapter();
        DataTable dt = new DataTable();
        cmdExcel.Connection = connExcel;

        //Get the name of First Sheet
        connExcel.Open();
        DataTable dtExcelSchema;
        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
        connExcel.Close();

        //Read Data from First Sheet
        connExcel.Open();
        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
        oda.SelectCommand = cmdExcel;
        oda.Fill(dt);
        connExcel.Close();
        return dt;
    }
}