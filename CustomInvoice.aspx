﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CustomInvoice.aspx.cs" Inherits="CustomInvoice" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Custom Invoice</title>
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <link href="_assets/jquery-ui.css" rel="stylesheet" />
    <link href="_assets/jquery.datepick.css" rel="stylesheet" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <script src="_assets/jquery.datepick.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtstartdate").datepicker({
                dateFormat: 'dd-MM-yy',
                changeMonth: true,
                changeYear: true
            });
            $("#txtenddate").datepicker({
                dateFormat: 'dd-MM-yy',
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div id="mainContentDiv">
            <div class="breadCrumb">
                Custom Invoice
            </div>
            <table class="StudentDetailTable">
                <tr>
                    <td class="formCaptionTd">From Date </td>
                    <td class="formFieldTd">
                        <asp:TextBox ID="txtstartdate" runat="server" ClientIDMode="Static" class=" standardField"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd">To Date </td>
                    <td class="formFieldTd">
                        <asp:TextBox ID="txtenddate" runat="server" ClientIDMode="Static" class=" standardField"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd">Fee Amount </td>
                    <td class="formFieldTd">
                        <asp:TextBox ID="txtfeeamount" runat="server" ClientIDMode="Static" class=" standardField"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtfeeamount" ErrorMessage="*" ForeColor="Red" ValidationGroup="ddlfee"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtfeeamount" ErrorMessage="Invalid Amount" ForeColor="Red" ValidationExpression="^[0-9]+([.][0-9]{1,10})?$" ValidationGroup="ddlfee"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd"></td>
                    <td class="formFieldTd">
                        <asp:Button runat="server" ID="btngeninvoice" Text="Generate Invoice" CssClass="standardFormButton" ValidationGroup="ddlfee" OnClick="btngeninvoice_Click" />
                        <asp:HiddenField ID="hfeffdate" runat="server" ClientIDMode="Static" />
                    </td>
                </tr>
                <tr id="tr_note" runat="server" visible="false">
                    <td class="formCaptionTd"></td>
                    <td class="formFieldTd">
                        Invoice Generate Successfully <asp:LinkButton ID="lbtninvoice" runat="server" OnClick="lbtninvoice_Click">Click</asp:LinkButton> here to View Invoices.
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
