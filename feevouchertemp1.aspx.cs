﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class feevouchertemp1 : AvaimaThirdpartyTool.AvaimaWebPage
{
    studentparameter objinvoice;
    studentinvoice objstudent;
    AvaimaStorageUploader objuploader;
    decimal totalfees = 0;
    int session = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                txtpaiddate.Text = DateTime.Now.ToString("dd MMM yyyy");
                bindpaymentmethod();
                Binddata();
            }
        }

        catch (Exception ex)
        {

        }

    }

    private void Binddata()
    {
        DataSet ds = new DataSet();
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            string query = "feesvoucher";
            using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
            {
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@instanceid", this.InstanceID);
                da.SelectCommand.Parameters.AddWithValue("@studentid", Request["Studentid"]);
                da.SelectCommand.Parameters.AddWithValue("@PID", Request["PID"]);
                da.Fill(ds);
            }
        }
        if (ds.Tables[0].Rows.Count > 0)
        {
            objuploader = new AvaimaStorageUploader();
            string headerfile = objuploader.Singlefilelink(this.InstanceID, ds.Tables[0].Rows[0]["headerfile"].ToString());
            img_header.Src = headerfile;
            img_header1.Src = headerfile;
            img_header2.Src = headerfile;
            Footer1.InnerHtml = ds.Tables[0].Rows[0]["footer"].ToString();
            Footer2.InnerHtml = Footer1.InnerHtml;
            Footer3.InnerHtml = Footer1.InnerHtml;
        }
        if (ds.Tables[2].Rows.Count > 0)
        {
            lblclass.Text = ds.Tables[2].Rows[0]["title"].ToString();
            lblclass1.Text = ds.Tables[2].Rows[0]["title"].ToString();
            lblclass2.Text = ds.Tables[2].Rows[0]["title"].ToString();
        }
        if (ds.Tables[1].Rows.Count > 0)
        {
            //lblissuedate.Text = Convert.ToDateTime(ds.Tables[1].Rows[0]["issuedate"].ToString()).ToString("dd-MMMM-yyyy");
            //lblregno.Text = ds.Tables[1].Rows[0]["Regno"].ToString();
            lblgrno.Text = ds.Tables[1].Rows[0]["Regno"].ToString();
            lblstudentname.Text = ds.Tables[1].Rows[0]["studentname"].ToString();
            lblfathername.Text = ds.Tables[1].Rows[0]["ParentName"].ToString();

            //lblissuedate1.Text = Convert.ToDateTime(ds.Tables[1].Rows[0]["issuedate"].ToString()).ToString("dd-MMMM-yyyy");
            //lblregno1.Text = ds.Tables[1].Rows[0]["Regno"].ToString();
            lblgrno1.Text = ds.Tables[1].Rows[0]["Regno"].ToString();
            lblstudentname1.Text = ds.Tables[1].Rows[0]["studentname"].ToString();
            lblfathername1.Text = ds.Tables[1].Rows[0]["ParentName"].ToString();

            //lblissuedate2.Text = Convert.ToDateTime(ds.Tables[1].Rows[0]["issuedate"].ToString()).ToString("dd-MMMM-yyyy");
            //lblregno2.Text = ds.Tables[1].Rows[0]["Regno"].ToString();
            lblgrno2.Text = ds.Tables[1].Rows[0]["Regno"].ToString();
            lblstudentname2.Text = ds.Tables[1].Rows[0]["studentname"].ToString();
            lblfathername2.Text = ds.Tables[1].Rows[0]["ParentName"].ToString();
        }

        if (ds.Tables[3].Rows.Count > 0)
        {
            hfid.Value = ds.Tables[3].Rows[0]["recid"].ToString();
            hffeebalance.Value = ds.Tables[3].Rows[0]["feesbalance"].ToString();
            hffeepaid.Value = ds.Tables[3].Rows[0]["feespaid"].ToString();
            ////lblslipno.Text = ds.Tables[3].Rows[0]["recid"].ToString();
            ////lblslipno1.Text = ds.Tables[3].Rows[0]["recid"].ToString();
            ////lblslipno2.Text = ds.Tables[3].Rows[0]["recid"].ToString();
            DateTime dtstart = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
            if (dtstart.Month >= Convert.ToInt32(ds.Tables[0].Rows[0]["month"].ToString()))
            {
                session = dtstart.Year;
                //lblsession1.Text = dtstart.Year + "-" + (dtstart.Year + 1);
                //lblsession2.Text = dtstart.Year + "-" + (dtstart.Year + 1);
            }
            else
            {
                session = (dtstart.Year - 1);
                //lblsession1.Text = (dtstart.Year - 1) + "-" + dtstart.Year;
                //lblsession2.Text = (dtstart.Year - 1) + "-" + dtstart.Year;
            }

            lblperiod.Text = dtstart.ToString("MMMM - yyyy");
            lblperiod1.Text = dtstart.ToString("MMMM - yyyy");
            lblperiod2.Text = dtstart.ToString("MMMM - yyyy");
            decimal vale = Convert.ToDecimal(ds.Tables[4].Rows[0]["Totalfees"].ToString()) - Convert.ToDecimal(ds.Tables[4].Rows[0]["feespaid"].ToString()) + Convert.ToDecimal(ds.Tables[4].Rows[0]["charges"].ToString()) - Convert.ToDecimal(ds.Tables[4].Rows[0]["discount"].ToString());

            tabletr(ds.Tables[3], vale.ToString("0.00"));
        }
        if (ds.Tables[5].Rows.Count > 0)
        {
            DateTime enddate = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
            string[] duedate = ds.Tables[5].Rows[0]["Fee_DueDate"].ToString().Split('#');
            string[] afterduedate = ds.Tables[5].Rows[0]["LateFeesStartDate"].ToString().Split('#');
            enddate = enddate.AddDays(Convert.ToDouble(duedate[0]));
            enddate = enddate.AddDays(Convert.ToDouble(afterduedate[0]));
            lblduedate.Text = enddate.ToString("dd-MMM-yyyy");
            lblduedate1.Text = lblduedate.Text;
            lblduedate2.Text = lblduedate.Text;
            decimal latefee = 0;
            try
            {
                if (enddate > DateTime.Now)
                    latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["LateFeeCharges"].ToString());
            }
            catch
            {
                if (enddate > DateTime.Now)
                    latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["latefees"].ToString());
            }
            latefee = latefee + totalfees;
            lbldueamount.Text = latefee.ToString();
            lbldueamount1.Text = lbldueamount.Text;
            lbldueamount2.Text = lbldueamount.Text;
        }
        if (ds.Tables[6].Rows.Count > 0)
        {
            generatehtml(ds.Tables[6], session);
            //repthistory.DataSource = ds.Tables[6];
            //repthistory.DataBind();

            //repthistory1.DataSource = ds.Tables[6];
            //repthistory1.DataBind();

            // repthistory2.DataSource = ds.Tables[6];
            //repthistory2.DataBind();
        }
    }
    private void tabletr(DataTable dt, string balance)
    {
        totalfees = 0;
        decimal balanceval = 0;
        lblclassfee.Text = dt.Rows[0]["TotalPackagefees"].ToString();
        lblclassfee1.Text = dt.Rows[0]["TotalPackagefees"].ToString();
        lblclassfee2.Text = dt.Rows[0]["TotalPackagefees"].ToString();

        hffees.Value = dt.Rows[0]["Totalfees"].ToString();

        TableRow row = new TableRow();
        row.Cells.Add(new TableCell() { Text = "Tuition  Fee " + lblperiod.Text, CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "Vch#" + dt.Rows[0]["recid"].ToString() + " ", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = dt.Rows[0]["TotalPackagefees"].ToString(), CssClass = "Tabletr" });
        tbl.Rows.Add(row);
        row = new TableRow();
        row.Cells.Add(new TableCell() { Text = "Tuition  Fee " + lblperiod.Text, CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "Vch#" + dt.Rows[0]["recid"].ToString() + " ", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = dt.Rows[0]["TotalPackagefees"].ToString(), CssClass = "Tabletr" });
        tbl1.Rows.Add(row);
        row = new TableRow();
        row.Cells.Add(new TableCell() { Text = "Tuition  Fee " + lblperiod.Text, CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "Vch#" + dt.Rows[0]["recid"].ToString() + " ", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = dt.Rows[0]["TotalPackagefees"].ToString(), CssClass = "Tabletr" });
        tbl2.Rows.Add(row);
        balanceval = Convert.ToDecimal(balance) - Convert.ToDecimal(dt.Rows[0]["Totalfees"].ToString());
        totalfees = Convert.ToDecimal(dt.Rows[0]["TotalPackagefees"].ToString());

        DataTable dttable = new DataTable();
        objinvoice = new studentparameter();
        dttable = objinvoice.getstudentinvoiceadjust(dt.Rows[0]["recid"].ToString());

        if (Convert.ToDecimal(dt.Rows[0]["TotalCharges"].ToString()) > 0)
        {
            row = new TableRow();
            row.Cells.Add(new TableCell() { Text = "Total Individual Charges", CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = dt.Rows[0]["TotalCharges"].ToString(), CssClass = "Tabletr" });
            tbl.Rows.Add(row);
            row = new TableRow();
            row.Cells.Add(new TableCell() { Text = "Total Individual Charges", CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = dt.Rows[0]["TotalCharges"].ToString(), CssClass = "Tabletr" });
            tbl1.Rows.Add(row);
            row = new TableRow();
            row.Cells.Add(new TableCell() { Text = "Total Individual Charges", CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = dt.Rows[0]["TotalCharges"].ToString(), CssClass = "Tabletr" });
            tbl2.Rows.Add(row);
            totalfees += Convert.ToDecimal(dt.Rows[0]["TotalCharges"].ToString());
        }
        if (Convert.ToDecimal(dt.Rows[0]["Totaldiscount"].ToString()) > 0)
        {
            row = new TableRow();
            row.Cells.Add(new TableCell() { Text = "Total Individual Discount", CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = dt.Rows[0]["Totaldiscount"].ToString(), CssClass = "Tabletr" });
            tbl.Rows.Add(row);
            row = new TableRow();
            row.Cells.Add(new TableCell() { Text = "Total Individual Discount", CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = dt.Rows[0]["Totaldiscount"].ToString(), CssClass = "Tabletr" });
            tbl1.Rows.Add(row);
            row = new TableRow();
            row.Cells.Add(new TableCell() { Text = "Total Individual Discount", CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = dt.Rows[0]["Totaldiscount"].ToString(), CssClass = "Tabletr" });
            tbl2.Rows.Add(row);
            totalfees -= Convert.ToDecimal(dt.Rows[0]["Totaldiscount"].ToString());
        }
        if (dttable.Rows.Count > 0)
        {
            for (int asd = 0; asd < dttable.Rows.Count; asd++)
            {
                if (dttable.Rows[asd]["type"].ToString() == "2")
                {
                    row = new TableRow();
                    row.Cells.Add(new TableCell() { Text = dttable.Rows[asd]["comments"].ToString(), CssClass = "Tabletr" });
                    row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
                    row.Cells.Add(new TableCell() { Text = dttable.Rows[asd]["amount"].ToString(), CssClass = "Tabletr" });
                    tbl.Rows.Add(row);
                    row = new TableRow();
                    row.Cells.Add(new TableCell() { Text = dttable.Rows[asd]["comments"].ToString(), CssClass = "Tabletr" });
                    row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
                    row.Cells.Add(new TableCell() { Text = dttable.Rows[asd]["amount"].ToString(), CssClass = "Tabletr" });
                    tbl1.Rows.Add(row);
                    row = new TableRow();
                    row.Cells.Add(new TableCell() { Text = dttable.Rows[asd]["comments"].ToString(), CssClass = "Tabletr" });
                    row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
                    row.Cells.Add(new TableCell() { Text = dttable.Rows[asd]["amount"].ToString(), CssClass = "Tabletr" });
                    tbl2.Rows.Add(row);

                    totalfees += Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                    balanceval = balanceval - Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                }
                else if (dttable.Rows[asd]["type"].ToString() == "1")
                {
                    row = new TableRow();
                    row.Cells.Add(new TableCell() { Text = dttable.Rows[asd]["comments"].ToString(), CssClass = "Tabletr" });
                    row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
                    row.Cells.Add(new TableCell() { Text = dttable.Rows[asd]["amount"].ToString(), CssClass = "Tabletr" });
                    tbl.Rows.Add(row);
                    row = new TableRow();
                    row.Cells.Add(new TableCell() { Text = dttable.Rows[asd]["comments"].ToString(), CssClass = "Tabletr" });
                    row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
                    row.Cells.Add(new TableCell() { Text = dttable.Rows[asd]["amount"].ToString(), CssClass = "Tabletr" });
                    tbl1.Rows.Add(row);
                    row = new TableRow();
                    row.Cells.Add(new TableCell() { Text = dttable.Rows[asd]["comments"].ToString(), CssClass = "Tabletr" });
                    row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
                    row.Cells.Add(new TableCell() { Text = dttable.Rows[asd]["amount"].ToString(), CssClass = "Tabletr" });
                    tbl2.Rows.Add(row);
                    hfdiscount.Value = dttable.Rows[asd]["amount"].ToString();
                    totalfees -= Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                    balanceval = balanceval + Convert.ToDecimal(dttable.Rows[asd]["amount"].ToString());
                }
            }
        }
        else
        {
            if (Convert.ToDecimal(dt.Rows[0]["extracharges"].ToString()) > 0)
            {
                row = new TableRow();
                row.Cells.Add(new TableCell() { Text = dt.Rows[0]["chargescomment"].ToString(), CssClass = "Tabletr" });
                row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
                row.Cells.Add(new TableCell() { Text = dt.Rows[0]["extracharges"].ToString(), CssClass = "Tabletr" });
                tbl.Rows.Add(row);
                row = new TableRow();
                row.Cells.Add(new TableCell() { Text = dt.Rows[0]["chargescomment"].ToString(), CssClass = "Tabletr" });
                row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
                row.Cells.Add(new TableCell() { Text = dt.Rows[0]["extracharges"].ToString(), CssClass = "Tabletr" });
                tbl1.Rows.Add(row);
                row = new TableRow();
                row.Cells.Add(new TableCell() { Text = dt.Rows[0]["chargescomment"].ToString(), CssClass = "Tabletr" });
                row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
                row.Cells.Add(new TableCell() { Text = dt.Rows[0]["extracharges"].ToString(), CssClass = "Tabletr" });
                tbl2.Rows.Add(row);
                hfcharge.Value = dt.Rows[0]["extracharges"].ToString();
                totalfees += Convert.ToDecimal(dt.Rows[0]["extracharges"].ToString());
            }
            if (Convert.ToDecimal(dt.Rows[0]["extradiscount"].ToString()) > 0)
            {
                row = new TableRow();
                row.Cells.Add(new TableCell() { Text = dt.Rows[0]["discountcomment"].ToString(), CssClass = "Tabletr" });
                row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
                row.Cells.Add(new TableCell() { Text = dt.Rows[0]["extradiscount"].ToString(), CssClass = "Tabletr" });
                tbl.Rows.Add(row);
                row = new TableRow();
                row.Cells.Add(new TableCell() { Text = dt.Rows[0]["discountcomment"].ToString(), CssClass = "Tabletr" });
                row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
                row.Cells.Add(new TableCell() { Text = dt.Rows[0]["extradiscount"].ToString(), CssClass = "Tabletr" });
                tbl1.Rows.Add(row);
                row = new TableRow();
                row.Cells.Add(new TableCell() { Text = dt.Rows[0]["discountcomment"].ToString(), CssClass = "Tabletr" });
                row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
                row.Cells.Add(new TableCell() { Text = dt.Rows[0]["extradiscount"].ToString(), CssClass = "Tabletr" });
                tbl2.Rows.Add(row);
                hfdiscount.Value = dt.Rows[0]["extradiscount"].ToString();
                totalfees -= Convert.ToDecimal(dt.Rows[0]["extradiscount"].ToString());
            }
        }
        if (Convert.ToDecimal(dt.Rows[0]["adjustment"].ToString()) > 0)
        {
            row = new TableRow();
            row.Cells.Add(new TableCell() { Text = dt.Rows[0]["adjustmentcomment"].ToString(), CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = dt.Rows[0]["adjustment"].ToString(), CssClass = "Tabletr" });
            tbl.Rows.Add(row);
            row = new TableRow();
            row.Cells.Add(new TableCell() { Text = dt.Rows[0]["adjustmentcomment"].ToString(), CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = dt.Rows[0]["adjustment"].ToString(), CssClass = "Tabletr" });
            tbl1.Rows.Add(row);
            row = new TableRow();
            row.Cells.Add(new TableCell() { Text = dt.Rows[0]["adjustmentcomment"].ToString(), CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
            row.Cells.Add(new TableCell() { Text = dt.Rows[0]["adjustment"].ToString(), CssClass = "Tabletr" });
            tbl2.Rows.Add(row);
            hfadjustment.Value = dt.Rows[0]["adjustment"].ToString();
            totalfees += Convert.ToDecimal(dt.Rows[0]["adjustment"].ToString());
        }

        row = new TableRow();
        row.Cells.Add(new TableCell() { Text = "Previous Balance", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = balanceval.ToString(), CssClass = "Tabletr" });
        tbl.Rows.Add(row);
        row = new TableRow();
        row.Cells.Add(new TableCell() { Text = "Previous Balance", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = balanceval.ToString(), CssClass = "Tabletr" });
        tbl1.Rows.Add(row);
        row = new TableRow();
        row.Cells.Add(new TableCell() { Text = "Previous Balance", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = balanceval.ToString(), CssClass = "Tabletr" });
        tbl2.Rows.Add(row);
        row = new TableRow();
        row.Cells.Add(new TableCell() { Text = "Admission Fee", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "", CssClass = "Tabletr" });
        tbl.Rows.Add(row);
        row = new TableRow();
        row.Cells.Add(new TableCell() { Text = "Admission Fee", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "", CssClass = "Tabletr" });
        tbl1.Rows.Add(row);
        row = new TableRow();
        row.Cells.Add(new TableCell() { Text = "Admission Fee", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "", CssClass = "Tabletr" });
        tbl2.Rows.Add(row);
        row = new TableRow();
        row.Cells.Add(new TableCell() { Text = "Annual Fee", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "", CssClass = "Tabletr" });
        tbl.Rows.Add(row);
        row = new TableRow();
        row.Cells.Add(new TableCell() { Text = "Annual Fee", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "", CssClass = "Tabletr" });
        tbl1.Rows.Add(row);
        row = new TableRow();
        row.Cells.Add(new TableCell() { Text = "Annual Fee", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "", CssClass = "Tabletr" });
        tbl2.Rows.Add(row);
        row = new TableRow();
        row.Cells.Add(new TableCell() { Text = "Examination Fee", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "", CssClass = "Tabletr" });
        tbl.Rows.Add(row);
        row = new TableRow();
        row.Cells.Add(new TableCell() { Text = "Examination Fee", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "", CssClass = "Tabletr" });
        tbl1.Rows.Add(row);
        row = new TableRow();
        row.Cells.Add(new TableCell() { Text = "Examination Fee", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "Vch#__________  ", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "", CssClass = "Tabletr" });
        tbl2.Rows.Add(row);
        row = new TableRow();
        row.CssClass = "totalAm";
        row.Cells.Add(new TableCell() { Text = "<b>Total Due</b>", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "<b>" + (totalfees + balanceval).ToString() + "</b>", CssClass = "Tabletr" });
        tbl.Rows.Add(row);
        row = new TableRow();
        row.CssClass = "totalAm";
        row.Cells.Add(new TableCell() { Text = "<b>Total Due</b>", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "<b>" + (totalfees + balanceval).ToString() + "</b>", CssClass = "Tabletr" });
        tbl1.Rows.Add(row);
        row = new TableRow();
        row.CssClass = "totalAm";
        row.Cells.Add(new TableCell() { Text = "<b>Total Due</b>", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "", CssClass = "Tabletr" });
        row.Cells.Add(new TableCell() { Text = "<b>" + (totalfees + balanceval).ToString() + "</b>", CssClass = "Tabletr" });
        tbl2.Rows.Add(row);
        lbltotladue.Text = (totalfees + balanceval).ToString();
        totalfees = totalfees + balanceval;
    }
    protected void repthistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        object container = e.Item.DataItem;

    }
    private void generatehtml(DataTable dt, int year)
    {
        StringBuilder HTML = new StringBuilder();
        for (int cell = 0; cell < 6; cell++)
        {
            string ss = getmonth(cell, year);
            bool chdt = false;
            if (dt.Rows.Count > 0)
            {
                for (int aasd = 0; aasd < dt.Rows.Count; aasd++)
                {
                    if (ss == dt.Rows[aasd]["months"].ToString())
                    {
                        HTML.AppendLine("<tr><td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dt.Rows[aasd]["months"].ToString() + "</td>");
                        HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dt.Rows[aasd]["recid"].ToString() + "</td>");
                        HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dt.Rows[aasd]["totalfees"].ToString() + "</td></tr>");
                        dt.Rows.RemoveAt(aasd);
                        chdt = true;
                    }
                }
            }
            if (!chdt)
            {
                HTML.AppendLine("<tr><td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + ss + "</td>");
                HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\"> </td>");
                HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\"> </td></tr>");
            }
        }

        lt_history.Text = HTML.ToString();
        lt_history1.Text = HTML.ToString();
        lt_history2.Text = HTML.ToString();
        StringBuilder HTML1 = new StringBuilder();
        for (int cell1 = 6; cell1 < 12; cell1++)
        {
            string ss = getmonth(cell1, year);
            bool chdt1 = false;
            if (dt.Rows.Count > 0)
            {
                for (int assd = 0; assd < dt.Rows.Count; assd++)
                {
                    if (ss == dt.Rows[assd]["months"].ToString())
                    {
                        HTML1.AppendLine("<tr><td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dt.Rows[assd]["months"].ToString() + "</td>");
                        HTML1.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dt.Rows[assd]["recid"].ToString() + "</td>");
                        HTML1.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dt.Rows[assd]["totalfees"].ToString() + "</td></tr>");
                        dt.Rows.RemoveAt(assd);
                        chdt1 = true;
                    }
                }
            }
            if(!chdt1)
            {
                HTML1.AppendLine("<tr><td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + ss + "</td>");
                HTML1.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\"> </td>");
                HTML1.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\"> </td></tr>");
            }
        }
        Literal1.Text = HTML1.ToString();
        Literal2.Text = HTML1.ToString();
        Literal3.Text = HTML1.ToString();
    }
    private string getmonth(int month, int year)
    {
        int mon = month + 3;
        if (mon > 12)
        {
            mon = mon - 12;
            year = year + 1;
        }
        DateTime dd = Convert.ToDateTime(mon + "/1/" + year);
        return dd.ToString("MMM - yyyy");
    }


    protected void btndisamount_Click(object sender, EventArgs e)
    {
        if (hfid.Value != "0")
        {
            objinvoice = new studentparameter();
            //objinvoice.ID = Convert.ToInt32(hfid.Value);

            //objinvoice.Totalfees = Convert.ToDecimal(hffees.Value) - Convert.ToDecimal(txtdisamount.Text);
            //objinvoice.feesbalance = objinvoice.Totalfees - Convert.ToDecimal(hffeepaid.Value);
            //objstudent = new studentinvoice();
            //objstudent.adddiscount(objinvoice);
            objinvoice.Insertinvoiceajustments(txtdiscountcomment.Text, txtdisamount.Text, hfid.Value, "1");
            Binddata();
        }
    }
    protected void btnchargeamount_Click(object sender, EventArgs e)
    {
        if (hfid.Value != "0")
        {
            objinvoice = new studentparameter();
            //objinvoice.ID = Convert.ToInt32(hfid.Value);

            //objinvoice.Totalfees = Convert.ToDecimal(hffees.Value) + Convert.ToDecimal(txtchargeamount.Text);
            //objinvoice.feesbalance = objinvoice.Totalfees - Convert.ToDecimal(hffeepaid.Value);
            //objstudent = new studentinvoice();
            //objstudent.addcharges(objinvoice);
            objinvoice.Insertinvoiceajustments(txtchargescomment.Text, txtchargeamount.Text, hfid.Value, "2");
            Binddata();
        }
    }
    protected void btnadjustamount_Click(object sender, EventArgs e)
    {
        if (hfid.Value != "0")
        {
            objinvoice = new studentparameter();
            objinvoice.ID = Convert.ToInt32(hfid.Value);
            objinvoice.adjustmentcomment = txtajdustmentcomment.Text;
            if (ddlAtype.SelectedValue == "0")
            {
                objinvoice.Totalfees = Convert.ToDecimal(hffees.Value) + Convert.ToDecimal(txtadjustamount.Text);
                objinvoice.adjustment = Convert.ToDecimal(txtadjustamount.Text) + (Convert.ToDecimal(hfadjustment.Value));
            }
            else
            {
                objinvoice.adjustment = Convert.ToDecimal(txtadjustamount.Text) - (Convert.ToDecimal(hfadjustment.Value));
                objinvoice.Totalfees = Convert.ToDecimal(hffees.Value) - Convert.ToDecimal(txtadjustamount.Text);
            }
            objinvoice.adjustmentflag = ddlAtype.SelectedValue;
            objinvoice.feesbalance = objinvoice.Totalfees - Convert.ToDecimal(hffeepaid.Value);
            objstudent = new studentinvoice();
            objstudent.addadjustment(objinvoice);
            Binddata();
        }
    }

    /*Payment*/
    private void bindpaymentmethod()
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("select * from paymentmethod WHERE instanceid = @InstanceId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        ddlpayment.DataSource = dt;
                        ddlpayment.DataTextField = "paymentmethod";
                        ddlpayment.DataValueField = "recid";
                        ddlpayment.DataBind();
                        ddlpayment.Items.Insert(0, new ListItem("Select", "0"));
                    }
                    else
                    {
                        ddlpayment.DataSource = null;
                        ddlpayment.DataBind();
                        ddlpayment.Items.Insert(0, new ListItem("Select", "0"));
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnprocessed_Click(object sender, EventArgs e)
    {
        try
        {
            objinvoice = new studentparameter();
            objstudent = new studentinvoice();
            decimal payment = Convert.ToDecimal(txtpayment.Text);
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                decimal value = Convert.ToDecimal(txtpayment.Text) - Convert.ToDecimal(lbltotladue.Text);
                if (value < 0)
                    value = 0;

                using (SqlCommand cmd = new SqlCommand("insertbalpayment", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@studentid", Request["Studentid"]);
                    cmd.Parameters.AddWithValue("@Balanceamount", value);
                    cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                    cmd.ExecuteNonQuery();
                }
            }
            DataTable dtunpaid = new DataTable();
            dtunpaid = objstudent.Getunpaidinvoice(Request["Studentid"]);
            decimal amount = 0;
            if (dtunpaid.Rows.Count > 0)
            {
                foreach (DataRow item in dtunpaid.Rows)
                {
                    if (payment == 0)
                    {
                        break;
                    }
                    string lblmonth = item["Monthrange"].ToString();
                    string[] stringSeparators = new string[] { "to" };
                    string[] stringSeparatorscom = new string[] { ":" };
                    string date = "";
                    try
                    {
                        date = lblmonth.Split(stringSeparators, StringSplitOptions.None)[1].Trim(' ');
                    }
                    catch
                    {
                        date = lblmonth.Split(stringSeparatorscom, StringSplitOptions.None)[1].Trim(' ');
                    }
                    string lblpaid = item["feespaid"].ToString();
                    string lblbalance = item["feesbalance"].ToString();
                    string hfrecid = item["recid"].ToString();
                    objinvoice.ID = Convert.ToInt32(hfrecid);
                    decimal balance = Convert.ToDecimal(lblbalance);
                    if (payment > balance || payment == balance)
                    {
                        amount = payment - balance;
                        payment = amount;
                        objinvoice.paidcomplete = "1";
                        objinvoice.feespaid = Convert.ToDecimal(lblpaid) + balance;
                        objinvoice.feesbalance = 0;
                        objstudent.updatepayments(objinvoice);
                        if (payment == 0)
                            objstudent.insertfeecollection(Request["Studentid"], txtpayment.Text, Convert.ToDateTime(date), this.InstanceID, ddlpayment.SelectedValue, txtpaycomment.Text, Convert.ToDateTime(txtpaiddate.Text), HttpContext.Current.User.Identity.Name, txtpaidby.Text);
                    }
                    else
                    {
                        amount = balance - payment;
                        objinvoice.feespaid = Convert.ToDecimal(lblpaid) + payment;
                        payment = 0;
                        objinvoice.paidcomplete = "0";
                        objinvoice.feesbalance = amount;
                        objstudent.updatepayments(objinvoice);
                        objstudent.insertfeecollection(Request["Studentid"], txtpayment.Text, Convert.ToDateTime(date), this.InstanceID, ddlpayment.SelectedValue, txtpaycomment.Text, Convert.ToDateTime(txtpaiddate.Text), HttpContext.Current.User.Identity.Name, txtpaidby.Text);
                    }
                }
            }
            Binddata();
            txtpayment.Text = "";
            txtpayment.Focus();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnaddmethod_Click(object sender, EventArgs e)
    {
        studentinvoice obj = new studentinvoice();
        obj.addPaymentMethod(txtMethodName.Text, this.InstanceID);
        ddlpayment.Items.Clear();
        bindpaymentmethod();
    }
}