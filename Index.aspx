﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Index" %>

<%@ Register Src="~/WebApplication1/PagerControl.ascx" TagPrefix="uc1" TagName="PagerControl" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        .bordercell {
            border: 1px solid black;
        }
    </style>
    <title></title>
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <link href="_assets/css/style.css" rel="stylesheet" />
    <link href="_assets/jquery-ui.css" rel="stylesheet" />
    <link href="_assets/jquery.datepick.css" rel="stylesheet" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <script src="_assets/jquery.datepick.js"></script>
    <script src="_assets/js/pages/indexscript.js"></script>

</head>
<body>
    <form id="form1" runat="server" novalidate>
        <div id="mainContentDiv">
            <div id="divmenu" runat="server">
                <div id="divddlhide" runat="server">
                    <asp:LinkButton ID="lnkaddstudent" Text="Add Student" OnClick="lnkaddstudnet_Click" runat="server"></asp:LinkButton>
                    |
            <asp:LinkButton ID="lnkadddepartment" Text="Add Class" runat="server" OnClick="lnkaddpartment_Click"></asp:LinkButton>
                    |
            <asp:LinkButton ID="lnkmove" Text="Move" runat="server"></asp:LinkButton>
                    |
            <asp:LinkButton ID="lbtn_del" Text="Remove" runat="server"></asp:LinkButton>

                    <%--<asp:LinkButton ID="LinkButton1" Text="Delete" runat="server" OnClientClick="return removeConfirmation(this)" OnClick="lbtndel_Click"></asp:LinkButton>--%>

                    |
            <asp:LinkButton ID="lnksearch" Text="Search" runat="server"></asp:LinkButton>
                    <asp:Label ID="lbluploadsep" runat="server" Text="|" Visible="false"></asp:Label>
                    <asp:LinkButton ID="lbtnupload" Text="Uploadfile" runat="server" ClientIDMode="Static" Visible="false"></asp:LinkButton>
                </div>
                <asp:Label ID="lblexception" Text="" runat="server" ForeColor="red"></asp:Label>
                <div id="divalign" runat="server" style="width: 550px; position: relative">
                    <asp:Label runat="server" Text="." ID="lblallignment" ForeColor="White" Style="position: absolute"></asp:Label>

                    <asp:LinkButton ID="root" Text="Root" runat="server" OnClick="root_Click"></asp:LinkButton>
                    <asp:Label runat="server" ID="lblslash" Text="/"></asp:Label>
                    <asp:Repeater runat="server" ID="rptBreadCrum" OnItemDataBound="rptBreadCrum_ItemDataBound" OnItemCommand="rptBreadCrum_ItemCommand">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lbtnBreadCrum" CommandName="breadcrum"></asp:LinkButton>
                            / 
                        </ItemTemplate>
                    </asp:Repeater>
                    <div id="divbreadcrum" runat="server" style="display: none">

                        <asp:Label ID="lblhirerchy" Text="Root ->" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div runat="server" title="Upload File" id="div_upload" clientidmode="Static" style="background: #f5f5f5; display: none; width: 300px; padding: 10px">

                <div style="color: red !important;">
                    <b>Note:</b> <a href="Sample-Excel-File.xlsx" target="_blank" style="color: blue !important;">Click Here</a> to Download Simple File<br />
                </div>
                <br />
                <div>
                    <b>Format guidelines for Excel File:</b>
                    <br />
                    <br />
                    <ul style="display: list-item !important; margin: 0px 0px 0px 20px;">
                        <li>Only - (  ) alphabets and numbers are allowed only for <b>Roll number</b>. Any other characters will be stripped.</li>
                        <li>For <b>Phone number</b> only - (  ) + and numbers are allowed. Use standard format. Any other characters will be stripped.</li>
                        <li>If you already have records, and you upload a file then the new records in the file you are uploading will be added.
                             The existing ones will not be deleted or effected in any way. Duplicate records will not be entered and you will be notified.</li>
                        <li><b>Tip:</b> Avoid empty cells in the excel file especially the roll number.</li>                       
                    </ul>

                </div>
                <br />
                <br />
                <asp:FileUpload ID="FileUpload1" runat="server" Width="181px" /><asp:Button ID="btnupload" runat="server" Text="Upload" CssClass="standardButton" OnClick="btnupload_Click" />
            </div>
            <div id="searchdate" style="display: none">
                <asp:HiddenField runat="server" ID="hfdivstate" Value="0" />
                <table class="smallGrid">
                    <tr>
                        <td>Search by:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtsearchtext" CssClass=" standardField"></asp:TextBox>
                        </td>

                    </tr>

                    <tr class="smallGrid">
                        <td></td>
                        <td>
                            <asp:Button ID="btnserachdate" Text=" Search " OnClick="btnsearch_click" runat="server" CssClass="standardFormButton" />
                            <asp:Button ID="btn_cancel" Text=" Close Search & Result " runat="server" OnClick="btn_cancel_click" CssClass="standardFormButton" />
                        </td>

                    </tr>
                </table>
            </div>


            <div id="divresult" runat="server">
                <asp:Repeater ID="RptrFeesManagement" runat="server" OnItemCommand="RptrFeesManagement_itemcommand" OnItemDataBound="RptrFeesManagement_itemdatabound" OnItemCreated="RptrFeesManagement_ItemCreated">
                    <HeaderTemplate>
                        <table id="tblconctract" class="smallGrid">
                            <tr class="smallGridHead">
                                <td>
                                    <asp:CheckBox ID="chkselectall" runat="server" CssClass="cb_select_all" />
                                </td>
                                <td></td>
                                <td style="width: 350px">Title 
                                </td>
                                <td  runat="server" id="thHeadArchive">Archived Students
                                </td>
                                <td>Modification Date
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="smallGrid">
                            <td>
                                <asp:CheckBox ID="chkselecteditem" runat="server" CssClass="cb_select" />
                                <asp:HiddenField runat="server" ID="hf_ID" Value='<%# Eval("ID") %>' />
                            </td>
                            <td class="folderLink">
                                <asp:Image ID="imgfolder" ImageUrl="images/Folder.png" runat="server" />
                                <asp:Label ID="lblstudentcheck" Text='<%# Eval("IsStudent") %>' Visible="False" runat="server"></asp:Label>
                            </td>
                            <td>

                                <asp:LinkButton ID="lnktitle" Text='<%# Eval("Title") %>' CommandArgument='<%# Eval("ID") %>' CommandName="search" runat="server"></asp:LinkButton>
                                &nbsp;<asp:Label runat="server" ID="lblcount"></asp:Label>
                                <%--<asp:ImageButton runat="server" ID="imgedit" ImageUrl="images/pencil.png" CommandName="edit" CommandArgument='<%# Eval("ID") %>' />--%>
                                <asp:ImageButton ID="imgedit" ImageUrl="images/pencil.png" runat="server" CommandName="edit" CommandArgument='<%# Eval("ID") %>' />
                                <asp:Label runat="server" ID="lblpackagename" Style="float: right" Text='<%# Eval("packagename") %>'></asp:Label>
                            </td>

                             <td  id="tdArchive" runat="server">
                                <asp:LinkButton runat="server" ID="lnkArchive" Text="View" CommandName="archive" CommandArgument='<%# Eval("ID") %>'></asp:LinkButton>
                            </td>
                            <td>
                                <asp:Label runat="server" Text='<%# Eval("ModificationDate") %>' ID="lblmodeficationdate"></asp:Label>

                            </td>
                        </tr>
                    </ItemTemplate>

                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <asp:HiddenField runat="server" ID="hfparentid" />

                <asp:Label ID="lblgriderror" runat="server" Text="" ForeColor="red"></asp:Label>
                <div style="width: 300px">
                    <uc1:PagerControl runat="server" ID="PagerControl" PageSize="20" />
                    <%--<uc1:PagerControl runat="server" ID="pg_Control" PageSize="20" />--%>
                </div>

            </div>
            <div id="divtreeview" title="Move" style="display: none">
                <asp:TreeView ID="TreeView1" runat="server" ImageSet="Arrows" CollapseImageToolTip="">
                </asp:TreeView>
                <br />
                <br />
                Package Effect From: 
                <asp:TextBox ID="txtstartdate" runat="server" ClientIDMode="Static" class=" standardField"></asp:TextBox><br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" PlaceHolder="Date" ControlToValidate="txtstartdate" ValidationGroup="scv"></asp:RequiredFieldValidator>
                <br />
                <br />
                <asp:Button ID="btntreeview" runat="server" Text="Move" OnClick="btntreeview_Click" CssClass="standardFormButton" ValidationGroup="scv" />
                <asp:Button ID="btncancel" Text="Cancel" runat="server" CssClass="standardFormButton" />
                <asp:HiddenField ID="hftreenodevalue" Value="" runat="server" />
                <br />
                <br />
                <b>Note:</b>To move the student to another class, package of that class can be effected on defined date.

            </div>
        </div>


        <div id="divdepart" title="Class Info" style="display: none">
            <asp:Panel ID="pnladd" DefaultButton="btninsert" runat="server">
                <br />
                <asp:HiddenField runat="server" ID="hfeditid" Value="0" />
                <table>
                    <tr>
                        <td>Class Name</td>
                        <td>
                            <asp:TextBox ID="txttitle" runat="server" CssClass=" standardField" required></asp:TextBox>

                        </td>
                        <td>
                            <asp:Label runat="server" ID="lbltitlereq" ForeColor="red" Text="*"></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td>Select Package</td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlpackage" required>
                            </asp:DropDownList>
                            <asp:Label runat="server" ID="lblddlerror" ForeColor="red" Text="*"></asp:Label></td>

                    </tr>
                    <tr style="display: none">
                        <td>Select Date Setting</td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddldateset">
                                <asp:ListItem Value="">Select</asp:ListItem>
                            </asp:DropDownList>
                        </td>

                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btninsert" Text="submit" runat="server" OnClick="btninsert_Click" CssClass="standardFormButton" /></td>
                    </tr>
                    <asp:Label ID="lblerror" Text="" ForeColor="red" runat="server"></asp:Label>
                    <asp:HiddenField ID="HiddenField1" Value="0" runat="server" />
                    <asp:HiddenField ID="hfstudentemail" Value="0" runat="server" />
                    <asp:HiddenField ID="hfparentemail" Value="0" runat="server" />
                </table>
            </asp:Panel>
        </div>
        <div id="removemail" style="display: none;" title="Confirmation">
            Are you sure you want to Delete the following record<b id="b_name_to_be_deleted"></b>?
                       <asp:Label ID="lblrem" runat="server"></asp:Label>
        </div>

        <%-- <div id="div_delete" style="display: none;color:red; font-weight:bold;" title="Confirmation">
             Deleting a record will delete ALL its payment history and everything else associated with that record. 
            Even entering the same record with the same roll number will not bring the history back. 
             Be wise - and think hard before you delete.
            <br /> <br />
            <asp:LinkButton ID="lnkDelRecords" Text="Delete" runat="server" OnClientClick="return confirm('Are You Sure You Want to Delete. !')" OnClick="lbtndel_Click"></asp:LinkButton>
             <asp:LinkButton ID="lnkCancel" Text="Cancel" runat="server" ></asp:LinkButton>
        </div>--%>

        <div id="div_delete" style="display: none;" title="Confirmation">
            <asp:Label runat="server" ID="lblRemoveReason" Text="Please select a reason:"></asp:Label>
            &nbsp;
            <asp:DropDownList runat="server" ID="ddlRemoveReason" ValidationGroup="rr">
                <asp:ListItem Value="" Text="Select"></asp:ListItem>
                <asp:ListItem Value="Trash" Text="Trash"></asp:ListItem>
                <asp:ListItem Value="Alumni" Text="Alumni"></asp:ListItem>
                <asp:ListItem Value="Termination" Text="Termination"></asp:ListItem>
                <asp:ListItem Value="Suspended" Text="Suspended"></asp:ListItem>
            </asp:DropDownList>
            &nbsp;
            <asp:RequiredFieldValidator runat="server" ID="rqrd" ForeColor="Red" ControlToValidate="ddlRemoveReason" SetFocusOnError="true" ErrorMessage="Required!" ValidationGroup="rr"></asp:RequiredFieldValidator>
            <br />
            <br />
            <br />
            <asp:Button ID="lnkDelRecords" Text="Remove" runat="server" OnClick="lbtndel_Click" ValidationGroup="rr"></asp:Button>
            <asp:Button ID="lnkCancel" Text="Cancel" runat="server"></asp:Button>
        </div>

    </form>
</body>
</html>
