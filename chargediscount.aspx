﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="chargediscount.aspx.cs" Inherits="chargediscount" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        .bordercell {
            border: 1px solid black;
        }
    </style>
    <title></title>
    <link href="_assets/css/style.css" rel="stylesheet" />
    <link href="_assets/jquery-ui.css" rel="stylesheet" />
    <link href="_assets/jquery.datepick.css" rel="stylesheet" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <script src="_assets/jquery.datepick.js"></script>
    <script>
        function opendetailedit() {
            var dlg = $("#diveditpackage").dialog({
                height: 500,
                width: 350,
                show: {
                    height: 100
                    //effect: "blind",
                    //duration: 1000
                },
                modal: true,
                hide: {
                    effect: "blind",
                    //duration: 1000
                }
            });
            //resizeiframepage();

            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");

            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="mainContentDiv">
            <table>
                <tr>
                    <td class="formCaptionTd" width="150px">Title</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtchargetitle" CssClass=" standardField"></asp:TextBox></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="formCaptionTd" width="150px">Package Type</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlchargetype">
                            <asp:ListItem Value="Charge" Text="Charge"></asp:ListItem>
                            <asp:ListItem Value="Discount" Text="Discount"></asp:ListItem>
                        </asp:DropDownList></td>
                    <td></td>

                </tr>
                <tr>
                    <td class="formCaptionTd" width="150px">Amount Type</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlamounttype">
                            <asp:ListItem Value="%age" Text="Percentage"></asp:ListItem>
                            <asp:ListItem Value="Fixed" Text="Fixed"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td></td>

                </tr>
                <tr>
                    <td class="formCaptionTd" width="150px">Amount</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtamount" CssClass=" standardField"></asp:TextBox></td>
                    <td></td>

                </tr>
                <tr>
                    <td class="formCaptionTd" width="150px"></td>
                    <td>
                        <asp:Button runat="server" ID="btnsubmit" Text="Save" OnClick="btnsubmit_click" CssClass="standardFormButton"/></td>
                    <td></td>

                </tr>
            </table>

        
        <div id="divresult" runat="server">
            <asp:Repeater ID="rptrpakagegrid" runat="server" OnItemDataBound="rptrpakagegrid_itemdatabound" OnItemCommand="rptrpakagegrid_itemcommand">
                <HeaderTemplate>
                    <table id="tblpackage" class="smallGrid">
                        <tr class="smallGridHead">
                            <td style="display: none"></td>
                            <td>Title 
                            </td>
                            <td>Package Type
                            </td>
                            <td>Amount Type</td>
                            <td>Amount
                            </td>
                            <%--<td></td>--%>
                            <td></td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="smallGrid">
                        <td style="display: none">
                            <asp:HiddenField runat="server" ID="hf_ID" Value='<%# Eval("ID") %>' />
                        </td>
                        <td>

                            <asp:Label ID="lblcatcheck" Text='<%# Eval("Title") %>' runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbltype" Text='<%# Eval("ChargeType") %>' runat="server"></asp:Label>
                        </td>

                        <td>
                            <asp:Label runat="server" ID="lblisfixed" Text='<%# Eval("IsFixed") %>'></asp:Label>

                        </td>


                        <td>
                            <asp:Label runat="server" ID="lblamount" Text='<%# Eval("Amount") %>'></asp:Label>
                        </td>
                       <%-- <td>
                            <asp:LinkButton runat="server" ID="lnkedit" Text="EDIT" CommandName="edit" CommandArgument='<%# Eval("ID") %>'></asp:LinkButton>
                        </td>--%>
                        <td>
                            <asp:LinkButton runat="server" ID="lnkdelete" Text="Delete" OnClientClick="return confirm('Are You sure you want to delete')" CommandName="delete" CommandArgument='<%# Eval("ID") %>'></asp:LinkButton>
                        </td>

                    </tr>
                </ItemTemplate>

                <FooterTemplate>
                    </table>
                </FooterTemplate>

            </asp:Repeater>
            <asp:Label ID="lblgriderror" runat="server" Text="" ForeColor="red"></asp:Label>
            <div style="width: 300px">
                <%--<uc1:PagerControl runat="server" ID="pg_Control" PageSize="20" />--%>
            </div>
</div>
        </div>
        
        
        <div id="diveditpackage" style="display: none">
            <table>
                <tr>
                    <td  width="150px">Title</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtedittitle" CssClass=" standardField"></asp:TextBox></td>
                </tr>
                <tr>
                    <td width="150px"></td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddleditcharge">
                            <asp:ListItem Value="Charge" Text="Charge"></asp:ListItem>
                            <asp:ListItem Value="Discount" Text="Discount"></asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td  width="150px">Charge Type</td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddleditamounttype">
                            <asp:ListItem Value="%age" Text="%age"></asp:ListItem>
                            <asp:ListItem Value="Fixed" Text="Fixed"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td  width="150px">Amount</td>
                    <td>
                        <asp:TextBox runat="server" ID="txteditamount" CssClass=" standardField"></asp:TextBox></td>
                </tr>
                <tr>
                    <td  width="150px"></td>
                    <td>
                        <asp:Button runat="server" ID="btnedit" Text=" Update " OnClick="btnupdate_click" CssClass="standardFormButton"/></td>
                </tr>
            </table>
            <asp:HiddenField runat="server" ID="hfeditpackageid"/>
        </div>
        

    </form>
</body>
</html>
