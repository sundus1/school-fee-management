﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="note.ascx.cs" Inherits="WebApplication1_note" %>

 <%--<link rel="stylesheet" type="text/css" media="screen" href="style.css" />
    <link rel="stylesheet" href="_assets/jquery-ui.css" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>--%>

<script type="text/javascript">

    $(function () {
        $("body").on("click", "#lnknewnote", function () {
            var dlg = $("#notediv").dialog({
                width:500,
                show: {
                    height: 100
                },
                modal: true,
                hide: {
                    effect: "blind",
                }
            });
            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");
            return false;
        });
    });
</script>

<style>
    .formTitleDiv {
        border-top: 1px solid #b5d1e6;
        border-bottom: 1px solid #b5d1e6;
        background: #e9f9ff;
        padding: 5px 0 4px 11px;
        font-size: 16px;
    }
    .standardField {}
</style>

<table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td colspan="2">
            <%--<div class="formTitleDiv">
                Notes
            </div>--%>
        </td>
    </tr>
    <tr>
        <td class="formCaptionTd" width="150px" style="text-align: right">
          Notes :  
        </td>
        <td style="margin: 6px 0 0 0 !important;float: left">
            <asp:LinkButton ID="lnknewnote" ClientIDMode="Static" runat="server" Text="Add Note For This Contract"></asp:LinkButton>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <table class="smallGrid">
                <asp:Repeater ID="rptnote" runat="server" OnItemDataBound="rptnote_ItemDataBound">
                    <HeaderTemplate>
                        <tr class="smallGridHead">
                            
                            <td>Title
                            </td>
                            <td>Note
                            </td>
                            <td colspan="2">Modification
                            </td>

                        </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                           
                            <td>
                                <asp:Label ID="lbltitle" runat="server" Text='<%# Eval("Title") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblnote" runat="server" Text='<%# Eval("Note") %>'></asp:Label>
                            </td>
                             <td>
                                <asp:Label ID="lbldate" runat="server" ></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbltime" runat="server"></asp:Label>
                            </td>

                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </td>
    </tr>
</table>

<div id="notediv" title="Add Note" style="display: none">
    <table>
        <tr>
        <td class="formCaptionTd" width="150px">Title:
        </td>
        <td class="formFieldTd">
            <asp:TextBox ID="txtitle" runat="server" CssClass="standardField" MaxLength="200" Width="352px" />
            <asp:RequiredFieldValidator ID="rfvfirstname" runat="server" ErrorMessage="Required"
                ControlToValidate="txtitle" Display="None" SetFocusOnError="true" ValidationGroup="asdnotes" />
        </td>
    </tr>
    <tr>
        <td class="formCaptionTd">Note:
        </td>
        <td class="formFieldTd">
            <asp:TextBox ID="txtnote" runat="server" CssClass="standardField" MaxLength="200"
                TextMode="MultiLine" Width="350px" Height="300px" />
            <asp:RequiredFieldValidator ID="rfvlastname" runat="server" ErrorMessage="Required"
                ControlToValidate="txtnote" Display="None" SetFocusOnError="true" ValidationGroup="asdnotes" />
        </td>
    </tr>
    <tr>
        <td class="formCaptionTd">&nbsp;
        </td>
        <td class="formFieldTd">
            <asp:Button ID="btnupdate" runat="server" Text="Save" CssClass="prmLeftPlainLink"
                ToolTip="Save" ValidationGroup="asdnotes" OnClick="btnupdate_Click" />
            <%--<asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="prmLeftPlainLink"
                                    ToolTip="Exit without saving" />--%>
        </td>
    </tr>
    </table>
</div>
<br />
