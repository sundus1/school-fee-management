﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="userprofile.ascx.cs" Inherits="WebApplication1_userprofile" %>
<style type="text/css">
    .auto-style1 {
        height: 20px;
    }
</style>
<%--<script type="text/javascript" src="_assets/jquery-1.9.1.js"></script>--%>
<script type="text/javascript">
    function addAnotherPhone(phone, state) {
        var trphone = document.getElementById(phone);
        if (state == 'add') {
            trphone.style.display = "block";
            trphone.style.display = "";
        }
        else if (state == 'del') {
            delvalue(trphone);
            trphone.style.display = "none";
        }
        else
            return false;
    }

    function addrow(row, count, type) {
        var trrow = document.getElementById(row);
        var tr = "<tr><td class=\"formCaptionTd\">" + type + ":</td><td class=\"formFieldTd\"> <input type=\"text\" id=\"" + type + count + "\" name=\"" + type + count + "\" class=\"standardField\" MaxLength=\"50\" />&nbsp;<a id=\"a" + type + count + "\" href=\"javascript:addrow('" + type + count + "','" + (parseInt(count) + 1) + "','" + type + "');\">Add Row</a>&nbsp;<a id=\"d" + type + count + "\" href=\"javascript:delrow('" + type + count + "','" + type + (parseInt(count) - 1) + "','" + (parseInt(count) - 1) + "','" + type + "');\">Delete Row</a></td></tr>";
        $(trrow).parent().children("a").remove();
        $(trrow).parent().parent().after(tr);
    }

    function delrow(row, prerow, count, type) {
        var trrow = document.getElementById(row);
        var trrow1 = document.getElementById(prerow);
        var tr = "";
        if (prerow.indexOf('0') > 0) {
            trrow1 = document.getElementById(prerow);
            tr = "<a id=\"a" + type + count + "\" href=\"javascript:addrow('" + type + count + "','" + (parseInt(count) + 1) + "','" + type + "');\">Add Row</a>";
        }
        else
            tr = "<a id=\"a" + type + count + "\" href=\"javascript:addrow('" + type + count + "','" + (parseInt(count) + 1) + "','" + type + "');\">Add Row</a>&nbsp;<a id=\"d" + type + count + "\" href=\"javascript:delrow('" + type + count + "','" + type + (parseInt(count) - 1) + "','" + (parseInt(count) - 1) + "','" + type + "');\">Delete Row</a>";
        //$(trrow).parent().children("a").remove();
        $(trrow).parent().parent().remove();
        $(trrow1).parent().append(tr);
    }



    function delvalue(val) {
        $(val).find("input[type=text]").val("")

    }

</script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">        
        <ContentTemplate>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <div style=" text-align:center; position:fixed; top:0px; left:50%;">
                        <img src="images/loading.gif" alt="Loading..." title="Loading..." />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="mainContentDiv">
                <div id="generrortop" runat="server"></div>
                <div class="breadCrumb" id="divgridRelatedMenu" runat="server">
                    <asp:Label ID="lblpage" runat="server" />
                </div>
                <div class="formBodyDiv">
                    <div class="formDiv">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td colspan="2">
                                    <div class="formTitleDiv">Contact Information</div>
                                    <asp:Label runat="server" ID="lblmessage" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr style="display: none">
                                <td>Role</td>
                                <td><asp:DropDownList ID="ddlrole" runat="server">
                                    <asp:ListItem Text="" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Witness" Value="Witness"></asp:ListItem>
                                    <asp:ListItem Text="Contractor" Value="Contractor"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
		                    <tr ID="pg6" runat="server">
			                    <td class="formCaptionTd">
			                        First name:
			                    </td>
			                    <td class="formFieldTd">
			                        <asp:TextBox ID="txtfirstname" runat="server" CssClass="standardField" MaxLength="200" />
                                    <asp:RequiredFieldValidator ID="rfvfirstname" runat="server" 
                                        ErrorMessage="Required" ControlToValidate="txtfirstname" Display="None" 
                                        SetFocusOnError="true" ValidationGroup="asdwer" />
                                    <%--<ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="rfvfirstname" />--%>
			                    </td>
		                    </tr>
		                    <tr ID="pg7" runat="server">
			                    <td class="formCaptionTd">
			                        Last name:
			                    </td>
			                    <td class="formFieldTd">
			                        <asp:TextBox ID="txtlastname" runat="server" CssClass="standardField" MaxLength="200" />
                                    <asp:RequiredFieldValidator ID="rfvlastname" runat="server" 
                                        ErrorMessage="Required" ControlToValidate="txtlastname" Display="None" 
                                        SetFocusOnError="true" ValidationGroup="asdwer" />
                                    <%--<ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" TargetControlID="rfvlastname" />--%>
			                    </td>
		                    </tr>
                            <tr>
                                <td class="formCaptionTd">
                                    Email:
                                </td>
                                <td class="formFieldTd">
                                    <asp:TextBox ID="txtemail" runat="server" CssClass="standardField" MaxLength="50" />
                                </td>
                            </tr>
                            <tr>
                                <td class="formCaptionTd">
                                    Alternate email:
                                </td>
                                <td class="formFieldTd">
                                    <asp:TextBox ID="Alternateemail0" runat="server" CssClass="standardField" MaxLength="50" ClientIDMode="Static" />&nbsp;<a id="aAlternate email0" href="javascript:addrow('<%=Alternateemail0.ClientID %>','1','Alternateemail');">Add Row</a>
                                </td>
                            </tr>
                            <asp:Literal ID="lt_Emails" runat="server"> </asp:Literal>
                            <tr>
                                <td class="formCaptionTd">
                                    Phone:
                                </td>
                                <td class="formFieldTd">
                                    <asp:TextBox ID="Phone0" runat="server" CssClass="standardField" MaxLength="50" ClientIDMode="Static" />&nbsp;<a id="aPhone0" href="javascript:addrow('<%=Phone0.ClientID %>','1','Phone');">Add Row</a>
                                </td>
                            </tr>
                            <asp:Literal ID="lt_phone" runat="server"></asp:Literal>
                            <tr>
                                <td class="formCaptionTd">
                                    Cell phone:
                                </td>
                                <td class="formFieldTd">
                                    <asp:TextBox ID="Cellphone0" runat="server" CssClass="standardField" MaxLength="50" ClientIDMode="Static" />&nbsp;<a id="aCell phone0" href="javascript:addrow('<%=Cellphone0.ClientID %>','1','Cellphone');">Add Row</a>
                                </td>
                            </tr>
                            <asp:Literal ID="lt_cphone" runat="server"></asp:Literal>
                            <tr>
                                <td class="formCaptionTd">
                                    Fax:
                                </td>
                                <td class="formFieldTd">
                                    <asp:TextBox ID="Fax0" runat="server" CssClass="standardField" MaxLength="50"  ClientIDMode="Static" />&nbsp;<a id="aFax0" href="javascript:addrow('<%=Fax0.ClientID %>','1','Fax');">Add Row</a>
                                </td>
                            </tr>
		                    <asp:Literal ID="lt_fax" runat="server"></asp:Literal>
		                    <tr ID="pg12" runat="server" Visible="false">
			                    <td class="auto-style1">Remove Done jobs:</td>
			                    <td class="auto-style1">
                                    <asp:DropDownList ID="ddlremjobs" runat="server">
			                            <asp:ListItem Selected="True" Text="Select" Value="-1"></asp:ListItem>
			                            <asp:ListItem Text="Immediately" Value="0"></asp:ListItem>
			                            <asp:ListItem Text="After 1 day" Value="1"></asp:ListItem>
			                            <asp:ListItem Text="After 2 days" Value="2"></asp:ListItem>
			                            <asp:ListItem Text="After 3 days" Value="3"></asp:ListItem>
			                            <asp:ListItem Text="After 4 days" Value="4"></asp:ListItem>
			                            <asp:ListItem Text="After 5 days" Value="5"></asp:ListItem>
			                            <asp:ListItem Text="After 6 days" Value="6"></asp:ListItem>
			                            <asp:ListItem Text="After 1 Week" Value="7"></asp:ListItem>
			                            <asp:ListItem Text="After 2 Weeks" Value="14"></asp:ListItem>
			                            <asp:ListItem Text="After 3 Weeks" Value="21"></asp:ListItem>
			                            <asp:ListItem Text="After 4 Weeks" Value="28"></asp:ListItem>
			                            <asp:ListItem Text="After 5 Weeks" Value="35"></asp:ListItem>
			                            <asp:ListItem Text="After 6 Weeks" Value="42"></asp:ListItem>
			                            <asp:ListItem Text="After 7 Weeks" Value="49"></asp:ListItem>
			                            <asp:ListItem Text="After 8 Weeks" Value="56"></asp:ListItem>
			                            <asp:ListItem Text="After 9 Weeks" Value="63"></asp:ListItem>
			                            <asp:ListItem Text="After 10 Weeks" Value="70"></asp:ListItem>
			                            <asp:ListItem Text="After 11 Weeks" Value="77"></asp:ListItem>
			                        </asp:DropDownList>
                                </td>
                            </tr>
                            <tr ID="pg13" runat="server" Visible="false">
                                <td class="formCaptionTd">&nbsp;</td>
			                    <td class="formFieldTd">
                                    <asp:CheckBox ID="chbremjobsman" runat="server" Text="I will remove jobs manually." Checked="true" />
                                </td>
                            </tr>
                            <tr>
                                <td class="formCaptionTd">
                                    Address:
                                </td>
                                <td class="formFieldTd">
                                    <asp:TextBox ID="txtaddress" runat="server" TextMode="MultiLine" CssClass="standardField" MaxLength="100" Rows="3" />
                                </td>
                            </tr>
                            <tr>
                                <td class="formCaptionTd">
                                    Zip/Postal Code:
                                </td>
                                <td class="formFieldTd">
                                    <asp:TextBox ID="txtzipcode" runat="server" CssClass="standardField" MaxLength="50" />
                                </td>
                            </tr>
                            <tr>
                                <td class="formCaptionTd">
                                    City:
                                </td>
                                <td class="formFieldTd">
                                    <asp:TextBox ID="txtcity" runat="server" CssClass="standardField" MaxLength="50" />
                                </td>
                            </tr>
                            <tr>
                                <td class="formCaptionTd">
                                    State/Province:
                                </td>
                                <td class="formFieldTd">
                                    <asp:TextBox ID="txtstate" runat="server" CssClass="standardField" MaxLength="50" />
                                </td>
                            </tr>
                           <tr>
                                <td class="formCaptionTd">
                                    Country:
                                </td>
                                <td class="formFieldTd">
                                    <asp:DropDownList ID="ddlcountry" runat="server" CssClass="standardSelect">
                                    </asp:DropDownList>&nbsp;
                                    <a id="a3" href="javascript:addAnotherPhone('<%=tr_add1.ClientID %>','add');">Add address</a>
                                </td>
                            </tr>
                            <tr id="tr_add1"  style="display:none" runat="server">
                                <td colspan="2">
                                    <table>
                                        <tr>
                                            <td class="formCaptionTd">
                                                Address:
                                            </td>
                                            <td class="formFieldTd">
                                                <asp:TextBox ID="txtaddress1" runat="server" TextMode="MultiLine" CssClass="standardField" MaxLength="100" Rows="3" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="formCaptionTd">
                                                Zip/Postal Code:
                                            </td>
                                            <td class="formFieldTd">
                                                <asp:TextBox ID="txtzipcode1" runat="server" CssClass="standardField" MaxLength="50" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="formCaptionTd">
                                                City:
                                            </td>
                                            <td class="formFieldTd">
                                                <asp:TextBox ID="txtcity1" runat="server" CssClass="standardField" MaxLength="50" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="formCaptionTd">
                                                State/Province:
                                            </td>
                                            <td class="formFieldTd">
                                                <asp:TextBox ID="txtstate1" runat="server" CssClass="standardField" MaxLength="50" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="formCaptionTd">
                                                Country:
                                            </td>
                                            <td class="formFieldTd">
                                                <asp:DropDownList ID="ddlcountry1" runat="server" CssClass="standardSelect">
                                                </asp:DropDownList>
                                                &nbsp;
                                                <a id="a5" href="javascript:addAnotherPhone('<%=tr_add2.ClientID %>','add');">Add address</a>&nbsp;
                                                <a id="a7" href="javascript:addAnotherPhone('<%=tr_add1.ClientID %>','del');">Delete</a>
                                            </td>
                                        </tr>
                                        </table>
                                    </td>
                            </tr>
                            <tr  id="tr_add2" style="display:none" runat="server">
                                <td colspan="2">
                                    <table>
                                        <tr>
                                            <td class="formCaptionTd">
                                                Address:
                                            </td>
                                            <td class="formFieldTd">
                                                <asp:TextBox ID="txtaddress2" runat="server" TextMode="MultiLine" CssClass="standardField" MaxLength="100" Rows="3" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="formCaptionTd">
                                                Zip/Postal Code:
                                            </td>
                                            <td class="formFieldTd">
                                                <asp:TextBox ID="txtzipcode2" runat="server" CssClass="standardField" MaxLength="50" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="formCaptionTd">
                                                City:
                                            </td>
                                            <td class="formFieldTd">
                                                <asp:TextBox ID="txtcity2" runat="server" CssClass="standardField" MaxLength="50" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="formCaptionTd">
                                                State/Province:
                                            </td>
                                            <td class="formFieldTd">
                                                <asp:TextBox ID="txtstate2" runat="server" CssClass="standardField" MaxLength="50" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="formCaptionTd">
                                                Country:
                                            </td>
                                            <td class="formFieldTd">
                                                <asp:DropDownList ID="ddlcountry2" runat="server" CssClass="standardSelect">
                                                </asp:DropDownList>&nbsp;
                                                <a id="a6" href="javascript:addAnotherPhone('<%=tr_add2.ClientID %>','del');">Delete</a>
                                            </td>
                                        </tr>
                                        </table>
                                    </td>
                            </tr>
                            <tr ID="pg8" runat="server">
			                    <td class="formCaptionTd">
			                        Gender:
			                    </td>
			                    <td class="formFieldTd">
			                        <asp:DropDownList ID="ddlgender" runat="server">
			                            <asp:ListItem Text="Male" Selected="True"></asp:ListItem>
			                            <asp:ListItem Text="Female"></asp:ListItem>
			                        </asp:DropDownList>
			                    </td>
		                    </tr>
		                    <tr ID="pg9" runat="server">
			                    <td class="formCaptionTd">
			                        Date of Birth:
			                    </td>
			                    <td class="formFieldTd">
			                        <asp:DropDownList ID="ddlmonth" runat="server"></asp:DropDownList>
			                        <asp:DropDownList ID="ddlday" runat="server"></asp:DropDownList>
			                        <asp:DropDownList ID="ddlyear" runat="server"></asp:DropDownList>
                                    <asp:CustomValidator ID="cvdob" runat="server" ErrorMessage="Invalid Day for Selected Month" ControlToValidate="ddlyear" SetFocusOnError="true" Display="None" OnServerValidate="cvdob_ServerValidate" />
                                    <%--<ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="cvdob" />--%>
			                    </td>
		                    </tr>
                            <tr>
                                <td class="formCaptionTd">
                                    Website:
                                </td>
                                <td class="formFieldTd">
                                    <asp:TextBox ID="Website0" runat="server" CssClass="standardField" MaxLength="50" ClientIDMode="Static" />&nbsp;<a id="aWebsite0" href="javascript:addrow('<%=Website0.ClientID %>','1','Website');">Add Row</a>
                                </td>
                            </tr>
                            <asp:Literal ID="lt_web" runat="server"></asp:Literal>
                            <tr ID="Tr1" runat="server">
                                <td colspan="2">
                                    <div class="formTitleDiv">Company Information</div>
                                </td>
                            </tr>  
                            <tr ID="pg10" runat="server">
                                <td class="formCaptionTd">
                                    Industry:
                                </td>
                                <td class="formFieldTd">
                                    <asp:DropDownList ID="ddlindustry" runat="server" CssClass="standardSelect">
                                        <asp:ListItem Value="">Select</asp:ListItem>
                                        <asp:ListItem>Accounting and Auditing Services</asp:ListItem>
                                        <asp:ListItem>Advertising and PR Services</asp:ListItem>
                                        <asp:ListItem>Aerospace and Defense</asp:ListItem>
                                        <asp:ListItem>Agriculture/Forestry/Fishing</asp:ListItem>
                                        <asp:ListItem>Architectural and Design Services</asp:ListItem>
                                        <asp:ListItem>Automotive and Parts Mfg</asp:ListItem>
                                        <asp:ListItem>Automotive Sales and Repair Services</asp:ListItem>
                                        <asp:ListItem>Banking</asp:ListItem>
                                        <asp:ListItem>Biotechnology/Pharmaceuticals</asp:ListItem>
                                        <asp:ListItem>Broadcasting, Music, and Film</asp:ListItem>
                                        <asp:ListItem>Business Services - Other</asp:ListItem>
                                        <asp:ListItem>Chemicals/Petro-Chemicals</asp:ListItem>
                                        <asp:ListItem>Clothing and Textile Manufacturing</asp:ListItem>
                                        <asp:ListItem>Computer Hardware</asp:ListItem>
                                        <asp:ListItem>Computer Software</asp:ListItem>
                                        <asp:ListItem>Computer/IT Services</asp:ListItem>
                                        <asp:ListItem>Construction - Industrial Facilities and Infrastructure</asp:ListItem>
                                        <asp:ListItem>Construction - Residential & Commercial/Office</asp:ListItem>
                                        <asp:ListItem>Consumer Packaged Goods Manufacturing</asp:ListItem>
                                        <asp:ListItem>Education</asp:ListItem>
                                        <asp:ListItem>Electronics, Components, and Semiconductor Mfg</asp:ListItem>
                                        <asp:ListItem>Energy and Utilities</asp:ListItem>
                                        <asp:ListItem>Engineering Services</asp:ListItem>
                                        <asp:ListItem>Entertainment Venues and Theaters</asp:ListItem>
                                        <asp:ListItem>Financial Services</asp:ListItem>
                                        <asp:ListItem>Food and Beverage Production</asp:ListItem>
                                        <asp:ListItem>Government and Military</asp:ListItem>
                                        <asp:ListItem>Healthcare Services</asp:ListItem>
                                        <asp:ListItem>Hotels and Lodging</asp:ListItem>
                                        <asp:ListItem>Insurance</asp:ListItem>
                                        <asp:ListItem>Internet Services</asp:ListItem>
                                        <asp:ListItem>Legal Services</asp:ListItem>
                                        <asp:ListItem>Management Consulting Services</asp:ListItem>
                                        <asp:ListItem>Manufacturing - Other</asp:ListItem>
                                        <asp:ListItem>Marine Mfg & Services</asp:ListItem>
                                        <asp:ListItem>Medical Devices and Supplies</asp:ListItem>
                                        <asp:ListItem>Metals and Minerals</asp:ListItem>
                                        <asp:ListItem>Nonprofit Charitable Organizations</asp:ListItem>
                                        <asp:ListItem>Other/Not Classified</asp:ListItem>
                                        <asp:ListItem>Performing and Fine Arts</asp:ListItem>
                                        <asp:ListItem>Personal and Household Services</asp:ListItem>
                                        <asp:ListItem>Printing and Publishing</asp:ListItem>
                                        <asp:ListItem>Real Estate/Property Management</asp:ListItem>
                                        <asp:ListItem>Rental Services</asp:ListItem>
                                        <asp:ListItem>Restaurant/Food Services</asp:ListItem>
                                        <asp:ListItem>Retail</asp:ListItem>
                                        <asp:ListItem>Security and Surveillance</asp:ListItem>
                                        <asp:ListItem>Sports and Physical Recreation</asp:ListItem>
                                        <asp:ListItem>Staffing/Employment Agencies</asp:ListItem>
                                        <asp:ListItem>Telecommunications Services</asp:ListItem>
                                        <asp:ListItem>Transport and Storage - Materials</asp:ListItem>
                                        <asp:ListItem>Travel, Transportation and Tourism</asp:ListItem>
                                        <asp:ListItem>Waste Management</asp:ListItem>
                                        <asp:ListItem>Wholesale Trade/Import-Export</asp:ListItem>
                                        <asp:ListItem>Other</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr ID="pg11" runat="server">
                                <td class="formCaptionTd">
                                    Organization:
                                </td>
                                <td class="formFieldTd">
                                    <asp:TextBox ID="txtorganization" runat="server" CssClass="standardField" MaxLength="50" />
                                </td>
                            </tr>
                            <tr>
                                <td class="formCaptionTd">
                                    Designation:
                                </td>
                                <td class="formFieldTd">
                                    <asp:TextBox ID="txtdesignation" runat="server" CssClass="standardField" MaxLength="50" />
                                </td>
                            </tr>  
                            <tr>
                                <td class="formCaptionTd">
                                    Company Website:
                                </td>
                                <td class="formFieldTd">
                                    <asp:TextBox ID="Companywebsite0" runat="server" CssClass="standardField" MaxLength="50" ClientIDMode="Static" />&nbsp;<a id="aCompany website0" href="javascript:addrow('<%=Companywebsite0.ClientID %>','1','Companywebsite');">Add Row</a>
                                </td>
                            </tr>
                            <asp:Literal ID="lt_cweb" runat="server"></asp:Literal>                                           
                            <tr>
                                <td class="formCaptionTd">&nbsp;</td>
                                <td class="formFieldTd">
                                    <asp:Button ID="btnupdate" runat="server" Text="Save" 
                                        CssClass="prmLeftPlainLink" OnClick="btnupdate_Click" ToolTip="Save" 
                                        ValidationGroup="asdwer" />
                                    <%--<asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="prmLeftPlainLink" ToolTip="Exit without saving" />--%>
                                </td>
                            </tr>
			            </table>
		            </div>
		        </div>
                <div id="generrorbottom" runat="server"></div>
            </div>
            <asp:HiddenField ID="hfuserid" runat="server" />
            <asp:HiddenField ID="hfoldpassword" runat="server" />
            <asp:HiddenField ID="hfGen" runat="server" />
            <asp:HiddenField ID="hffullname" runat="server" />
            <asp:HiddenField ID="hftimezone" runat="server" />
            <%--<telerik:RadWindow ID="rwusers" runat="server" Modal="True" AutoSize="True" Behaviors="Close" MinHeight="100" MinWidth="500">
                <ContentTemplate>
                    You will not be able to use AVAIMA untill you fill mandatory fields for your profile
                </ContentTemplate>
            </telerik:RadWindow>--%>
        </ContentTemplate>
	</asp:UpdatePanel>
