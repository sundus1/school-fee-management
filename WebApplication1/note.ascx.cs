﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebApplication1_note : System.Web.UI.UserControl
{
    AvaimaTimeZoneAPI current = new AvaimaTimeZoneAPI();
    private string App_id = "85";
    protected void Page_Load(object sender, EventArgs e)
    {
        bind();
    }
    private void bind()
    {
        AvaimaUserProfile obj = new AvaimaUserProfile();
        DataTable dtnote = new DataTable();
        //Note obj = new Note();
        dtnote = obj.GetNote(App_id, Request["id"]);
        if (dtnote.Rows.Count > 0)
        {
            rptnote.DataSource = dtnote;
            rptnote.DataBind();
        }
    }

    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            AvaimaUserProfile obj = new AvaimaUserProfile();
            obj.insertnote(DateTime.Now,  txtitle.Text, txtnote.Text, Request["id"], App_id);
            bind();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "mainscript12", "alert('Record Note Successfully!!!');", true);
            //ShowMessage("Insert Successfully!!!", false);
        }
        catch (Exception ex)
        {
            //ShowMessage(ex.Message, true);
        }
        txtitle.Text = "";
        txtnote.Text = "";
    }

    protected void rptnote_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Time
            object objtime = e.Item.DataItem;
            string curentdate = current.GetDate(DataBinder.Eval(objtime,"Time").ToString(), HttpContext.Current.User.Identity.Name);
            string curenttime = current.GetTime(DataBinder.Eval(objtime, "Time").ToString(), HttpContext.Current.User.Identity.Name);
            ((Label)e.Item.FindControl("lbldate")).Text = curentdate.ToString();
            ((Label)e.Item.FindControl("lbltime")).Text = curenttime.ToString();
        }
    }
}