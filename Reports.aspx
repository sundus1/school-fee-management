﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Reports.aspx.cs" Inherits="Reports" %>

<%@ Register Src="~/WebApplication1/PagerControl.ascx" TagPrefix="uc1" TagName="PagerControl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reports</title>
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <style type="text/css">
        .auto-style1 {
            height: 24px;
            width: 157px;
        }

        .div-stats {
            padding: 5px;
            margin: 5px;
            background: #d2eeff;
            width: 750px;
            height: 50px;
            text-align: center;
        }

        }
    </style>

    <asp:PlaceHolder runat="server" ID="phSearchText">
        <script type="text/javascript">
            function resizeiframepage() {
                var iframe = window.parent.document.getElementById("MainContent_ifapp");
                var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
                if (innerDoc.body.offsetHeight) {
                    iframe.height = innerDoc.body.offsetHeight + 400 + "px";
                }
                else if (iframe.Document && iframe.Document.body.scrollHeight) {
                    iframe.style.height = iframe.Document.body.scrollHeight + "px";
                }
            }
        </script>
    </asp:PlaceHolder>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="upnlattrib" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="mainContentDiv">
                    <div class="breadCrumb">
                        Reports
                    </div>
                    <table class="StudentDetailTable">
                        <tr>
                            <td class="formCaptionTd">Class </td>
                            <td class="formFieldTd">
                                <asp:DropDownList ID="ddlclass" runat="server">
                                    <asp:ListItem Value="0">All</asp:ListItem>
                                </asp:DropDownList>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="ddlclass" ForeColor="Red" ValidationGroup="report" InitialValue="0"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="formCaptionTd">Type </td>
                            <td class="formFieldTd">
                                <asp:DropDownList ID="ddlType" runat="server">
                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                    <asp:ListItem Value="All">All</asp:ListItem>
                                    <asp:ListItem Value="1">Fee Paid Student</asp:ListItem>
                                    <asp:ListItem Value="2">Fee Defaulter Student </asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlType" ErrorMessage="*" ForeColor="Red" ValidationGroup="report" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="formCaptionTd">Report Month
                            </td>
                            <td class="formFieldTd">
                                <asp:DropDownList ID="ddlmonth" runat="server">
                                    <asp:ListItem Value="1">January</asp:ListItem>
                                    <asp:ListItem Value="2">February</asp:ListItem>
                                    <asp:ListItem Value="3">March</asp:ListItem>
                                    <asp:ListItem Value="4">April</asp:ListItem>
                                    <asp:ListItem Value="5">May</asp:ListItem>
                                    <asp:ListItem Value="6">June</asp:ListItem>
                                    <asp:ListItem Value="7">July</asp:ListItem>
                                    <asp:ListItem Value="8">August</asp:ListItem>
                                    <asp:ListItem Value="9">September</asp:ListItem>
                                    <asp:ListItem Value="10">October</asp:ListItem>
                                    <asp:ListItem Value="11">November</asp:ListItem>
                                    <asp:ListItem Value="12">December</asp:ListItem>
                                </asp:DropDownList>&nbsp;
                                <asp:DropDownList ID="ddlyear" runat="server">
                                    <asp:ListItem Value="2015">2015</asp:ListItem>
                                    <asp:ListItem Value="2016">2016</asp:ListItem>
                                    <asp:ListItem Value="2017">2017</asp:ListItem>
                                    <asp:ListItem Value="2018">2018</asp:ListItem>
                                    <asp:ListItem Value="2019">2019</asp:ListItem>
                                    <asp:ListItem Value="2020">2020</asp:ListItem>
                                    <asp:ListItem Value="2021">2021</asp:ListItem>
                                    <asp:ListItem Value="2022">2022</asp:ListItem>
                                    <asp:ListItem Value="2023">2023</asp:ListItem>
                                    <asp:ListItem Value="2024">2024</asp:ListItem>
                                    <asp:ListItem Value="2025">2025</asp:ListItem>
                                    </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="formCaptionTd"></td>
                            <td class="formFieldTd">
                                <asp:Button ID="btnreport" runat="server" Text="Generate Report" CssClass="standardFormButton" OnClick="btnreport_Click" ValidationGroup="report" />
                            </td>
                        </tr>
                        <tr>
                            <td class="formCaptionTd"></td>
                            <td class="formFieldTd">
                                <asp:LinkButton ID="lbtnexport" runat="server" Text="Export To Excel" OnClick="lbtnexport_Click" Visible="false"></asp:LinkButton>
                            </td>
                        </tr>
                        <%--<tr>
                            <td class="formCaptionTd">Student</td>
                            <td class="formFieldTd">
                                <asp:DropDownList ID="ddlstudent" runat="server">
                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlstudent" ErrorMessage="*" ForeColor="Red" ValidationGroup="report"></asp:RequiredFieldValidator>
                            </td>
                        </tr>--%>
                    </table>
                    <div id="div_stats" runat="server" visible="false" class="div-stats">
                        <h2>Fee Statistics</h2>
                        <br>
                        Total Defaulter student:
                        <asp:Label ID="lbldef" runat="server"></asp:Label>&nbsp;|&nbsp;Total Defaulter Amount:
                        <asp:Label ID="lbldeftamount" runat="server"></asp:Label>&nbsp;|&nbsp;
                        Total Paid student:
                        <asp:Label ID="lblpaid" runat="server"></asp:Label>
                        &nbsp;|&nbsp;Total Paid Amount:
                        <asp:Label ID="lblpaidamount" runat="server"></asp:Label>
                    </div>
                    <div>
                        <asp:Repeater ID="reptDefaulter" runat="server" OnItemDataBound="reptDefaulter_ItemDataBound">
                            <HeaderTemplate>
                                <table class="smallGrid" style="width: 760px">
                                    <tr class="smallGridHead">
                                        <td>Student ID</td>
                                        <td>Student Name</td>
                                        <td>Father Name</td>
                                        <td>Class</td>
                                        <td>Amount(Fee)</td>
                                        <td></td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="smallGrid">
                                    <td>
                                        <asp:Label ID="lblSID" runat="server" Text='<%# Eval("studentrollnumber") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblSName" runat="server" Text='<%# Eval("title") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblparent" runat="server" Text='<%# Eval("parentname") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblclass" runat="server" Text='<%# Eval("Class") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblamount" runat="server" Text='<%# Eval("fee") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("Defaulter") %>'></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                    <div style="width: 300px" id="divpaging" runat="server">
                        <uc1:PagerControl runat="server" ID="PagerControl" PageSize="50" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="uprogattrib" runat="server" AssociatedUpdatePanelID="upnlattrib">
            <ProgressTemplate>
                <div style="text-align: center; position: fixed; top: 0px; left: 50%;">
                    <img src="http://www.avaima.com/main/Control_Panel/Assets/Images/loading.gif" alt="Loading..." title="Loading..." />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form>
</body>
</html>
