﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="feeReports.aspx.cs" Inherits="feeReports" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reports</title>
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <link href="_assets/jquery-ui.css" rel="stylesheet" />
    <link href="_assets/jquery.datepick.css" rel="stylesheet" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <script src="_assets/jquery.datepick.js"></script>
    <style type="text/css">
        .auto-style1 {
            height: 24px;
            width: 157px;
        }

        .div-stats {
            padding: 5px;
            margin: 5px;
            background: #d2eeff;
            width: 670px;
            height: 50px;
            text-align: center;
        }

        .feedetail td {
            padding: 6px 20px;
        }


        .feedetail {
            margin: 11px 0 0 121px;
        }
    </style>

   
   
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="upnlattrib" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                 <asp:PlaceHolder runat="server" ID="phSearchText">
        <script type="text/javascript">
            function resizeiframepage() {
                var iframe = window.parent.document.getElementById("MainContent_ifapp");
                var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
                if (innerDoc.body.offsetHeight) {
                    iframe.height = innerDoc.body.offsetHeight + 400 + "px";
                }
                else if (iframe.Document && iframe.Document.body.scrollHeight) {
                    iframe.style.height = iframe.Document.body.scrollHeight + "px";
                }
            }
            Sys.Application.add_load(function () {
                resizeiframepage();

                $("#txtfrom").datepicker({
                    dateFormat: 'dd MM yy',
                    changeMonth: true,
                    changeYear: true,
                    onSelect: function (selected, evnt) {
                        resizeiframepage();
                    }
                });
                $("#txtto").datepicker({
                    dateFormat: 'dd MM yy',
                    changeMonth: true,
                    changeYear: true,
                    onSelect: function (selected, evnt) {
                        resizeiframepage();
                    }
                });

                $("#btnreport").click(function () {
                    var fromDate = new Date($('#txtfrom').val());
                    var toDate = new Date($('#txtto').val());
                    if (toDate < fromDate) {
                        $('#lblerrormsg').text("Error: Range Invalid");
                        return false;
                    }
                    else {

                        $('#lblerrormsg').text("");
                        return true;
                    }
                });
            });
        </script>
        </asp:PlaceHolder>
                <div id="mainContentDiv">
                    <div class="breadCrumb">
                        Fee Collection  Report
                    </div>
                    <table class="StudentDetailTable">
                        <tr>
                            <td class="formCaptionTd">From </td>
                            <td class="formFieldTd">
                                <asp:TextBox ID="txtfrom" runat="server" ClientIDMode="Static" CssClass="standardField"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="formCaptionTd">To </td>
                            <td class="formFieldTd">
                                <asp:TextBox ID="txtto" runat="server" ClientIDMode="Static" CssClass="standardField"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="formCaptionTd"></td>
                            <td class="formFieldTd">
                                <asp:Button ID="btnreport" runat="server" Text="Generate Report" OnClick="btnreport_Click" CssClass="standardFormButton" ValidationGroup="report" ClientIDMode="Static" />
                                <asp:Label ID="lblerrormsg" runat="server" ClientIDMode="Static" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <%--<tr>
                            <td class="formCaptionTd">Student</td>
                            <td class="formFieldTd">
                                <asp:DropDownList ID="ddlstudent" runat="server">
                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlstudent" ErrorMessage="*" ForeColor="Red" ValidationGroup="report"></asp:RequiredFieldValidator>
                            </td>
                        </tr>--%>
                    </table>
                    <div>
                        <asp:Table ID="tbl" runat="server" border="1" CellSpacing="0" class="feedetail">
                        </asp:Table>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="uprogattrib" runat="server" AssociatedUpdatePanelID="upnlattrib">
            <ProgressTemplate>
                <div style="text-align: center; position: fixed; top: 0px; left: 50%;">
                    <img src="http://www.avaima.com/main/Control_Panel/Assets/Images/loading.gif" alt="Loading..." title="Loading..." />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form>
</body>
</html>
