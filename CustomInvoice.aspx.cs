﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CustomInvoice : AvaimaThirdpartyTool.AvaimaWebPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtstartdate.Text = DateTime.Now.ToString("dd-MMMM-yyyy");
            txtenddate.Text = DateTime.Now.ToString("dd-MMMM-yyyy");
            hfeffdate.Value = DateTime.Now.ToString("dd-MMMM-yyyy");
        }
    }
    protected void btngeninvoice_Click(object sender, EventArgs e)
    {
        try
        {
            generateinvoice(Request["stid"]);
            ScriptManager.RegisterStartupScript(this, GetType(), "updatealert", "alert('Invoice Generated sucessfully !')", true);
            tr_note.Visible = true;
            txtfeeamount.Text = "";
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "updatealert", "alert('Error! Try Again')", true);
        }
    }
    private void generateinvoice(string studentid)
    {
        studentinvoice objstudent = new studentinvoice();
        studentparameter objinvoice = new studentparameter();
        objinvoice.Monthrange = Convert.ToDateTime(txtstartdate.Text).ToString("MMM dd, yyyy") + " to " + Convert.ToDateTime(txtenddate.Text).ToString("MMM dd, yyyy");
        objinvoice.TotalPackagefees = Convert.ToDecimal(txtfeeamount.Text);
        objinvoice.Totalcharges = 0;
        objinvoice.Totaldiscount = 0;
        objinvoice.feespaid = 0;
        objinvoice.feesbalance = Convert.ToDecimal(txtfeeamount.Text);
        objinvoice.Totalfees = Convert.ToDecimal(txtfeeamount.Text);
        objinvoice.extracharges = 0;
        objinvoice.extradiscount = 0;
        objinvoice.adjustment = 0;
        objinvoice.studentid = studentid;
        objinvoice.paidcomplete = "0";
        objinvoice.fromdate = Convert.ToDateTime(txtstartdate.Text);
        objinvoice.todate = Convert.ToDateTime(txtenddate.Text);
        int d = objstudent.insertinvoice(objinvoice);
    }
    protected void lbtninvoice_Click(object sender, EventArgs e)
    {
        this.Redirect("Invoice.aspx?studid=" + Request["stid"] + "&PID=" + Request["PID"]);
    }
}