﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class feevouchertemplate : System.Web.UI.UserControl
{   
    studentinvoice objinvoice;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            binddata();
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            string id = "0";
            if (rbtntemp1.Checked)
                id = "1";
            else if (rbtntemp2.Checked)
                id = "2";

            objinvoice = new studentinvoice();
            objinvoice.SavefeeVoucher(Request.QueryString["instanceid"].ToString(), id);
            Divmsg.InnerHtml = "Record Save Successfully!!";
        }
        catch (Exception ex)
        {
        }
    }
    private void binddata()
    {
        objinvoice = new studentinvoice();
        DataTable dt = new DataTable();
        dt = objinvoice.Getfeevouchertemplate(Request.QueryString["instanceid"].ToString());
        if (dt.Rows.Count > 0)
        {
            if (dt.Rows[0]["templateid"].ToString() == "1")
                rbtntemp1.Checked = true;
            else if (dt.Rows[0]["templateid"].ToString() == "2")
                rbtntemp2.Checked = true;
        }
    }
}