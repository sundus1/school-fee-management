﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports : AvaimaThirdpartyTool.AvaimaWebPage
{
    studentinvoice objinvoice;
    decimal deft = 0;
    decimal paid = 0;
    int paidst = 0;
    int deftst = 0;
    DataTable dtrecords = new DataTable();
    PagedDataSource pds = new PagedDataSource();
    protected void Page_Load(object sender, EventArgs e)
    {
        PagerControl.OnPageIndex_Changed += pg_rptrcontract_OnPageIndex_Changed;
        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(this.lbtnexport);
        if(!IsPostBack)
        {            
            PagerControl.CurrentPage = 0;
            bindclass();
            ddlmonth.SelectedValue = DateTime.Now.Month.ToString();
            ddlyear.SelectedValue = DateTime.Now.Year.ToString();
        }
    }
    public void pg_rptrcontract_OnPageIndex_Changed(object sender, EventArgs e)
    {
        clickevent();
    }
    private void bindclass()
    {
        DataTable dtclasses = new DataTable();
        objinvoice = new studentinvoice();
        dtclasses = objinvoice.GetClasses(this.InstanceID);
        if (dtclasses.Rows.Count > 0)
        {
            ddlclass.DataSource = dtclasses;
            ddlclass.DataTextField = "Title";
            ddlclass.DataValueField = "ID";
            ddlclass.DataBind();
            ddlclass.Items.Insert(0, new ListItem("All","0"));
        }
        //DataTable dtDefaulter = new DataTable();
        //dtDefaulter = objinvoice.GetDefaulter(this.InstanceID);
        //if (dtDefaulter.Rows.Count > 0)
        //{
        //    reptDefaulter.DataSource = dtDefaulter;
        //    reptDefaulter.DataBind();
           
        //}
        //else
        //{
        //    reptDefaulter.DataSource = null;
        //    reptDefaulter.DataBind();
            
        //}
    }
    //protected void ddlclass_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    DataTable dtstudent = new DataTable();
    //    objinvoice = new studentinvoice();
    //    dtstudent = objinvoice.GetStudents(ddlclass.SelectedValue);
    //    if (dtstudent.Rows.Count > 0)
    //    {
    //        ddlstudent.DataSource = dtstudent;
    //        ddlstudent.DataTextField = "Title";
    //        ddlstudent.DataValueField = "ID";
    //        ddlstudent.DataBind();
    //        ddlstudent.Items.Insert(0, "Select");
    //    }
    //}
    protected void btnreport_Click(object sender, EventArgs e)
    {
        try
        {
            PagerControl.CurrentPage = 0;
            clickevent();
            ScriptManager.RegisterStartupScript(this, GetType(), "resizeiframepageb", "resizeiframepage();", true);
        }
        catch
        {
        }
    }

    private void clickevent()
    {
        string date = "";
        if (ddlmonth.SelectedValue == "2")
        {
            date = new DateTime(Convert.ToInt32(ddlyear.SelectedValue), Convert.ToInt32(ddlmonth.SelectedValue), 28).ToString();
        }
        else
            date = new DateTime(Convert.ToInt32(ddlyear.SelectedValue), Convert.ToInt32(ddlmonth.SelectedValue), 30).ToString();

        objinvoice = new studentinvoice();
        if (ddlType.SelectedValue == "1")
        {
            dtrecords = objinvoice.GetFeePaid(this.InstanceID, ddlclass.SelectedValue, date);
            bindgrid(dtrecords);
        }
        else if (ddlType.SelectedValue == "2")
        {
            dtrecords = objinvoice.GetDefaulterbyclass(this.InstanceID, ddlclass.SelectedValue, date);
            bindgrid(dtrecords);
        }
        else if (ddlType.SelectedValue == "All")
        {
            dtrecords = objinvoice.GetAllStudent(this.InstanceID, ddlclass.SelectedValue, date);
            bindgrid(dtrecords);
        }
    }

    private void bindgrid(DataTable dtrecords)
    {
        if (dtrecords.Rows.Count > 0)
        {
            pds.AllowPaging = true;
            pds.DataSource = dtrecords.DefaultView;
            pds.CurrentPageIndex = PagerControl.CurrentPage;
            pds.PageSize = PagerControl.PageSize;
            PagerControl.TotalRecords = dtrecords.Rows.Count;

            reptDefaulter.DataSource = pds;
            reptDefaulter.DataBind();
            PagerControl.LoadStatus();
            lblmsg.Text = "";
            div_stats.Visible = true;
            divpaging.Visible = true;
            lbtnexport.Visible = true;
            if (deftst == 0)
            {
                for (int f = 0; f < dtrecords.Rows.Count; f++)
                {
                    if (dtrecords.Rows[f]["Defaulter"].ToString() == "Defaulter")
                    {
                        deft += Convert.ToDecimal(dtrecords.Rows[f]["fee"].ToString());
                        deftst += 1;
                    }
                    else
                    {
                        paid += Convert.ToDecimal(dtrecords.Rows[f]["feespaid"].ToString());
                        paidst += 1;
                    }
                }
                lbldef.Text = deftst.ToString();
                lbldeftamount.Text = deft.ToString();
                lblpaid.Text = paidst.ToString();
                lblpaidamount.Text = paid.ToString();
            }
        }
        else
        {
            reptDefaulter.DataSource = null;
            reptDefaulter.DataBind();
            lblmsg.Text = "No Record Found For Class: " + ddlclass.SelectedItem.Text;
            div_stats.Visible = false;
            divpaging.Visible = false;
            lbtnexport.Visible = false;
        }
    }
    protected void reptDefaulter_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            object container = e.Item.DataItem;
            Label lblamount = e.Item.FindControl("lblamount") as Label;
            Label Label1 = e.Item.FindControl("Label1") as Label;
            if (DataBinder.Eval(container, "Defaulter").ToString() == "Defaulter")
            {
                lblamount.Text = DataBinder.Eval(container, "fee").ToString();
                Label1.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                lblamount.Text = DataBinder.Eval(container, "feespaid").ToString();
                Label1.ForeColor = System.Drawing.Color.Green;
            }
            

        }
    }
    protected void lbtnexport_Click(object sender, EventArgs e)
    {
        //Get the data from database into datatable
        clickevent();
        
        DataTable dt = new DataTable();
        dt.Columns.Add("title", typeof(string));
        dt.Columns.Add("parentname", typeof(string));
        dt.Columns.Add("Class", typeof(string));
        dt.Columns.Add("fee", typeof(decimal));
        dt.Columns.Add("feespaid", typeof(decimal));
        dt.Columns.Add("Defaulter", typeof(string));



        var query = from r in dtrecords.AsEnumerable()
                    
                    let objectArray = new object[]
            {
                r.Field<string>("title"), r.Field<string>("parentname"), r.Field<string>("Class"), r.Field<decimal>("fee"), r.Field<decimal>("feespaid"), r.Field<string>("Defaulter")
            }
                    select objectArray;

        foreach (var array in query)
        {
            dt.Rows.Add(array);
        }


        //Create a dummy GridView
        GridView GridView1 = new GridView();
        GridView1.AllowPaging = false;
        GridView1.DataSource = dt;
        GridView1.DataBind();

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=" + ddlType.SelectedItem.Text + ".xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            GridView1.HeaderRow.BackColor = Color.White;
            foreach (TableCell cell in GridView1.HeaderRow.Cells)
            {
                cell.BackColor = GridView1.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in GridView1.Rows)
            {
                row.BackColor = Color.White;
                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = GridView1.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = GridView1.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            GridView1.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }
}