﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FeesPackage.aspx.cs" Inherits="FeesPackage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <link href="_assets/jquery-ui.css" rel="stylesheet" />
    <link href="_assets/jquery.datepick.css" rel="stylesheet" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <script src="_assets/jquery.datepick.js"></script>
    <script>
        function CheckDetail() {
            if ($("#txtfeeitemtitle").val() != "" && $("#txtfeeitemtitle").val() != "") {
                return true;
            } else {
                return false;
            }
        }

        function opendetailedit() {
            var dlg = $("#diveditdetail").dialog({
                height: 200,
                width: 350,
                show: {
                    height: 100
                    //effect: "blind",
                    //duration: 1000
                },
                modal: true,
                hide: {
                    effect: "blind",
                    //duration: 1000
                }
            });
            resizeiframepage();

            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");

            return false;
        }

        $(document).ready(function () {
            $("#lnkshowhistory").click(function () {
                var dlg = $("#divstudenthistory").dialog({
                    height: 500,
                    width: 400,
                    show: {
                        height: 100
                        //effect: "blind",
                        //duration: 1000
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                        //duration: 1000
                    }
                });
                resizeiframepage();

                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
                //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
            });

            $("#lbtnchanges").click(function () {
                var dlg = $("#div1").dialog({
                    width: 770,
                    show: {
                        height: 100
                        //effect: "blind",
                        //duration: 1000
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                        //duration: 1000
                    }
                });
                resizeiframepage();

                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
                //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
            });
        });
        function Openitemdiv(value) {
            $("#hfflag").val(value);
            var dlg = $("#divpackage").dialog({
                width: 475,
                show: {
                    height: 100
                    //effect: "blind",
                    //duration: 1000
                },
                modal: true,
                hide: {
                    effect: "blind",
                    //duration: 1000
                }
            });
            resizeiframepage();

            dlg.parent().appendTo($("form:first"));
            dlg.parent().css("z-index", "1000");

            return false;
            //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
        }
        $(document).ready(function () {


            $("#lbtnchange").click(function () {
                if ($("#ddltype").val() == "Months") {
                    $("#tr_month").css("display", "none");
                }
                else {
                    $("#tr_month").removeAttr("style");
                }
                var dlg = $("#divfeecycle").dialog({
                    width: 475,
                    show: {
                        //effect: "blind",
                        //duration: 1000
                    },
                    modal: true,
                    hide: {
                        effect: "blind",
                        //duration: 1000
                    }
                });
                resizeiframepage();

                dlg.parent().appendTo($("form:first"));
                dlg.parent().css("z-index", "1000");

                return false;
                //$("#dialog").parent().css("z-index", "1000").appendTo($("form:first"));
            });
            $("#txtstartdate").datepicker({
                dateFormat: 'dd-MM-yy',
                changeMonth: true,
                changeYear: true
            });
            $("#txtannualdate").datepicker({
                dateFormat: 'dd MM',
                changeMonth: true,
                changeYear: true
            });

            $("#txteffecteddate").datepicker({
                dateFormat: 'dd-MM-yy',
                changeMonth: true,
                minDate: $("#hfeffdate").val(),
                changeYear: true
            });

            $("#txtsubfeesstartdate").datepicker({
                dateFormat: 'dd-MM-yy',
                changeMonth: true,
                changeYear: true
            });
            $("#txtsubfeesenddate").datepicker({
                dateFormat: 'dd-MM-yy',
                changeMonth: true,
                changeYear: true
            });
            $("#chksetting").change(function () {
                if (!this.checked) {
                    $("#tr_1").removeAttr("style");
                    $("#tr_2").removeAttr("style");
                    $("#tr_3").removeAttr("style");
                    $("#tr_4").removeAttr("style");
                    $("#tr_5").removeAttr("style");
                }
                else {
                    $("#tr_1").css("display", "none");
                    $("#tr_2").css("display", "none");
                    $("#tr_3").css("display", "none");
                    $("#tr_4").css("display", "none");
                    $("#tr_5").css("display", "none");
                }
                resizeiframepage();
            });
            //$("#txtfcday").datepicker({
            //    changeMonth: true,
            //    changeYear: true
            //});

        });
        //function setdate(format) {
        //    $("#txtfcday").datepicker("option", "dateFormat", format);
        //}
        function Showtxtmsg() {
            var num = $("#ddlnum").val();
            var val = $("#ddltype").val();
            var date = $("#txtstartdate").val();
            var date = date.split('-');
            if (val == "Day") {
                $("#lbltxtmsg").text('Fees will be due after "' + num + ' Days".');
                $("#lbtnchange").text('');
            }
            else if (val == "Weeks") {
                $("#lbltxtmsg").text('Fees will be due after "' + num + '" week.');
                $("#lbtnchange").text('');
                //$("#divchk").css("display", "none");
                //$("#chksetcyle").removeAttr("checked");
                //$("#tr_fcday").css("display", "none");
                //$("#txtfcday").val('');
            }
            else if (val == "Months") {
                $("#lbltxtmsg").text('Fees will be due on "Day ' + date[0] + '" of every month.');
                if ($("#hfpakageid").val() != "")
                    $("#lbtnchange").text('[Change]');
            }
            else if (val == "Years") {
                $("#lbltxtmsg").text('Fees will be due on "' + date[1] + " " + date[0] + '" of every year.');
                if ($("#hfpakageid").val() != "")
                    $("#lbtnchange").text('[Change]');
            }
            else {
                $("#lbltxtmsg").text('');
                $("#lbtnchange").text('');
            }
        }
        function resizeiframepage() {
            var iframe = window.parent.document.getElementById("MainContent_ifapp");
            var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
            if (innerDoc.body.offsetHeight) {
                iframe.height = innerDoc.body.offsetHeight + 400 + "px";
            }
            else if (iframe.Document && iframe.Document.body.scrollHeight) {
                iframe.style.height = iframe.Document.body.scrollHeight + "px";
            }
        }
        function checkdates() {
            Page_ClientValidate();
            if (new Date($("#txtsubfeesenddate").val()) <= new Date($("#txtsubfeesstartdate").val()) || $("#txtsubfeesstartdate").val() == $("#txtsubfeesenddate").val()) {
                alert("End Date Must not be less than Start Date.");
                return false;
            }
        }
    </script>
    <style type="text/css">
        .auto-style3
        {
            width: 227px;
        }

        .auto-style4
        {
            width: 203px;
        }

        .auto-style6
        {
            padding: 6px 11px 6px 50px;
            text-align: right;
            height: 24px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="mainContentDiv">
            <table>
                <tr>
                    <td class="formCaptionTd">
                        <b>
                            <asp:Label ID="lbltitle" runat="server" Text="Create Fee Package"></asp:Label></b>
                    </td>
                    <td class="formFieldTd"></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="formFieldTd"></td>
                </tr>
                <tr>
                    <td class="formCaptionTd">Title</td>
                    <td class="formFieldTd">
                        <asp:HiddenField runat="server" ID="hfpakageid" ClientIDMode="Static" />
                        <input type="text" id="txttitle" runat="server" required class=" standardField" />
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd">Fee Amount</td>
                    <td class="formFieldTd">
                        <input type="text" id="txtfeeamount" runat="server" required class=" standardField" />
                        <asp:HiddenField ID="hffeeamount" runat="server" />
                    </td>
                </tr>

                <tr>
                    <td class="formCaptionTd">Fee Cycle</td>
                    <td class="formFieldTd">
                        <asp:DropDownList runat="server" ID="ddlnum" required ClientIDMode="Static">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                            <asp:ListItem Value="6">6</asp:ListItem>
                            <asp:ListItem Value="7">7</asp:ListItem>
                            <asp:ListItem Value="8">8</asp:ListItem>
                            <asp:ListItem Value="9">9</asp:ListItem>
                            <asp:ListItem Value="10">10</asp:ListItem>
                            <asp:ListItem Value="11">11</asp:ListItem>
                            <asp:ListItem Value="12">12</asp:ListItem>
                            <asp:ListItem Value="13">13</asp:ListItem>
                            <asp:ListItem Value="14">14</asp:ListItem>
                            <asp:ListItem Value="15">15</asp:ListItem>
                            <asp:ListItem Value="16">16</asp:ListItem>
                            <asp:ListItem Value="17">17</asp:ListItem>
                            <asp:ListItem Value="18">18</asp:ListItem>
                            <asp:ListItem Value="19">19</asp:ListItem>
                            <asp:ListItem Value="20">20</asp:ListItem>
                            <asp:ListItem Value="21">21</asp:ListItem>
                            <asp:ListItem Value="22">22</asp:ListItem>
                            <asp:ListItem Value="23">23</asp:ListItem>
                            <asp:ListItem Value="24">24</asp:ListItem>
                            <asp:ListItem Value="25">25</asp:ListItem>
                            <asp:ListItem Value="26">26</asp:ListItem>
                            <asp:ListItem Value="27">27</asp:ListItem>
                            <asp:ListItem Value="28">28</asp:ListItem>
                            <asp:ListItem Value="29">29</asp:ListItem>
                            <asp:ListItem Value="30">30</asp:ListItem>
                        </asp:DropDownList>&nbsp;
                        <asp:DropDownList ID="ddltype" runat="server" required ClientIDMode="Static" onchange="Showtxtmsg()">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="Day">Days</asp:ListItem>
                            <asp:ListItem Value="Weeks">Weeks</asp:ListItem>
                            <asp:ListItem Value="Months">Months</asp:ListItem>
                            <asp:ListItem Value="Years">Years</asp:ListItem>
                        </asp:DropDownList>
                        <%--<div style="display: none" id="divchk" ClientIDMode="Static" runat="server">
                            <asp:CheckBox ID="chksetcyle" runat="server" ClientIDMode="Static" Text="Set Fees Cycle start Date" />
                        </div>--%>
                    </td>
                </tr>
                <tr id="tr_fcday" clientidmode="Static" runat="server">
                    <td class="formCaptionTd"></td>
                    <td class="formFieldTd">
                        <asp:Label ID="lbltxtmsg" runat="server" ClientIDMode="Static"></asp:Label>&nbsp;<asp:LinkButton ID="lbtnchange" runat="server" ClientIDMode="Static"></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd"></td>
                    <td class="formFieldTd">
                        <asp:CheckBox ID="chksetting" runat="server" Text="Use currency, late fees and tax values from Settings" ClientIDMode="Static" />
                    </td>
                </tr>
                <%--Start Setting --%>
                <tr id="tr_1" runat="server" clientidmode="Static">
                    <td class="formCaptionTd">Select Currency</td>
                    <td class="formFieldTd">
                        <asp:DropDownList runat="server" ID="ddlcurrencyselect">
                            <asp:ListItem Value="">Select </asp:ListItem>
                            <asp:ListItem Value="USD">USD - US Dollar</asp:ListItem>
                            <asp:ListItem Value="EUR">EUR - Euro</asp:ListItem>
                            <asp:ListItem Value="GBP">GBP - British Pound</asp:ListItem>
                            <asp:ListItem Value="INR">INR - Indian Rupee</asp:ListItem>
                            <asp:ListItem Value="AUD">AUD - Australian Dollar</asp:ListItem>
                            <asp:ListItem Value="CAD">CAD - Canadian Dollar</asp:ListItem>
                            <asp:ListItem Value="AED">AED - Emirati Dirham</asp:ListItem>
                            <asp:ListItem Value="AED">AED - Emirati Dirham</asp:ListItem>
                            <asp:ListItem Value="AFN">AFN - Afghan Afghani</asp:ListItem>
                            <asp:ListItem Value="ALL">ALL - Albanian Lek</asp:ListItem>
                            <asp:ListItem Value="AMD">AMD - Armenian Dram</asp:ListItem>
                            <asp:ListItem Value="ANG">ANG - Dutch Guilder</asp:ListItem>
                            <asp:ListItem Value="AOA">AOA - Angolan Kwanza</asp:ListItem>
                            <asp:ListItem Value="ARS">ARS - Argentine Peso</asp:ListItem>
                            <asp:ListItem Value="AUD">AUD - Australian Dollar</asp:ListItem>
                            <asp:ListItem Value="AWG">AWG - Aruban or Dutch Guilder</asp:ListItem>
                            <asp:ListItem Value="AZN">AZN - Azerbaijani New Manat</asp:ListItem>
                            <asp:ListItem Value="BAM">BAM - Bosnian Convertible Marka</asp:ListItem>
                            <asp:ListItem Value="BBD">BBD - Barbadian or Bajan Dollar</asp:ListItem>
                            <asp:ListItem Value="BDT">BDT - Bangladeshi Taka</asp:ListItem>
                            <asp:ListItem Value="BGN">BGN - Bulgarian Lev</asp:ListItem>
                            <asp:ListItem Value="BHD">BHD - Bahraini Dinar</asp:ListItem>
                            <asp:ListItem Value="BIF">BIF - Burundian Franc</asp:ListItem>
                            <asp:ListItem Value="BMD">BMD - Bermudian Dollar</asp:ListItem>
                            <asp:ListItem Value="BND">BND - Bruneian Dollar</asp:ListItem>
                            <asp:ListItem Value="BOB">BOB - Bolivian Boliviano</asp:ListItem>
                            <asp:ListItem Value="BRL">BRL - Brazilian Real</asp:ListItem>
                            <asp:ListItem Value="BSD">BSD - Bahamian Dollar</asp:ListItem>
                            <asp:ListItem Value="BTN">BTN - Bhutanese Ngultrum</asp:ListItem>
                            <asp:ListItem Value="BWP">BWP - Botswana Pula</asp:ListItem>
                            <asp:ListItem Value="BYR">BYR - Belarusian Ruble</asp:ListItem>
                            <asp:ListItem Value="BZD">BZD - Belizean Dollar</asp:ListItem>
                            <asp:ListItem Value="CAD">CAD - Canadian Dollar</asp:ListItem>
                            <asp:ListItem Value="CDF">CDF - Congolese Franc</asp:ListItem>
                            <asp:ListItem Value="CHF">CHF - Swiss Franc</asp:ListItem>
                            <asp:ListItem Value="CLP">CLP - Chilean Peso</asp:ListItem>
                            <asp:ListItem Value="CNY">CNY - Chinese Yuan Renminbi</asp:ListItem>
                            <asp:ListItem Value="COP">COP - Colombian Peso</asp:ListItem>
                            <asp:ListItem Value="CRC">CRC - Costa Rican Colon</asp:ListItem>
                            <asp:ListItem Value="CUC">CUC - Cuban Convertible Peso</asp:ListItem>
                            <asp:ListItem Value="CUP">CUP - Cuban Peso</asp:ListItem>
                            <asp:ListItem Value="CVE">CVE - Cape Verdean Escudo</asp:ListItem>
                            <asp:ListItem Value="CZK">CZK - Czech Koruna</asp:ListItem>
                            <asp:ListItem Value="DJF">DJF - Djiboutian Franc</asp:ListItem>
                            <asp:ListItem Value="DKK">DKK - Danish Krone</asp:ListItem>
                            <asp:ListItem Value="DOP">DOP - Dominican Peso</asp:ListItem>
                            <asp:ListItem Value="DZD">DZD - Algerian Dinar</asp:ListItem>
                            <asp:ListItem Value="EGP">EGP - Egyptian Pound</asp:ListItem>
                            <asp:ListItem Value="ERN">ERN - Eritrean Nakfa</asp:ListItem>
                            <asp:ListItem Value="ETB">ETB - Ethiopian Birr</asp:ListItem>
                            <asp:ListItem Value="EUR">EUR - Euro</asp:ListItem>
                            <asp:ListItem Value="FJD">FJD - Fijian Dollar</asp:ListItem>
                            <asp:ListItem Value="FKP">FKP - Falkland Island Pound</asp:ListItem>
                            <asp:ListItem Value="GBP">GBP - British Pound</asp:ListItem>
                            <asp:ListItem Value="GEL">GEL - Georgian Lari</asp:ListItem>
                            <asp:ListItem Value="GGP">GGP - Guernsey Pound</asp:ListItem>
                            <asp:ListItem Value="GHS">GHS - Ghanaian Cedi</asp:ListItem>
                            <asp:ListItem Value="GIP">GIP - Gibraltar Pound</asp:ListItem>
                            <asp:ListItem Value="GMD">GMD - Gambian Dalasi</asp:ListItem>
                            <asp:ListItem Value="GNF">GNF - Guinean Franc</asp:ListItem>
                            <asp:ListItem Value="GTQ">GTQ - Guatemalan Quetzal</asp:ListItem>
                            <asp:ListItem Value="GYD">GYD - Guyanese Dollar</asp:ListItem>
                            <asp:ListItem Value="HKD">HKD - Hong Kong Dollar</asp:ListItem>
                            <asp:ListItem Value="HNL">HNL - Honduran Lempira</asp:ListItem>
                            <asp:ListItem Value="HNL">HNL - Croatian Kuna</asp:ListItem>
                            <asp:ListItem Value="HTG">HTG - Haitian Gourde</asp:ListItem>
                            <asp:ListItem Value="HUF">HUF - Hungarian Forint</asp:ListItem>
                            <asp:ListItem Value="IDR">IDR - Indonesian Rupiah</asp:ListItem>
                            <asp:ListItem Value="ILS">ILS - Israeli Shekel</asp:ListItem>
                            <asp:ListItem Value="IMP">IMP - Isle of Man Pound</asp:ListItem>
                            <asp:ListItem Value="INR">INR - Indian Rupee</asp:ListItem>
                            <asp:ListItem Value="IQD">IQD - Iraqi Dinar</asp:ListItem>
                            <asp:ListItem Value="IRR">IRR - Iranian Rial</asp:ListItem>
                            <asp:ListItem Value="ISK">ISK - Icelandic Krona</asp:ListItem>
                            <asp:ListItem Value="JEP">JEP - Jersey Pound</asp:ListItem>
                            <asp:ListItem Value="JMD">JMD - Jamaican Dollar</asp:ListItem>
                            <asp:ListItem Value="JOD">JOD - Jordanian Dinar</asp:ListItem>
                            <asp:ListItem Value="JPY">JPY - Japanese Yen</asp:ListItem>
                            <asp:ListItem Value="KES">KES - Kenyan Shilling</asp:ListItem>
                            <asp:ListItem Value="KGS">KGS - Kyrgyzstani Som</asp:ListItem>
                            <asp:ListItem Value="KHR">KHR - Cambodian Riel</asp:ListItem>
                            <asp:ListItem Value="KMF">KMF - Comoran Franc</asp:ListItem>
                            <asp:ListItem Value="KPW">KPW - North Korean Won</asp:ListItem>
                            <asp:ListItem Value="KRW">KRW - South Korean Won</asp:ListItem>
                            <asp:ListItem Value="KWD">KWD - Kuwaiti Dinar</asp:ListItem>
                            <asp:ListItem Value="KYD">KYD - Caymanian Dollar</asp:ListItem>
                            <asp:ListItem Value="KZT">KZT - Kazakhstani Tenge</asp:ListItem>
                            <asp:ListItem Value="LAK">LAK - Lao or Laotian Kip</asp:ListItem>
                            <asp:ListItem Value="LBP">LBP - Lebanese Pound</asp:ListItem>
                            <asp:ListItem Value="LKR">LKR - Sri Lankan Rupee</asp:ListItem>
                            <asp:ListItem Value="LRD">LRD - Liberian Dollar</asp:ListItem>
                            <asp:ListItem Value="LSL">LSL - Basotho Loti</asp:ListItem>
                            <asp:ListItem Value="LTL">LTL - Lithuanian Litas</asp:ListItem>
                            <asp:ListItem Value="LVL">LVL - Latvian Lat</asp:ListItem>
                            <asp:ListItem Value="LYD">LYD - Libyan Dinar</asp:ListItem>
                            <asp:ListItem Value="MAD">MAD - Moroccan Dirham</asp:ListItem>
                            <asp:ListItem Value="MDL">MDL - Moldovan Leu</asp:ListItem>
                            <asp:ListItem Value="MGA">MGA - Malagasy Ariary</asp:ListItem>
                            <asp:ListItem Value="MKD">MKD - Macedonian Denar</asp:ListItem>
                            <asp:ListItem Value="MMK">MMK - Burmese Kyat</asp:ListItem>
                            <asp:ListItem Value="MNT">MNT - Mongolian Tughrik</asp:ListItem>
                            <asp:ListItem Value="MOP">MOP - Macau Pataca</asp:ListItem>
                            <asp:ListItem Value="MRO">MRO - Mauritanian Ouguiya</asp:ListItem>
                            <asp:ListItem Value="MUR">MUR - Mauritian Rupee</asp:ListItem>
                            <asp:ListItem Value="MVR">MVR - Maldivian Rufiyaa</asp:ListItem>
                            <asp:ListItem Value="MWK">MWK - Malawian Kwacha</asp:ListItem>
                            <asp:ListItem Value="MXN">MXN - Mexican Peso</asp:ListItem>
                            <asp:ListItem Value="MYR">MYR - Malaysian Ringgit</asp:ListItem>
                            <asp:ListItem Value="MZN">MZN - Mozambican Metical</asp:ListItem>
                            <asp:ListItem Value="NAD">NAD - Namibian Dollar</asp:ListItem>
                            <asp:ListItem Value="NGN">NGN - Nigerian Naira</asp:ListItem>
                            <asp:ListItem Value="NIO">NIO - Nicaraguan Cordoba</asp:ListItem>
                            <asp:ListItem Value="NOK">NOK - Norwegian Krone</asp:ListItem>
                            <asp:ListItem Value="NPR">NPR - Nepalese Rupee</asp:ListItem>
                            <asp:ListItem Value="NZD">NZD - New Zealand Dollar</asp:ListItem>
                            <asp:ListItem Value="OMR">OMR - Omani Rial</asp:ListItem>
                            <asp:ListItem Value="PAB">PAB - Panamanian Balboa</asp:ListItem>
                            <asp:ListItem Value="PEN">PEN - Peruvian Nuevo Sol</asp:ListItem>
                            <asp:ListItem Value="PGK">PGK - Papua New Guinean Kina</asp:ListItem>
                            <asp:ListItem Value="PHP">PHP - Philippine Peso</asp:ListItem>
                            <asp:ListItem Value="PKR">PKR - Pakistani Rupee</asp:ListItem>
                            <asp:ListItem Value="PLN">PLN - Polish Zloty</asp:ListItem>
                            <asp:ListItem Value="PYG">PYG - Paraguayan Guarani</asp:ListItem>
                            <asp:ListItem Value="QAR">QAR - Qatari Riyal</asp:ListItem>
                            <asp:ListItem Value="RON">RON - Romanian New Leu</asp:ListItem>
                            <asp:ListItem Value="RSD">RSD - Serbian Dinar</asp:ListItem>
                            <asp:ListItem Value="RUB">RUB - Russian Ruble</asp:ListItem>
                            <asp:ListItem Value="RWF">RWF - Rwandan Franc</asp:ListItem>
                            <asp:ListItem Value="SAR">SAR - Saudi Arabian Riyal</asp:ListItem>
                            <asp:ListItem Value="SBD">SBD - Solomon Islander Dollar</asp:ListItem>
                            <asp:ListItem Value="SCR">SCR - Seychellois Rupee</asp:ListItem>
                            <asp:ListItem Value="SDG">SDG - Sudanese Pound</asp:ListItem>
                            <asp:ListItem Value="SEK">SEK - Swedish Krona</asp:ListItem>
                            <asp:ListItem Value="SEK">SEK - Singapore Dollar</asp:ListItem>
                            <asp:ListItem Value="SHP">SHP - Saint Helenian Pound</asp:ListItem>
                            <asp:ListItem Value="SLL">SLL - Sierra Leonean Leone</asp:ListItem>
                            <asp:ListItem Value="SOS">SOS - Somali Shilling</asp:ListItem>
                            <asp:ListItem Value="SPL">SPL - Seborgan Luigino</asp:ListItem>
                            <asp:ListItem Value="SRD">SRD - Surinamese Dollar</asp:ListItem>
                            <asp:ListItem Value="STD">STD - Sao Tomean Dobra</asp:ListItem>
                            <asp:ListItem Value="SVC">SVC - Salvadoran Colon</asp:ListItem>
                            <asp:ListItem Value="SYP">SYP - Syrian Pound</asp:ListItem>
                            <asp:ListItem Value="SZL">SZL - Swazi Lilangeni</asp:ListItem>
                            <asp:ListItem Value="THB">THB - Thai Baht</asp:ListItem>
                            <asp:ListItem Value="TJS">TJS - Tajikistani Somoni</asp:ListItem>
                            <asp:ListItem Value="TMT">TMT - Turkmenistani Manat</asp:ListItem>
                            <asp:ListItem Value="TND">TND - Tunisian Dinar</asp:ListItem>
                            <asp:ListItem Value="TOP">TOP - Tongan Pa'anga</asp:ListItem>
                            <asp:ListItem Value="TRY">TRY - Turkish Lira</asp:ListItem>
                            <asp:ListItem Value="TTD">TTD - Trinidadian Dollar</asp:ListItem>
                            <asp:ListItem Value="TVD">TVD - Tuvaluan Dollar</asp:ListItem>
                            <asp:ListItem Value="TWD">TWD - Taiwan New Dollar</asp:ListItem>
                            <asp:ListItem Value="TZS">TZS - Tanzanian Shilling</asp:ListItem>
                            <asp:ListItem Value="UAH">UAH - Ukrainian Hryvna</asp:ListItem>
                            <asp:ListItem Value="UGX">UGX - Ugandan Shilling</asp:ListItem>
                            <asp:ListItem Value="USD">USD - US Dollar</asp:ListItem>
                            <asp:ListItem Value="UYU">UYU - Uruguayan Peso</asp:ListItem>
                            <asp:ListItem Value="UZS">UZS - Uzbekistani Som</asp:ListItem>
                            <asp:ListItem Value="VEF">VEF - Venezuelan Bolivar</asp:ListItem>
                            <asp:ListItem Value="VND">VND - Vietnamese Dong</asp:ListItem>
                            <asp:ListItem Value="VUV">VUV - Ni-Vanuatu Vatu</asp:ListItem>
                            <asp:ListItem Value="WST">WST - Samoan Tala</asp:ListItem>
                            <asp:ListItem Value="XAF">XAF - Central African CFA Franc BEAC</asp:ListItem>
                            <asp:ListItem Value="XAG">XAG - Silver Ounce</asp:ListItem>
                            <asp:ListItem Value="XAU">XAU - Gold Ounce</asp:ListItem>
                            <asp:ListItem Value="XBT">XBT - Bitcoin</asp:ListItem>
                            <asp:ListItem Value="XCD">XCD - East Caribbean Dollar</asp:ListItem>
                            <asp:ListItem Value="XDR">XDR - IMF Special Drawing Rights</asp:ListItem>
                            <asp:ListItem Value="XOF">XOF - CFA Franc</asp:ListItem>
                            <asp:ListItem Value="XPD">XPD - Palladium Ounce</asp:ListItem>
                            <asp:ListItem Value="XPF">XPF - CFP Franc</asp:ListItem>
                            <asp:ListItem Value="XPT">XPT - Platinum Ounce</asp:ListItem>
                            <asp:ListItem Value="YER">YER - Yemeni Rial</asp:ListItem>
                            <asp:ListItem Value="ZAR">ZAR - South African Rand</asp:ListItem>
                            <asp:ListItem Value="ZMW">ZMW - Zambian Kwacha</asp:ListItem>
                            <asp:ListItem Value="ZWD">ZWD - Zimbabwean Dollar</asp:ListItem>

                        </asp:DropDownList>

                    </td>

                </tr>

                <tr id="tr_2" runat="server" clientidmode="Static">
                    <td class="formCaptionTd">Late Fee Charges</td>
                    <td class="formFieldTd">
                        <input type="text" id="txtlatefee" runat="server" class=" standardField" value="0" />
                        <asp:HiddenField ID="hflatefee" runat="server" />
                    </td>
                </tr>

                <tr id="tr_3" runat="server" clientidmode="Static">
                    <td class="formCaptionTd">Fees Due</td>
                    <td class="formFieldTd">
                        <asp:DropDownList runat="server" ID="ddlddate">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                            <asp:ListItem Value="6">6</asp:ListItem>
                            <asp:ListItem Value="7">7</asp:ListItem>
                            <asp:ListItem Value="8">8</asp:ListItem>
                            <asp:ListItem Value="9">9</asp:ListItem>
                            <asp:ListItem Value="10">10</asp:ListItem>
                            <asp:ListItem Value="11">11</asp:ListItem>
                            <asp:ListItem Value="12">12</asp:ListItem>
                            <asp:ListItem Value="13">13</asp:ListItem>
                            <asp:ListItem Value="14">14</asp:ListItem>
                            <asp:ListItem Value="15">15</asp:ListItem>
                            <asp:ListItem Value="16">16</asp:ListItem>
                            <asp:ListItem Value="17">17</asp:ListItem>
                            <asp:ListItem Value="18">18</asp:ListItem>
                            <asp:ListItem Value="19">19</asp:ListItem>
                            <asp:ListItem Value="20">20</asp:ListItem>
                            <asp:ListItem Value="21">21</asp:ListItem>
                            <asp:ListItem Value="22">22</asp:ListItem>
                            <asp:ListItem Value="23">23</asp:ListItem>
                            <asp:ListItem Value="24">24</asp:ListItem>
                            <asp:ListItem Value="25">25</asp:ListItem>
                            <asp:ListItem Value="26">26</asp:ListItem>
                            <asp:ListItem Value="27">27</asp:ListItem>
                            <asp:ListItem Value="28">28</asp:ListItem>
                            <asp:ListItem Value="29">29</asp:ListItem>
                            <asp:ListItem Value="30">30</asp:ListItem>
                            <asp:ListItem Value="31">31</asp:ListItem>
                        </asp:DropDownList>

                        &nbsp;
                        <asp:DropDownList ID="ddlsdays" runat="server" ClientIDMode="Static">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="Day">Days</asp:ListItem>
                            <asp:ListItem Value="Weeks">Weeks</asp:ListItem>
                            <asp:ListItem Value="Months">Months</asp:ListItem>
                            <asp:ListItem Value="Years">Years</asp:ListItem>
                        </asp:DropDownList>
                        &nbsp;
                        After Package Start Date.
                    </td>

                </tr>

                <tr id="tr_4" runat="server" clientidmode="Static">
                    <td class="formCaptionTd">Late Fees Start</td>
                    <td class="formFieldTd">
                        <asp:DropDownList runat="server" ID="ddldsdate">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                            <asp:ListItem Value="6">6</asp:ListItem>
                            <asp:ListItem Value="7">7</asp:ListItem>
                            <asp:ListItem Value="8">8</asp:ListItem>
                            <asp:ListItem Value="9">9</asp:ListItem>
                            <asp:ListItem Value="10">10</asp:ListItem>
                            <asp:ListItem Value="11">11</asp:ListItem>
                            <asp:ListItem Value="12">12</asp:ListItem>
                            <asp:ListItem Value="13">13</asp:ListItem>
                            <asp:ListItem Value="14">14</asp:ListItem>
                            <asp:ListItem Value="15">15</asp:ListItem>
                            <asp:ListItem Value="16">16</asp:ListItem>
                            <asp:ListItem Value="17">17</asp:ListItem>
                            <asp:ListItem Value="18">18</asp:ListItem>
                            <asp:ListItem Value="19">19</asp:ListItem>
                            <asp:ListItem Value="20">20</asp:ListItem>
                            <asp:ListItem Value="21">21</asp:ListItem>
                            <asp:ListItem Value="22">22</asp:ListItem>
                            <asp:ListItem Value="23">23</asp:ListItem>
                            <asp:ListItem Value="24">24</asp:ListItem>
                            <asp:ListItem Value="25">25</asp:ListItem>
                            <asp:ListItem Value="26">26</asp:ListItem>
                            <asp:ListItem Value="27">27</asp:ListItem>
                            <asp:ListItem Value="28">28</asp:ListItem>
                            <asp:ListItem Value="29">29</asp:ListItem>
                            <asp:ListItem Value="30">30</asp:ListItem>
                            <asp:ListItem Value="31">31</asp:ListItem>
                        </asp:DropDownList>
                        &nbsp;
                        <asp:DropDownList ID="ddlstype" runat="server" ClientIDMode="Static">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="Day">Days</asp:ListItem>
                            <asp:ListItem Value="Weeks">Weeks</asp:ListItem>
                            <asp:ListItem Value="Months">Months</asp:ListItem>
                            <asp:ListItem Value="Years">Years</asp:ListItem>
                        </asp:DropDownList>
                        &nbsp;
                        After Due Date.
                    </td>

                </tr>

                <tr id="tr_5" runat="server" clientidmode="Static">
                    <td class="formCaptionTd">Tax &nbsp;
                <asp:CheckBox runat="server" ID="chktax" Style="float: right" AutoPostBack="True" OnCheckedChanged="chktax_CheckedChanged" />
                    </td>
                    <td class="formFieldTd">
                        <asp:TextBox runat="server" ID="txttax" CssClass="standardField tax"></asp:TextBox>&nbsp;&nbsp;
                        <asp:DropDownList runat="server" ID="ddltaxtype" CssClass="tax">
                            <asp:ListItem Value="0" Text="% Age"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Fixed"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:HiddenField ID="hftxttax" runat="server" />
                    </td>

                </tr>
                <%--End Date --%>
                <tr id="trEffecteddate" runat="server" visible="false">
                    <td class="auto-style6">Changes Effected From:
                    </td>
                    <td class="formFieldTd">
                        <asp:TextBox ID="txteffecteddate" runat="server" CssClass="standardField" ClientIDMode="Static" required></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Invalid" ControlToValidate="txteffecteddate" ForeColor="Red" ValidationExpression="^[\d]{1,2}-[\w]+-[\d]{4}$" ValidationGroup="asds"></asp:RegularExpressionValidator><br />
                        <asp:LinkButton ID="lbtnchanges" runat="server" Text="Show Package Changes" ClientIDMode="Static"></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd"></td>
                    <td class="formFieldTd">
                        <asp:Button runat="server" ID="btnsavepakage" Text="Save Package" OnClick="btnsavepakage_clicks" CssClass="standardFormButton" ValidationGroup="asds" />&nbsp;
                        <asp:Button runat="server" ID="btnsavegoback" Text="Save and Go Back" CssClass="standardFormButton" OnClick="btnsavegoback_Click" />
                        <asp:HiddenField ID="hfeffdate" runat="server" ClientIDMode="Static" />
                    </td>
                </tr>

            </table>

            <div class="formTitleDiv" runat="server" id="dicCharges">Aditional Charges</div>
            <div>
                <table>
                    <tr id="tr_addcharges" runat="server">
                        <td class="formCaptionTd">Additional Charges</td>
                        <td class="formFieldTd textOnly">
                            <asp:LinkButton ID="lbtnadditem" runat="server" Text="Add Items" ClientIDMode="Static" OnClientClick="Openitemdiv('0');return false"></asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd"></td>

                        <td class="formFieldTd">
                            <asp:Repeater ID="rptrfeedetail" runat="server" OnItemCommand="rptrfeedetail_ItemCommand" OnItemDataBound="rptrfeedetail_ItemDataBound">
                                <HeaderTemplate>
                                    <table class="smallGrid">
                                        <tr class="smallGridHead">
                                            <td width="125px">Title
                                            </td>
                                            <td>Amount</td>
                                            <td width="125px">Modified</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="smallGrid">
                                        <td>
                                            <asp:Label ID="lblgridtitle" Text='<%# Eval("DetaiilTitle") %>' runat="server"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblgridamount" Text='<%# Eval("DetailAmount") %>' runat="server"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblgriddate" runat="server"></asp:Label></td>
                                        <td>
                                            <asp:LinkButton ID="lnkedit" CommandName="edit" Text="Edit" CommandArgument='<%# Eval("DID") %>' runat="server"></asp:LinkButton></td>
                                        <td>
                                            <asp:LinkButton ID="lnkdelete" CommandName="delete" OnClientClick="return confirm('Are You sure you want to delete')" Text="Delete" CommandArgument='<%# Eval("DID") %>' runat="server"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:LinkButton runat="server" ID="lnkshowhistory" Text=""></asp:LinkButton>
                        </td>

                    </tr>

                </table>
            </div>
            <div class="formTitleDiv" runat="server" id="dicdiscount">Discount</div>
            <div>
                <table>
                    <tr id="tr1" runat="server">
                        <td class="formCaptionTd">Additional Discount</td>
                        <td class="formFieldTd textOnly">
                            <asp:LinkButton ID="lbtndiscount" runat="server" Text="Add Items" ClientIDMode="Static" OnClientClick="Openitemdiv('1');return false"></asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd"></td>

                        <td class="formFieldTd">
                            <asp:Repeater ID="reptdiscount" runat="server" OnItemCommand="rptrfeedetail_ItemCommand" OnItemDataBound="rptrfeedetail_ItemDataBound">
                                <HeaderTemplate>
                                    <table class="smallGrid">
                                        <tr class="smallGridHead">
                                            <td width="125px">Title
                                            </td>
                                            <td>Amount</td>
                                            <td width="125px">Modified</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="smallGrid">
                                        <td>
                                            <asp:Label ID="lblgridtitle" Text='<%# Eval("DetaiilTitle") %>' runat="server"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblgridamount" Text='<%# Eval("DetailAmount") %>' runat="server"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblgriddate" runat="server"></asp:Label></td>
                                        <td>
                                            <asp:LinkButton ID="lnkedit" CommandName="edit" Text="Edit" CommandArgument='<%# Eval("DID") %>' runat="server"></asp:LinkButton></td>
                                        <td>
                                            <asp:LinkButton ID="lnkdelete" CommandName="delete" OnClientClick="return confirm('Are You sure you want to delete')" Text="Delete" CommandArgument='<%# Eval("DID") %>' runat="server"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:LinkButton runat="server" ID="LinkButton1" Text=""></asp:LinkButton>
                        </td>

                    </tr>
                </table>
            </div>
            <div class="formTitleDiv" runat="server" id="divsubfeesheading">Holiday Package</div>
            <div id="divsubfees" runat="server">
                <table>
                    <tr>
                        <td class="formCaptionTd">Holiday Start Date </td>
                        <td class="formFieldTd">
                            <asp:TextBox ID="txtsubfeesstartdate" runat="server" CssClass="standardField" ClientIDMode="Static"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ControlToValidate="txtsubfeesstartdate" ForeColor="Red" ValidationGroup="sub"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Holiday End Date </td>
                        <td class="formFieldTd">
                            <asp:TextBox ID="txtsubfeesenddate" runat="server" CssClass="standardField" ClientIDMode="Static"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ControlToValidate="txtsubfeesenddate" ForeColor="Red" ValidationGroup="sub"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Fees</td>
                        <td class="formFieldTd">
                            <asp:TextBox ID="txtsubfees" runat="server" CssClass="standardField"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="txtsubfees" ForeColor="Red" ValidationGroup="sub"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid" ControlToValidate="txtsubfees" ForeColor="Red" ValidationExpression="^[0-9]+$" ValidationGroup="sub"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Comments </td>
                        <td class="formFieldTd">
                            <asp:TextBox ID="txtsubfeescomments" runat="server" CssClass="standardField" TextMode="MultiLine" Rows="3"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" ControlToValidate="txtsubfeescomments" ForeColor="Red" ValidationGroup="sub"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">
                            <asp:Button ID="btnsubfees" runat="server" Text="Save" CssClass="standardFormButton" ValidationGroup="sub" OnClientClick="return checkdates()" OnClick="btnsubfees_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd"></td>

                        <td class="formFieldTd">
                            <asp:Repeater ID="reptholidaypackage" runat="server" OnItemCommand="reptholidaypackage_ItemCommand">
                                <HeaderTemplate>
                                    <table class="smallGrid">
                                        <tr class="smallGridHead">
                                            <td>Fees
                                            </td>
                                            <td>From</td>
                                            <td>TO</td>
                                            <td></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="smallGrid">
                                        <td>
                                            <asp:Label ID="lblgridtitle" Text='<%# Eval("fees") %>' runat="server"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblgridamount" Text='<%# Eval("startdate") %>' runat="server"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblgriddate" Text='<%# Eval("enddate") %>' runat="server"></asp:Label></td>
                                        <td>
                                            <asp:LinkButton ID="lnkdelete" CommandName="delete" OnClientClick="return confirm('Are You sure you want to delete')" Text="Delete" CommandArgument='<%# Eval("recid") %>' runat="server"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>

                    </tr>
                </table>
            </div>
            <asp:HiddenField ID="hfflag" runat="server" ClientIDMode="Static" />

            <div class="formTitleDiv" runat="server" id="divanualfeeheading">Annual Fees (Annual Charges)</div>
            <div id="divanualfee" runat="server">
                <table>
                    <tr>
                        <td class="formCaptionTd">Title</td>
                        <td class="formFieldTd">
                            <asp:TextBox ID="txtpacktitle" runat="server" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Fees(Charges) </td>
                        <td class="formFieldTd">
                            <asp:TextBox ID="txtannualfee" runat="server" CssClass="standardField" ClientIDMode="Static"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*" ControlToValidate="txtannualfee" ForeColor="Red" ValidationGroup="subannul"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtannualfee" ErrorMessage="Invalid" ForeColor="Red" ValidationGroup="subannul" ValidationExpression="^(\d{1,18})(.\d{0,2})?$"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Select Date </td>
                        <td class="formFieldTd">
                            <asp:TextBox ID="txtannualdate" runat="server" CssClass="standardField" ClientIDMode="Static"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*" ControlToValidate="txtannualdate" ForeColor="Red" ValidationGroup="subannul"></asp:RequiredFieldValidator><br />
                            <b>Note:</b> This fee is charges on select date every year.
                        </td>
                    </tr>
                     <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">
                            <asp:Button ID="btnannual" runat="server" Text="Save" CssClass="standardFormButton" ValidationGroup="subannul" OnClick="btnannual_Click"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd"></td>

                        <td class="formFieldTd">
                            <asp:Repeater ID="Reptannual" runat="server" OnItemCommand="Reptannual_ItemCommand">
                                <HeaderTemplate>
                                    <table class="smallGrid">
                                        <tr class="smallGridHead">
                                            <td>Title</td>
                                            <td>Fees
                                            </td>
                                            <td>Month of Year</td>
                                            <td></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="smallGrid">
                                        <td>
                                            <asp:Label ID="Label4" Text='<%# String.IsNullOrEmpty(Eval("title").ToString()) ? "Annual Fee" : Eval("title") %>' runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblgridtitle" Text='<%# Eval("fee") %>' runat="server"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblgridamount" Text='<%# Eval("Month") %>' runat="server"></asp:Label></td>
                                        <td>
                                            <asp:LinkButton ID="lnkdelete" CommandName="delete" OnClientClick="return confirm('Are You sure you want to delete')" Text="Delete" CommandArgument='<%# Eval("recid") %>' runat="server"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>

                    </tr>
                </table>
            </div>

            <div style="display: none">
                Package Detail:<br />
                <asp:Repeater ID="reptdetail" runat="server" OnItemDataBound="reptdetail_ItemDataBound">
                    <HeaderTemplate>
                        <table class="smallGrid">
                            <tr class="smallGridHead">
                                <td>Title</td>
                                <td>Fees</td>
                                <td>Late Fees</td>
                                <td>Additional Charges</td>
                                <td>Total</td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="lbltitle" runat="server" Text='<%# Eval("Title") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblfees" runat="server" Text='<%# Eval("Amount") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblLateFeeCharges" runat="server" Text='<%# Eval("LateFeeCharges") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbladditioncharges" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblTotalamount" runat="server" Text='<%# Eval("Totalamount") %>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <div id="divpackage" title="Package" style="display: none">
                <asp:Panel ID="Panel1" DefaultButton="btnsumbit" runat="server">
                    <table>
                        <tr>
                            <td>
                                <b>
                                    <asp:Label runat="server" Text="Add Fees Item" ID="lblfeesitem"></asp:Label></b></td>
                            <td class="auto-style4"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Title</td>
                            <td>Amount</td>
                        </tr>
                        <tr>
                            <td class="auto-style3">
                                <input type="text" id="txtfeeitemtitle" runat="server" class=" standardField" /></td>
                            <td class="auto-style4">
                                <input style="float: left" type="text" id="txtfeeitemamount" runat="server" class=" standardField" /></td>
                            <td>
                                <asp:Button runat="server" Text="Add" ID="btnsumbit" OnClientClick="return CheckDetail()" OnClick="btnsumbit_Click" /></td>

                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <div id="diveditdetail" title="Edit Detail" style="display: none">
                <table>
                    <tr>
                        <asp:HiddenField runat="server" ID="hfdetaileditid" />
                        <asp:HiddenField runat="server" ID="hfrecamount" />
                        <td>Title : 
                        </td>
                        <td>
                            <asp:TextBox runat="server" CssClass="standardField" ID="txtedittitle"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Amount : </td>
                        <td>
                            <asp:TextBox runat="server" CssClass="standardField" ID="txteditamount"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button runat="server" ID="btnsaveedit" Text=" Update " OnClick="btnsaveedit_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="div1" style="display: none" title="Package Change History">
                <asp:Repeater ID="reptpackhistory" runat="server" OnItemCommand="reptpackhistory_ItemCommand" OnItemDataBound="reptpackhistory_ItemDataBound">
                    <HeaderTemplate>
                        <table class="smallGrid">
                            <tr class="smallGridHead">
                                <td>Start Date</td>
                                <td>End Date</td>
                                <td>Total  Charges</td>
                                <td>Total Discount</td>
                                <td>Total Amount</td>
                                <td>Update By</td>
                                <td></td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="lblsdate" runat="server" Text='<%# Convert.ToDateTime(Eval("startday").ToString()).ToString("dd-MMMM-yyyy") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbledate" runat="server" Text='<%# Convert.ToDateTime(Eval("enddate").ToString()).ToString("dd-MMMM-yyyy") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Totalcharges") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Totaldiscount") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Totalamount") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblupdateby" runat="server"></asp:Label>
                                <asp:HiddenField ID="hfuserid" runat="server" Value='<%# Eval("userid") %>' />
                            </td>
                            <td>
                                <asp:LinkButton ID="lbtndel" runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%# Eval("recid") %>'></asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>


            <div id="divstudenthistory" style="display: none" title="Package History">
                <asp:Repeater ID="rptrhistory" runat="server" OnItemDataBound="rptrhistory_itemdatabound">
                    <HeaderTemplate>
                        <table class="smallGrid">
                            <tr class="smallGridHead">
                                <td>Message</td>
                                <td>DateTime</td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="smallGrid" style="width: 200px">
                            <td>
                                <asp:Label ID="lblgridisfixed" Text='<%# Eval("Message") %>' runat="server"></asp:Label></td>
                            <td>
                                <asp:Label ID="lblgridamount" Text='<%# Eval("DateTime") %>' runat="server"></asp:Label></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <div id="divfeecycle" style="display: none" title="Fees Cycle Start">
                <p>Use this Form to Set a Date on which the cycle will repeat.</p>
                <br />
                <table>
                    <tr>
                        <td class="formCaptionTd">Start Date</td>
                        <td class="formFieldTd">
                            <asp:TextBox ID="txtstartdate" runat="server" ClientIDMode="Static" class=" standardField" onchange="Showtxtmsg()"></asp:TextBox>
                        </td>
                    </tr>
                    <%--<tr>
                        <td class="formCaptionTd">Repeat Date
                        </td>
                        <td class="formFieldTd">
                            <asp:DropDownList ID="ddldays" runat="server">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                                <asp:ListItem Value="11">11</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="13">13</asp:ListItem>
                                <asp:ListItem Value="14">14</asp:ListItem>
                                <asp:ListItem Value="15">15</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="17">17</asp:ListItem>
                                <asp:ListItem Value="18">18</asp:ListItem>
                                <asp:ListItem Value="19">19</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="21">21</asp:ListItem>
                                <asp:ListItem Value="22">22</asp:ListItem>
                                <asp:ListItem Value="23">23</asp:ListItem>
                                <asp:ListItem Value="24">24</asp:ListItem>
                                <asp:ListItem Value="25">25</asp:ListItem>
                                <asp:ListItem Value="26">26</asp:ListItem>
                                <asp:ListItem Value="27">27</asp:ListItem>
                                <asp:ListItem Value="28">28</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>--%>
                    <%--<tr id="tr_month" runat="server" ClientIDMode="Static">
                        <td class="formCaptionTd">Repeat Month
                        </td>
                        <td class="formFieldTd">
                            <asp:DropDownList ID="ddlmonth" runat="server"  >
                                <asp:ListItem Value="0">Select</asp:ListItem>
                                <asp:ListItem Value="1">January</asp:ListItem>
                                <asp:ListItem Value="2">February</asp:ListItem>
                                <asp:ListItem Value="3">March</asp:ListItem>
                                <asp:ListItem Value="4">April</asp:ListItem>
                                <asp:ListItem Value="5">May</asp:ListItem>
                                <asp:ListItem Value="6">June</asp:ListItem>
                                <asp:ListItem Value="7">July</asp:ListItem>
                                <asp:ListItem Value="8">August</asp:ListItem>
                                <asp:ListItem Value="9">September</asp:ListItem>
                                <asp:ListItem Value="10">October</asp:ListItem>
                                <asp:ListItem Value="11">November</asp:ListItem>
                                <asp:ListItem Value="12">december</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">
                            <asp:Button ID="btndatesave" runat="server" Text="Save" CssClass="standardFormButton" OnClick="btndatesave_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
