﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InvoiceSetting.aspx.cs" Inherits="InvoiceSetting" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div id="mainContentDiv">
            <div class="breadCrumb">
                Invoice Setting
            </div>
            <table class="StudentDetailTable">
                <tr>
                    <td class="formCaptionTd">School Name:</td>
                    <td class="formFieldTd">
                        <asp:TextBox ID="txtschool" runat="server" CssClass="standardField"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd">Session Start Month:</td>
                    <td class="formFieldTd">
                        <asp:DropDownList ID="ddlmonth" runat="server">
                            <asp:ListItem Value="1">January</asp:ListItem>
                            <asp:ListItem Value="2">February</asp:ListItem>
                            <asp:ListItem Value="3">March</asp:ListItem>
                            <asp:ListItem Value="4">April</asp:ListItem>
                            <asp:ListItem Value="5">May</asp:ListItem>
                            <asp:ListItem Value="6">June</asp:ListItem>
                            <asp:ListItem Value="7">July</asp:ListItem>
                            <asp:ListItem Value="8">August</asp:ListItem>
                            <asp:ListItem Value="9">September</asp:ListItem>
                            <asp:ListItem Value="10">October</asp:ListItem>
                            <asp:ListItem Value="11">November</asp:ListItem>
                            <asp:ListItem Value="12">December</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr id="tr_logo" runat="server" visible="false">
                    <td class="formCaptionTd"></td>
                    <td class="formFieldTd">
                        <a id="ank_logo" runat="server" href="#" target="_blank">
                            <asp:Image ID="img_logo" runat="server" Width="200px" /></a>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd">Logo:
                    </td>
                    <td class="formFieldTd">
                        <asp:FileUpload ID="fu_logo" runat="server" />
                    </td>
                </tr>
                <tr id="tr_header" runat="server" visible="false">
                    <td class="formCaptionTd"></td>
                    <td class="formFieldTd">
                        <a id="ank_header" runat="server" href="#" target="_blank">
                            <asp:Image ID="img_header" runat="server" Width="200px" /></a>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd">Header:
                    </td>
                    <td class="formFieldTd">
                        <asp:FileUpload ID="fu_header" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd">Footer:
                    </td>
                    <td class="formFieldTd">
                        <asp:TextBox ID="txtfooter" runat="server" TextMode="MultiLine" CssClass="standardField" Width="364px" Height="169px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd"></td>
                    <td class="formFieldTd">
                        <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="standardFormButton" OnClick="btnsave_Click" />
                        <asp:Button ID="btnupdate" runat="server" Text="Update" CssClass="standardFormButton" Visible="false" OnClick="btnupdate_Click" />
                        <asp:HiddenField ID="hfheader" runat="server" />
                        <asp:HiddenField ID="hflogo" runat="server" />
                        <asp:HiddenField ID="hfheaderid" runat="server" Value="0" />
                        <asp:HiddenField ID="hflogoid" runat="server" Value="0" />
                        <asp:HiddenField ID="hfrecid" runat="server" Value="0" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
