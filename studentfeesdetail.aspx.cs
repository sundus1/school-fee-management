﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class studentfeesdetail : AvaimaThirdpartyTool.AvaimaWebPage
{
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
            if (!string.IsNullOrEmpty(Request["id"]))
            {
                hfstudentid.Value = Request["id"];
                studentfeespackage(hfstudentid.Value);
                ShowPackageHistory();

            }
        //ScriptManager.RegisterStartupScript(this, GetType(), "resize", "resizeiframepage()", true);
    }

    public void studentfeespackage(string id)
    {
        try
        {
            Setpackageid(id);
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                string query="sp_getallpakagedeatilwithstudentid";
                if (hfpackageid.Value != "0")
                    query = "sp_getallpakagedeatilwithstudentidbystudent";

                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    if (con.State.ToString() != "Open")
                        con.Open();
                    cmd.Parameters.AddWithValue("@studentid", hfstudentid.Value);
                    cmd.Parameters.AddWithValue("@packageid", "");
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataSet ds = new DataSet();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        rptrfeedetail.DataSource = ds.Tables[1];
                        rptrfeedetail.DataBind();

                    }
                    else
                    {
                        tr_package.Visible = false;
                    }
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        txttitle.Value = ds.Tables[0].Rows[0]["Title"].ToString();
                        txtfeeamount.Value = ds.Tables[0].Rows[0]["Amount"].ToString();
                        txtlatefee.Value = ds.Tables[0].Rows[0]["LateFeeCharges"].ToString();
                        hfpackageletfeesamount.Value = ds.Tables[0].Rows[0]["LateFeeCharges"].ToString();
                        hfpackagetotalamount.Value = ds.Tables[0].Rows[0]["Totalamount"].ToString();
                        //hfpackageletfeesamount.Value = ds.Tables[0].Rows[0]["LateFeeCharges"].ToString();
                    }
                    //if (ds.Tables[2].Rows.Count > 0)
                    //{
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        rptrfeechargediscount.DataSource = ds.Tables[2];
                        rptrfeechargediscount.DataBind();
                    }
                    else
                    {
                        tr_charge.Visible = false;
                    }

                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        hfbalamount.Value = ds.Tables[3].Rows[0]["Balanceamount"].ToString();
                    }

                }
            }
            calculatefees();
        }
        catch (Exception ex)
        {
        }
    }

    public void calculatefees()
    {
        DataSet ds = new DataSet();
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            string query = "Get_FeesDetails";
            if (hfsetting.Value != "0")
                query = "Get_FeesDetailsbystudent";

            using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
            {
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@StudentID", Request.QueryString["id"]);
                if (hfsetting.Value != "0")
                    da.SelectCommand.Parameters.AddWithValue("@id", hfsetting.Value);
                da.Fill(ds);
            }
        }
        if (ds.Tables[0].Rows.Count == 0)
        {
            this.Redirect("Settings.aspx");
        }
        decimal totalactualfees = 0; // will hold data after calculation
        int totalmonth = 0; // will hold data after calculation

        decimal netfee = Convert.ToDecimal(hfpackagetotalamount.Value);
        decimal latefeecharge = Convert.ToDecimal(hfpackageletfeesamount.Value); // pick up from db
        decimal latefee_expirydate = 0; // pick up from db

        int feeduedate = Convert.ToInt32(ds.Tables[0].Rows[0]["Fee_DueDate"]);
        //int f_date = Convert.ToInt32(feeduedate.ToString("dd")); // extracting just date from due date

        int feeexpiredate = Convert.ToInt32(ds.Tables[0].Rows[0]["LateFeesEndDate"]);
        //int fx_date = Convert.ToInt32(feeexpiredate.ToString("dd")); // extracting date from expiry date
        DateTime todaydate = DateTime.Now;
        DateTime lastsubmitdate = Convert.ToDateTime(ds.Tables[1].Rows[0]["SubmitDate"]);
        int month = Convert.ToInt32(todaydate.ToString("MM"));
        int year = Convert.ToInt32(todaydate.ToString("yyyy"));
        int date = Convert.ToInt32(todaydate.ToString("dd"));

        //-- today date first
        if (todaydate.Year > lastsubmitdate.Year)
        {
            totalmonth = ((todaydate.Year - lastsubmitdate.Year) * 12) + todaydate.Month - lastsubmitdate.Month;
            totalactualfees = calculatenetfees(netfee) * totalmonth;

            if (date > feeexpiredate) // if expiry date passed and then late fee date also passed
            {
                decimal latecharge = latefeecharge + latefee_expirydate;
                totalactualfees += latecharge;
            }

            else if (date > feeduedate)
            {
                totalactualfees += latefeecharge;


            } // if only expiry date passed

            if (totalmonth > 1)
            {
                //totalactualfees = totalmonth * totalactualfees;
                totalactualfees += (totalmonth - 1) * latefeecharge;
            }

        }
        else if (date <= feeduedate && lastsubmitdate.Month != month) // fees on time
            totalactualfees = calculatenetfees(netfee);
        else // if candidate is late
        {
            if (date > feeexpiredate && lastsubmitdate.Month != month) // if expiry date passed and then late fee date also passed
                totalactualfees = calculatenetfees(netfee) + latefeecharge + latefee_expirydate;
            else if (date > feeduedate && lastsubmitdate.Month != month)
            {
                totalactualfees = calculatenetfees(netfee) + latefeecharge;

            } // if only expiry date passed



        }
        if (todaydate.Year <= lastsubmitdate.Year)
        {
            int monthcount = (month) - lastsubmitdate.Month;
            if (monthcount > 0)
            {
                totalactualfees = monthcount * totalactualfees;
                totalactualfees += (monthcount - 1) * latefeecharge;
            }
        }
        //divMessage.InnerHtml = "<h2>Your Total Amount to be submitted is:" + ds.Tables[0].Rows[0]["Currency"] + " " + lblTotalAmount.Text + "</h2>";
        if (totalactualfees > 0)
        {
            if (ds.Tables[0].Rows[0]["Istax"].ToString() == "True")
            {
                if (ds.Tables[0].Rows[0]["Isfixed"].ToString() == "0") // %age     ,Amount
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["Amount"])))
                    {
                        totalactualfees = (totalactualfees * ((100 + Convert.ToDecimal(ds.Tables[0].Rows[0]["Amount"])) / 100));
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["Amount"])))
                    {
                        totalactualfees = totalactualfees + Convert.ToDecimal(ds.Tables[0].Rows[0]["Amount"]);
                    }
                }
            }
        }
        if (Convert.ToDecimal(hfbalamount.Value) < 0)
            totalactualfees = totalactualfees + Math.Abs(Convert.ToDecimal(hfbalamount.Value));
        else
            totalactualfees = totalactualfees - Math.Abs(Convert.ToDecimal(hfbalamount.Value));

        hf_TotalAmount.Value = totalactualfees.ToString();

    }

    public decimal calculatenetfees(decimal totalfees)
    {
        //
        var calculatecharge = 0;
        var discountpercend = 0;
        DataSet ds = new DataSet();
        try
        {

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_getallchargediscountdetails", con))
                {
                    if (con.State.ToString() != "Open")
                        con.Open();
                    //cmd.Parameters.AddWithValue("@studentid", hfstudentid.Value);
                    //cmd.Parameters.AddWithValue("@packageid", "");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@StudentID", Request.QueryString["id"]);
                    ds = new DataSet();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);

                }
            }
            //-- 0 disc per , 1 disc fix ,2 char per , 3 char fix
            //if (ds.Tables[0].Rows.Count > 0)
            ////if (ds.Tables[0].Rows[0]["count"].ToString() != "")
            ////{
            ////    totalfees = (totalfees * ((100 - Convert.ToDecimal(ds.Tables[0].Rows[0]["count"])) / 100));
            ////}
            //if (ds.Tables[1].Rows.Count > 0)
            if (ds.Tables[0].Rows[0]["count"].ToString() != "")
            {
                totalfees = totalfees - Convert.ToDecimal(ds.Tables[1].Rows[0]["count"]);
            }
            //if (ds.Tables[2].Rows.Count > 0)
            ////if (ds.Tables[2].Rows[0]["count"].ToString() != "")
            ////{
            ////    totalfees = (totalfees * ((100 + Convert.ToDecimal(ds.Tables[2].Rows[0]["count"])) / 100));
            ////}
            //if (ds.Tables[3].Rows.Count > 0)
            if (ds.Tables[1].Rows[0]["count"].ToString() != "")
            {
                totalfees = totalfees + Convert.ToDecimal(ds.Tables[3].Rows[0]["count"]);
            }

            return totalfees;

        }

        catch (Exception ex)
        {
            return 0;
        }

    }

    protected void btnclick(object sender, EventArgs e)
    {
        calculatefees();
        //calculatenetfees(25000);
    }
    protected void btnPay_Click(object sender, EventArgs e)
    {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("Add_StudentHistory", conn))
            {
                conn.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Studentid", Request.QueryString["id"]);
                cmd.Parameters.AddWithValue("@SubmitDate", Convert.ToDateTime(DateTime.Now));
                cmd.Parameters.AddWithValue("@AmountPaid", Convert.ToDecimal(txtpayamount.Text));
                cmd.Parameters.AddWithValue("@instanceId", this.InstanceID);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            decimal value = Convert.ToDecimal(txtpayamount.Text) - Convert.ToDecimal(hf_TotalAmount.Value);
            using (SqlCommand cmd = new SqlCommand("insertbalpayment", conn))
            {
                conn.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@studentid", Request.QueryString["id"]);
                cmd.Parameters.AddWithValue("@Balanceamount", value);
                cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                cmd.ExecuteNonQuery();
            }
        }
        this.Redirect("studentfeesdetail.aspx?id=" + hfstudentid.Value);
    }

    protected void rptrfeedetail_itemdatabound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            AvaimaTimeZoneAPI objtime = new AvaimaTimeZoneAPI();
            //objtime.GetDate()
        }
    }
    public void ShowPackageHistory()
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT Feesubmit,SubmitDate,ID FROM tbl_feecollection WHERE Studentid=@id and InstanceId=@instanceId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@id", Request["id"]);
                    cmd.Parameters.AddWithValue("@instanceId",this.InstanceID);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    rptrhistory.DataSource = dt;
                    rptrhistory.DataBind();
                }
            }
        }
        catch (Exception)
        {


        }
    }

    protected void rptrhistory_itemdatabound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
            object container = e.Item.DataItem;
            Label lblgridisfixed = e.Item.FindControl("lblgridisfixed") as Label;
            lblgridisfixed.Text = "Fee Submitted on ";

            lblgridisfixed.Text += objATZ.GetDate(DataBinder.Eval(container, "SubmitDate").ToString(), HttpContext.Current.User.Identity.Name);
            lblgridisfixed.Text += " " + objATZ.GetTime(DataBinder.Eval(container, "SubmitDate").ToString(), HttpContext.Current.User.Identity.Name);
        }
    }

    private void Setpackageid(string studentid)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("select foldersetting,packageid from tbl_feesManagement where studendId=@studendId", con))
            {
                cmd.Parameters.AddWithValue("@studendId", studentid);
                DataTable dt1 = new DataTable();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt1);
                hfsetting.Value = String.IsNullOrEmpty(dt1.Rows[0]["foldersetting"].ToString()) ? "0" : dt1.Rows[0]["foldersetting"].ToString();
                hfpackageid.Value = String.IsNullOrEmpty(dt1.Rows[0]["packageid"].ToString()) ? "0" : dt1.Rows[0]["packageid"].ToString();
            }
        }
    }
}