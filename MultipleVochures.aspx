﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MultipleVochures.aspx.cs" Inherits="MultipleVochures" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <link href="_assets/css/voucher.css" rel="stylesheet" type="text/css" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("input:radio[name=grpfeevoucher]").click(function () {
                var value = $(this).val();
                if (value == "rbtnclass") {
                    $("#tr_class").show();
                }
                else
                    $("#tr_class").hide();
            });
            setvalue();
        });
        function setvalue() {
            var chval = document.getElementById("rbtnclass");
            if (chval.checked) {
                $("#tr_class").show();
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="mainContentDiv">

            <div class="formDiv">
                <table>
                    <tr>
                        <td colspan="2">
                            <div class="formTitleDiv">Generate Fee Vouchers</div>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Create Vouchers For Month: </td>
                        <td class="formFieldTd">
                            <asp:DropDownList runat="server" ID="ddlMonths" ValidationGroup="genarate">
                                <asp:ListItem Value="" Text="Select Month"></asp:ListItem>
                               <%-- <asp:ListItem Value="01" Text="January"></asp:ListItem>
                                <asp:ListItem Value="02" Text="Febuary"></asp:ListItem>
                                <asp:ListItem Value="03" Text="March"></asp:ListItem>
                                <asp:ListItem Value="04" Text="April"></asp:ListItem>
                                <asp:ListItem Value="05" Text="May"></asp:ListItem>
                                <asp:ListItem Value="06" Text="June"></asp:ListItem>
                                <asp:ListItem Value="07" Text="July"></asp:ListItem>
                                <asp:ListItem Value="08" Text="August"></asp:ListItem>
                                <asp:ListItem Value="09" Text="Spetember"></asp:ListItem>
                                <asp:ListItem Value="10" Text="October"></asp:ListItem>
                                <asp:ListItem Value="11" Text="November"></asp:ListItem>
                                <asp:ListItem Value="12" Text="December"></asp:ListItem>--%>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="rqrdddlMonths" ControlToValidate="ddlMonths" ErrorMessage="Please select month" SetFocusOnError="true" ForeColor="Red" ValidationGroup="genarate"></asp:RequiredFieldValidator>
                        </td>
                    </tr>                    
                   <%-- <tr>
                        <td></td>
                        <td><asp:Button ID="CreateVoucher" runat="server" OnClick="CreateVoucher_Click" Text="Create Vouchers" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><asp:Label runat="server" ID="lblmsg" ForeColor="Green"></asp:Label></td>
                    </tr>--%>
                    <tr>
                        <td class="formCaptionTd">Multiple Vouchers For</td>
                        <td class="formFieldTd">
                            <asp:RadioButton ID="rbtnclass" runat="server" Text="Generate Class Vouchers" GroupName="grpfeevoucher" ClientIDMode="Static" /><br />
                            <asp:RadioButton ID="rbtnall" runat="server" Text="Generate All Vouchers" GroupName="grpfeevoucher" ClientIDMode="Static" Checked="true" />
                        </td>
                    </tr>
                    <tr style="display: none;" id="tr_class">
                        <td class="formCaptionTd">Select Class</td>
                        <td class="formFieldTd">
                            <asp:DropDownList ID="ddlclass" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">
                            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Generate Vouchers" ValidationGroup="genarate" /><br />
                            <a runat="server" id="ankdownload" target="_blank" visible="false">View and Print Vouchers</a>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hftemplate" runat="server" Value="0" />
                <b>Note</b>
                <ul style="padding: 0 0 0 20px;">
                    <li>"Generate Class Vouchers" Generate Vouchers For all the students in selected class.</li>
                    <li>"Generate All Vouchers" Generate Vouchers For all classes</li>
                </ul>
            </div>
        </div>
    </form>
</body>
</html>
