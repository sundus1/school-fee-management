﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Invoice : AvaimaThirdpartyTool.AvaimaWebPage
{
    PagedDataSource pds = new PagedDataSource();
    string Totalcharges = "0";
    string Totaldiscount = "0";
    studentinvoice objstudent;
    studentparameter objinvoice;
    decimal totaldue = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        PagerControl.OnPageIndex_Changed += pg_rptrcontract_OnPageIndex_Changed;
        if (!IsPostBack)
        {
            txtpaiddate.Text = DateTime.Now.ToString("dd MMM yyyy");
            bindlink();
            PagerControl.CurrentPage = 0;
            bindpaymentmethod();
            calculatefees(Request["studid"], Request["PID"]);
            bindinvoice();
            txtpayment.Focus();
        }

    }
    public void pg_rptrcontract_OnPageIndex_Changed(object sender, EventArgs e)
    {
        bindinvoice();
    }
    private void bindpaymentmethod()
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("select * from paymentmethod WHERE instanceid = @InstanceId", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        ddlpayment.DataSource = dt;
                        ddlpayment.DataTextField = "paymentmethod";
                        ddlpayment.DataValueField = "recid";
                        ddlpayment.DataBind();
                        ddlpayment.Items.Insert(0, new ListItem("Select", "0"));
                    }
                    else
                    {
                        ddlpayment.DataSource = null;
                        ddlpayment.DataBind();
                        ddlpayment.Items.Insert(0, new ListItem("Select", "0"));
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void bindinvoice()
    {
        DataTable dtinvoices = new DataTable();
        objstudent = new studentinvoice();
        dtinvoices = objstudent.getstudentinvoice(Request["studid"]);
        if (dtinvoices.Rows.Count > 0)
        {
            reptinvoice.DataSource = dtinvoices;
            reptinvoice.DataBind();
            hfrecid.Value = dtinvoices.Rows[dtinvoices.Rows.Count - 1]["recid"].ToString();
        }
        else
        {
            reptinvoice.DataSource = null;
            reptinvoice.DataBind();
            lbltotladue.Text = "0.00";
        }
        DataTable dtlastpaymentdate = new DataTable();
        dtlastpaymentdate = objstudent.getLastpaymentdate(Request["studid"]);
        if (dtlastpaymentdate.Rows.Count > 0)
        {
            p_lastpayment.Attributes.Remove("style");
            AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
            lblpaydate.Text = objATZ.GetDate(dtlastpaymentdate.Rows[0]["paymentdate"].ToString(), HttpContext.Current.User.Identity.Name);
            lblpaymethod.Text = dtlastpaymentdate.Rows[0]["PMethod"].ToString();
            AvaimaUserProfile avaimauser = new AvaimaUserProfile();
            string[] userinfo = avaimauser.Getonlineuserinfo(dtlastpaymentdate.Rows[0]["userid"].ToString()).Split('#');
            lblreceivedby.Text = userinfo[0] + " " + userinfo[1];
        }
        else
        {
            p_lastpayment.Style.Add("display", "none");
        }

        DataSet ds = new DataSet();
        ds = objstudent.GetPaymentHistory(Request["studid"]);
        if (ds.Tables[0].Rows.Count > 0)
        {
            pds.AllowPaging = true;
            pds.DataSource = ds.Tables[0].DefaultView;
            pds.CurrentPageIndex = PagerControl.CurrentPage;
            pds.PageSize = PagerControl.PageSize;
            PagerControl.TotalRecords = ds.Tables[0].Rows.Count;

            repthistory.DataSource = pds;
            repthistory.DataBind();
            PagerControl.LoadStatus();
        }
        else
        {
            repthistory.DataSource = null;
            repthistory.DataBind();
        }
        lblName.Text = ds.Tables[1].Rows[0]["title"].ToString();
    }

    public void calculatefees(string studentid, string pid)
    {
        DataSet ds = new DataSet();
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            string query = "getdatesbyids";
            using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
            {
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@StudentID", studentid);
                da.SelectCommand.Parameters.AddWithValue("@Pid", pid);
                da.Fill(ds);
            }
        }
        decimal totalactualfees = 0; // will hold data after calculation
        int totalmonth = 0; // will hold data after calculation
        int geninvoiceday = 0;
        if (ds.Tables[7].Rows.Count > 0)
        {
            geninvoiceday = Convert.ToInt32(ds.Tables[7].Rows[0]["generatedate"].ToString());
        }
        decimal netfee = Convert.ToDecimal(ds.Tables[2].Rows[0]["Totalamount"].ToString());
        decimal latefeecharge = Convert.ToDecimal(ds.Tables[2].Rows[0]["LateFeeCharges"].ToString()); // pick up from db
        decimal latefee_expirydate = 0; // pick up from db
        string[] feeduedatearray = ds.Tables[0].Rows[0]["Fee_DueDate"].ToString().Split('#');
        int feeduedate = Convert.ToInt32(feeduedatearray[0]);
        string cyclestartdate = ds.Tables[2].Rows[0]["cyclestartday"].ToString();
        DateTime todaydate = DateTime.Now;
        DateTime Packagestartdate = Convert.ToDateTime(ds.Tables[2].Rows[0]["startday"]);
        DateTime lastsubmitdate;
        if (ds.Tables[4].Rows.Count > 0)
        {
            lastsubmitdate = Convert.ToDateTime(ds.Tables[4].Rows[0]["todate"]);
            lastsubmitdate = lastsubmitdate.AddDays(1);
        }
        else
        {
            lastsubmitdate = Convert.ToDateTime(ds.Tables[1].Rows[0]["SubmitDate"]);
        }
        //DateTime packagedate = Convert.ToDateTime(ds.Tables[1].Rows[0]["packagedate"]);
        int month = Convert.ToInt32(todaydate.ToString("MM"));
        int year = Convert.ToInt32(todaydate.ToString("yyyy"));
        int date = Convert.ToInt32(todaydate.ToString("dd"));

        int w = 0;
        int days = 0;
        string[] cycle = ds.Tables[2].Rows[0]["paymentby"].ToString().Split('#');
        if (cycle[1] == "Months")
        {
            DateTime datemonth = lastsubmitdate.AddDays(-geninvoiceday);
            w = ((todaydate.Year - lastsubmitdate.Year) * 12) + (todaydate.Month) - datemonth.Month;
            days = 30;
            if (w == 0 && todaydate > datemonth)
            {
                 w = w + 1;
                
            }
             w = Convert.ToInt32(Math.Floor(Convert.ToDecimal(w / Convert.ToInt32(cycle[0]))));
        }
        else if (cycle[1] == "Weeks")
        {
            w = NumberOfWeeks(lastsubmitdate, todaydate);
            days = 7;
        }
        else if (cycle[1] == "Years")
        {
            TimeSpan span = todaydate.Subtract(lastsubmitdate);
            if (span.Days <= 365)
                w = 1;
            else
            {
                decimal dds = Math.Ceiling(Convert.ToDecimal(span.Days) / 365);
                w = Convert.ToInt32(dds);
            }
            days = 365;
        }
        else if (cycle[1] == "Day")
        {
            TimeSpan span = todaydate.Subtract(lastsubmitdate);
            w = span.Days;
            days = 1;
        }
        //DataTable dtincoice = new DataTable();
        //dtincoice.Columns.Add("Monthrange");
        //dtincoice.Columns.Add("TotalPackagefees");
        //dtincoice.Columns.Add("Totalcharges");
        //dtincoice.Columns.Add("Totaldiscount");
        //dtincoice.Columns.Add("feespaid");
        //dtincoice.Columns.Add("feesbalance");
        //dtincoice.Columns.Add("month");
        //dtincoice.Columns.Add("Totalfees");
        DataTable prepackage = new DataTable();

        DateTime dtfdate = lastsubmitdate;
        objinvoice = new studentparameter();
        objinvoice.studentid = studentid;
        bool bit = false;
        bool chw = true;
        StringBuilder msg = new StringBuilder();
        for (int invoice = 0; invoice < w; invoice++)
        {
            string mon = "";
            netfee = Convert.ToDecimal(ds.Tables[2].Rows[0]["Totalamount"].ToString());
            objinvoice.TotalPackagefees = netfee;
            objinvoice.Totalfees = netfee;
            if (cycle[1] == "Months")
            {
                if (Convert.ToInt32(cyclestartdate) == dtfdate.Day || bit)
                {
                    objinvoice.Monthrange = dtfdate.ToString("MMM dd, yyyy") + " to " + dtfdate.AddMonths(Convert.ToInt32(cycle[0])).AddDays(-1).ToString("MMM dd, yyyy");
                    objinvoice.fromdate = dtfdate;
                    objinvoice.todate = dtfdate.AddMonths(Convert.ToInt32(cycle[0])).AddDays(-1);
                    dtfdate = dtfdate.AddMonths(Convert.ToInt32(cycle[0]));
                    chw = true;
                }
                else if (!bit)
                {
                    while (dtfdate > Packagestartdate)
                    {
                        Packagestartdate = Packagestartdate.AddMonths(Convert.ToInt32(cycle[0]));
                    }
                    objinvoice.Monthrange = dtfdate.ToString("MMM dd, yyyy") + " to " + Packagestartdate.AddDays(-1).ToString("MMM dd, yyyy");
                    objinvoice.fromdate = dtfdate;
                    objinvoice.todate = Packagestartdate.AddDays(-1);
                    TimeSpan da = Packagestartdate.AddDays(-1) - dtfdate;
                    dtfdate = Packagestartdate;
                    bit = true;
                    chw = false;
                    objinvoice.Totalfees = Math.Ceiling((((Convert.ToDecimal(netfee.ToString()) / Convert.ToDecimal(cycle[0])) * 12) / 365) * da.Days);
                    objinvoice.TotalPackagefees = objinvoice.Totalfees;
                }
                //if (dtfdate > DateTime.Now)
                //{
                //    break;
                //}
                msg = new StringBuilder();
                decimal h = 0;
                decimal beforeamount = 0;
                decimal afteramount = 0;
                TimeSpan beforedays = new TimeSpan();
                TimeSpan afterdays = new TimeSpan();
                objstudent = new studentinvoice();
                decimal Finalamount = 0;
                prepackage = objstudent.Getpreviouspackages(objinvoice.fromdate.ToString(), objinvoice.todate.ToString(), ds.Tables[0].Rows[0]["ID"].ToString());
                if (prepackage.Rows.Count > 0)
                {
                    DateTime def = objinvoice.fromdate;
                    DateTime dee = objinvoice.todate;
                    for (int y1 = 0; y1 < prepackage.Rows.Count; y1++)
                    {
                        beforeamount = 0;
                        afteramount = 0;
                        DateTime SD = Convert.ToDateTime(prepackage.Rows[y1]["startday"].ToString());
                        DateTime ED = Convert.ToDateTime(prepackage.Rows[y1]["enddate"].ToString());

                        if (SD.Date < def.Date && ED.Date > dee.Date)
                        {
                            Finalamount += Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString());
                            msg.Append("Previous Package Fees<br />");
                            if (!chw)
                            {
                                TimeSpan da = dee - def;
                                Finalamount = Math.Ceiling((((Finalamount / Convert.ToDecimal(cycle[0])) * 12) / 365) * da.Days);
                                chw = true;
                            }
                            break;
                        }

                        if (SD.Date == def.Date && ED.Date == dee.Date)
                        {
                            Finalamount += Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString());
                            msg.Append("Previous Package Fees<br />");
                            if (!chw)
                            {
                                TimeSpan da = dee - def;
                                Finalamount = Math.Ceiling((((Finalamount / Convert.ToDecimal(cycle[0])) * 12) / 365) * da.Days);
                                chw = true;
                            }
                            break;
                        }
                        else if (def.Date <= SD.Date && dee.Date >= ED.Date)
                        {
                            /*time beach ma*/
                            if (Finalamount == 0)
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                                beforedays = SD.Subtract(def);
                                beforeamount = (h / new DateTime(def.Year, def.Month, 1).AddMonths(1).AddDays(-1).Day) * beforedays.Days;
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            }
                            if (prepackage.Rows.Count == (y1 + 1))
                            {
                                h = netfee / Convert.ToDecimal(cycle[0]);
                            }
                            else if (prepackage.Rows.Count > (y1 + 1))
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1 + 1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            }
                            afterdays = dee.Subtract(ED);
                            afteramount = (h / new DateTime(dee.Year, dee.Month, 1).AddMonths(1).AddDays(-1).Day) * afterdays.Days;
                            Finalamount += Math.Round(beforeamount) + Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) + Math.Round(afteramount);


                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + ED.ToString("MMM dd, yyyy") + ": " + prepackage.Rows[y1]["Totalamount"].ToString() + "<br />");
                            msg.Append("Fees from " + ED.AddDays(+1).ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (SD.Date > def.Date && SD.Date <= dee.Date && ED.Date >= dee.Date)
                        {
                            /*startday beach ma*/
                            if (prepackage.Rows.Count > 1 && y1 > 1)
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1 - 1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                                beforedays = SD.Subtract(def);
                                beforeamount = (h / new DateTime(def.Year, def.Month, 1).AddMonths(1).AddDays(-1).Day) * beforedays.Days;
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            }
                            h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            afterdays = dee.Subtract(SD);
                            afteramount = (h / new DateTime(dee.Year, dee.Month, 1).AddMonths(1).AddDays(-1).Day) * afterdays.Days;
                            Finalamount += Math.Round(beforeamount) + Math.Round(afteramount);


                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (ED.Date > def.Date && ED.Date <= dee.Date && SD.Date <= def.Date)
                        {
                            /*Enddate beach ma*/
                            h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            beforedays = ED.Subtract(def);
                            beforeamount = (h / new DateTime(def.Year, def.Month, 1).AddMonths(1).AddDays(-1).Day) * beforedays.Days;
                            if (prepackage.Rows.Count == 1 || prepackage.Rows.Count == (y1 + 1))
                            {
                                h = netfee / Convert.ToDecimal(cycle[0]);
                                afterdays = dee.Subtract(ED);
                                afteramount = (h / new DateTime(dee.Year, dee.Month, 1).AddMonths(1).AddDays(-1).Day) * afterdays.Days;
                                Finalamount += Math.Round(beforeamount) + Math.Round(afteramount);

                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                                msg.Append("Fees from " + ED.ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                            }
                            else
                            {
                                Finalamount += Math.Round(beforeamount);
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                                def = ED;
                                continue;
                            }

                        }
                    }
                    netfee = Finalamount;
                    objinvoice.Totalfees = Finalamount;
                    objinvoice.TotalPackagefees = objinvoice.Totalfees;
                }



                if (ds.Tables[5].Rows.Count > 0)
                {
                    for (int y = 0; y < ds.Tables[5].Rows.Count; y++)
                    {
                        DateTime SD = Convert.ToDateTime(ds.Tables[5].Rows[y]["startdate"].ToString());
                        SD = new DateTime(objinvoice.fromdate.Year, SD.Month, SD.Day);
                        DateTime ED = Convert.ToDateTime(ds.Tables[5].Rows[y]["enddate"].ToString());
                        ED = new DateTime(objinvoice.todate.Year, ED.Month, ED.Day);
                        if (SD.Date == objinvoice.fromdate.Date && ED.Date == objinvoice.todate.Date)
                        {
                            objinvoice.Totalfees = Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString());
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees of Holiday Packages");
                        }
                        else if (objinvoice.fromdate.Date <= SD.Date && objinvoice.todate.Date >= ED.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = SD.Subtract(objinvoice.fromdate);
                            beforeamount = (h / new DateTime(objinvoice.fromdate.Year, objinvoice.fromdate.Month, 1).AddMonths(1).AddDays(-1).Day) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(ED);
                            afteramount = (h / new DateTime(objinvoice.todate.Year, objinvoice.todate.Month, 1).AddMonths(1).AddDays(-1).Day) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + ED.ToString("MMM dd, yyyy") + ": " + ds.Tables[5].Rows[y]["fees"].ToString() + "<br />");
                            msg.Append("Fees from " + ED.AddDays(+1).ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (SD.Date > objinvoice.fromdate.Date && SD.Date <= objinvoice.todate.Date && ED.Date >= objinvoice.todate.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = SD.Subtract(objinvoice.fromdate);
                            beforeamount = (h / new DateTime(objinvoice.fromdate.Year, objinvoice.fromdate.Month, 1).AddMonths(1).AddDays(-1).Day) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(SD);
                            afteramount = (Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) / new DateTime(objinvoice.todate.Year, objinvoice.todate.Month, 1).AddMonths(1).AddDays(-1).Day) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (ED.Date > objinvoice.fromdate.Date && ED.Date <= objinvoice.todate.Date && SD.Date <= objinvoice.fromdate.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = ED.Subtract(objinvoice.fromdate);
                            beforeamount = (Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) / new DateTime(objinvoice.fromdate.Year, objinvoice.fromdate.Month, 1).AddMonths(1).AddDays(-1).Day) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(ED);
                            afteramount = (h / new DateTime(objinvoice.todate.Year, objinvoice.todate.Month, 1).AddMonths(1).AddDays(-1).Day) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + ED.ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }

                    }

                }
            }
            else if (cycle[1] == "Weeks")
            {
                objinvoice.Monthrange = dtfdate.ToString("MMM dd, yyyy") + " to " + dtfdate.AddDays(Convert.ToInt32(cycle[0]) * 7).ToString("MMM dd, yyyy");
                objinvoice.fromdate = dtfdate;
                objinvoice.todate = dtfdate.AddDays(Convert.ToInt32(cycle[0]) * 7);
                dtfdate = dtfdate.AddDays(Convert.ToInt32(cycle[0]) * 7);
                if (dtfdate > DateTime.Now)
                {
                    break;
                }
                msg = new StringBuilder();
                decimal h = 0;
                decimal beforeamount = 0;
                decimal afteramount = 0;
                TimeSpan beforedays = new TimeSpan();
                TimeSpan afterdays = new TimeSpan();
                decimal Finalamount = 0;
                objstudent = new studentinvoice();
                prepackage = objstudent.Getpreviouspackages(objinvoice.fromdate.ToString(), objinvoice.todate.ToString(), ds.Tables[0].Rows[0]["ID"].ToString());
                if (prepackage.Rows.Count > 0)
                {
                    DateTime def = objinvoice.fromdate;
                    DateTime dee = objinvoice.todate;
                    for (int y1 = 0; y1 < prepackage.Rows.Count; y1++)
                    {
                        beforeamount = 0;
                        afteramount = 0;
                        DateTime SD = Convert.ToDateTime(prepackage.Rows[y1]["startday"].ToString());
                        DateTime ED = Convert.ToDateTime(prepackage.Rows[y1]["enddate"].ToString());
                        if (SD.Date < def.Date && ED.Date > dee.Date)
                        {
                            Finalamount += Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString());
                            msg.Append("Previous Package Fees<br />");
                            break;
                        }
                        if (SD.Date == def.Date && ED.Date == dee.Date)
                        {
                            Finalamount += Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString());
                            msg.Append("Previous Package Fees<br />");
                            break;
                        }
                        else if (def.Date <= SD.Date && dee.Date >= ED.Date)
                        {
                            /*time beach ma*/
                            if (Finalamount == 0)
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                                beforedays = SD.Subtract(def);
                                beforeamount = (h / 7) * beforedays.Days;
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            }
                            if (prepackage.Rows.Count == (y1 + 1))
                            {
                                h = netfee / Convert.ToDecimal(cycle[0]);
                            }
                            else if (prepackage.Rows.Count > (y1 + 1))
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1 + 1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            }
                            afterdays = dee.Subtract(ED);
                            afteramount = (h / 7) * afterdays.Days;
                            Finalamount += Math.Round(beforeamount) + Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) + Math.Round(afteramount);


                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + ED.ToString("MMM dd, yyyy") + ": " + prepackage.Rows[y1]["Totalamount"].ToString() + "<br />");
                            msg.Append("Fees from " + ED.AddDays(+1).ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (SD.Date > def.Date && SD.Date <= dee.Date && ED.Date >= dee.Date)
                        {
                            /*startday beach ma*/
                            if (prepackage.Rows.Count > 1 && y1 > 1)
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1 - 1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                                beforedays = SD.Subtract(def);
                                beforeamount = (h / 7) * beforedays.Days;
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            }
                            h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            afterdays = dee.Subtract(SD);
                            afteramount = (h / 7) * afterdays.Days;
                            Finalamount += Math.Round(beforeamount) + Math.Round(afteramount);


                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (ED.Date > def.Date && ED.Date <= dee.Date && SD.Date <= def.Date)
                        {
                            /*Enddate beach ma*/
                            h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            beforedays = ED.Subtract(def);
                            beforeamount = (h / ED.Subtract(SD).Days) * beforedays.Days;
                            if (prepackage.Rows.Count == 1 || prepackage.Rows.Count == (y1 + 1))
                            {
                                h = netfee / Convert.ToDecimal(cycle[0]);
                                afterdays = dee.Subtract(ED);
                                afteramount = (h / 7) * afterdays.Days;
                                Finalamount += Math.Round(beforeamount) + Math.Round(afteramount);

                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                                msg.Append("Fees from " + ED.ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                            }
                            else
                            {
                                Finalamount += Math.Round(beforeamount);
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                                def = ED;
                                continue;
                            }

                        }
                    }
                    netfee = Finalamount;
                    objinvoice.Totalfees = Finalamount;
                    objinvoice.TotalPackagefees = objinvoice.Totalfees;
                }


                if (ds.Tables[5].Rows.Count > 0)
                {
                    for (int y = 0; y < ds.Tables[5].Rows.Count; y++)
                    {
                        DateTime SD = Convert.ToDateTime(ds.Tables[5].Rows[y]["startdate"].ToString());
                        SD = new DateTime(objinvoice.fromdate.Year, SD.Month, SD.Day);
                        DateTime ED = Convert.ToDateTime(ds.Tables[5].Rows[y]["enddate"].ToString());
                        ED = new DateTime(objinvoice.todate.Year, ED.Month, ED.Day);
                        if (SD.Date == objinvoice.fromdate.Date && ED.Date == objinvoice.todate.Date)
                        {
                            objinvoice.Totalfees = Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString());
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees of Holiday Packages");
                        }
                        else if (objinvoice.fromdate.Date <= SD.Date && objinvoice.todate.Date >= ED.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = SD.Subtract(objinvoice.fromdate);
                            beforeamount = (h / 7) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(ED);
                            afteramount = (h / 7) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + ED.ToString("MMM dd, yyyy") + ": " + ds.Tables[5].Rows[y]["fees"].ToString() + "<br />");
                            msg.Append("Fees from " + ED.AddDays(+1).ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (SD.Date > objinvoice.fromdate.Date && SD.Date <= objinvoice.todate.Date && ED.Date >= objinvoice.todate.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = SD.Subtract(objinvoice.fromdate);
                            beforeamount = (h / 7) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(SD);
                            afteramount = (Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) / ED.Subtract(SD).Days) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (ED.Date > objinvoice.fromdate.Date && ED.Date <= objinvoice.todate.Date && SD.Date <= objinvoice.fromdate.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = ED.Subtract(objinvoice.fromdate);
                            beforeamount = (Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) / ED.Subtract(SD).Days) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(ED);
                            afteramount = (h / 7) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + ED.ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }

                    }

                }
            }
            else if (cycle[1] == "Years")
            {
                if (Convert.ToInt32(cyclestartdate) == dtfdate.Day || bit)
                {
                    objinvoice.Monthrange = dtfdate.ToString("MMM dd, yyyy") + " to " + dtfdate.AddYears(Convert.ToInt32(cycle[0])).AddDays(-1).ToString("MMM dd, yyyy");
                    objinvoice.fromdate = dtfdate;
                    objinvoice.todate = dtfdate.AddYears(Convert.ToInt32(cycle[0])).AddDays(-1);
                    dtfdate = dtfdate.AddYears(Convert.ToInt32(cycle[0]));
                }
                else if (!bit)
                {
                    while (dtfdate > Packagestartdate)
                    {
                        Packagestartdate = Packagestartdate.AddYears(Convert.ToInt32(cycle[0]));
                    }
                    objinvoice.Monthrange = dtfdate.ToString("MMM dd, yyyy") + " to " + Packagestartdate.AddDays(-1).ToString("MMM dd, yyyy");
                    objinvoice.fromdate = dtfdate;
                    objinvoice.todate = Packagestartdate.AddDays(-1);
                    TimeSpan da = Packagestartdate.AddDays(-1) - dtfdate;
                    dtfdate = Packagestartdate;
                    bit = true;
                    objinvoice.Totalfees = Math.Ceiling(((Convert.ToDecimal(netfee.ToString()) / Convert.ToDecimal(cycle[0])) / 365) * da.Days);
                    objinvoice.TotalPackagefees = objinvoice.Totalfees;
                }
                if (dtfdate > DateTime.Now)
                {
                    break;
                }
                msg = new StringBuilder();
                decimal h = 0;
                decimal beforeamount = 0;
                decimal afteramount = 0;
                TimeSpan beforedays = new TimeSpan();
                TimeSpan afterdays = new TimeSpan();
                decimal Finalamount = 0;
                objstudent = new studentinvoice();
                prepackage = objstudent.Getpreviouspackages(objinvoice.fromdate.ToString(), objinvoice.todate.ToString(), ds.Tables[0].Rows[0]["ID"].ToString());
                if (prepackage.Rows.Count > 0)
                {
                    DateTime def = objinvoice.fromdate;
                    DateTime dee = objinvoice.todate;
                    for (int y1 = 0; y1 < prepackage.Rows.Count; y1++)
                    {
                        beforeamount = 0;
                        afteramount = 0;
                        DateTime SD = Convert.ToDateTime(prepackage.Rows[y1]["startday"].ToString());
                        DateTime ED = Convert.ToDateTime(prepackage.Rows[y1]["enddate"].ToString());
                        if (SD.Date < def.Date && ED.Date > dee.Date)
                        {
                            Finalamount += Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString());
                            msg.Append("Previous Package Fees<br />");
                            if (!chw)
                            {
                                TimeSpan da = dee - def;
                                Finalamount = Math.Ceiling((((Finalamount / Convert.ToDecimal(cycle[0])) * 12) / 365) * da.Days);
                                chw = true;
                            }
                            break;
                        }
                        if (SD.Date == def.Date && ED.Date == dee.Date)
                        {
                            Finalamount += Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString());
                            msg.Append("Previous Package Fees<br />");
                            if (!chw)
                            {
                                TimeSpan da = dee - def;
                                Finalamount = Math.Ceiling((((Finalamount / Convert.ToDecimal(cycle[0])) * 12) / 365) * da.Days);
                                chw = true;
                            }
                            break;
                        }
                        else if (def.Date <= SD.Date && dee.Date >= ED.Date)
                        {
                            /*time beach ma*/
                            if (Finalamount == 0)
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                                beforedays = SD.Subtract(def);
                                beforeamount = (h / 365) * beforedays.Days;
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            }
                            if (prepackage.Rows.Count == (y1 + 1))
                            {
                                h = netfee / Convert.ToDecimal(cycle[0]);
                            }
                            else if (prepackage.Rows.Count > (y1 + 1))
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1 + 1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            }
                            afterdays = dee.Subtract(ED);
                            afteramount = (h / 365) * afterdays.Days;
                            Finalamount += Math.Round(beforeamount) + Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) + Math.Round(afteramount);


                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + ED.ToString("MMM dd, yyyy") + ": " + prepackage.Rows[y1]["Totalamount"].ToString() + "<br />");
                            msg.Append("Fees from " + ED.AddDays(+1).ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (SD.Date > def.Date && SD.Date <= dee.Date && ED.Date >= dee.Date)
                        {
                            /*startday beach ma*/
                            if (prepackage.Rows.Count > 1 && y1 > 1)
                            {
                                h = Convert.ToDecimal(prepackage.Rows[y1 - 1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                                beforedays = SD.Subtract(def);
                                beforeamount = (h / 365) * beforedays.Days;
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            }
                            h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            afterdays = dee.Subtract(SD);
                            afteramount = (h / ED.Subtract(SD).Days) * afterdays.Days;
                            Finalamount += Math.Round(beforeamount) + Math.Round(afteramount);


                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (ED.Date > def.Date && ED.Date <= dee.Date && SD.Date <= def.Date)
                        {
                            /*Enddate beach ma*/
                            h = Convert.ToDecimal(prepackage.Rows[y1]["Totalamount"].ToString()) / Convert.ToDecimal(cycle[0]);
                            beforedays = ED.Subtract(def);
                            beforeamount = (h / ED.Subtract(SD).Days) * beforedays.Days;
                            if (prepackage.Rows.Count == 1 || prepackage.Rows.Count == (y1 + 1))
                            {
                                h = netfee / Convert.ToDecimal(cycle[0]);
                                afterdays = dee.Subtract(ED);
                                afteramount = (h / 365) * afterdays.Days;
                                Finalamount += Math.Round(beforeamount) + Math.Round(afteramount);

                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                                msg.Append("Fees from " + ED.ToString("MMM dd, yyyy") + " to " + dee.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                            }
                            else
                            {
                                Finalamount += Math.Round(beforeamount);
                                msg.Append("Fees from " + def.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                                def = ED;
                                continue;
                            }

                        }
                    }
                    netfee = Finalamount;
                    objinvoice.Totalfees = Finalamount;
                    objinvoice.TotalPackagefees = objinvoice.Totalfees;
                }

                if (ds.Tables[5].Rows.Count > 0)
                {
                    for (int y = 0; y < ds.Tables[5].Rows.Count; y++)
                    {
                        DateTime SD = Convert.ToDateTime(ds.Tables[5].Rows[y]["startdate"].ToString());
                        SD = new DateTime(objinvoice.fromdate.Year, SD.Month, SD.Day);
                        DateTime ED = Convert.ToDateTime(ds.Tables[5].Rows[y]["enddate"].ToString());
                        ED = new DateTime(objinvoice.todate.Year, ED.Month, ED.Day);
                        if (SD.Date == objinvoice.fromdate.Date && ED.Date == objinvoice.todate.Date)
                        {
                            objinvoice.Totalfees = Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString());
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees of Holiday Packages");
                        }
                        else if (objinvoice.fromdate.Date <= SD.Date && objinvoice.todate.Date >= ED.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = SD.Subtract(objinvoice.fromdate);
                            beforeamount = (h / 365) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(ED);
                            afteramount = (h / 365) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + ED.ToString("MMM dd, yyyy") + ": " + ds.Tables[5].Rows[y]["fees"].ToString() + "<br />");
                            msg.Append("Fees from " + ED.AddDays(+1).ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (SD.Date > objinvoice.fromdate.Date && SD.Date <= objinvoice.todate.Date && ED.Date >= objinvoice.todate.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = SD.Subtract(objinvoice.fromdate);
                            beforeamount = (h / 365) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(SD);
                            afteramount = (Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) / ED.Subtract(SD).Days) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + SD.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + SD.ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }
                        else if (ED.Date > objinvoice.fromdate.Date && ED.Date <= objinvoice.todate.Date && SD.Date <= objinvoice.fromdate.Date)
                        {
                            h = netfee / Convert.ToDecimal(cycle[0]);
                            beforedays = ED.Subtract(objinvoice.fromdate);
                            beforeamount = (Convert.ToDecimal(ds.Tables[5].Rows[y]["fees"].ToString()) / ED.Subtract(SD).Days) * beforedays.Days;
                            afterdays = objinvoice.todate.Subtract(ED);
                            afteramount = (h / 365) * afterdays.Days;
                            objinvoice.Totalfees = Math.Round(beforeamount) + Math.Round(afteramount);
                            objinvoice.TotalPackagefees = objinvoice.Totalfees;
                            msg.Append("Fees from " + objinvoice.fromdate.ToString("MMM dd, yyyy") + " to " + ED.AddDays(-1).ToString("MMM dd, yyyy") + ": " + Math.Round(beforeamount) + "<br />");
                            msg.Append("Fees from " + ED.ToString("MMM dd, yyyy") + " to " + objinvoice.todate.ToString("MMM dd, yyyy") + ": " + Math.Round(afteramount) + "<br />");
                        }

                    }

                }
            }
            else if (cycle[1] == "Day")
            {
                objinvoice.Monthrange = dtfdate.ToString("MMM dd, yyyy") + " to " + dtfdate.AddDays(Convert.ToInt32(cycle[0]) * 1).ToString("MMM dd, yyyy");
                objinvoice.fromdate = dtfdate;
                objinvoice.todate = dtfdate.AddDays(Convert.ToInt32(cycle[0]) * 1);
                dtfdate = dtfdate.AddDays(Convert.ToInt32(cycle[0]) * 1);

                if (dtfdate > DateTime.Now)
                {
                    break;
                }
            }
            calculatenetfees(studentid);
            objinvoice.feespaid = 0;

            objinvoice.Totalfees = objinvoice.Totalfees - Convert.ToDecimal(Totaldiscount);
            objinvoice.Totalfees = objinvoice.Totalfees + Convert.ToDecimal(Totalcharges);
            if (ds.Tables[0].Rows[0]["Istax"].ToString() == "True")
            {
                if (ds.Tables[0].Rows[0]["Isfixed"].ToString() == "0") // %age     ,Amount
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["Amount"])))
                    {
                        objinvoice.Totalfees = (objinvoice.Totalfees * ((100 + Convert.ToDecimal(ds.Tables[0].Rows[0]["Amount"])) / 100));
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["Amount"])))
                    {
                        objinvoice.Totalfees = objinvoice.Totalfees + Convert.ToDecimal(ds.Tables[0].Rows[0]["Amount"]);
                    }
                }
            }
            //if (ds.Tables[8].Rows.Count > 0)
            //{
            //    for (int dds = 0; dds < ds.Tables[8].Rows.Count; dds++)
            //    {
            //        if (!Convert.ToBoolean(ds.Tables[8].Rows[dds]["isdiscount"].ToString()))
            //        {
            //            objinvoice.Totalfees += Convert.ToDecimal(ds.Tables[8].Rows[dds]["DetailAmount"].ToString());
            //        }
            //        else
            //        {
            //            objinvoice.Totalfees -= Convert.ToDecimal(ds.Tables[8].Rows[dds]["DetailAmount"].ToString());
            //        }
            //    }
            //}

            if (ds.Tables[6].Rows.Count > 0)
            {
                for (int annual = 0; annual < ds.Tables[6].Rows.Count; annual++)
                {
                    DateTime dtannula = Convert.ToDateTime(ds.Tables[6].Rows[annual]["Month"].ToString() + " " + DateTime.Now.Year.ToString());
                    if (dtannula >= objinvoice.fromdate && dtannula <= objinvoice.todate)
                    {
                        objinvoice.Totalfees += Convert.ToDecimal(ds.Tables[6].Rows[annual]["fee"].ToString());
                    }
                }
            }
            
            objinvoice.feesbalance = objinvoice.Totalfees - objinvoice.feespaid;
            objinvoice.Totalcharges = Convert.ToDecimal(Totalcharges);
            objinvoice.Totaldiscount = Convert.ToDecimal(Totaldiscount);
            objinvoice.feescomments = msg.ToString();
            objstudent = new studentinvoice();
            int id = objstudent.insertinvoice(objinvoice);
            if (ds.Tables[8].Rows.Count > 0)
            {
                for (int dds = 0; dds < ds.Tables[8].Rows.Count; dds++)
                {
                    if (!Convert.ToBoolean(ds.Tables[8].Rows[dds]["isdiscount"].ToString()))
                    {
                        objinvoice.Insertinvoiceajustments(ds.Tables[8].Rows[dds]["DetaiilTitle"].ToString(), ds.Tables[8].Rows[dds]["DetailAmount"].ToString(), id.ToString(), "2");
                    }
                    else
                    {
                        objinvoice.Insertinvoiceajustments(ds.Tables[8].Rows[dds]["DetaiilTitle"].ToString(), ds.Tables[8].Rows[dds]["DetailAmount"].ToString(), id.ToString(), "1");
                    }
                }
            }

            if (ds.Tables[6].Rows.Count > 0)
            {
                for (int annual = 0; annual < ds.Tables[6].Rows.Count; annual++)
                {
                    DateTime dtannula = Convert.ToDateTime(ds.Tables[6].Rows[annual]["Month"].ToString() + " " + DateTime.Now.Year.ToString());
                    if (dtannula >= objinvoice.fromdate && dtannula <= objinvoice.todate)
                    {
                        objinvoice.Insertinvoiceajustments(String.IsNullOrEmpty(ds.Tables[6].Rows[annual]["title"].ToString()) ? "Annual Fee" : ds.Tables[6].Rows[annual]["title"].ToString(), ds.Tables[6].Rows[annual]["fee"].ToString(), id.ToString(), "2");
                    }
                }
            }
            //dtincoice.Rows.Add(new object[] { objinvoice.Monthrange, objinvoice.TotalPackagefees, Totalcharges, Totaldiscount, objinvoice.feespaid.ToString(), objinvoice.feesbalance.ToString(), mon, objinvoice.Totalfees.ToString() });
        }


        //if (dtincoice.Rows.Count > 0)
        //{
        //    reptinvoice.DataSource = dtincoice;
        //    reptinvoice.DataBind();
        //}
    }
    private int NumberOfWeeks(DateTime dateFrom, DateTime dateTo)
    {
        TimeSpan Span = dateTo.Subtract(dateFrom);

        if (Span.Days <= 7)
        {
            return 1;
        }

        int weeks = Span.Days / 7;

        return weeks;
    }
    public void calculatenetfees(string Studentid)
    {
        DataSet ds = new DataSet();
        try
        {

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_getallchargediscountdetails", con))
                {
                    if (con.State.ToString() != "Open")
                        con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@StudentID", Studentid);
                    ds = new DataSet();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);

                }
            }
            if (ds.Tables[0].Rows[0]["count"].ToString() != "")
            {
                Totalcharges = ds.Tables[1].Rows[0]["count"].ToString();
            }
            if (ds.Tables[1].Rows[0]["count"].ToString() != "")
            {
                Totaldiscount = ds.Tables[0].Rows[0]["count"].ToString();
            }
        }

        catch (Exception ex)
        {

        }

    }

    protected void btndisamount_Click(object sender, EventArgs e)
    {
        objinvoice = new studentparameter();
        //objinvoice.ID = Convert.ToInt32(Decryptdata(hfid.Value));
        
        //if (hfAT.Value == "0")
        //    objinvoice.Totalfees = Convert.ToDecimal(Decryptdata(hftotalamount.Value)) - Convert.ToDecimal(txtdisamount.Text);
        //else
        //    objinvoice.Totalfees = Convert.ToDecimal(Decryptdata(hftotalamount.Value)) - Convert.ToDecimal(txtdisamount.Text);
        //objinvoice.feesbalance = objinvoice.Totalfees - Convert.ToDecimal(Decryptdata(hffeespaid.Value));
        //objstudent = new studentinvoice();
        //objstudent.adddiscount(objinvoice);

        objinvoice.Insertinvoiceajustments(txtdiscountcomment.Text, txtdisamount.Text, Decryptdata(hfid.Value), "1");

        bindinvoice();
    }
    protected void btnchargeamount_Click(object sender, EventArgs e)
    {
        objinvoice = new studentparameter();
        //objinvoice.ID = Convert.ToInt32(Decryptdata(hfid.Value));
       
        //if (hfAT.Value == "0")
        //    objinvoice.Totalfees = Convert.ToDecimal(Decryptdata(hftotalamount.Value)) + Convert.ToDecimal(txtchargeamount.Text);
        //else
        //    objinvoice.Totalfees = Convert.ToDecimal(Decryptdata(hftotalamount.Value)) + Convert.ToDecimal(txtchargeamount.Text);
        //objinvoice.feesbalance = objinvoice.Totalfees - Convert.ToDecimal(Decryptdata(hffeespaid.Value));
        //objstudent = new studentinvoice();
        //objstudent.addcharges(objinvoice);

        objinvoice.Insertinvoiceajustments(txtchargescomment.Text, txtchargeamount.Text, Decryptdata(hfid.Value), "2");

        bindinvoice();
    }
    protected void btnadjustamount_Click(object sender, EventArgs e)
    {
        objinvoice = new studentparameter();
        objinvoice.ID = Convert.ToInt32(Decryptdata(hfid.Value));
        objinvoice.adjustment = Convert.ToDecimal(txtadjustamount.Text);
        objinvoice.adjustmentcomment = txtajdustmentcomment.Text;
        if (ddlAtype.SelectedValue == "0")
        {
            objinvoice.Totalfees = Convert.ToDecimal(Decryptdata(hftotalpackagefees.Value)) + Convert.ToDecimal(Decryptdata(hfIDcharges.Value)) - Convert.ToDecimal(Decryptdata(hfIDdiscount.Value)) + Convert.ToDecimal(txtadjustamount.Text) + Convert.ToDecimal(hfEC.Value) - Convert.ToDecimal(hfED.Value);
            objinvoice.adjustment = Convert.ToDecimal(txtadjustamount.Text);
        }
        else
        {
            objinvoice.adjustment = Convert.ToDecimal(txtadjustamount.Text) * -1;
            objinvoice.Totalfees = Convert.ToDecimal(Decryptdata(hftotalpackagefees.Value)) + Convert.ToDecimal(Decryptdata(hfIDcharges.Value)) - Convert.ToDecimal(Decryptdata(hfIDdiscount.Value)) - Convert.ToDecimal(txtadjustamount.Text) + Convert.ToDecimal(hfEC.Value) - Convert.ToDecimal(hfED.Value);
        }
        objinvoice.adjustmentflag = ddlAtype.SelectedValue;
        objinvoice.feesbalance = objinvoice.Totalfees - Convert.ToDecimal(Decryptdata(hffeespaid.Value));
        objstudent = new studentinvoice();
        objstudent.addadjustment(objinvoice);
        bindinvoice();
    }
    protected void reptinvoice_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            decimal valsuma = 0;
            object container = e.Item.DataItem;
            DataTable dttable = new DataTable();
            objinvoice = new studentparameter();
            dttable = objinvoice.getstudentinvoiceadjust(DataBinder.Eval(container, "recid").ToString());
            Label lblbalance = e.Item.FindControl("lblbalance") as Label;
            if (dttable.Rows.Count > 0)
            {
                Repeater reptadjust = e.Item.FindControl("reptadjust") as Repeater;
                reptadjust.DataSource = dttable;
                reptadjust.DataBind();
                for (int suma = 0; suma < dttable.Rows.Count; suma++)
                {
                    if (dttable.Rows[suma]["type"].ToString() == "2")
                    {
                        valsuma += Convert.ToDecimal(dttable.Rows[suma]["amount"].ToString());
                    }
                    else if (dttable.Rows[suma]["type"].ToString() == "1")
                    {
                        valsuma -= Convert.ToDecimal(dttable.Rows[suma]["amount"].ToString());
                    }
                }

                Label Label1 = e.Item.FindControl("Label1") as Label;
                Label1.Text = (Convert.ToDecimal(DataBinder.Eval(container, "Totalfees").ToString()) + valsuma).ToString("0.00");
               
                lblbalance.Text = (Convert.ToDecimal(Label1.Text) - Convert.ToDecimal(DataBinder.Eval(container, "feespaid").ToString())).ToString("0.00");
                
            }
            else
            {
                if (Convert.ToDecimal(DataBinder.Eval(container, "extradiscount").ToString()) > 0)
                {
                    HtmlTableRow tr_discount = e.Item.FindControl("tr_discount") as HtmlTableRow;
                    tr_discount.Attributes.Remove("style");
                    Image imgED = e.Item.FindControl("imgED") as Image;
                    imgED.ToolTip = DataBinder.Eval(container, "discountcomment").ToString();
                    imgED.Attributes.Add("title", DataBinder.Eval(container, "discountcomment").ToString());
                }
                else
                {
                    HtmlTableRow tr_discount = e.Item.FindControl("tr_discount") as HtmlTableRow;
                    tr_discount.Style.Add("display", "none");
                }
                if (Convert.ToDecimal(DataBinder.Eval(container, "extracharges").ToString()) > 0)
                {
                    HtmlTableRow tr_charges = e.Item.FindControl("tr_charges") as HtmlTableRow;
                    tr_charges.Attributes.Remove("style");
                    Image imgEC = e.Item.FindControl("imgEC") as Image;
                    imgEC.ToolTip = DataBinder.Eval(container, "chargescomment").ToString();
                    imgEC.Attributes.Add("title", DataBinder.Eval(container, "chargescomment").ToString());
                }
                else
                {
                    HtmlTableRow tr_charges = e.Item.FindControl("tr_charges") as HtmlTableRow;
                    tr_charges.Style.Add("display", "none");
                }

            }


            if (Convert.ToDecimal(DataBinder.Eval(container, "adjustment").ToString()) > 0)
            {
                HtmlTableRow tr_adjustment = e.Item.FindControl("tr_adjustment") as HtmlTableRow;
                tr_adjustment.Attributes.Remove("style");
                Image imgEA = e.Item.FindControl("imgEA") as Image;
                imgEA.ToolTip = DataBinder.Eval(container, "adjustmentcomment").ToString();
                imgEA.Attributes.Add("title", DataBinder.Eval(container, "adjustmentcomment").ToString());
            }
            else
            {
                HtmlTableRow tr_adjustment = e.Item.FindControl("tr_adjustment") as HtmlTableRow;
                tr_adjustment.Style.Add("display", "none");
            }
            Image Image1 = e.Item.FindControl("Image1") as Image;
            if (!string.IsNullOrEmpty(DataBinder.Eval(container, "feescomments").ToString()))
            {
                Image1.Visible = true;
                Image1.ToolTip = DataBinder.Eval(container, "feescomments").ToString();
                Image1.Attributes.Add("title", DataBinder.Eval(container, "feescomments").ToString());
            }
            else
                Image1.Visible = false;
            totaldue += Convert.ToDecimal(lblbalance.Text);
            lbltotladue.Text = totaldue.ToString();

            //if (this.InstanceID == "9b36e01e-9d84-45d2-9e53-c4830ef3c145")
            //{
            //    Label lblTPfess = e.Item.FindControl("lblTPfess") as Label;
            //    lblTPfess.Text = (Convert.ToDecimal(DataBinder.Eval(container, "TotalPackagefees").ToString()) - Convert.ToDecimal(DataBinder.Eval(container, "extracharges").ToString())).ToString();
            //}
        }
    }
    protected string encodedvalue(object value)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[value.ToString().Length];
        encode = Encoding.UTF8.GetBytes(value.ToString());
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }
    private string Decryptdata(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }


    protected void reptinvoice_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        Image img = e.Item.FindControl("tbimage") as Image;
        objinvoice = new studentparameter();
        objstudent = new studentinvoice();
        if (e.CommandName == "charges")
        {
            objinvoice.ID = Convert.ToInt32(Decryptdata(img.Attributes["data-id"].ToString()));
            objinvoice.extracharges = 0;
            objinvoice.chargescomments = "";
            if (img.Attributes["adjustmentflag"].ToString() == "0")
                objinvoice.Totalfees = Convert.ToDecimal(Decryptdata(img.Attributes["TotalPackagefees"].ToString())) + Convert.ToDecimal(Decryptdata(img.Attributes["Totalcharges"].ToString())) - Convert.ToDecimal(Decryptdata(img.Attributes["Totaldiscount"].ToString())) - Convert.ToDecimal(img.Attributes["extradiscount"].ToString()) + Convert.ToDecimal(img.Attributes["adjustment"].ToString());
            else
                objinvoice.Totalfees = Convert.ToDecimal(Decryptdata(img.Attributes["TotalPackagefees"].ToString())) + Convert.ToDecimal(Decryptdata(img.Attributes["Totalcharges"].ToString())) - Convert.ToDecimal(Decryptdata(img.Attributes["Totaldiscount"].ToString())) - Convert.ToDecimal(img.Attributes["extradiscount"].ToString()) - Convert.ToDecimal(img.Attributes["adjustment"].ToString());
            objinvoice.feesbalance = objinvoice.Totalfees - Convert.ToDecimal(Decryptdata(img.Attributes["paid-amount"].ToString()));
            objstudent.addcharges(objinvoice);
        }
        else if (e.CommandName == "discount")
        {

            objinvoice.ID = Convert.ToInt32(Decryptdata(img.Attributes["data-id"].ToString()));
            objinvoice.extradiscount = 0;
            objinvoice.discountcomments = "";
            if (img.Attributes["adjustmentflag"].ToString() == "0")
                objinvoice.Totalfees = Convert.ToDecimal(Decryptdata(img.Attributes["TotalPackagefees"].ToString())) + Convert.ToDecimal(Decryptdata(img.Attributes["Totalcharges"].ToString())) - Convert.ToDecimal(Decryptdata(img.Attributes["Totaldiscount"].ToString())) + Convert.ToDecimal(img.Attributes["extracharges"].ToString()) + Convert.ToDecimal(img.Attributes["adjustment"].ToString());
            else
                objinvoice.Totalfees = Convert.ToDecimal(Decryptdata(img.Attributes["TotalPackagefees"].ToString())) + Convert.ToDecimal(Decryptdata(img.Attributes["Totalcharges"].ToString())) - Convert.ToDecimal(Decryptdata(img.Attributes["Totaldiscount"].ToString())) + Convert.ToDecimal(img.Attributes["extracharges"].ToString()) - Convert.ToDecimal(img.Attributes["adjustment"].ToString());
            objinvoice.feesbalance = objinvoice.Totalfees - Convert.ToDecimal(Decryptdata(img.Attributes["paid-amount"].ToString()));

            objstudent.adddiscount(objinvoice);

        }
        else if (e.CommandName == "adjustment")
        {
            objinvoice.ID = Convert.ToInt32(Decryptdata(img.Attributes["data-id"].ToString()));
            objinvoice.adjustment = 0;
            objinvoice.adjustmentcomment = "";
            if (img.Attributes["adjustmentflag"].ToString() == "0")
                objinvoice.Totalfees = Convert.ToDecimal(Decryptdata(img.Attributes["TotalPackagefees"].ToString())) + Convert.ToDecimal(Decryptdata(img.Attributes["Totalcharges"].ToString())) - Convert.ToDecimal(Decryptdata(img.Attributes["Totaldiscount"].ToString())) + Convert.ToDecimal(img.Attributes["extracharges"].ToString()) - Convert.ToDecimal(img.Attributes["extradiscount"].ToString());
            else
                objinvoice.Totalfees = Convert.ToDecimal(Decryptdata(img.Attributes["TotalPackagefees"].ToString())) + Convert.ToDecimal(Decryptdata(img.Attributes["Totalcharges"].ToString())) - Convert.ToDecimal(Decryptdata(img.Attributes["Totaldiscount"].ToString())) + Convert.ToDecimal(img.Attributes["extracharges"].ToString()) - Convert.ToDecimal(img.Attributes["extradiscount"].ToString());

            objinvoice.adjustmentflag = img.Attributes["adjustmentflag"].ToString();
            objinvoice.feesbalance = objinvoice.Totalfees - Convert.ToDecimal(Decryptdata(img.Attributes["paid-amount"].ToString()));
            objstudent.addadjustment(objinvoice);
        }
        else if (e.CommandName == "invoicedel")
        {
            HiddenField hfrecid = e.Item.FindControl("hfrecid") as HiddenField;
            objstudent.deleteinvoice(hfrecid.Value);
        }
        bindinvoice();
    }
    protected void btnprocessed_Click(object sender, EventArgs e)
    {
        try
        {
            objinvoice = new studentparameter();
            objstudent = new studentinvoice();
            decimal payment = Convert.ToDecimal(txtpayment.Text);
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                decimal value = Convert.ToDecimal(txtpayment.Text) - Convert.ToDecimal(lbltotladue.Text);
                if (value < 0)
                    value = 0;

                using (SqlCommand cmd = new SqlCommand("insertbalpayment", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@studentid", Request["studid"]);
                    cmd.Parameters.AddWithValue("@Balanceamount", value);
                    cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                    cmd.ExecuteNonQuery();
                }
            }
            decimal amount = 0;
            foreach (RepeaterItem item in reptinvoice.Items)
            {
                if (payment == 0)
                {
                    break;
                }
                Label lblmonth = item.FindControl("lblmonth") as Label;
                string[] stringSeparators = new string[] { "to" };
                string[] stringSeparatorscom = new string[] { ":" };
                string date = "";
                try
                {
                    date = lblmonth.Text.Split(stringSeparators, StringSplitOptions.None)[1].Trim(' ');
                }
                catch
                {
                    date = lblmonth.Text.Split(stringSeparatorscom, StringSplitOptions.None)[1].Trim(' ');
                }
                Label lblpaid = item.FindControl("lblpaid") as Label;
                Label lblbalance = item.FindControl("lblbalance") as Label;
                HiddenField hfrecid = item.FindControl("hfrecid") as HiddenField;
                objinvoice.ID = Convert.ToInt32(hfrecid.Value);
                decimal balance = Convert.ToDecimal(lblbalance.Text);
                if (payment > balance || payment == balance)
                {
                    amount = payment - balance;
                    payment = amount;
                    objinvoice.paidcomplete = "1";
                    objinvoice.feespaid = Convert.ToDecimal(lblpaid.Text) + balance;
                    objinvoice.feesbalance = 0;
                    objstudent.updatepayments(objinvoice);
                    if (payment == 0)
                        objstudent.insertfeecollection(Request["studid"], txtpayment.Text, Convert.ToDateTime(date), this.InstanceID, ddlpayment.SelectedValue, txtpaycomment.Text, Convert.ToDateTime(txtpaiddate.Text), HttpContext.Current.User.Identity.Name, txtpaidby.Text);
                }
                else
                {
                    amount = balance - payment;
                    objinvoice.feespaid = Convert.ToDecimal(lblpaid.Text) + payment;
                    payment = 0;
                    objinvoice.paidcomplete = "0";
                    objinvoice.feesbalance = amount;
                    objstudent.updatepayments(objinvoice);
                    objstudent.insertfeecollection(Request["studid"], txtpayment.Text, Convert.ToDateTime(date), this.InstanceID, ddlpayment.SelectedValue, txtpaycomment.Text, Convert.ToDateTime(txtpaiddate.Text), HttpContext.Current.User.Identity.Name, txtpaidby.Text);
                }
            }
            bindinvoice();
            txtpayment.Text = "";
            txtpayment.Focus();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void repthistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            object container = e.Item.DataItem;
            Image imgEC = e.Item.FindControl("imgEC") as Image;
            string html = "Payment Method: " + DataBinder.Eval(container, "type").ToString();
            html += "<br />Comments: " + DataBinder.Eval(container, "paymentcomments").ToString();
            imgEC.ToolTip = html;
            imgEC.Attributes.Add("title", html);
            HiddenField hfuserid = e.Item.FindControl("hfuserid") as HiddenField;
            AvaimaUserProfile avaimauser = new AvaimaUserProfile();
            string[] userinfo = avaimauser.Getonlineuserinfo(hfuserid.Value).Split('#');
            Label lblrecivedby = e.Item.FindControl("lblrecivedby") as Label;
            lblrecivedby.Text = userinfo[0] + " " + userinfo[1];
            Label lbldate = e.Item.FindControl("lbldate") as Label;
            lbldate.Text = Convert.ToDateTime(DataBinder.Eval(container, "Paymentdate").ToString()).ToString("ddd dd, MMM yyyy");
        }
    }
    protected void lbtnvoucher_Click(object sender, EventArgs e)
    {
        bindlink();
    }

    private void bindlink()
    {
        studentinvoice objinvoice = new studentinvoice();
        DataTable dt = new DataTable();
        dt = objinvoice.Getfeevouchertemplate(this.InstanceID);
        if (dt.Rows.Count > 0)
        {
            if (dt.Rows[0]["templateid"].ToString() == "1")
                ankvoucher.HRef = "feevouchertemp1.aspx?Studentid=" + Request["studid"] + "&PID=" + Request["PID"] + "&instanceid=" + this.InstanceID;
            else if (dt.Rows[0]["templateid"].ToString() == "2")
                ankvoucher.HRef = "feevouchertemp2.aspx?Studentid=" + Request["studid"] + "&PID=" + Request["PID"] + "&instanceid=" + this.InstanceID;
            else if (dt.Rows[0]["templateid"].ToString() == "99")
                ankvoucher.HRef = "feevouchertemp3.aspx?Studentid=" + Request["studid"] + "&PID=" + Request["PID"] + "&instanceid=" + this.InstanceID;
            else
                ankvoucher.HRef = "feevoucher.aspx?Studentid=" + Request["studid"] + "&PID=" + Request["PID"] + "&instanceid=" + this.InstanceID;
        }
        else
        {
            ankvoucher.HRef = "feevoucher.aspx?Studentid=" + Request["studid"] + "&PID=" + Request["PID"] + "&instanceid=" + this.InstanceID;
        }
    }
    protected void reptadjust_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        objinvoice = new studentparameter();
        if (e.CommandName == "charges")
        {
            HiddenField rowtype = e.Item.FindControl("rowtype") as HiddenField;
            HiddenField rowid = e.Item.FindControl("rowid") as HiddenField;
            Label Label2 = e.Item.FindControl("Label2") as Label;
            HiddenField indd=e.Item.FindControl("indd")as HiddenField;
            if (rowtype.Value == "1")
            {
                objinvoice.DELETEInvoiceAjustment(indd.Value, "0", Label2.Text, rowid.Value);
            }
            else if (rowtype.Value == "2")
            {
                objinvoice.DELETEInvoiceAjustment(indd.Value, "1", Label2.Text, rowid.Value);
            }
            bindinvoice();
        }
    }

    protected void btnaddmethod_Click(object sender, EventArgs e)
    {
        studentinvoice obj = new studentinvoice();
        obj.addPaymentMethod(txtMethodName.Text, this.InstanceID);
        ddlpayment.Items.Clear();
        bindpaymentmethod();
    }
}