﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="collection.aspx.cs" Inherits="collection" %>

<%@ Register Src="~/WebApplication1/PagerControl.ascx" TagPrefix="uc1" TagName="PagerControl" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        .bordercell
        {
            border: 1px solid black;
        }
    </style>
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <link href="_assets/jquery-ui.css" rel="stylesheet" />
    <link href="_assets/jquery.datepick.css" rel="stylesheet" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <script src="_assets/jquery.datepick.js"></script>
    <title></title>
    <asp:PlaceHolder runat="server" ID="phSearchText">
        <script type="text/javascript">
            $(document).ready(function () {

                SearchText();

                function SearchText() {
                    $("#<%=txttext.ClientID %>").autocomplete({
		                source: function (request, response) {
		                    $.ajax({
		                        url: "GetStudent.asmx/Getstudent",
		                        type: "POST",
		                        dataType: "json",
		                        contentType: "application/json; charset=utf-8",
		                        data: "{ 'prefix' : '" + $("#<%=txttext.ClientID %>").val() + "',instanceid:'" + $("#<%=hfinstanceid.ClientID %>").val() + "'}",
				                dataFilter: function (data) { return data; },
				                success: function (data) {
				                    response($.map(data.d, function (item) {
				                        return {
				                            label: item,
				                            value: item
				                        }
				                    }))
				                    //debugger;
				                    resizeiframepage();
				                },
				                error: function (result) {
				                    //alert("Error");
				                }
				            });
				        },
		                minLength: 1,
		                delay: 1000
		            });
                }
		    });

            function resizeiframepage() {
                var iframe = window.parent.document.getElementById("MainContent_ifapp");
                var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
                if (innerDoc.body.offsetHeight) {
                    iframe.height = innerDoc.body.offsetHeight + 400 + "px";
                }
                else if (iframe.Document && iframe.Document.body.scrollHeight) {
                    iframe.style.height = iframe.Document.body.scrollHeight + "px";
                }
            }
        </script>
    </asp:PlaceHolder>
</head>
<body>
    <form id="form1" runat="server">
        <div id="mainContentDiv">
            <div class="breadCrumb">
                <h2>Collection</h2>
            </div>
            <asp:LinkButton ID="lbtnfee" runat="server" Text="Download voucher for all students" OnClick="lbtnfee_Click"></asp:LinkButton><br /><br />
            <p>Search student(s) to view their voucher and collection payments</p>
            <table>
                <tr>
                    <td></td>
                    <td>
                        <asp:TextBox runat="server" ID="txttext" CssClass=" standardField" ClientIDMode="Static"></asp:TextBox></td>
                    <td>
                        <asp:Button runat="server" ID="Search" OnClick="btnsearch_Click" Text="Search" Style="padding: 2px 6px 2px 5px;" />
                        &nbsp;
                        <asp:LinkButton ID="btnshowall" runat="server" Text="View all students" Style="padding: 2px 6px 2px 5px;" OnClick="btnshowall_Click" />

                    </td>
                </tr>
                <%--<tr>
                    <td></td>
                    <td>
                        <asp:DropDownList ID="ddlclass" runat="server" Width="193px" Height="22px"></asp:DropDownList>
                    </td>
                    <td>
                         <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Generate Vouchers" /><br />
                         <a runat="server" id="ankdownload" target="_blank" visible="false">View and Print Vouchers</a>
                    </td>
                </tr>--%>
            </table>
            <asp:Repeater ID="RptrFeesManagement" runat="server" OnItemCommand="RptrFeesManagement_itemcommand" OnItemDataBound="RptrFeesManagement_itemdatabound">
                <HeaderTemplate>
                    <table id="tblconctract" class="smallGrid">
                        <tr class="smallGridHead">
                            <td>Student Roll Number</td>
                            <td>Student Full Name 
                            </td>
                            <td>Path</td>
                            <td style="display: none">Due</td>
                            <td>Modification Date
                            </td>
                            <td></td>
                            <%--<td></td>--%>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="smallGrid">

                        <td>
                            <asp:Label runat="server" ID="lblstudentid" Text='<%# Eval("StudendId") %>'></asp:Label></td>
                        <td>

                            <asp:LinkButton ID="lnktitle" Text='<%# Eval("Title") %>' CommandArgument='<%# Eval("StudendId") %>' CommandName="search" runat="server"></asp:LinkButton>
                            &nbsp;<asp:Label runat="server" ID="lblcount"></asp:Label>
                            <asp:HiddenField ID="hfsid" runat="server" Value='<%# Eval("SIDs") %>' />
                            <asp:HiddenField ID="hfpid" runat="server" Value='<%# Eval("PID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="lnkbreadroot" runat="server" Text="Root"></asp:Label>
                            <asp:Repeater ID="Breadrep" runat="server" OnItemDataBound="Breadrep_ItemDataBound">
                                <ItemTemplate>
                                    /
                    <asp:Label ID="lnkbreadcrum" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                        <td style="display: none">
                            <asp:Label ID="lbldue" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" Text='<%# Eval("ModificationDate") %>' ID="lblmodeficationdate"></asp:Label>

                        </td>
                        <td>
                            <%--<asp:LinkButton ID="lbtngenfeevoucher" runat="server" CommandName="feevoucher" Text="Generate Fee Voucher"></asp:LinkButton>--%>
                            <a id="ankvoch" runat="server" target="_blank">Generate Fee Voucher</a>
                        </td>
                       <%-- <td>
                            <asp:LinkButton ID="lbtnGenInvoice" runat="server" CommandName="Invoice" Text="Generate Custom Invoice"></asp:LinkButton>
                        </td>--%>
                    </tr>
                </ItemTemplate>

                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <div>
                <uc1:PagerControl runat="server" ID="PagerControl" PageSize="20" />
            </div>

            <asp:HiddenField ID="hfinstanceid" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="hftemplate" runat="server" Value="0" />
        </div>
    </form>
</body>
</html>
