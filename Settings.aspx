﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Settings.aspx.cs" Inherits="Settings" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <link href="_assets/jquery-ui.css" rel="stylesheet" />
    <link href="_assets/jquery.datepick.css" rel="stylesheet" />
    <script src="_assets/jquery-1.9.1.js"></script>
    <script src="_assets/jquery-ui.js"></script>
    <script src="_assets/jquery.datepick.js"></script>
    <title></title>
    <script>
        $(document).ready(function () {
            $('.lnkfeesedit').click(function () {
                $('#hffees').val($(this).parent().parent().find('#hf_ID').val());
                $("#txtfeetitle").val($(this).parent().parent().find('#lblfeetitle').text());
                $("#txtfeeamount").val($(this).parent().parent().find('#lblamount').text());

                $("#btnfeesave").val('Update Fee');
                return false;
            });
            $('.lnkOTfeesedit').click(function () {
                $('#hfOTfee').val($(this).parent().parent().find('#hf_ID').val());
                $("#txtOTfeetitle").val($(this).parent().parent().find('#lblfeetitle').text());
                $("#txtOTfeeamount").val($(this).parent().parent().find('#lblamount').text());

                $("#btnOTfee").val('Update Fee');
                return false;
            });
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="mainContentDiv">
            <asp:HiddenField runat="server" ID="hfcurrentinstance" Value="-" />
            <div class="formDiv">
                <table>
                    <tr>
                        <td colspan="2">
                            <div class="formTitleDiv">General Setting</div>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">School Name</td>
                        <td class="formFieldTd">
                            <asp:TextBox ID="txtschool" runat="server" CssClass="standardField"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">School Logo Image:
                        </td>
                        <td class="formFieldTd">
                            <asp:FileUpload ID="fu_logo" runat="server" />
                        </td>
                    </tr>
                    <tr id="tr_logo" runat="server" visible="false">
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">
                            <a id="ank_logo" runat="server" href="#" target="_blank">
                                <asp:Image ID="img_logo" runat="server" Width="200px" /></a>
                             <asp:HiddenField ID="hfrecid" runat="server" Value="0" />
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">New Session Start Month:</td>
                        <td class="formFieldTd">
                            <asp:DropDownList ID="ddlmonth" runat="server">
                                <asp:ListItem Value="1">January</asp:ListItem>
                                <asp:ListItem Value="2">February</asp:ListItem>
                                <asp:ListItem Value="3">March</asp:ListItem>
                                <asp:ListItem Value="4">April</asp:ListItem>
                                <asp:ListItem Value="5">May</asp:ListItem>
                                <asp:ListItem Value="6">June</asp:ListItem>
                                <asp:ListItem Value="7">July</asp:ListItem>
                                <asp:ListItem Value="8">August</asp:ListItem>
                                <asp:ListItem Value="9">September</asp:ListItem>
                                <asp:ListItem Value="10">October</asp:ListItem>
                                <asp:ListItem Value="11">November</asp:ListItem>
                                <asp:ListItem Value="12">December</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Select Currency</td>
                        <td class="formFieldTd">
                            <asp:DropDownList runat="server" ID="ddlcurrencyselect">
                                <asp:ListItem Value="">Select </asp:ListItem>
                                <asp:ListItem Value="USD">USD - US Dollar</asp:ListItem>
                                <asp:ListItem Value="EUR">EUR - Euro</asp:ListItem>
                                <asp:ListItem Value="GBP">GBP - British Pound</asp:ListItem>
                                <asp:ListItem Value="INR">INR - Indian Rupee</asp:ListItem>
                                <asp:ListItem Value="AUD">AUD - Australian Dollar</asp:ListItem>
                                <asp:ListItem Value="CAD">CAD - Canadian Dollar</asp:ListItem>
                                <asp:ListItem Value="AED">AED - Emirati Dirham</asp:ListItem>
                                <asp:ListItem Value="AED">AED - Emirati Dirham</asp:ListItem>
                                <asp:ListItem Value="AFN">AFN - Afghan Afghani</asp:ListItem>
                                <asp:ListItem Value="ALL">ALL - Albanian Lek</asp:ListItem>
                                <asp:ListItem Value="AMD">AMD - Armenian Dram</asp:ListItem>
                                <asp:ListItem Value="ANG">ANG - Dutch Guilder</asp:ListItem>
                                <asp:ListItem Value="AOA">AOA - Angolan Kwanza</asp:ListItem>
                                <asp:ListItem Value="ARS">ARS - Argentine Peso</asp:ListItem>
                                <asp:ListItem Value="AUD">AUD - Australian Dollar</asp:ListItem>
                                <asp:ListItem Value="AWG">AWG - Aruban or Dutch Guilder</asp:ListItem>
                                <asp:ListItem Value="AZN">AZN - Azerbaijani New Manat</asp:ListItem>
                                <asp:ListItem Value="BAM">BAM - Bosnian Convertible Marka</asp:ListItem>
                                <asp:ListItem Value="BBD">BBD - Barbadian or Bajan Dollar</asp:ListItem>
                                <asp:ListItem Value="BDT">BDT - Bangladeshi Taka</asp:ListItem>
                                <asp:ListItem Value="BGN">BGN - Bulgarian Lev</asp:ListItem>
                                <asp:ListItem Value="BHD">BHD - Bahraini Dinar</asp:ListItem>
                                <asp:ListItem Value="BIF">BIF - Burundian Franc</asp:ListItem>
                                <asp:ListItem Value="BMD">BMD - Bermudian Dollar</asp:ListItem>
                                <asp:ListItem Value="BND">BND - Bruneian Dollar</asp:ListItem>
                                <asp:ListItem Value="BOB">BOB - Bolivian Boliviano</asp:ListItem>
                                <asp:ListItem Value="BRL">BRL - Brazilian Real</asp:ListItem>
                                <asp:ListItem Value="BSD">BSD - Bahamian Dollar</asp:ListItem>
                                <asp:ListItem Value="BTN">BTN - Bhutanese Ngultrum</asp:ListItem>
                                <asp:ListItem Value="BWP">BWP - Botswana Pula</asp:ListItem>
                                <asp:ListItem Value="BYR">BYR - Belarusian Ruble</asp:ListItem>
                                <asp:ListItem Value="BZD">BZD - Belizean Dollar</asp:ListItem>
                                <asp:ListItem Value="CAD">CAD - Canadian Dollar</asp:ListItem>
                                <asp:ListItem Value="CDF">CDF - Congolese Franc</asp:ListItem>
                                <asp:ListItem Value="CHF">CHF - Swiss Franc</asp:ListItem>
                                <asp:ListItem Value="CLP">CLP - Chilean Peso</asp:ListItem>
                                <asp:ListItem Value="CNY">CNY - Chinese Yuan Renminbi</asp:ListItem>
                                <asp:ListItem Value="COP">COP - Colombian Peso</asp:ListItem>
                                <asp:ListItem Value="CRC">CRC - Costa Rican Colon</asp:ListItem>
                                <asp:ListItem Value="CUC">CUC - Cuban Convertible Peso</asp:ListItem>
                                <asp:ListItem Value="CUP">CUP - Cuban Peso</asp:ListItem>
                                <asp:ListItem Value="CVE">CVE - Cape Verdean Escudo</asp:ListItem>
                                <asp:ListItem Value="CZK">CZK - Czech Koruna</asp:ListItem>
                                <asp:ListItem Value="DJF">DJF - Djiboutian Franc</asp:ListItem>
                                <asp:ListItem Value="DKK">DKK - Danish Krone</asp:ListItem>
                                <asp:ListItem Value="DOP">DOP - Dominican Peso</asp:ListItem>
                                <asp:ListItem Value="DZD">DZD - Algerian Dinar</asp:ListItem>
                                <asp:ListItem Value="EGP">EGP - Egyptian Pound</asp:ListItem>
                                <asp:ListItem Value="ERN">ERN - Eritrean Nakfa</asp:ListItem>
                                <asp:ListItem Value="ETB">ETB - Ethiopian Birr</asp:ListItem>
                                <asp:ListItem Value="EUR">EUR - Euro</asp:ListItem>
                                <asp:ListItem Value="FJD">FJD - Fijian Dollar</asp:ListItem>
                                <asp:ListItem Value="FKP">FKP - Falkland Island Pound</asp:ListItem>
                                <asp:ListItem Value="GBP">GBP - British Pound</asp:ListItem>
                                <asp:ListItem Value="GEL">GEL - Georgian Lari</asp:ListItem>
                                <asp:ListItem Value="GGP">GGP - Guernsey Pound</asp:ListItem>
                                <asp:ListItem Value="GHS">GHS - Ghanaian Cedi</asp:ListItem>
                                <asp:ListItem Value="GIP">GIP - Gibraltar Pound</asp:ListItem>
                                <asp:ListItem Value="GMD">GMD - Gambian Dalasi</asp:ListItem>
                                <asp:ListItem Value="GNF">GNF - Guinean Franc</asp:ListItem>
                                <asp:ListItem Value="GTQ">GTQ - Guatemalan Quetzal</asp:ListItem>
                                <asp:ListItem Value="GYD">GYD - Guyanese Dollar</asp:ListItem>
                                <asp:ListItem Value="HKD">HKD - Hong Kong Dollar</asp:ListItem>
                                <asp:ListItem Value="HNL">HNL - Honduran Lempira</asp:ListItem>
                                <asp:ListItem Value="HNL">HNL - Croatian Kuna</asp:ListItem>
                                <asp:ListItem Value="HTG">HTG - Haitian Gourde</asp:ListItem>
                                <asp:ListItem Value="HUF">HUF - Hungarian Forint</asp:ListItem>
                                <asp:ListItem Value="IDR">IDR - Indonesian Rupiah</asp:ListItem>
                                <asp:ListItem Value="ILS">ILS - Israeli Shekel</asp:ListItem>
                                <asp:ListItem Value="IMP">IMP - Isle of Man Pound</asp:ListItem>
                                <asp:ListItem Value="INR">INR - Indian Rupee</asp:ListItem>
                                <asp:ListItem Value="IQD">IQD - Iraqi Dinar</asp:ListItem>
                                <asp:ListItem Value="IRR">IRR - Iranian Rial</asp:ListItem>
                                <asp:ListItem Value="ISK">ISK - Icelandic Krona</asp:ListItem>
                                <asp:ListItem Value="JEP">JEP - Jersey Pound</asp:ListItem>
                                <asp:ListItem Value="JMD">JMD - Jamaican Dollar</asp:ListItem>
                                <asp:ListItem Value="JOD">JOD - Jordanian Dinar</asp:ListItem>
                                <asp:ListItem Value="JPY">JPY - Japanese Yen</asp:ListItem>
                                <asp:ListItem Value="KES">KES - Kenyan Shilling</asp:ListItem>
                                <asp:ListItem Value="KGS">KGS - Kyrgyzstani Som</asp:ListItem>
                                <asp:ListItem Value="KHR">KHR - Cambodian Riel</asp:ListItem>
                                <asp:ListItem Value="KMF">KMF - Comoran Franc</asp:ListItem>
                                <asp:ListItem Value="KPW">KPW - North Korean Won</asp:ListItem>
                                <asp:ListItem Value="KRW">KRW - South Korean Won</asp:ListItem>
                                <asp:ListItem Value="KWD">KWD - Kuwaiti Dinar</asp:ListItem>
                                <asp:ListItem Value="KYD">KYD - Caymanian Dollar</asp:ListItem>
                                <asp:ListItem Value="KZT">KZT - Kazakhstani Tenge</asp:ListItem>
                                <asp:ListItem Value="LAK">LAK - Lao or Laotian Kip</asp:ListItem>
                                <asp:ListItem Value="LBP">LBP - Lebanese Pound</asp:ListItem>
                                <asp:ListItem Value="LKR">LKR - Sri Lankan Rupee</asp:ListItem>
                                <asp:ListItem Value="LRD">LRD - Liberian Dollar</asp:ListItem>
                                <asp:ListItem Value="LSL">LSL - Basotho Loti</asp:ListItem>
                                <asp:ListItem Value="LTL">LTL - Lithuanian Litas</asp:ListItem>
                                <asp:ListItem Value="LVL">LVL - Latvian Lat</asp:ListItem>
                                <asp:ListItem Value="LYD">LYD - Libyan Dinar</asp:ListItem>
                                <asp:ListItem Value="MAD">MAD - Moroccan Dirham</asp:ListItem>
                                <asp:ListItem Value="MDL">MDL - Moldovan Leu</asp:ListItem>
                                <asp:ListItem Value="MGA">MGA - Malagasy Ariary</asp:ListItem>
                                <asp:ListItem Value="MKD">MKD - Macedonian Denar</asp:ListItem>
                                <asp:ListItem Value="MMK">MMK - Burmese Kyat</asp:ListItem>
                                <asp:ListItem Value="MNT">MNT - Mongolian Tughrik</asp:ListItem>
                                <asp:ListItem Value="MOP">MOP - Macau Pataca</asp:ListItem>
                                <asp:ListItem Value="MRO">MRO - Mauritanian Ouguiya</asp:ListItem>
                                <asp:ListItem Value="MUR">MUR - Mauritian Rupee</asp:ListItem>
                                <asp:ListItem Value="MVR">MVR - Maldivian Rufiyaa</asp:ListItem>
                                <asp:ListItem Value="MWK">MWK - Malawian Kwacha</asp:ListItem>
                                <asp:ListItem Value="MXN">MXN - Mexican Peso</asp:ListItem>
                                <asp:ListItem Value="MYR">MYR - Malaysian Ringgit</asp:ListItem>
                                <asp:ListItem Value="MZN">MZN - Mozambican Metical</asp:ListItem>
                                <asp:ListItem Value="NAD">NAD - Namibian Dollar</asp:ListItem>
                                <asp:ListItem Value="NGN">NGN - Nigerian Naira</asp:ListItem>
                                <asp:ListItem Value="NIO">NIO - Nicaraguan Cordoba</asp:ListItem>
                                <asp:ListItem Value="NOK">NOK - Norwegian Krone</asp:ListItem>
                                <asp:ListItem Value="NPR">NPR - Nepalese Rupee</asp:ListItem>
                                <asp:ListItem Value="NZD">NZD - New Zealand Dollar</asp:ListItem>
                                <asp:ListItem Value="OMR">OMR - Omani Rial</asp:ListItem>
                                <asp:ListItem Value="PAB">PAB - Panamanian Balboa</asp:ListItem>
                                <asp:ListItem Value="PEN">PEN - Peruvian Nuevo Sol</asp:ListItem>
                                <asp:ListItem Value="PGK">PGK - Papua New Guinean Kina</asp:ListItem>
                                <asp:ListItem Value="PHP">PHP - Philippine Peso</asp:ListItem>
                                <asp:ListItem Value="PKR">PKR - Pakistani Rupee</asp:ListItem>
                                <asp:ListItem Value="PLN">PLN - Polish Zloty</asp:ListItem>
                                <asp:ListItem Value="PYG">PYG - Paraguayan Guarani</asp:ListItem>
                                <asp:ListItem Value="QAR">QAR - Qatari Riyal</asp:ListItem>
                                <asp:ListItem Value="RON">RON - Romanian New Leu</asp:ListItem>
                                <asp:ListItem Value="RSD">RSD - Serbian Dinar</asp:ListItem>
                                <asp:ListItem Value="RUB">RUB - Russian Ruble</asp:ListItem>
                                <asp:ListItem Value="RWF">RWF - Rwandan Franc</asp:ListItem>
                                <asp:ListItem Value="SAR">SAR - Saudi Arabian Riyal</asp:ListItem>
                                <asp:ListItem Value="SBD">SBD - Solomon Islander Dollar</asp:ListItem>
                                <asp:ListItem Value="SCR">SCR - Seychellois Rupee</asp:ListItem>
                                <asp:ListItem Value="SDG">SDG - Sudanese Pound</asp:ListItem>
                                <asp:ListItem Value="SEK">SEK - Swedish Krona</asp:ListItem>
                                <asp:ListItem Value="SEK">SEK - Singapore Dollar</asp:ListItem>
                                <asp:ListItem Value="SHP">SHP - Saint Helenian Pound</asp:ListItem>
                                <asp:ListItem Value="SLL">SLL - Sierra Leonean Leone</asp:ListItem>
                                <asp:ListItem Value="SOS">SOS - Somali Shilling</asp:ListItem>
                                <asp:ListItem Value="SPL">SPL - Seborgan Luigino</asp:ListItem>
                                <asp:ListItem Value="SRD">SRD - Surinamese Dollar</asp:ListItem>
                                <asp:ListItem Value="STD">STD - Sao Tomean Dobra</asp:ListItem>
                                <asp:ListItem Value="SVC">SVC - Salvadoran Colon</asp:ListItem>
                                <asp:ListItem Value="SYP">SYP - Syrian Pound</asp:ListItem>
                                <asp:ListItem Value="SZL">SZL - Swazi Lilangeni</asp:ListItem>
                                <asp:ListItem Value="THB">THB - Thai Baht</asp:ListItem>
                                <asp:ListItem Value="TJS">TJS - Tajikistani Somoni</asp:ListItem>
                                <asp:ListItem Value="TMT">TMT - Turkmenistani Manat</asp:ListItem>
                                <asp:ListItem Value="TND">TND - Tunisian Dinar</asp:ListItem>
                                <asp:ListItem Value="TOP">TOP - Tongan Pa'anga</asp:ListItem>
                                <asp:ListItem Value="TRY">TRY - Turkish Lira</asp:ListItem>
                                <asp:ListItem Value="TTD">TTD - Trinidadian Dollar</asp:ListItem>
                                <asp:ListItem Value="TVD">TVD - Tuvaluan Dollar</asp:ListItem>
                                <asp:ListItem Value="TWD">TWD - Taiwan New Dollar</asp:ListItem>
                                <asp:ListItem Value="TZS">TZS - Tanzanian Shilling</asp:ListItem>
                                <asp:ListItem Value="UAH">UAH - Ukrainian Hryvna</asp:ListItem>
                                <asp:ListItem Value="UGX">UGX - Ugandan Shilling</asp:ListItem>
                                <asp:ListItem Value="USD">USD - US Dollar</asp:ListItem>
                                <asp:ListItem Value="UYU">UYU - Uruguayan Peso</asp:ListItem>
                                <asp:ListItem Value="UZS">UZS - Uzbekistani Som</asp:ListItem>
                                <asp:ListItem Value="VEF">VEF - Venezuelan Bolivar</asp:ListItem>
                                <asp:ListItem Value="VND">VND - Vietnamese Dong</asp:ListItem>
                                <asp:ListItem Value="VUV">VUV - Ni-Vanuatu Vatu</asp:ListItem>
                                <asp:ListItem Value="WST">WST - Samoan Tala</asp:ListItem>
                                <asp:ListItem Value="XAF">XAF - Central African CFA Franc BEAC</asp:ListItem>
                                <asp:ListItem Value="XAG">XAG - Silver Ounce</asp:ListItem>
                                <asp:ListItem Value="XAU">XAU - Gold Ounce</asp:ListItem>
                                <asp:ListItem Value="XBT">XBT - Bitcoin</asp:ListItem>
                                <asp:ListItem Value="XCD">XCD - East Caribbean Dollar</asp:ListItem>
                                <asp:ListItem Value="XDR">XDR - IMF Special Drawing Rights</asp:ListItem>
                                <asp:ListItem Value="XOF">XOF - CFA Franc</asp:ListItem>
                                <asp:ListItem Value="XPD">XPD - Palladium Ounce</asp:ListItem>
                                <asp:ListItem Value="XPF">XPF - CFP Franc</asp:ListItem>
                                <asp:ListItem Value="XPT">XPT - Platinum Ounce</asp:ListItem>
                                <asp:ListItem Value="YER">YER - Yemeni Rial</asp:ListItem>
                                <asp:ListItem Value="ZAR">ZAR - South African Rand</asp:ListItem>
                                <asp:ListItem Value="ZMW">ZMW - Zambian Kwacha</asp:ListItem>
                                <asp:ListItem Value="ZWD">ZWD - Zimbabwean Dollar</asp:ListItem>

                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlcurrencyselect" ErrorMessage="*" ForeColor="Red" InitialValue="0" ValidationGroup="s"></asp:RequiredFieldValidator>
                        </td>

                    </tr>

                    <tr>
                        <td class="formCaptionTd">Late Fee Charges</td>
                        <td class="formFieldTd">
                            <input type="text" id="txtlatefee" runat="server" class=" standardField" /><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtlatefee" ErrorMessage="*" ForeColor="Red" ValidationGroup="s"></asp:RequiredFieldValidator>
                            &nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtlatefee" ErrorMessage="Invalid" ForeColor="Red" ValidationExpression="^[0-9]+$" ValidationGroup="s"></asp:RegularExpressionValidator>

                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Fee Issue Date</td>
                        <td class="formFieldTd">
                            <asp:DropDownList ID="ddlissuedate" runat="server">
                                <asp:ListItem Value="0">Same Day</asp:ListItem>
                                <asp:ListItem Value="1">1 day</asp:ListItem>
                                <asp:ListItem Value="2">2 days</asp:ListItem>
                                <asp:ListItem Value="3">3 days</asp:ListItem>
                                <asp:ListItem Value="4">4 days</asp:ListItem>
                                <asp:ListItem Value="5">5 days</asp:ListItem>
                                <asp:ListItem Value="6">6 days</asp:ListItem>
                                <asp:ListItem Value="7">7 days</asp:ListItem>
                                <asp:ListItem Value="8">8 days</asp:ListItem>
                                <asp:ListItem Value="9">9 days</asp:ListItem>
                                <asp:ListItem Value="10">10 days</asp:ListItem>
                                <asp:ListItem Value="11">11 days</asp:ListItem>
                                <asp:ListItem Value="12">12 days</asp:ListItem>
                                <asp:ListItem Value="13">13 days</asp:ListItem>
                                <asp:ListItem Value="14">14 days</asp:ListItem>
                                <asp:ListItem Value="15">15 days</asp:ListItem>
                                <asp:ListItem Value="20">20 days</asp:ListItem>
                                <asp:ListItem Value="25">25 days</asp:ListItem>
                                <asp:ListItem Value="28">28 days</asp:ListItem>
                                <asp:ListItem Value="45">45 days</asp:ListItem>
                                <asp:ListItem Value="60">60 days</asp:ListItem>
                                <asp:ListItem Value="75">75 days</asp:ListItem>
                                <asp:ListItem Value="90">90 days</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp; Invoice Generated and Emailed on this date before the fee cycle  start.
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Fees Due</td>
                        <td class="formFieldTd">
                            <asp:DropDownList runat="server" ID="ddlddate">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                                <asp:ListItem Value="11">11</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="13">13</asp:ListItem>
                                <asp:ListItem Value="14">14</asp:ListItem>
                                <asp:ListItem Value="15">15</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="17">17</asp:ListItem>
                                <asp:ListItem Value="18">18</asp:ListItem>
                                <asp:ListItem Value="19">19</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="21">21</asp:ListItem>
                                <asp:ListItem Value="22">22</asp:ListItem>
                                <asp:ListItem Value="23">23</asp:ListItem>
                                <asp:ListItem Value="24">24</asp:ListItem>
                                <asp:ListItem Value="25">25</asp:ListItem>
                                <asp:ListItem Value="26">26</asp:ListItem>
                                <asp:ListItem Value="27">27</asp:ListItem>
                                <asp:ListItem Value="28">28</asp:ListItem>
                                <asp:ListItem Value="29">29</asp:ListItem>
                                <asp:ListItem Value="30">30</asp:ListItem>
                                <asp:ListItem Value="31">31</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlddate" ErrorMessage="*" ForeColor="Red" InitialValue="0" ValidationGroup="s"></asp:RequiredFieldValidator>
                            &nbsp;
                        <asp:DropDownList ID="ddltype" runat="server" ClientIDMode="Static">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="Day">Days</asp:ListItem>
                            <asp:ListItem Value="Weeks">Weeks</asp:ListItem>
                            <asp:ListItem Value="Months">Months</asp:ListItem>
                            <asp:ListItem Value="Years">Years</asp:ListItem>
                        </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddltype" ErrorMessage="*" ForeColor="Red" InitialValue="0" ValidationGroup="s"></asp:RequiredFieldValidator>
                            &nbsp;
                        After Package Start Date.
                        </td>

                    </tr>

                    <tr>
                        <td class="formCaptionTd">Late Fees Start</td>
                        <td class="formFieldTd">
                            <asp:DropDownList runat="server" ID="ddldsdate">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                                <asp:ListItem Value="11">11</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="13">13</asp:ListItem>
                                <asp:ListItem Value="14">14</asp:ListItem>
                                <asp:ListItem Value="15">15</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="17">17</asp:ListItem>
                                <asp:ListItem Value="18">18</asp:ListItem>
                                <asp:ListItem Value="19">19</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="21">21</asp:ListItem>
                                <asp:ListItem Value="22">22</asp:ListItem>
                                <asp:ListItem Value="23">23</asp:ListItem>
                                <asp:ListItem Value="24">24</asp:ListItem>
                                <asp:ListItem Value="25">25</asp:ListItem>
                                <asp:ListItem Value="26">26</asp:ListItem>
                                <asp:ListItem Value="27">27</asp:ListItem>
                                <asp:ListItem Value="28">28</asp:ListItem>
                                <asp:ListItem Value="29">29</asp:ListItem>
                                <asp:ListItem Value="30">30</asp:ListItem>
                                <asp:ListItem Value="31">31</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddldsdate" ErrorMessage="*" ForeColor="Red" InitialValue="0" ValidationGroup="s"></asp:RequiredFieldValidator>
                            &nbsp;
                        <asp:DropDownList ID="ddlstype" runat="server" ClientIDMode="Static">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="Day">Days</asp:ListItem>
                            <asp:ListItem Value="Weeks">Weeks</asp:ListItem>
                            <asp:ListItem Value="Months">Months</asp:ListItem>
                            <asp:ListItem Value="Years">Years</asp:ListItem>
                        </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlstype" ErrorMessage="*" ForeColor="Red" InitialValue="0" ValidationGroup="s"></asp:RequiredFieldValidator>
                            &nbsp;
                        After Due Date.
                        </td>

                    </tr>

                    <tr>
                        <td class="formCaptionTd">Tax &nbsp;
                <asp:CheckBox runat="server" ID="chktax" Style="float: right" AutoPostBack="True" OnCheckedChanged="chktax_CheckedChanged" />
                        </td>
                        <td class="formFieldTd">
                            <asp:TextBox runat="server" ID="txttax" CssClass="standardField tax"></asp:TextBox>&nbsp;&nbsp;
                        <asp:DropDownList runat="server" ID="ddltaxtype" CssClass="tax">
                            <asp:ListItem Value="0" Text="% Age"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Fixed"></asp:ListItem>
                        </asp:DropDownList>
                        </td>

                    </tr>


                    <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">
                             <asp:HiddenField ID="hfheader" runat="server" />
                             <asp:HiddenField ID="hfheaderid" runat="server" Value="0" />
                            <asp:HiddenField ID="hflogo" runat="server" />
                            <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" Text="Save" CssClass="standardFormButton" ValidationGroup="s" /><br />
                            <%--<asp:LinkButton ID="lbtnInvoiceset" runat="server" Text="Click" OnClick="lbtnInvoiceset_Click"></asp:LinkButton>
                            for Invoice Setting--%>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="formTitleDiv">Admission Fee</div>

                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Fee Title
                        </td>
                        <td class="formFieldTd">
                            <asp:TextBox ID="txtfeetitle" runat="server" CssClass="standardField" ClientIDMode="Static"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtfeetitle" ErrorMessage="*" ForeColor="Red" ValidationGroup="fees"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Fee Amount
                        </td>
                        <td class="formFieldTd">
                            <asp:TextBox ID="txtfeeamount" runat="server" CssClass="standardField" ClientIDMode="Static"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtfeeamount" ErrorMessage="*" ForeColor="Red" ValidationGroup="fees"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtfeeamount" ErrorMessage="Invalid Amount" ForeColor="Red" ValidationExpression="^[0-9]+([.][0-9]{1,3})?$" ValidationGroup="fees"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">
                            <asp:HiddenField ID="hffees" runat="server" Value="0" ClientIDMode="Static" />
                            <asp:Button runat="server" ID="btnfeesave" Text="Save Fee" ClientIDMode="Static" CssClass="standardFormButton" ValidationGroup="fees" OnClick="btnfeesave_Click" /></td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">


                            <div id="div3" runat="server">
                                <asp:Repeater ID="reptfees" runat="server" OnItemCommand="reptfees_ItemCommand">
                                    <HeaderTemplate>
                                        <table id="tblpackage" class="smallGrid">
                                            <tr class="smallGridHead">
                                                <td style="display: none"></td>
                                                <td>Fee Title 
                                                </td>
                                                <td>Fee Amount</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class="smallGrid">
                                            <td style="display: none">
                                                <asp:HiddenField runat="server" ID="hf_ID" Value='<%# Eval("recid") %>' ClientIDMode="Static" />
                                            </td>
                                            <td>

                                                <asp:Label ID="lblfeetitle" Text='<%# Eval("feetitle") %>' runat="server" ClientIDMode="Static"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblamount" Text='<%# Eval("Feeamount") %>' ClientIDMode="Static"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" ID="lnkedit" Text="Edit" CommandName="edit" CommandArgument='<%# Eval("recid") %>' CssClass="lnkfeesedit" ClientIDMode="Static"></asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" ID="lnkdelete" Text="Delete" OnClientClick="return confirm('Are You sure you want to delete')" CommandName="delete" CommandArgument='<%# Eval("recid") %>'></asp:LinkButton>
                                            </td>

                                        </tr>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>

                                </asp:Repeater>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="formTitleDiv">One Time Fee</div>

                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Fee Title
                        </td>
                        <td class="formFieldTd">
                            <asp:TextBox ID="txtOTfeetitle" runat="server" CssClass="standardField" ClientIDMode="Static"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtOTfeetitle" ErrorMessage="*" ForeColor="Red" ValidationGroup="OTfees"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Fee Amount
                        </td>
                        <td class="formFieldTd">
                            <asp:TextBox ID="txtOTfeeamount" runat="server" CssClass="standardField" ClientIDMode="Static"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtOTfeeamount" ErrorMessage="*" ForeColor="Red" ValidationGroup="OTfees"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtOTfeeamount" ErrorMessage="Invalid Amount" ForeColor="Red" ValidationExpression="^[0-9]+([.][0-9]{1,3})?$" ValidationGroup="OTfees"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">
                            <asp:HiddenField ID="hfOTfee" runat="server" Value="0" ClientIDMode="Static" />
                            <asp:Button runat="server" ID="btnOTfee" Text="Save Fee" ClientIDMode="Static" CssClass="standardFormButton" ValidationGroup="OTfees" OnClick="btnOTfee_Click" /></td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">


                            <div id="div4" runat="server">
                                <asp:Repeater ID="reptOTfee" runat="server" OnItemCommand="reptOTfee_ItemCommand">
                                    <HeaderTemplate>
                                        <table id="tblpackage" class="smallGrid">
                                            <tr class="smallGridHead">
                                                <td style="display: none"></td>
                                                <td>Fee Title 
                                                </td>
                                                <td>Fee Amount</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class="smallGrid">
                                            <td style="display: none">
                                                <asp:HiddenField runat="server" ID="hf_ID" Value='<%# Eval("recid") %>' ClientIDMode="Static" />
                                            </td>
                                            <td>

                                                <asp:Label ID="lblfeetitle" Text='<%# Eval("feetitle") %>' runat="server" ClientIDMode="Static"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblamount" Text='<%# Eval("Feeamount") %>' ClientIDMode="Static"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" ID="lnkedit" Text="Edit" CommandName="edit" CommandArgument='<%# Eval("recid") %>' CssClass="lnkOTfeesedit" ClientIDMode="Static"></asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" ID="lnkdelete" Text="Delete" OnClientClick="return confirm('Are You sure you want to delete')" CommandName="delete" CommandArgument='<%# Eval("recid") %>'></asp:LinkButton>
                                            </td>

                                        </tr>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>

                                </asp:Repeater>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="formTitleDiv">Invoice Setting</div>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Invoice Header Image:
                        </td>
                        <td class="formFieldTd">
                            <asp:FileUpload ID="fu_header" runat="server" />
                        </td>
                    </tr>
                    <tr id="tr_header" runat="server" visible="false">
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">
                            <a id="ank_header" runat="server" href="#" target="_blank">
                                <asp:Image ID="img_header" runat="server" Width="200px" /></a>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Invoice Footer Text:
                        </td>
                        <td class="formFieldTd">
                            <asp:TextBox ID="txtfooter" runat="server" TextMode="MultiLine" CssClass="standardField" Width="364px" Height="169px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">
                            <asp:HiddenField ID="hflogoid" runat="server" Value="0" />
                            <asp:Button runat="server" ID="btninvoice" Text="Save Invoice Setting" ClientIDMode="Static" CssClass="standardFormButton" OnClick="btninvoice_Click"/></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="formTitleDiv">Additional Charges</div>

                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">These charges apply on individual students.</td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Title</td>
                        <td class="formFieldTd">
                            <asp:HiddenField ID="hfcharges" runat="server" Value="0" />
                            <asp:TextBox runat="server" ID="txtcharges" CssClass=" standardField"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtcharges" ErrorMessage="*" ForeColor="Red" ValidationGroup="sac"></asp:RequiredFieldValidator>
                        </td>

                    </tr>

                    <tr>
                        <td class="formCaptionTd">Amount</td>
                        <td class="formFieldTd">
                            <asp:TextBox runat="server" ID="txtChargesamount" CssClass=" standardField"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtChargesamount" ErrorMessage="*" ForeColor="Red" ValidationGroup="sac"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtChargesamount" ErrorMessage="Invalid" ForeColor="Red" ValidationExpression="^[0-9]+([.][0-9]{1,3})?$" ValidationGroup="sac"></asp:RegularExpressionValidator>
                        </td>


                    </tr>

                    <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">
                            <asp:Button runat="server" ID="Button1" Text="Save Additional Charges" CssClass="standardFormButton" ValidationGroup="sac" OnClick="Button1_Click" /></td>


                    </tr>
                    <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">


                            <div id="div1" runat="server">
                                <asp:Repeater ID="reptcharges" runat="server" OnItemCommand="reptcharges_ItemCommand">
                                    <HeaderTemplate>
                                        <table id="tblpackage" class="smallGrid">
                                            <tr class="smallGridHead">
                                                <td style="display: none"></td>
                                                <td>Title 
                                                </td>
                                                <td>Amount Type</td>
                                                <td>Amount
                                                </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class="smallGrid">
                                            <td style="display: none">
                                                <asp:HiddenField runat="server" ID="hf_ID" Value='<%# Eval("ID") %>' />
                                            </td>
                                            <td>

                                                <asp:Label ID="lblcatcheck" Text='<%# Eval("Title") %>' runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblisfixed" Text='<%# Eval("IsFixed") %>'></asp:Label>

                                            </td>


                                            <td>
                                                <asp:Label runat="server" ID="lblamount" Text='<%# Eval("Amount") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" ID="lnkedit" Text="Edit" CommandName="edit" CommandArgument='<%# Eval("ID") %>'></asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" ID="lnkdelete" Text="Delete" OnClientClick="return confirm('Are You sure you want to delete')" CommandName="delete" CommandArgument='<%# Eval("ID") %>'></asp:LinkButton>
                                            </td>

                                        </tr>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>

                                </asp:Repeater>
                                <asp:Label ID="Label1" runat="server" Text="" ForeColor="red"></asp:Label>
                                <div style="width: 300px">
                                    <%--<uc1:PagerControl runat="server" ID="pg_Control" PageSize="20" />--%>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="formTitleDiv">Discount Packages</div>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">These Discount apply on individual students.</td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Title</td>
                        <td class="formFieldTd">
                            <asp:HiddenField ID="hfdiscount" runat="server" Value="0" />
                            <asp:TextBox runat="server" ID="txtchargetitle" CssClass=" standardField"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtchargetitle" ErrorMessage="*" ForeColor="Red" ValidationGroup="sd"></asp:RequiredFieldValidator>
                        </td>

                    </tr>

                    <tr>
                        <td class="formCaptionTd">Amount</td>
                        <td class="formFieldTd">
                            <asp:TextBox runat="server" ID="txtamount" CssClass=" standardField"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtamount" ErrorMessage="*" ForeColor="Red" ValidationGroup="sd"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtamount" ErrorMessage="Invalid" ForeColor="Red" ValidationExpression="^[0-9]+([.][0-9]{1,3})?$" ValidationGroup="sd"></asp:RegularExpressionValidator>
                        </td>


                    </tr>

                    <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">
                            <asp:Button runat="server" ID="btnsubmit" Text="Save Discount" OnClick="btnsubmit_click" CssClass="standardFormButton" ValidationGroup="sd" /></td>


                    </tr>
                    <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">


                            <div id="divresult" runat="server">
                                <asp:Repeater ID="rptrpakagegrid" runat="server" OnItemCommand="rptrpakagegrid_itemcommand">
                                    <HeaderTemplate>
                                        <table id="tblpackage" class="smallGrid">
                                            <tr class="smallGridHead">
                                                <td style="display: none"></td>
                                                <td>Title 
                                                </td>
                                                <td>Amount Type</td>
                                                <td>Amount
                                                </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class="smallGrid">
                                            <td style="display: none">
                                                <asp:HiddenField runat="server" ID="hf_ID" Value='<%# Eval("ID") %>' />
                                            </td>
                                            <td>

                                                <asp:Label ID="lblcatcheck" Text='<%# Eval("Title") %>' runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblisfixed" Text='<%# Eval("IsFixed") %>'></asp:Label>

                                            </td>


                                            <td>
                                                <asp:Label runat="server" ID="lblamount" Text='<%# Eval("Amount") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" ID="lnkedit" Text="Edit" CommandName="edit" CommandArgument='<%# Eval("ID") %>'></asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" ID="lnkdelete" Text="Delete" OnClientClick="return confirm('Are You sure you want to delete')" CommandName="delete" CommandArgument='<%# Eval("ID") %>'></asp:LinkButton>
                                            </td>

                                        </tr>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>

                                </asp:Repeater>
                                <asp:Label ID="lblgriderror" runat="server" Text="" ForeColor="red"></asp:Label>
                                <div style="width: 300px">
                                    <%--<uc1:PagerControl runat="server" ID="pg_Control" PageSize="20" />--%>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="formTitleDiv">Payment Methods</div>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Title</td>
                        <td class="formFieldTd">
                            <asp:TextBox ID="txtPaymethod" runat="server" CssClass="standardField"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*" ControlToValidate="txtPaymethod" ForeColor="Red" ValidationGroup="pay"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">
                            <asp:Button ID="btnpaymentmethod" runat="server" Text="Save Payment Method" CssClass="standardFormButton" OnClick="btnpaymentmethod_Click" ValidationGroup="pay" />
                        </td>

                    </tr>
                    <tr>
                        <td class="formCaptionTd"></td>
                        <td class="formFieldTd">
                            <div id="div2" runat="server">
                                <asp:Repeater ID="reptpayment" runat="server" OnItemCommand="reptpayment_ItemCommand">
                                    <HeaderTemplate>
                                        <table id="tblpackage" class="smallGrid">
                                            <tr class="smallGridHead">
                                                <td>Title 
                                                </td>
                                                <td></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class="smallGrid">
                                            <td>

                                                <asp:Label ID="lblcatcheck" Text='<%# Eval("paymentmethod") %>' runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" ID="lnkdelete" Text="Delete" OnClientClick="return confirm('Are You sure you want to delete')" CommandName="delete" CommandArgument='<%# Eval("recid") %>'></asp:LinkButton>
                                            </td>

                                        </tr>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>

                                </asp:Repeater>

                            </div>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
    </form>
</body>
</html>
