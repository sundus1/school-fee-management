﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class collection : AvaimaThirdpartyTool.AvaimaWebPage
{
    string Totalcharges = "";
    string Totaldiscount = "";
    studentinvoice objstudent;
    studentparameter objinvoice;
    AvaimaStorageUploader objuploader;
    decimal totalfees = 0;
    decimal totaldue = 0;
    PagedDataSource pds = new PagedDataSource();
    private Dictionary<string, string> dict = new Dictionary<string, string>();
    protected void Page_Load(object sender, EventArgs e)
    {
        PagerControl.OnPageIndex_Changed += pg_rptrcontract_OnPageIndex_Changed;
        if (!IsPostBack)
        {
            binddata();
            PagerControl.CurrentPage = 0;
            txttext.Focus();
            //Rptrfillfeesmanagement();
            hfinstanceid.Value = this.InstanceID;
        }
    }
    public void pg_rptrcontract_OnPageIndex_Changed(object sender, EventArgs e)
    {
        Rptrfillfeesmanagement();
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        PagerControl.CurrentPage = 0;
        Rptrfillfeesmanagement();
    }
    public void Rptrfillfeesmanagement()
    {
        try
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_getallsearchrecord", con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@searchstr", txttext.Text);
                    cmd.Parameters.AddWithValue("@instanceid", this.InstanceID);
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);

                    pds.AllowPaging = true;
                    pds.DataSource = dt.DefaultView;
                    pds.CurrentPageIndex = PagerControl.CurrentPage;
                    pds.PageSize = PagerControl.PageSize;
                    PagerControl.TotalRecords = dt.Rows.Count;

                    RptrFeesManagement.DataSource = pds;
                    RptrFeesManagement.DataBind();
                    PagerControl.LoadStatus();
                }
                con.Close();
                txttext.Focus();
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void Breadrep_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.DataItem != null)
        {
            Label lnkbreadcrum = e.Item.FindControl("lnkbreadcrum") as Label;
            //lnkbreadcrum.CommandArgument = DataBinder.Eval(e.Item.DataItem, "Key").ToString();
            //revalue.Value = DataBinder.Eval(e.Item.DataItem, "Key").ToString();
            lnkbreadcrum.Text = DataBinder.Eval(e.Item.DataItem, "Value").ToString();
        }
    }
    protected void RptrFeesManagement_itemcommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //var container = e.Item.DataItem;
            HiddenField PID = e.Item.FindControl("hfpid") as HiddenField;
            HiddenField hfsid = e.Item.FindControl("hfsid") as HiddenField;
            if (e.CommandName == "search")
                this.Redirect("Invoice.aspx?studid=" + hfsid.Value + "&PID=" + PID.Value);
            else if (e.CommandName == "feevoucher")
            {
                if (hftemplate.Value == "1")
                    this.Redirect("feevouchertemp1.aspx?Studentid=" + hfsid.Value + "&PID=" + PID.Value);
                else if (hftemplate.Value == "2")
                    this.Redirect("feevouchertemp2.aspx?Studentid=" + hfsid.Value + "&PID=" + PID.Value);
                else if (hftemplate.Value == "99")
                    this.Redirect("feevouchertemp3.aspx?Studentid=" + hfsid.Value + "&PID=" + PID.Value);
                else
                    this.Redirect("feevoucher.aspx?Studentid=" + hfsid.Value + "&PID=" + PID.Value);
            }
            else if (e.CommandName == "Invoice")
                this.Redirect("CustomInvoice.aspx?stid=" + hfsid.Value + "&PID=" + PID.Value);
            //BindRepeater(e.CommandArgument.ToString());
        }
    }
    public void breadcrum(int id)
    {

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ToString()))
        {
            using (SqlCommand cmd = new SqlCommand("SELECT ID,Title,PID FROM tbl_feesManagement WHERE InstanceId=@InstanceId and ID=@id and IsStudent =0", con))
            {
                cmd.Parameters.AddWithValue("@InstanceId", this.InstanceID);
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    dict.Add(ds.Tables[0].Rows[0]["ID"].ToString(), ds.Tables[0].Rows[0]["Title"].ToString());
                    if (Convert.ToInt32(ds.Tables[0].Rows[0]["PID"]) != 0)
                    {

                        breadcrum(Convert.ToInt32(ds.Tables[0].Rows[0]["PID"]));
                    }

                    //rptrcontract.DataSource = ds.Tables[0];
                    //rptrcontract.DataBind();
                }
            }
        }
    }
    protected void RptrFeesManagement_itemdatabound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
            object container = e.Item.DataItem;

            //Label lbldue = e.Item.FindControl("lbldue") as Label;
            //lbldue.Text = calculatefees(DataBinder.Eval(container, "SIDs").ToString(), DataBinder.Eval(container, "PID").ToString());


            Label lblmodeficationdate = e.Item.FindControl("lblmodeficationdate") as Label;
            lblmodeficationdate.Text = objATZ.GetDate(DataBinder.Eval(container, "ModificationDate").ToString(), HttpContext.Current.User.Identity.Name);
            breadcrum(Convert.ToInt32(DataBinder.Eval(container, "PID").ToString()));
            if (dict.Count > 0)
            {
                var reversedDict = dict.Reverse();
                //rptBreadCrum.DataSource = reversedDict;
                //rptBreadCrum.DataBind();
                Repeater Breadrep = e.Item.FindControl("Breadrep") as Repeater;
                Breadrep.DataSource = reversedDict;
                Breadrep.DataBind();
                dict = new Dictionary<string, string>();
            }


            HtmlAnchor ankvoch = e.Item.FindControl("ankvoch") as HtmlAnchor;
            if (hftemplate.Value == "1")
                ankvoch.HRef = "feevouchertemp1.aspx?Studentid=" + DataBinder.Eval(container, "SIDs").ToString() + "&PID=" + DataBinder.Eval(container, "PID").ToString() + "&instanceid=" + hfinstanceid.Value;
            else if (hftemplate.Value == "2")
                ankvoch.HRef = "feevouchertemp2.aspx?Studentid=" + DataBinder.Eval(container, "SIDs").ToString() + "&PID=" + DataBinder.Eval(container, "PID").ToString() + "&instanceid=" + hfinstanceid.Value;
            else if (hftemplate.Value == "99")
                ankvoch.HRef = "feevouchertemp3.aspx?Studentid=" + DataBinder.Eval(container, "SIDs").ToString() + "&PID=" + DataBinder.Eval(container, "PID").ToString() + "&instanceid=" + hfinstanceid.Value;
            else
                ankvoch.HRef = "feevoucher.aspx?Studentid=" + DataBinder.Eval(container, "SIDs").ToString() + "&PID=" + DataBinder.Eval(container, "PID").ToString() + "&instanceid=" + hfinstanceid.Value;

        }
    }

    public decimal calculatenetfees(decimal totalfees, string Studentid)
    {
        DataSet ds = new DataSet();
        try
        {

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_getallchargediscountdetails", con))
                {
                    if (con.State.ToString() != "Open")
                        con.Open();
                    //cmd.Parameters.AddWithValue("@studentid", hfstudentid.Value);
                    //cmd.Parameters.AddWithValue("@packageid", "");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@StudentID", Studentid);
                    ds = new DataSet();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);

                }
            }
            //-- 0 disc per , 1 disc fix ,2 char per , 3 char fix
            //if (ds.Tables[0].Rows.Count > 0)
            ////if (ds.Tables[0].Rows[0]["count"].ToString() != "")
            ////{
            ////    totalfees = (totalfees * ((100 - Convert.ToDecimal(ds.Tables[0].Rows[0]["count"])) / 100));
            ////}
            //if (ds.Tables[1].Rows.Count > 0)
            if (ds.Tables[0].Rows[0]["count"].ToString() != "")
            {
                totalfees = totalfees - Convert.ToDecimal(ds.Tables[0].Rows[0]["count"]);
            }
            //if (ds.Tables[2].Rows.Count > 0)
            ////if (ds.Tables[2].Rows[0]["count"].ToString() != "")
            ////{
            ////    totalfees = (totalfees * ((100 + Convert.ToDecimal(ds.Tables[2].Rows[0]["count"])) / 100));
            ////}
            //if (ds.Tables[3].Rows.Count > 0)
            if (ds.Tables[1].Rows[0]["count"].ToString() != "")
            {
                totalfees = totalfees + Convert.ToDecimal(ds.Tables[1].Rows[0]["count"]);
            }

            return totalfees;

        }

        catch (Exception ex)
        {
            return 0;
        }

    }
    private void binddata()
    {
        studentinvoice objinvoice = new studentinvoice();
        DataTable dt = new DataTable();
        dt = objinvoice.Getfeevouchertemplate(this.InstanceID);
        if (dt.Rows.Count > 0)
        {
            hftemplate.Value = dt.Rows[0]["templateid"].ToString();               
        }

        //DataTable dtclass = new DataTable();
        //dtclass = objinvoice.GetClasses(this.InstanceID);
        //if (dtclass.Rows.Count > 0)
        //{
        //    ddlclass.DataSource = dtclass;
        //    ddlclass.DataValueField = "ID";
        //    ddlclass.DataTextField = "Title";
        //    ddlclass.DataBind();
        //    ddlclass.Items.Insert(0, new ListItem("All Classes", "0"));
        //}

    }
    public string calculatefees(string studentid,string pid)
    {
        DataSet ds = new DataSet();
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
        {
            string query = "getdatesbyids";
            using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
            {
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@StudentID", studentid);
                da.SelectCommand.Parameters.AddWithValue("@Pid", pid);
                da.Fill(ds);
            }
        }
        decimal totalactualfees = 0; // will hold data after calculation
        int totalmonth = 0; // will hold data after calculation

        decimal netfee = Convert.ToDecimal(ds.Tables[2].Rows[0]["Totalamount"].ToString());
        decimal latefeecharge = Convert.ToDecimal(ds.Tables[2].Rows[0]["LateFeeCharges"].ToString()); // pick up from db
        decimal latefee_expirydate = 0; // pick up from db
        string[] feeduedatearray = ds.Tables[0].Rows[0]["Fee_DueDate"].ToString().Split('#');
        int feeduedate = Convert.ToInt32(feeduedatearray[0]);
        //int f_date = Convert.ToInt32(feeduedate.ToString("dd")); // extracting just date from due date

        //int feeexpiredate = Convert.ToInt32(ds.Tables[0].Rows[0]["LateFeesEndDate"]);
        //int fx_date = Convert.ToInt32(feeexpiredate.ToString("dd")); // extracting date from expiry date
        DateTime todaydate = DateTime.Now;
        DateTime lastsubmitdate = Convert.ToDateTime(ds.Tables[1].Rows[0]["SubmitDate"]);
        int month = Convert.ToInt32(todaydate.ToString("MM"));
        int year = Convert.ToInt32(todaydate.ToString("yyyy"));
        int date = Convert.ToInt32(todaydate.ToString("dd"));

        //-- today date first
        if (todaydate.Year > lastsubmitdate.Year)
        {
            totalmonth = ((todaydate.Year - lastsubmitdate.Year) * 12) + todaydate.Month - lastsubmitdate.Month;
            totalactualfees = calculatenetfees(netfee, studentid) * totalmonth;

            //if (date > feeexpiredate) // if expiry date passed and then late fee date also passed
            //{
            //    decimal latecharge = latefeecharge + latefee_expirydate;
            //    totalactualfees += latecharge;
            //}

            if (date > feeduedate)
            {
                totalactualfees += latefeecharge;


            } // if only expiry date passed

            if (totalmonth > 1)
            {
                //totalactualfees = totalmonth * totalactualfees;
                totalactualfees += (totalmonth - 1) * latefeecharge;
            }

        }
        else if (date <= feeduedate && lastsubmitdate.Month != month) // fees on time
            totalactualfees = calculatenetfees(netfee,studentid);
        else // if candidate is late
        {
            //if (date > feeexpiredate && lastsubmitdate.Month != month) // if expiry date passed and then late fee date also passed
            //    totalactualfees = calculatenetfees(netfee) + latefeecharge + latefee_expirydate;
            if (date > feeduedate && lastsubmitdate.Month != month)
            {
                totalactualfees = calculatenetfees(netfee,studentid) + latefeecharge;

            } // if only expiry date passed



        }
        if (todaydate.Year <= lastsubmitdate.Year)
        {
            int monthcount = (month) - lastsubmitdate.Month;
            if (monthcount > 0)
            {
                totalactualfees = monthcount * totalactualfees;
                totalactualfees += (monthcount - 1) * latefeecharge;
            }
        }
        if (lastsubmitdate.Day < date)
        {
            TimeSpan span = todaydate.Subtract(lastsubmitdate);
            totalactualfees = ((calculatenetfees(netfee, studentid) * 12) / 365) * Convert.ToDecimal(span.Days);
        }
        //divMessage.InnerHtml = "<h2>Your Total Amount to be submitted is:" + ds.Tables[0].Rows[0]["Currency"] + " " + lblTotalAmount.Text + "</h2>";
        if (totalactualfees > 0)
        {
            if (ds.Tables[0].Rows[0]["Istax"].ToString() == "True")
            {
                if (ds.Tables[0].Rows[0]["Isfixed"].ToString() == "0") // %age     ,Amount
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["Amount"])))
                    {
                        totalactualfees = (totalactualfees * ((100 + Convert.ToDecimal(ds.Tables[0].Rows[0]["Amount"])) / 100));
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["Amount"])))
                    {
                        totalactualfees = totalactualfees + Convert.ToDecimal(ds.Tables[0].Rows[0]["Amount"]);
                    }
                }
            }
        }
        string hfbalamount="";
        if (ds.Tables[3].Rows.Count > 0)
        {
            hfbalamount = ds.Tables[3].Rows[0]["Balanceamount"].ToString();
        }
        if (!String.IsNullOrEmpty(hfbalamount))
            if (Convert.ToDecimal(hfbalamount) < 0)
                totalactualfees = totalactualfees + Math.Abs(Convert.ToDecimal(hfbalamount));
            else
                totalactualfees = totalactualfees - Math.Abs(Convert.ToDecimal(hfbalamount));

        return Math.Round(totalactualfees).ToString();

    }
    protected void btnshowall_Click(object sender, EventArgs e)
    {
        PagerControl.CurrentPage = 0;
        txttext.Text = "";
        Rptrfillfeesmanagement();
    }

    /*Invoice Code*/
#region Invoice
    //private StringBuilder SendInvoice(string Studentid, string InstanceID, string PID, string parentemail, string schoolname)
    //{
    //    DataSet ds = new DataSet();
    //    AvaimaEmailAPI objemail = new AvaimaEmailAPI();
    //    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
    //    {
    //        string query = "feesvoucher";
    //        using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
    //        {
    //            da.SelectCommand.CommandType = CommandType.StoredProcedure;
    //            da.SelectCommand.Parameters.AddWithValue("@instanceid", InstanceID);
    //            da.SelectCommand.Parameters.AddWithValue("@studentid", Studentid);
    //            da.SelectCommand.Parameters.AddWithValue("@PID", PID);
    //            da.Fill(ds);
    //        }
    //    }
    //    string headerfile = "";
    //    string Footer = "";
    //    string lblclass = "";
    //    string lblissuedate = "";
    //    string lblregno = "";
    //    string lblstudentname = "";
    //    string lblfathername = "";
    //    string lblslipno = "";
    //    string lblsession = "";
    //    string lblperiod = "";
    //    string lblduedate = "";
    //    string lbldueamount = "";
    //    string fees = "0";
    //    string charges = "0";
    //    string Discount = "0";
    //    string ecomment = "";
    //    string ecommentamount = "0";
    //    string extradiscount = "";
    //    string extradiscountamount = "0";
    //    string adjustmentcomment = "";
    //    string adjustmentcommentamount = "0";
    //    decimal balanceval = 0;
    //    if (ds.Tables[0].Rows.Count > 0)
    //    {
    //        objuploader = new AvaimaStorageUploader();
    //        headerfile = objuploader.Singlefilelink(InstanceID, ds.Tables[0].Rows[0]["headerfile"].ToString());
    //        Footer = ds.Tables[0].Rows[0]["footer"].ToString();
    //    }
    //    if (ds.Tables[2].Rows.Count > 0)
    //    {
    //        lblclass = ds.Tables[2].Rows[0]["title"].ToString();
    //    }
    //    if (ds.Tables[1].Rows.Count > 0)
    //    {
    //        lblissuedate = Convert.ToDateTime(ds.Tables[1].Rows[0]["issuedate"].ToString()).ToString("dd-MMMM-yyyy");
    //        lblregno = ds.Tables[1].Rows[0]["Regno"].ToString();
    //        lblstudentname = ds.Tables[1].Rows[0]["studentname"].ToString();
    //        lblfathername = ds.Tables[1].Rows[0]["ParentName"].ToString();
    //    }
    //    if (ds.Tables[3].Rows.Count > 0)
    //    {
    //        lblslipno = ds.Tables[3].Rows[0]["recid"].ToString();

    //        DateTime dtstart = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
    //        if (ds.Tables[0].Rows.Count > 0 && dtstart.Month >= Convert.ToInt32(ds.Tables[0].Rows[0]["month"].ToString()))
    //        {
    //            lblsession = dtstart.Year + "-" + (dtstart.Year + 1);
    //        }
    //        else
    //        {
    //            lblsession = (dtstart.Year - 1) + "-" + dtstart.Year;
    //        }

    //        lblperiod = dtstart.ToString("MMM yyyy");

    //        fees = ds.Tables[3].Rows[0]["TotalPackagefees"].ToString();
    //        balanceval = Convert.ToDecimal(ds.Tables[4].Rows[0]["balance"].ToString()) - Convert.ToDecimal(ds.Tables[3].Rows[0]["Totalfees"].ToString());
    //        totalfees = Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalPackagefees"].ToString());
    //        if (Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString()) > 0)
    //        {
    //            charges = ds.Tables[3].Rows[0]["TotalCharges"].ToString();
    //            totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString());
    //        }
    //        if (Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString()) > 0)
    //        {
    //            Discount = ds.Tables[3].Rows[0]["Totaldiscount"].ToString();
    //            totalfees -= Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString());
    //        }
    //        if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString()) > 0)
    //        {
    //            ecomment = ds.Tables[3].Rows[0]["chargescomment"].ToString();
    //            ecommentamount = ds.Tables[3].Rows[0]["extracharges"].ToString();
    //            totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString());
    //        }
    //        if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString()) > 0)
    //        {
    //            extradiscount = ds.Tables[3].Rows[0]["discountcomment"].ToString();
    //            extradiscountamount = ds.Tables[3].Rows[0]["extradiscount"].ToString();
    //            totalfees -= Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString());
    //        }
    //        if (Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString()) > 0)
    //        {
    //            adjustmentcomment = ds.Tables[3].Rows[0]["adjustmentcomment"].ToString();
    //            adjustmentcommentamount = ds.Tables[3].Rows[0]["adjustment"].ToString();
    //            totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString());
    //        }
    //        totalfees = totalfees + balanceval;
    //    }
    //    if (ds.Tables[5].Rows.Count > 0)
    //    {
    //        DateTime enddate = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
    //        string[] duedate = ds.Tables[5].Rows[0]["Fee_DueDate"].ToString().Split('#');
    //        string[] afterduedate = ds.Tables[5].Rows[0]["LateFeesStartDate"].ToString().Split('#');
    //        enddate = enddate.AddDays(Convert.ToDouble(duedate[0]));
    //        enddate = enddate.AddDays(Convert.ToDouble(afterduedate[0]));
    //        lblduedate = enddate.ToString("dd-MMM-yyyy");

    //        decimal latefee = 0;
    //        try
    //        {
    //            latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["LateFeeCharges"].ToString());
    //        }
    //        catch
    //        {
    //            latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["latefees"].ToString());
    //        }
    //        latefee = latefee + totalfees;
    //        lbldueamount = latefee.ToString();
    //    }


    //    StringBuilder HTML = new StringBuilder();
    //    HTML.AppendLine("<div style=\" width: 285px; min-height: 300px; border-right: 1px dotted #000; float: left; padding: 0 5px \"><div style=\"height: auto;\"><img src=\"" + headerfile + "\" alt=\"img\" style=\"width:100%;\" /></div>");
    //    HTML.AppendLine("<div style=\"height: 25px; border-top: 2px solid #000; border-bottom: 1px solid #000; padding: 0 8px; \"><h2 style=\"float: left; font-size: 15px; padding: 0px; margin: 0px; line-height: 27px; color: #666;\"><span>Slip. ID </span><span>" + lblslipno + "</span></h2><h2 style=\"float: right; font-size: 14px; padding: 0px; margin: 0px; line-height: 26px; text-align: center; color: #666;\">Parent COPY</h2>");
    //    HTML.AppendLine("<div style=\" clear: both;\"></div><table width=\"100%\" border=\"0\" style=\"margin: 7px 0 0 0; font-size: 14px; float: left;\"><tr><td style=\"text-align: left; width: 82px;\" scope=\"col\"><b>Class:</b>" + lblclass + "<br />");
    //    HTML.AppendLine("<b>Issue Date:</b> " + lblissuedate + "<br /><b>Reg. No.:</b> " + lblregno + "<br />");
    //    HTML.AppendLine("<b>Session:</b> " + lblsession + "<br /><b>G. R. No.:</b> " + lblregno + "<br /><b>Period:</b> " + lblperiod + "<br />");
    //    HTML.AppendLine("</td></tr></table><table width=\"100%\" border=\"0\" style=\"margin: 7px 0 0 0; font-size: 14px; float: left;\"><tr>");
    //    HTML.AppendLine("<td style=\"width: 100% !important; margin: 10px 0 0 0 !important; padding: 10px 3px 0 !important; font-size: 13px;\" colspan=\"2\">Student&acute;s Name: <b>" + lblstudentname + "</b></td></tr>");
    //    HTML.AppendLine("<tr><td style=\"width: 100% !important; margin: 10px 0 0 0 !important; padding: 10px 3px 0 !important; font-size: 13px;\" colspan=\"2\">Father&acute;s Name: " + lblfathername + "</td></tr></table></div>");
    //    HTML.AppendLine("<div style=\" clear: both;\"></div><div style=\" height: 25px; border-top: 2px solid #000; padding: 0 8px; \"><h2 style=\"float: left; font-size: 15px; padding: 0px; margin: 0px; line-height: 27px;\"><span>Fee Details</span></h2></div>");
    //    HTML.AppendLine("<table width=\"100% \" border=\"0\" cellspacing=\"0\" style=\"font-size: 14px;\">");
    //    HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Tuition  Fee " + lblperiod + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px; \">" + fees + "</td></tr>");
    //    if (charges != "0")
    //        HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Total Individual Charges</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + charges + "</td></tr>");
    //    if (Discount != "0")
    //        HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Total Individual Discount</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + Discount + "</td></tr>");
    //    if (ecommentamount != "0")
    //        HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + ecomment + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + ecommentamount + "</td></tr>");
    //    if (extradiscountamount != "0")
    //        HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + extradiscount + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + extradiscountamount + "</td></tr>");
    //    if (adjustmentcommentamount != "0")
    //        HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + adjustmentcomment + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + adjustmentcommentamount + "</td></tr>");

    //    HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Previous Balance</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + balanceval.ToString() + "</td></tr>");
    //    HTML.AppendLine("<tr style=\"background: #d9d9d9;\"><td style=\" border-bottom: 2px solid #000; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\"><b>Total Due</b></td><td style=\" border-bottom: 2px solid #000; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\"><b>" + totalfees.ToString() + "</b></td></tr>");
    //    HTML.AppendLine("</table>");
    //    HTML.AppendLine("<table style=\" font-size: 13px; margin: 10px 0 0 0; padding: 0 3px;\" width=\"100%\" border=\"0\">");
    //    HTML.AppendLine("<tr><td colspan=\"2\">Due Date: <b>" + lblduedate + "</b></td></tr>");
    //    HTML.AppendLine("<tr><td>Amount payable after due date</td><td style=\"text-align: right;\">" + lbldueamount + "</td></tr></table>");
    //    HTML.AppendLine("<br /><br /><br /><table width=\"100%\" border=\"0\" style=\"font-size: 14px; font-weight: bold;\"");
    //    HTML.AppendLine("<tr><td style=\"border-top: 1px solid #000;\">Cashier</td><td style=\"border-top: 1px solid #000;\">Officer</td></tr></table>");
    //    HTML.AppendLine("<div style=\"font-size: 11px; color: #b5b5b5; font-weight: lighter; padding: 11px 5px 0px 5px;\">" + Footer + "</div></div>");

    //    /***********/

    //    HTML.AppendLine("<div style=\" width: 285px; min-height: 300px; border-right: 1px dotted #000; float: left; padding: 0 5px \"><div style=\"height: auto;\"><img src=\"" + headerfile + "\" alt=\"img\" style=\"width:100%;\" /></div>");
    //    HTML.AppendLine("<div style=\"height: 25px; border-top: 2px solid #000; border-bottom: 1px solid #000; padding: 0 8px; \"><h2 style=\"float: left; font-size: 15px; padding: 0px; margin: 0px; line-height: 27px; color: #666;\"><span>Slip. ID </span><span>" + lblslipno + "</span></h2><h2 style=\"float: right; font-size: 14px; padding: 0px; margin: 0px; line-height: 26px; text-align: center; color: #666;\">SCHOOL COPY</h2>");
    //    HTML.AppendLine("<div style=\" clear: both;\"></div><table width=\"100%\" border=\"0\" style=\"margin: 7px 0 0 0; font-size: 14px; float: left;\"><tr><td style=\"text-align: left; width: 82px;\" scope=\"col\"><b>Class:</b>" + lblclass + "<br />");
    //    HTML.AppendLine("<b>Issue Date:</b> " + lblissuedate + "<br /><b>Reg. No.:</b> " + lblregno + "<br />");
    //    HTML.AppendLine("<b>Session:</b> " + lblsession + "<br /><b>G. R. No.:</b> " + lblregno + "<br /><b>Period:</b> " + lblperiod + "<br />");
    //    HTML.AppendLine("</td></tr></table><table width=\"100%\" border=\"0\" style=\"margin: 7px 0 0 0; font-size: 14px; float: left;\"><tr>");
    //    HTML.AppendLine("<td style=\"width: 100% !important; margin: 10px 0 0 0 !important; padding: 10px 3px 0 !important; font-size: 13px;\" colspan=\"2\">Student&acute;s Name: <b>" + lblstudentname + "</b></td></tr>");
    //    HTML.AppendLine("<tr><td style=\"width: 100% !important; margin: 10px 0 0 0 !important; padding: 10px 3px 0 !important; font-size: 13px;\" colspan=\"2\">Father&acute;s Name: " + lblfathername + "</td></tr></table></div>");
    //    HTML.AppendLine("<div style=\" clear: both;\"></div><div style=\" height: 25px; border-top: 2px solid #000; padding: 0 8px; \"><h2 style=\"float: left; font-size: 15px; padding: 0px; margin: 0px; line-height: 27px;\"><span>Fee Details</span></h2></div>");
    //    HTML.AppendLine("<table width=\"100% \" border=\"0\" cellspacing=\"0\" style=\"font-size: 14px;\">");
    //    HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Tuition  Fee " + lblperiod + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px; \">" + fees + "</td></tr>");
    //    if (charges != "0")
    //        HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Total Individual Charges</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + charges + "</td></tr>");
    //    if (Discount != "0")
    //        HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Total Individual Discount</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + Discount + "</td></tr>");
    //    if (ecommentamount != "0")
    //        HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + ecomment + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + ecommentamount + "</td></tr>");
    //    if (extradiscountamount != "0")
    //        HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + extradiscount + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + extradiscountamount + "</td></tr>");
    //    if (adjustmentcommentamount != "0")
    //        HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + adjustmentcomment + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + adjustmentcommentamount + "</td></tr>");

    //    HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Previous Balance</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + balanceval.ToString() + "</td></tr>");
    //    HTML.AppendLine("<tr style=\"background: #d9d9d9;\"><td style=\" border-bottom: 2px solid #000; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\"><b>Total Due</b></td><td style=\" border-bottom: 2px solid #000; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\"><b>" + totalfees.ToString() + "</b></td></tr>");
    //    HTML.AppendLine("</table>");
    //    HTML.AppendLine("<table style=\" font-size: 13px; margin: 10px 0 0 0; padding: 0 3px;\" width=\"100%\" border=\"0\">");
    //    HTML.AppendLine("<tr><td colspan=\"2\">Due Date: <b>" + lblduedate + "</b></td></tr>");
    //    HTML.AppendLine("<tr><td>Amount payable after due date</td><td style=\"text-align: right;\">" + lbldueamount + "</td></tr></table>");
    //    HTML.AppendLine("<br /><br /><br /><table width=\"100%\" border=\"0\" style=\"font-size: 14px; font-weight: bold;\"");
    //    HTML.AppendLine("<tr><td style=\"border-top: 1px solid #000;\">Cashier</td><td style=\"border-top: 1px solid #000;\">Officer</td></tr></table>");
    //    HTML.AppendLine("<div style=\"font-size: 11px; color: #b5b5b5; font-weight: lighter; padding: 11px 5px 0px 5px;\">" + Footer + "</div></div>");


    //    /***********/

    //    HTML.AppendLine("<div style=\" width: 285px; min-height: 300px;  float: left; padding: 0 5px \"><div style=\"height: auto;\"><img src=\"" + headerfile + "\" alt=\"img\" style=\"width:100%;\" /></div>");
    //    HTML.AppendLine("<div style=\"height: 25px; border-top: 2px solid #000; border-bottom: 1px solid #000; padding: 0 8px; \"><h2 style=\"float: left; font-size: 15px; padding: 0px; margin: 0px; line-height: 27px; color: #666;\"><span>Slip. ID </span><span>" + lblslipno + "</span></h2><h2 style=\"float: right; font-size: 14px; padding: 0px; margin: 0px; line-height: 26px; text-align: center; color: #666;\">BANK COPY</h2>");
    //    HTML.AppendLine("<div style=\" clear: both;\"></div><table width=\"100%\" border=\"0\" style=\"margin: 7px 0 0 0; font-size: 14px; float: left;\"><tr><td style=\"text-align: left; width: 82px;\" scope=\"col\"><b>Class:</b>" + lblclass + "<br />");
    //    HTML.AppendLine("<b>Issue Date:</b> " + lblissuedate + "<br /><b>Reg. No.:</b> " + lblregno + "<br />");
    //    HTML.AppendLine("<b>Session:</b> " + lblsession + "<br /><b>G. R. No.:</b> " + lblregno + "<br /><b>Period:</b> " + lblperiod + "<br />");
    //    HTML.AppendLine("</td></tr></table><table width=\"100%\" border=\"0\" style=\"margin: 7px 0 0 0; font-size: 14px; float: left;\"><tr>");
    //    HTML.AppendLine("<td style=\"width: 100% !important; margin: 10px 0 0 0 !important; padding: 10px 3px 0 !important; font-size: 13px;\" colspan=\"2\">Student&acute;s Name: <b>" + lblstudentname + "</b></td></tr>");
    //    HTML.AppendLine("<tr><td style=\"width: 100% !important; margin: 10px 0 0 0 !important; padding: 10px 3px 0 !important; font-size: 13px;\" colspan=\"2\">Father&acute;s Name: " + lblfathername + "</td></tr></table></div>");
    //    HTML.AppendLine("<div style=\" clear: both;\"></div><div style=\" height: 25px; border-top: 2px solid #000; padding: 0 8px; \"><h2 style=\"float: left; font-size: 15px; padding: 0px; margin: 0px; line-height: 27px;\"><span>Fee Details</span></h2></div>");
    //    HTML.AppendLine("<table width=\"100% \" border=\"0\" cellspacing=\"0\" style=\"font-size: 14px;\">");
    //    HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Tuition  Fee " + lblperiod + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px; \">" + fees + "</td></tr>");
    //    if (charges != "0")
    //        HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Total Individual Charges</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + charges + "</td></tr>");
    //    if (Discount != "0")
    //        HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Total Individual Discount</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + Discount + "</td></tr>");
    //    if (ecommentamount != "0")
    //        HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + ecomment + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + ecommentamount + "</td></tr>");
    //    if (extradiscountamount != "0")
    //        HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + extradiscount + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + extradiscountamount + "</td></tr>");
    //    if (adjustmentcommentamount != "0")
    //        HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + adjustmentcomment + "</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + adjustmentcommentamount + "</td></tr>");

    //    HTML.AppendLine("<tr><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">Previous Balance</td><td style=\" border-bottom: 1px solid #000; padding: 2px 6px;padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\">" + balanceval.ToString() + "</td></tr>");
    //    HTML.AppendLine("<tr style=\"background: #d9d9d9;\"><td style=\" border-bottom: 2px solid #000; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\"><b>Total Due</b></td><td style=\" border-bottom: 2px solid #000; padding-top: 3px; padding-bottom: 3px; padding-left: 11px;\"><b>" + totalfees.ToString() + "</b></td></tr>");
    //    HTML.AppendLine("</table>");
    //    HTML.AppendLine("<table style=\" font-size: 13px; margin: 10px 0 0 0; padding: 0 3px;\" width=\"100%\" border=\"0\">");
    //    HTML.AppendLine("<tr><td colspan=\"2\">Due Date: <b>" + lblduedate + "</b></td></tr>");
    //    HTML.AppendLine("<tr><td>Amount payable after due date</td><td style=\"text-align: right;\">" + lbldueamount + "</td></tr></table>");
    //    HTML.AppendLine("<br /><br /><br /><table width=\"100%\" border=\"0\" style=\"font-size: 14px; font-weight: bold;\"");
    //    HTML.AppendLine("<tr><td style=\"border-top: 1px solid #000;\">Cashier</td><td style=\"border-top: 1px solid #000;\">Officer</td></tr></table>");
    //    HTML.AppendLine("<div style=\"font-size: 11px; color: #b5b5b5; font-weight: lighter; padding: 11px 5px 0px 5px;\">" + Footer + "</div></div>");
    //    HTML.AppendLine("<div class=\"pageBreak\"></div> <br />");
    //    return HTML;
    //}
    //protected void Button1_Click(object sender, EventArgs e)
    //{
    //    StringBuilder strHTMLContent = new StringBuilder();
    //    strHTMLContent.AppendLine("<style>@media print {.pageBreak {page-break-before: always;}} .Tabletr{height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;}</style>");
    //    DataTable dtstudent = new DataTable();
    //    objstudent = new studentinvoice();
    //    if (ddlclass.SelectedValue != "0")
    //        dtstudent = objstudent.GetStudents(ddlclass.SelectedValue);
    //    else
    //        dtstudent = objstudent.GetStudentsbyinstanceid(this.InstanceID);

    //    if (dtstudent.Rows.Count > 0)
    //    {
    //        if (hftemplate.Value == "2")
    //        {
    //            strHTMLContent.AppendLine("<body style=\"font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #000;\">");
    //        }
    //        else if (hftemplate.Value == "1")
    //            strHTMLContent.AppendLine("<body style=\"font-size: 11px !important; color: #000;\">");

    //        for (int st = 0; st < dtstudent.Rows.Count; st++)
    //        {
    //            if (hftemplate.Value == "1")
    //            {
    //                string htmll = SendInvoicetemp1(dtstudent.Rows[st]["StudendId"].ToString(), this.InstanceID, dtstudent.Rows[st]["PID"].ToString(), "", "").ToString();
    //                if (!String.IsNullOrEmpty(htmll))
    //                    strHTMLContent.AppendLine(htmll);
    //            }
    //            else if (hftemplate.Value == "2")
    //            {
    //                strHTMLContent.AppendLine(SendInvoicetemp2(dtstudent.Rows[st]["StudendId"].ToString(), this.InstanceID, dtstudent.Rows[st]["PID"].ToString(), "", "").ToString());
    //            }
    //            else
    //                strHTMLContent.AppendLine(SendInvoice(dtstudent.Rows[st]["StudendId"].ToString(), this.InstanceID, dtstudent.Rows[st]["PID"].ToString(), "", "").ToString());
    //        }
    //        if (hftemplate.Value == "1" || hftemplate.Value == "2")
    //        {
    //            strHTMLContent.AppendLine("</body>");
    //        }
    //    }
    //    AvaimaStorageUploader objuploader = new AvaimaStorageUploader();
    //    objuploader.insertfileuploaded("Feevoucher", "html", "8ee08d08-68ab-4415-8b50-9fa722151856", HttpContext.Current.User.Identity.Name, this.InstanceID, this.InstanceID, Encoding.ASCII.GetBytes(strHTMLContent.ToString()));
    //    string path = objuploader.Singlefilelink(this.InstanceID, "Feevoucher.html");
    //    ankdownload.HRef = path;
    //    ankdownload.Visible = true;
    //    ScriptManager.RegisterStartupScript(this, GetType(), "savedrecords", "alert('Save Successfully!!')", true);
    //    //ICDMComm cdm = new ICDMComm();
    //    //cdm.SaveFile(Server.MapPath("~\\Excelfile\\") + FileUpload1.FileName, FileUpload1.FileBytes, "aaaara1982");
    //}
    //private StringBuilder SendInvoicetemp1(string Studentid, string InstanceID, string PID, string parentemail, string schoolname)
    //{
    //    DataSet ds = new DataSet();
    //    AvaimaEmailAPI objemail = new AvaimaEmailAPI();
    //    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
    //    {
    //        string query = "feesvoucher";
    //        using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
    //        {
    //            da.SelectCommand.CommandType = CommandType.StoredProcedure;
    //            da.SelectCommand.Parameters.AddWithValue("@instanceid", InstanceID);
    //            da.SelectCommand.Parameters.AddWithValue("@studentid", Studentid);
    //            da.SelectCommand.Parameters.AddWithValue("@PID", PID);
    //            da.Fill(ds);
    //        }
    //    }
    //    StringBuilder HTML = new StringBuilder();
    //    string headerfile = "";
    //    string Footer = "";
    //    string lblclass = "";
    //    string lblissuedate = "";
    //    string lblregno = "";
    //    string lblstudentname = "";
    //    string lblfathername = "";
    //    string lblslipno = "";
    //    int lblsession = 0;
    //    string lblperiod = "";
    //    string lblduedate = "";
    //    string lbldueamount = "";
    //    string fees = "0";
    //    string charges = "0";
    //    string Discount = "0";
    //    string ecomment = "";
    //    string ecommentamount = "0";
    //    string extradiscount = "";
    //    string extradiscountamount = "0";
    //    string adjustmentcomment = "";
    //    string adjustmentcommentamount = "0";
    //    decimal balanceval = 0;
    //    decimal classfees = 0;
    //    if (ds.Tables[0].Rows.Count > 0)
    //    {
    //        objuploader = new AvaimaStorageUploader();
    //        headerfile = objuploader.Singlefilelink(InstanceID, ds.Tables[0].Rows[0]["headerfile"].ToString());
    //        Footer = ds.Tables[0].Rows[0]["footer"].ToString();
    //    }
    //    if (ds.Tables[2].Rows.Count > 0)
    //    {
    //        lblclass = ds.Tables[2].Rows[0]["title"].ToString();
    //    }
    //    if (ds.Tables[1].Rows.Count > 0)
    //    {
    //        lblissuedate = Convert.ToDateTime(ds.Tables[1].Rows[0]["issuedate"].ToString()).ToString("dd-MMMM-yyyy");
    //        lblregno = ds.Tables[1].Rows[0]["Regno"].ToString();
    //        lblstudentname = ds.Tables[1].Rows[0]["studentname"].ToString();
    //        lblfathername = ds.Tables[1].Rows[0]["ParentName"].ToString();
    //    }
    //    if (ds.Tables[3].Rows.Count > 0)
    //    {
    //        lblslipno = ds.Tables[3].Rows[0]["recid"].ToString();

    //        DateTime dtstart = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
    //        if (ds.Tables[0].Rows.Count > 0 && dtstart.Month >= Convert.ToInt32(ds.Tables[0].Rows[0]["month"].ToString()))
    //        {
    //            lblsession = dtstart.Year;
    //        }
    //        else
    //        {
    //            lblsession = (dtstart.Year - 1);
    //        }

    //        lblperiod = dtstart.ToString("MMMM - yyyy");

    //        fees = ds.Tables[3].Rows[0]["TotalPackagefees"].ToString();
    //        classfees = Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalPackagefees"].ToString());
    //        balanceval = Convert.ToDecimal(ds.Tables[4].Rows[0]["balance"].ToString()) - Convert.ToDecimal(ds.Tables[3].Rows[0]["Totalfees"].ToString());
    //        totalfees = Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalPackagefees"].ToString());
    //        if (Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString()) > 0)
    //        {
    //            charges = ds.Tables[3].Rows[0]["TotalCharges"].ToString();
    //            totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString());
    //        }
    //        if (Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString()) > 0)
    //        {
    //            Discount = ds.Tables[3].Rows[0]["Totaldiscount"].ToString();
    //            totalfees -= Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString());
    //        }
    //        if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString()) > 0)
    //        {
    //            ecomment = ds.Tables[3].Rows[0]["chargescomment"].ToString();
    //            ecommentamount = ds.Tables[3].Rows[0]["extracharges"].ToString();
    //            totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString());
    //        }
    //        if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString()) > 0)
    //        {
    //            extradiscount = ds.Tables[3].Rows[0]["discountcomment"].ToString();
    //            extradiscountamount = ds.Tables[3].Rows[0]["extradiscount"].ToString();
    //            totalfees -= Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString());
    //        }
    //        if (Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString()) > 0)
    //        {
    //            adjustmentcomment = ds.Tables[3].Rows[0]["adjustmentcomment"].ToString();
    //            adjustmentcommentamount = ds.Tables[3].Rows[0]["adjustment"].ToString();
    //            totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString());
    //        }
    //        totalfees = totalfees + balanceval;
    //        if (ds.Tables[5].Rows.Count > 0)
    //        {
    //            DateTime enddate = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
    //            string[] duedate = ds.Tables[5].Rows[0]["Fee_DueDate"].ToString().Split('#');
    //            string[] afterduedate = ds.Tables[5].Rows[0]["LateFeesStartDate"].ToString().Split('#');
    //            enddate = enddate.AddDays(Convert.ToDouble(duedate[0]));
    //            enddate = enddate.AddDays(Convert.ToDouble(afterduedate[0]));
    //            lblduedate = enddate.ToString("dd-MMM-yyyy");

    //            decimal latefee = 0;
    //            try
    //            {
    //                latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["LateFeeCharges"].ToString());
    //            }
    //            catch
    //            {
    //                latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["latefees"].ToString());
    //            }
    //            latefee = latefee + totalfees;
    //            lbldueamount = latefee.ToString();
    //        }

    //        /******************************/

    //        HTML.AppendLine("<div style=\"width: 285px; border: 1px solid #000; float: left; margin-right: 7px\">");
    //        HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; height: 63px; width: 100%; position: relative\"><div style=\"position: absolute; right: 5px; top: 3px;\">Student Copy</div>");
    //        HTML.AppendLine("<div style=\"font-size: 22px; margin: 13px 0 0 0; text-align: center; display: inline-block; width: 100%; line-height: 40px;\">");
    //        HTML.AppendLine("<img src=\"" + headerfile + "\" alt=\"img\"/></div> </div>");
    //        HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; width: 275px; padding: 3px 5px;\">");
    //        HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">GR. No<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
    //        HTML.AppendLine("<div style=\"width: 86px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblregno + "</div>");
    //        HTML.AppendLine("<div style=\"float: right; height: 18px; width: 90px; text-align: center; line-height: 18px; border: 1px solid #000; background: #ccc;\">Orignal Voucher</div>");
    //        HTML.AppendLine("<div style=\"clear: both;\"></div><div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Father Name<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
    //        HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblfathername + "</div><div style=\"clear: both;\"></div>");
    //        HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Student Name<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
    //        HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblstudentname + "</div><div style=\"clear: both;\"></div>");
    //        HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Class<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
    //        HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblclass + "</div><div style=\"clear: both;\"></div>");
    //        HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Class Fee<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
    //        HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + classfees + "</div> <div style=\"clear: both;\"></div>");
    //        HTML.AppendLine("<h2 style=\"text-align: center; padding: 0px; margin: 4px 0 0 0;\">Fee Voucher " + lblperiod + "</h2></div>");
    //        HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; width: 100%;\">");
    //        HTML.AppendLine("<table width=\"50%\" style=\"float:left; font-size: 10px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><th style=\"height: 24px; width: 25%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Month</th>");
    //        HTML.AppendLine("<th style=\"height: 24px; width: 10%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Vch#</th><th style=\"height: 24px; width: 15%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Amount</th></tr>");
    //        string ss = ggs(ds, lblsession);
    //        HTML.AppendLine(ss);
    //        HTML.AppendLine("<table width=\"100% \" border=\"0\" cellspacing=\"0\" Style=\"padding: 5px; margin: 5px 0 0 0; font-size: 10px;\">");
    //        HTML.AppendLine("<tr><td class=\"Tabletr\">Tuition  Fee " + lblperiod + "</td><td class=\"Tabletr\">Vch#____________</td><td class=\"Tabletr\">" + fees + "</td></tr>");
    //        if (charges != "0")
    //            HTML.AppendLine("<tr><td class=\"Tabletr\">Total Individual Charges</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\">" + charges + "</td></tr>");
    //        if (Discount != "0")
    //            HTML.AppendLine("<tr><td class=\"Tabletr\">Total Individual Discount</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\">" + Discount + "</td></tr>");
    //        if (ecommentamount != "0")
    //            HTML.AppendLine("<tr><td class=\"Tabletr\">" + ecomment + "</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + ecommentamount + "</td></tr>");
    //        if (extradiscountamount != "0")
    //            HTML.AppendLine("<tr><td class=\"Tabletr\">" + extradiscount + "</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\">" + extradiscountamount + "</td></tr>");
    //        if (adjustmentcommentamount != "0")
    //            HTML.AppendLine("<tr><td class=\"Tabletr\">" + adjustmentcomment + "</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + adjustmentcommentamount + "</td></tr>");

    //        HTML.AppendLine("<tr><td class=\"Tabletr\">Previous Balance</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + balanceval.ToString() + "</td></tr>");
    //        HTML.AppendLine("<tr><td class=\"Tabletr\">Admission Fee</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\"></td></tr>");
    //        HTML.AppendLine("<tr><td class=\"Tabletr\">Annual Fee</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\"></td></tr>");
    //        HTML.AppendLine("<tr><td class=\"Tabletr\">Examination Fee Fee</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\"></td></tr>");
    //        HTML.AppendLine("<tr><td class=\"Tabletr\"><b>Total Due</b></td><td class=\"Tabletr\"> </td><td class=\"Tabletr\"><b>" + totalfees.ToString() + "</b></td></tr>");
    //        HTML.AppendLine("</table>");
    //        HTML.AppendLine("<table style=\"font-size: 10px;\">");
    //        HTML.AppendLine("<tr><td colspan=\"2\">Due Date: <b>" + lblduedate + "</b></td></tr>");
    //        HTML.AppendLine("<tr><td>Amount payable after due date</td><td style=\"text-align: right;\">" + lbldueamount + "</td></tr></table><br /><br /><br />");
    //        HTML.AppendLine("<p style=\"top: 43px; left: 17px; text-align: center;\"><b>Receiver Signature &amp; Stamp</b></p></div>");
    //        HTML.AppendLine("<div>" + Footer + "</div></div>");

    //        /********************************/
    //        HTML.AppendLine("<div style=\"width: 285px; border: 1px solid #000; float: left; margin-right: 7px\">");
    //        HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; height: 63px; width: 100%; position: relative\"><div style=\"position: absolute; right: 5px; top: 3px;\">School Copy</div>");
    //        HTML.AppendLine("<div style=\"font-size: 22px; margin: 13px 0 0 0; text-align: center; display: inline-block; width: 100%; line-height: 40px;\">");
    //        HTML.AppendLine("<img src=\"" + headerfile + "\" alt=\"img\"/></div> </div>");
    //        HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; width: 275px; padding: 3px 5px;\">");
    //        HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">GR. No<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
    //        HTML.AppendLine("<div style=\"width: 86px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblregno + "</div>");
    //        HTML.AppendLine("<div style=\"float: right; height: 18px; width: 90px; text-align: center; line-height: 18px; border: 1px solid #000; background: #ccc;\">Orignal Voucher</div>");
    //        HTML.AppendLine("<div style=\"clear: both;\"></div><div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Father Name<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
    //        HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblfathername + "</div><div style=\"clear: both;\"></div>");
    //        HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Student Name<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
    //        HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblstudentname + "</div><div style=\"clear: both;\"></div>");
    //        HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Class<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
    //        HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblclass + "</div><div style=\"clear: both;\"></div>");
    //        HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Class Fee<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
    //        HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + classfees + "</div> <div style=\"clear: both;\"></div>");
    //        HTML.AppendLine("<h2 style=\"text-align: center; padding: 0px; margin: 4px 0 0 0;\">Fee Voucher " + lblperiod + "</h2></div>");
    //        HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; width: 100%;\">");
    //        HTML.AppendLine("<table width=\"50%\" style=\"float:left; font-size: 10px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><th style=\"height: 24px; width: 25%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Month</th>");
    //        HTML.AppendLine("<th style=\"height: 24px; width: 10%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Vch#</th><th style=\"height: 24px; width: 15%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Amount</th></tr>");
    //        HTML.AppendLine(ss);
    //        HTML.AppendLine("<table width=\"100% \" border=\"0\" cellspacing=\"0\" Style=\"padding: 5px; margin: 5px 0 0 0;font-size: 10px;\">");
    //        HTML.AppendLine("<tr><td class=\"Tabletr\">Tuition  Fee " + lblperiod + "</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + fees + "</td></tr>");
    //        if (charges != "0")
    //            HTML.AppendLine("<tr><td class=\"Tabletr\">Total Individual Charges</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + charges + "</td></tr>");
    //        if (Discount != "0")
    //            HTML.AppendLine("<tr><td class=\"Tabletr\">Total Individual Discount</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + Discount + "</td></tr>");
    //        if (ecommentamount != "0")
    //            HTML.AppendLine("<tr><td class=\"Tabletr\">" + ecomment + "</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + ecommentamount + "</td></tr>");
    //        if (extradiscountamount != "0")
    //            HTML.AppendLine("<tr><td class=\"Tabletr\">" + extradiscount + "</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + extradiscountamount + "</td></tr>");
    //        if (adjustmentcommentamount != "0")
    //            HTML.AppendLine("<tr><td class=\"Tabletr\">" + adjustmentcomment + "</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\">" + adjustmentcommentamount + "</td></tr>");

    //        HTML.AppendLine("<tr><td class=\"Tabletr\">Previous Balance</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + balanceval.ToString() + "</td></tr>");
    //        HTML.AppendLine("<tr><td class=\"Tabletr\">Admission Fee</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\"></td></tr>");
    //        HTML.AppendLine("<tr><td class=\"Tabletr\">Annual Fee</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\"></td></tr>");
    //        HTML.AppendLine("<tr><td class=\"Tabletr\">Examination Fee Fee</td><td class=\"Tabletr\">Vch#____________   </td><td class=\"Tabletr\"></td></tr>");
    //        HTML.AppendLine("<tr><td class=\"Tabletr\"><b>Total Due</b></td><td class=\"Tabletr\"></td><td class=\"Tabletr\"><b>" + totalfees.ToString() + "</b></td></tr>");
    //        HTML.AppendLine("</table>");
    //        HTML.AppendLine("<table style=\"font-size: 10px;\">");
    //        HTML.AppendLine("<tr><td colspan=\"2\">Due Date: <b>" + lblduedate + "</b></td></tr>");
    //        HTML.AppendLine("<tr><td>Amount payable after due date</td><td style=\"text-align: right;\">" + lbldueamount + "</td></tr></table><br /><br /><br />");
    //        HTML.AppendLine("<p style=\"top: 43px; left: 17px; text-align: center;\"><b>Receiver Signature &amp; Stamp</b></p></div>");
    //        HTML.AppendLine("<div>" + Footer + "</div></div>");

    //        /****************************/
    //        HTML.AppendLine("<div style=\"width: 285px; border: 1px solid #000; float: left; margin-right: 7px\">");
    //        HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; height: 63px; width: 100%; position: relative\"><div style=\"position: absolute; right: 5px; top: 3px;\">Bank Copy</div>");
    //        HTML.AppendLine("<div style=\"font-size: 22px; margin: 13px 0 0 0; text-align: center; display: inline-block; width: 100%; line-height: 40px;\">");
    //        HTML.AppendLine("<img src=\"" + headerfile + "\" alt=\"img\"/></div> </div>");
    //        HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; width: 275px; padding: 3px 5px;\">");
    //        HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">GR. No<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
    //        HTML.AppendLine("<div style=\"width: 86px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblregno + "</div>");
    //        HTML.AppendLine("<div style=\"float: right; height: 18px; width: 90px; text-align: center; line-height: 18px; border: 1px solid #000; background: #ccc;\">Orignal Voucher</div>");
    //        HTML.AppendLine("<div style=\"clear: both;\"></div><div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Father Name<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
    //        HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblfathername + "</div><div style=\"clear: both;\"></div>");
    //        HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Student Name<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
    //        HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblstudentname + "</div><div style=\"clear: both;\"></div>");
    //        HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Class<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
    //        HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + lblclass + "</div><div style=\"clear: both;\"></div>");
    //        HTML.AppendLine("<div style=\"width: 87px; height: 12px; float: left; margin: 4px 0;\">Class Fee<span style=\"float: right; text-align: right; line-height: 12px; font-weight: bold;\">:</span></div>");
    //        HTML.AppendLine("<div style=\"width: 180px; height: 12px; float: left; padding: 0 0 0 7px; margin: 4px 0;\">" + classfees + "</div> <div style=\"clear: both;\"></div>");
    //        HTML.AppendLine("<h2 style=\"text-align: center; padding: 0px; margin: 4px 0 0 0;\">Fee Voucher " + lblperiod + "</h2></div>");
    //        HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; width: 100%;\">");
    //        HTML.AppendLine("<table width=\"50%\" style=\"float:left; font-size: 10px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><th style=\"height: 24px; width: 25%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Month</th>");
    //        HTML.AppendLine("<th style=\"height: 24px; width: 10%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Vch#</th><th style=\"height: 24px; width: 15%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Amount</th></tr>");
    //        HTML.AppendLine(ss);
    //        HTML.AppendLine("<table width=\"100% \" border=\"0\" cellspacing=\"0\" Style=\"padding: 5px; margin: 5px 0 0 0;font-size: 10px;\">");
    //        HTML.AppendLine("<tr><td class=\"Tabletr\">Tuition  Fee " + lblperiod + "</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + fees + "</td></tr>");
    //        if (charges != "0")
    //            HTML.AppendLine("<tr><td class=\"Tabletr\">Total Individual Charges</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + charges + "</td></tr>");
    //        if (Discount != "0")
    //            HTML.AppendLine("<tr><td class=\"Tabletr\">Total Individual Discount</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + Discount + "</td></tr>");
    //        if (ecommentamount != "0")
    //            HTML.AppendLine("<tr><td class=\"Tabletr\">" + ecomment + "</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\">" + ecommentamount + "</td></tr>");
    //        if (extradiscountamount != "0")
    //            HTML.AppendLine("<tr><td class=\"Tabletr\">" + extradiscount + "</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + extradiscountamount + "</td></tr>");
    //        if (adjustmentcommentamount != "0")
    //            HTML.AppendLine("<tr><td class=\"Tabletr\">" + adjustmentcomment + "</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + adjustmentcommentamount + "</td></tr>");

    //        HTML.AppendLine("<tr><td class=\"Tabletr\">Previous Balance</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\">" + balanceval.ToString() + "</td></tr>");
    //        HTML.AppendLine("<tr><td class=\"Tabletr\">Admission Fee</td><td class=\"Tabletr\">Vch#____________    </td><td class=\"Tabletr\"></td></tr>");
    //        HTML.AppendLine("<tr><td class=\"Tabletr\">Annual Fee</td><td class=\"Tabletr\">Vch#____________     </td><td class=\"Tabletr\"></td></tr>");
    //        HTML.AppendLine("<tr><td class=\"Tabletr\">Examination Fee Fee</td><td class=\"Tabletr\">Vch #     </td><td class=\"Tabletr\"></td></tr>");
    //        HTML.AppendLine("<tr><td class=\"Tabletr\"><b>Total Due</b></td><td class=\"Tabletr\"></td><td class=\"Tabletr\"><b>" + totalfees.ToString() + "</b></td></tr>");
    //        HTML.AppendLine("</table>");
    //        HTML.AppendLine("<table style=\"font-size: 10px;\">");
    //        HTML.AppendLine("<tr><td colspan=\"2\">Due Date: <b>" + lblduedate + "</b></td></tr>");
    //        HTML.AppendLine("<tr><td>Amount payable after due date</td><td style=\"text-align: right;\">" + lbldueamount + "</td></tr></table><br /><br /><br />");
    //        HTML.AppendLine("<p style=\"top: 43px; left: 17px; text-align: center;\"><b>Receiver Signature &amp; Stamp</b></p></div>");
    //        HTML.AppendLine("<div>" + Footer + "</div></div>");
    //        HTML.AppendLine("<div class=\"pageBreak\"></div> <br />");

    //    }
    //    return HTML;

    //}
    //private string ggs(DataSet ds, int lblsession)
    //{
    //    StringBuilder HTML = new StringBuilder();
    //    DataTable dthis = new DataTable();
    //    dthis = ds.Tables[6];
    //    for (int cell = 0; cell < 6; cell++)
    //    {
    //        string ss = getmonth(cell, lblsession);
    //        if (dthis.Rows.Count > 0 && ss == dthis.Rows[0]["months"].ToString())
    //        {
    //            HTML.AppendLine("<tr><td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dthis.Rows[0]["months"].ToString() + "</td>");
    //            HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dthis.Rows[0]["recid"].ToString() + "</td>");
    //            HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dthis.Rows[0]["totalfees"].ToString() + "</td></tr>");
    //            dthis.Rows.RemoveAt(0);
    //        }
    //        else
    //        {
    //            HTML.AppendLine("<tr><td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + ss + "</td>");
    //            HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\"> </td>");
    //            HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\"> </td></tr>");
    //        }
    //    }
    //    HTML.AppendLine("</table>");
    //    HTML.AppendLine("<table width=\"50%\" style=\"float:right; font-size: 10px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><th style=\"height: 24px; width: 25%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Month</th>");
    //    HTML.AppendLine("<th style=\"height: 24px;width: 10%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Vch#</th><th style=\"height: 24px; width: 15%; border-right: 1px dashed #aaa; border-bottom: 1px solid #000;\" scope=\"col\">Amount</th></tr>");

    //    for (int cell = 6; cell < 12; cell++)
    //    {
    //        string ss = getmonth(cell, lblsession);
    //        if (dthis.Rows.Count > 0 && ss == dthis.Rows[0]["months"].ToString())
    //        {
    //            HTML.AppendLine("<tr><td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dthis.Rows[0]["months"].ToString() + "</td>");
    //            HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dthis.Rows[0]["recid"].ToString() + "</td>");
    //            HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + dthis.Rows[0]["totalfees"].ToString() + "</td></tr>");
    //            dthis.Rows.RemoveAt(0);
    //        }
    //        else
    //        {
    //            HTML.AppendLine("<tr><td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\">" + ss + "</td>");
    //            HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\"> </td>");
    //            HTML.AppendLine("<td style=\"height: 20px; border-right: 1px dashed #aaa; border-bottom: 1px dashed #aaa; text-align: center;\"> </td></tr>");
    //        }
    //    }
    //    HTML.AppendLine("</table>");

    //    return HTML.ToString();
    //}
    //private string getmonth(int month, int year)
    //{
    //    int mon = month + 4;
    //    if (mon > 12)
    //    {
    //        mon = mon - 12;
    //        year = year + 1;
    //    }
    //    DateTime dd = Convert.ToDateTime(mon + "/1/" + year);
    //    return dd.ToString("MMM - yyyy");
    //}
    //private StringBuilder SendInvoicetemp2(string Studentid, string InstanceID, string PID, string parentemail, string schoolname)
    //{
    //    DataSet ds = new DataSet();
    //    AvaimaEmailAPI objemail = new AvaimaEmailAPI();
    //    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["feesconn"].ConnectionString))
    //    {
    //        string query = "feesvoucher";
    //        using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
    //        {
    //            da.SelectCommand.CommandType = CommandType.StoredProcedure;
    //            da.SelectCommand.Parameters.AddWithValue("@instanceid", InstanceID);
    //            da.SelectCommand.Parameters.AddWithValue("@studentid", Studentid);
    //            da.SelectCommand.Parameters.AddWithValue("@PID", PID);
    //            da.Fill(ds);
    //        }
    //    }
    //    string headerfile = "";
    //    string Footer = "";
    //    string lblclass = "";
    //    string lblissuedate = "";
    //    string lblregno = "";
    //    string lblstudentname = "";
    //    string lblfathername = "";
    //    string lblslipno = "";
    //    string lblsession = "";
    //    string lblperiod = "";
    //    string lblduedate = "";
    //    string lbldueamount = "";
    //    string fees = "0";
    //    string charges = "0";
    //    string Discount = "0";
    //    string ecomment = "";
    //    string ecommentamount = "0";
    //    string extradiscount = "";
    //    string extradiscountamount = "0";
    //    string adjustmentcomment = "";
    //    string adjustmentcommentamount = "0";
    //    decimal balanceval = 0;
    //    decimal classfees = 0;
    //    if (ds.Tables[0].Rows.Count > 0)
    //    {
    //        objuploader = new AvaimaStorageUploader();
    //        headerfile = objuploader.Singlefilelink(InstanceID, ds.Tables[0].Rows[0]["headerfile"].ToString());
    //        Footer = ds.Tables[0].Rows[0]["footer"].ToString();
    //    }
    //    if (ds.Tables[2].Rows.Count > 0)
    //    {
    //        lblclass = ds.Tables[2].Rows[0]["title"].ToString();
    //    }
    //    if (ds.Tables[1].Rows.Count > 0)
    //    {
    //        lblissuedate = Convert.ToDateTime(ds.Tables[1].Rows[0]["issuedate"].ToString()).ToString("dd-MMMM-yyyy");
    //        lblregno = ds.Tables[1].Rows[0]["Regno"].ToString();
    //        lblstudentname = ds.Tables[1].Rows[0]["studentname"].ToString();
    //        lblfathername = ds.Tables[1].Rows[0]["ParentName"].ToString();
    //    }
    //    if (ds.Tables[3].Rows.Count > 0)
    //    {
    //        lblslipno = ds.Tables[3].Rows[0]["recid"].ToString();

    //        DateTime dtstart = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
    //        if (ds.Tables[0].Rows.Count > 0 && dtstart.Month >= Convert.ToInt32(ds.Tables[0].Rows[0]["month"].ToString()))
    //        {
    //            lblsession = dtstart.Year + "-" + (dtstart.Year + 1);
    //        }
    //        else
    //        {
    //            lblsession = (dtstart.Year - 1) + "-" + dtstart.Year;
    //        }

    //        lblperiod = dtstart.ToString("MMMM - yyyy");

    //        fees = ds.Tables[3].Rows[0]["TotalPackagefees"].ToString();
    //        classfees = Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalPackagefees"].ToString());
    //        balanceval = Convert.ToDecimal(ds.Tables[4].Rows[0]["balance"].ToString()) - Convert.ToDecimal(ds.Tables[3].Rows[0]["Totalfees"].ToString());
    //        totalfees = Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalPackagefees"].ToString());
    //        if (Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString()) > 0)
    //        {
    //            charges = ds.Tables[3].Rows[0]["TotalCharges"].ToString();
    //            totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalCharges"].ToString());
    //        }
    //        if (Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString()) > 0)
    //        {
    //            Discount = ds.Tables[3].Rows[0]["Totaldiscount"].ToString();
    //            totalfees -= Convert.ToDecimal(ds.Tables[3].Rows[0]["Totaldiscount"].ToString());
    //        }
    //        if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString()) > 0)
    //        {
    //            ecomment = ds.Tables[3].Rows[0]["chargescomment"].ToString();
    //            ecommentamount = ds.Tables[3].Rows[0]["extracharges"].ToString();
    //            totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["extracharges"].ToString());
    //        }
    //        if (Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString()) > 0)
    //        {
    //            extradiscount = ds.Tables[3].Rows[0]["discountcomment"].ToString();
    //            extradiscountamount = ds.Tables[3].Rows[0]["extradiscount"].ToString();
    //            totalfees -= Convert.ToDecimal(ds.Tables[3].Rows[0]["extradiscount"].ToString());
    //        }
    //        if (Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString()) > 0)
    //        {
    //            adjustmentcomment = ds.Tables[3].Rows[0]["adjustmentcomment"].ToString();
    //            adjustmentcommentamount = ds.Tables[3].Rows[0]["adjustment"].ToString();
    //            totalfees += Convert.ToDecimal(ds.Tables[3].Rows[0]["adjustment"].ToString());
    //        }
    //        totalfees = totalfees + balanceval;
    //    }
    //    if (ds.Tables[5].Rows.Count > 0)
    //    {
    //        DateTime enddate = Convert.ToDateTime(ds.Tables[3].Rows[0]["fromdate"].ToString());
    //        string[] duedate = ds.Tables[5].Rows[0]["Fee_DueDate"].ToString().Split('#');
    //        string[] afterduedate = ds.Tables[5].Rows[0]["LateFeesStartDate"].ToString().Split('#');
    //        enddate = enddate.AddDays(Convert.ToDouble(duedate[0]));
    //        enddate = enddate.AddDays(Convert.ToDouble(afterduedate[0]));
    //        lblduedate = enddate.ToString("dd-MMM-yyyy");

    //        decimal latefee = 0;
    //        try
    //        {
    //            latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["LateFeeCharges"].ToString());
    //        }
    //        catch
    //        {
    //            latefee = Convert.ToDecimal(ds.Tables[5].Rows[0]["latefees"].ToString());
    //        }
    //        latefee = latefee + totalfees;
    //        lbldueamount = latefee.ToString();
    //    }

    //    /******************************/
    //    StringBuilder HTML = new StringBuilder();
    //    HTML.AppendLine("<div style=\"height: 660px; width: 290px; border: 1px solid #000; float: left; margin-right: 5px\">");
    //    HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; height: 114px; width: 100%; position: relative\">");
    //    HTML.AppendLine("<div style=\"width: 110px; float: left; padding: 13px 0 0 13px;\"><img src=\"" + headerfile + "\" alt=\"img\"/></div>");
    //    HTML.AppendLine("<div style=\"width: 166px; float: left;\"><div style=\"border: 1px solid #000; height: 32px; width: 147px; padding: 1px; margin: 5px 8px 0 0px; float: right\">");
    //    HTML.AppendLine("<div style=\"width: 145px; height: 30px; line-height: 30px; border: 1px solid #000; font-size: 15px; text-align: center\">SCHOOL COPY</div></div>");
    //    HTML.AppendLine("<div style=\"clear: both;\"></div><div style=\"font-size: 15px; font-weight: bold; line-height: 28px; padding: 10px 0 0 0;\">" + schoolname + "<br />Monthly Fee Challan</div></div></div>");
    //    HTML.AppendLine("<div style=\"width: 100%; height: 103px; border-bottom: 1px solid #000; font-size: 13px; font-weight: bold; line-height: 20px;\">" + Footer + " </div>");
    //    HTML.AppendLine("<div style=\"width: 100%;\"><div style=\"padding: 10px 0 0 10px; font-size: 13px; font-weight: bold; line-height: 21px;\">");
    //    HTML.AppendLine("Challan No. <span>" + lblslipno + "</span><br />");
    //    HTML.AppendLine("Name of Student: <span>" + lblstudentname + "</span><br />");
    //    HTML.AppendLine("Roll No. <span>" + lblregno + "</span><br /><br />");
    //    HTML.AppendLine("Billing Month: <span>" + lblperiod + "</span><br />");
    //    HTML.AppendLine("Issuance Date: <span>" + lblissuedate + "</span><br />");
    //    HTML.AppendLine("Due Date: <span>" + lblduedate + "</span><br /> </div>");
    //    HTML.AppendLine("<table width=\"96%\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" style=\"background-color: #000; font-size: 13px; font-weight: bold; margin: 20px 0 0 5px;\">");
    //    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Tuition  Fee " + lblperiod + "</td><td style=\"background: #FFF; padding: 5px\">" + fees + "</td></tr>");
    //    if (charges != "0")
    //        HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Charges</td><td style=\"background: #FFF; padding: 5px\">" + charges + "</td></tr>");
    //    if (Discount != "0")
    //        HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Discount</td><td style=\"background: #FFF; padding: 5px\">" + Discount + "</td></tr>");
    //    if (ecommentamount != "0")
    //        HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + ecomment + "</td><td style=\"background: #FFF; padding: 5px\">" + ecommentamount + "</td></tr>");
    //    if (extradiscountamount != "0")
    //        HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + extradiscount + "</td><td style=\"background: #FFF; padding: 5px\">" + extradiscountamount + "</td></tr>");
    //    if (adjustmentcommentamount != "0")
    //        HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + adjustmentcomment + "</td><td style=\"background: #FFF; padding: 5px\">" + adjustmentcommentamount + "</td></tr>");

    //    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Previous Balance</td><td style=\"background: #FFF; padding: 5px\">" + balanceval.ToString() + "</td></tr>");
    //    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\"><b>Total Due</b></td><td style=\"background: #FFF; padding: 5px\"><b>" + totalfees.ToString() + "</b></td></tr>");
    //    HTML.AppendLine("</table></div>");
    //    HTML.AppendLine("<p style=\"font-size: 13px; font-weight: bold; text-align: center;\">Late Payment Surcharge: Rs. 10/- Per day.</p>");
    //    HTML.AppendLine("<p style=\"font-size: 13px; font-weight: bold; padding: 15px 0 0 10px; margin: 53px 0 0 0;\">Bank Stamp<br />&amp; Signature: <span>_________________________</span></p></div>");
    //    /****************************/

    //    HTML.AppendLine("<div style=\"height: 660px; width: 290px; border: 1px solid #000; float: left; margin-right: 5px\">");
    //    HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; height: 114px; width: 100%; position: relative\">");
    //    HTML.AppendLine("<div style=\"width: 110px; float: left; padding: 13px 0 0 13px;\"><img src=\"" + headerfile + "\" alt=\"img\"/></div>");
    //    HTML.AppendLine("<div style=\"width: 166px; float: left;\"><div style=\"border: 1px solid #000; height: 32px; width: 147px; padding: 1px; margin: 5px 8px 0 0px; float: right\">");
    //    HTML.AppendLine("<div style=\"width: 145px; height: 30px; line-height: 30px; border: 1px solid #000; font-size: 15px; text-align: center\">PARENT'S COPY</div></div>");
    //    HTML.AppendLine("<div style=\"clear: both;\"></div><div style=\"font-size: 15px; font-weight: bold; line-height: 28px; padding: 10px 0 0 0;\">" + schoolname + "<br />Monthly Fee Challan</div></div></div>");
    //    HTML.AppendLine("<div style=\"width: 100%; height: 103px; border-bottom: 1px solid #000; font-size: 13px; font-weight: bold; line-height: 20px;\">" + Footer + " </div>");
    //    HTML.AppendLine("<div style=\"width: 100%;\"><div style=\"padding: 10px 0 0 10px; font-size: 13px; font-weight: bold; line-height: 21px;\">");
    //    HTML.AppendLine("Challan No. <span>" + lblslipno + "</span><br />");
    //    HTML.AppendLine("Name of Student: <span>" + lblstudentname + "</span><br />");
    //    HTML.AppendLine("Roll No. <span>" + lblregno + "</span><br /><br />");
    //    HTML.AppendLine("Billing Month: <span>" + lblperiod + "</span><br />");
    //    HTML.AppendLine("Issuance Date: <span>" + lblissuedate + "</span><br />");
    //    HTML.AppendLine("Due Date: <span>" + lblduedate + "</span><br /> </div>");
    //    HTML.AppendLine("<table width=\"96%\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" style=\"background-color: #000; font-size: 13px; font-weight: bold; margin: 20px 0 0 5px;\">");
    //    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Tuition  Fee " + lblperiod + "</td><td style=\"background: #FFF; padding: 5px\">" + fees + "</td></tr>");
    //    if (charges != "0")
    //        HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Charges</td><td style=\"background: #FFF; padding: 5px\">" + charges + "</td></tr>");
    //    if (Discount != "0")
    //        HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Discount</td><td style=\"background: #FFF; padding: 5px\">" + Discount + "</td></tr>");
    //    if (ecommentamount != "0")
    //        HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + ecomment + "</td><td style=\"background: #FFF; padding: 5px\">" + ecommentamount + "</td></tr>");
    //    if (extradiscountamount != "0")
    //        HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + extradiscount + "</td><td style=\"background: #FFF; padding: 5px\">" + extradiscountamount + "</td></tr>");
    //    if (adjustmentcommentamount != "0")
    //        HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + adjustmentcomment + "</td><td style=\"background: #FFF; padding: 5px\">" + adjustmentcommentamount + "</td></tr>");

    //    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Previous Balance</td><td style=\"background: #FFF; padding: 5px\">" + balanceval.ToString() + "</td></tr>");
    //    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\"><b>Total Due</b></td><td style=\"background: #FFF; padding: 5px\"><b>" + totalfees.ToString() + "</b></td></tr>");
    //    HTML.AppendLine("</table></div>");
    //    HTML.AppendLine("<p style=\"font-size: 13px; font-weight: bold; text-align: center;\">Late Payment Surcharge: Rs. 10/- Per day.</p>");
    //    HTML.AppendLine("<p style=\"font-size: 13px; font-weight: bold; padding: 15px 0 0 10px; margin: 53px 0 0 0;\">Bank Stamp<br />&amp; Signature: <span>_________________________</span></p></div>");
    //    /****************************/

    //    HTML.AppendLine("<div style=\"height: 660px; width: 290px; border: 1px solid #000; float: left; margin-right: 5px\">");
    //    HTML.AppendLine("<div style=\"border-bottom: 1px solid #000; height: 114px; width: 100%; position: relative\">");
    //    HTML.AppendLine("<div style=\"width: 110px; float: left; padding: 13px 0 0 13px;\"><img src=\"" + headerfile + "\" alt=\"img\"/></div>");
    //    HTML.AppendLine("<div style=\"width: 166px; float: left;\"><div style=\"border: 1px solid #000; height: 32px; width: 147px; padding: 1px; margin: 5px 8px 0 0px; float: right\">");
    //    HTML.AppendLine("<div style=\"width: 145px; height: 30px; line-height: 30px; border: 1px solid #000; font-size: 15px; text-align: center\">BANK COPY</div></div>");
    //    HTML.AppendLine("<div style=\"clear: both;\"></div><div style=\"font-size: 15px; font-weight: bold; line-height: 28px; padding: 10px 0 0 0;\">" + schoolname + "<br />Monthly Fee Challan</div></div></div>");
    //    HTML.AppendLine("<div style=\"width: 100%; height: 103px; border-bottom: 1px solid #000; font-size: 13px; font-weight: bold; line-height: 20px;\">" + Footer + " </div>");
    //    HTML.AppendLine("<div style=\"width: 100%;\"><div style=\"padding: 10px 0 0 10px; font-size: 13px; font-weight: bold; line-height: 21px;\">");
    //    HTML.AppendLine("Challan No. <span>" + lblslipno + "</span><br />");
    //    HTML.AppendLine("Name of Student: <span>" + lblstudentname + "</span><br />");
    //    HTML.AppendLine("Roll No. <span>" + lblregno + "</span><br /><br />");
    //    HTML.AppendLine("Billing Month: <span>" + lblperiod + "</span><br />");
    //    HTML.AppendLine("Issuance Date: <span>" + lblissuedate + "</span><br />");
    //    HTML.AppendLine("Due Date: <span>" + lblduedate + "</span><br /> </div>");
    //    HTML.AppendLine("<table width=\"96%\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" style=\"background-color: #000; font-size: 13px; font-weight: bold; margin: 20px 0 0 5px;\">");
    //    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Tuition  Fee " + lblperiod + "</td><td style=\"background: #FFF; padding: 5px\">" + fees + "</td></tr>");
    //    if (charges != "0")
    //        HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Charges</td><td style=\"background: #FFF; padding: 5px\">" + charges + "</td></tr>");
    //    if (Discount != "0")
    //        HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Total Individual Discount</td><td style=\"background: #FFF; padding: 5px\">" + Discount + "</td></tr>");
    //    if (ecommentamount != "0")
    //        HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + ecomment + "</td><td style=\"background: #FFF; padding: 5px\">" + ecommentamount + "</td></tr>");
    //    if (extradiscountamount != "0")
    //        HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + extradiscount + "</td><td style=\"background: #FFF; padding: 5px\">" + extradiscountamount + "</td></tr>");
    //    if (adjustmentcommentamount != "0")
    //        HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">" + adjustmentcomment + "</td><td style=\"background: #FFF; padding: 5px\">" + adjustmentcommentamount + "</td></tr>");

    //    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\">Previous Balance</td><td style=\"background: #FFF; padding: 5px\">" + balanceval.ToString() + "</td></tr>");
    //    HTML.AppendLine("<tr><td style=\"background: #FFF; padding: 5px\"><b>Total Due</b></td><td style=\"background: #FFF; padding: 5px\"><b>" + totalfees.ToString() + "</b></td></tr>");
    //    HTML.AppendLine("</table></div>");
    //    HTML.AppendLine("<p style=\"font-size: 13px; font-weight: bold; text-align: center;\">Late Payment Surcharge: Rs. 10/- Per day.</p>");
    //    HTML.AppendLine("<p style=\"font-size: 13px; font-weight: bold; padding: 15px 0 0 10px; margin: 53px 0 0 0;\">Bank Stamp<br />&amp; Signature: <span>_________________________</span></p></div>");
    //    HTML.AppendLine("<div class=\"pageBreak\"></div> <br />");
    //    /****************************/

    //    return HTML;
    //}
#endregion
    /*Invoice Code*/
    protected void lbtnfee_Click(object sender, EventArgs e)
    {
        this.Redirect("MultipleVochures.aspx");
    }
}